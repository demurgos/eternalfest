COMMENT ON SCHEMA public IS '{"version": "V005"}';

--- Checks that the `run_result.score` field is an array of 1 or 2 Sint32.
CREATE OR REPLACE FUNCTION test_run_result_scores(
  IN scores JSON
)
  RETURNS BOOLEAN
  LANGUAGE SQL
  IMMUTABLE
AS
$$
SELECT
    json_typeof($1) = 'array'
    AND 1 <= json_array_length($1)
    AND json_array_length($1) <= 2
    AND false NOT IN (
    SELECT
      (json_typeof(score.value) = 'number' AND -2147483648 <= score.value::text::int AND score.value::text::int < 2147483648) AS is_ok
    FROM json_array_elements($1) as score
  );
$$;
