CREATE DOMAIN run_id AS UUID;

ALTER TABLE runs
  ALTER COLUMN game_channel_key SET NOT NULL,
  ALTER COLUMN version_major SET NOT NULL,
  ALTER COLUMN version_minor SET NOT NULL,
  ALTER COLUMN version_patch SET NOT NULL,
  DROP CONSTRAINT run_game__fk;
