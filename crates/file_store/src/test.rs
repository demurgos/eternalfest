use eternalfest_core::blob::{BlobStore, CreateBlobOptions};
use eternalfest_core::clock::VirtualClock;
use eternalfest_core::core::Instant;
use eternalfest_core::file::{
  CreateStoreFileOptions, DirectoryId, DirectoryIdRef, Drive, FileStore, GetDriveByOwnerOptions,
};
use eternalfest_core::types::ApiRef;
use eternalfest_core::user::{ShortUser, UserIdRef, UserStore};
use eternalfest_core::uuid::UuidGenerator;
use std::str::FromStr;

#[macro_export]
macro_rules! test_file_store {
  ($(#[$meta:meta])* || $api:expr) => {
    register_test!($(#[$meta])*, $api, test_get_user_own_drive);
    register_test!($(#[$meta])*, $api, test_prevent_file_in_a_file);
  };
}

macro_rules! register_test {
  ($(#[$meta:meta])*, $api:expr, $test_name:ident) => {
    #[tokio::test]
    $(#[$meta])*
    async fn $test_name() {
      crate::test::$test_name($api).await;
    }
  };
}

pub(crate) struct TestApi<TyBlobStore, TyClock, TyFileStore, TyUserStore, TyUuidGenerator>
where
  TyBlobStore: BlobStore,
  TyClock: ApiRef<VirtualClock>,
  TyFileStore: FileStore,
  TyUserStore: UserStore,
  TyUuidGenerator: UuidGenerator,
{
  pub(crate) blob_store: TyBlobStore,
  pub(crate) clock: TyClock,
  pub(crate) file_store: TyFileStore,
  pub(crate) user_store: TyUserStore,
  pub(crate) uuid_generator: TyUuidGenerator,
}

pub(crate) async fn test_get_user_own_drive<TyBlobStore, TyClock, TyFileStore, TyUserStore, TyUuidGenerator>(
  api: TestApi<TyBlobStore, TyClock, TyFileStore, TyUserStore, TyUuidGenerator>,
) where
  TyBlobStore: BlobStore,
  TyClock: ApiRef<VirtualClock>,
  TyFileStore: FileStore,
  TyUserStore: UserStore,
  TyUuidGenerator: UuidGenerator,
{
  api.clock.as_ref().advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = api
    .user_store
    .upsert_from_etwin(&ShortUser {
      id: api.uuid_generator.next().into(),
      display_name: "Alice".parse().unwrap(),
    })
    .await
    .unwrap();

  let actual = api
    .file_store
    .get_drive_by_owner(&GetDriveByOwnerOptions { id: alice.id })
    .await
    .unwrap();

  let expected = Drive {
    id: actual.id,
    owner: UserIdRef { id: alice.id },
    root: DirectoryIdRef { id: actual.root.id },
    created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
    updated_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
  };

  assert_eq!(actual, expected);
}

pub(crate) async fn test_prevent_file_in_a_file<TyBlobStore, TyClock, TyFileStore, TyUserStore, TyUuidGenerator>(
  api: TestApi<TyBlobStore, TyClock, TyFileStore, TyUserStore, TyUuidGenerator>,
) where
  TyBlobStore: BlobStore,
  TyClock: ApiRef<VirtualClock>,
  TyFileStore: FileStore,
  TyUserStore: UserStore,
  TyUuidGenerator: UuidGenerator,
{
  api.clock.as_ref().advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = api
    .user_store
    .upsert_from_etwin(&ShortUser {
      id: api.uuid_generator.next().into(),
      display_name: "Alice".parse().unwrap(),
    })
    .await
    .unwrap();

  let drive = api
    .file_store
    .get_drive_by_owner(&GetDriveByOwnerOptions { id: alice.id })
    .await
    .unwrap();

  let blob = api
    .blob_store
    .create_blob(&CreateBlobOptions {
      media_type: "text/plain".parse().unwrap(),
      data: b"Hello, World!".to_vec(),
    })
    .await
    .unwrap();

  let file = api
    .file_store
    .create_file(&CreateStoreFileOptions {
      parent_id: drive.root.id,
      blob_id: blob.id,
      display_name: "hello.txt".parse().unwrap(),
      check_owner: None,
    })
    .await
    .unwrap();

  let actual = api
    .file_store
    .create_file(&CreateStoreFileOptions {
      parent_id: DirectoryId::from_str(&file.id.to_hex()).unwrap(),
      blob_id: blob.id,
      display_name: "hello.txt".parse().unwrap(),
      check_owner: None,
    })
    .await;

  assert!(actual.is_err());
}
