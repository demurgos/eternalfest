use clap::Parser;
use eternalfest_cli::cmd::dump;
use std::process;

#[tokio::main]
async fn main() {
  let args: dump::DumpArgs = dump::DumpArgs::parse();

  let res = dump::dump(&args).await;

  if let Err(e) = res {
    eprintln!("{e}");
    process::exit(1);
  }
}
