use clap::Parser;
use eternalfest_cli::{run, CliArgs};
use std::process;

#[tokio::main]
async fn main() {
  let args: CliArgs = CliArgs::parse();

  let res = run(&args).await;

  if let Err(e) = res {
    eprintln!("{e}");
    process::exit(1);
  }
}
