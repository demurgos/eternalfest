use clap::Parser;
use eternalfest_blob_store::pg::PgBlobStore;
use eternalfest_buffer_store::fs::FsBufferStore;
use eternalfest_config::Config;
use eternalfest_core::clock::VirtualClock;
use eternalfest_core::types::AnyError;
use eternalfest_core::uuid::Uuid4Generator;
use etwin_core::core::Instant;
use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
use sqlx::PgPool;

/// Arguments to the `clean` task.
#[derive(Debug, Parser)]
pub struct CleanArgs {}

pub async fn run(_args: &CleanArgs) -> Result<(), AnyError> {
  let config: Config = eternalfest_config::find_config(std::env::current_dir().unwrap()).unwrap();
  let database: PgPool = PgPoolOptions::new()
    .max_connections(5)
    .connect_with(
      PgConnectOptions::new()
        .host(&config.db.host)
        .port(config.db.port)
        .database(&config.db.name)
        .username(&config.db.admin_user)
        .password(&config.db.admin_password),
    )
    .await
    .unwrap();

  {
    let database = PgPoolRef(&database);
    let clock = VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0));
    let uuid_generator = Uuid4Generator;
    let data_root = config.data.root.to_file_path().expect("InvalidDataRoot");
    let buffer_store = FsBufferStore::new(&uuid_generator, data_root).await;
    let blob_store = PgBlobStore::new(&buffer_store, &clock, database, &uuid_generator);

    eprintln!("Starting clean-up");

    blob_store.mark_unused_blobs_for_deletion().await?;
    blob_store.exec_blob_deletions().await?;

    eprintln!("Done");
  }

  // await blob.markUnusedBlobsForDeletion({type: actorType.ActorType.System});
  // await blob.execBlobDeletions({type: actorType.ActorType.System});

  database.close().await;

  Ok(())
}

struct PgPoolRef<'a>(&'a PgPool);

impl<'a> AsRef<PgPool> for PgPoolRef<'a> {
  fn as_ref(&self) -> &PgPool {
    self.0
  }
}
