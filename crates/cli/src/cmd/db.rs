use clap::Parser;
use eternalfest_core::types::AnyError;
use eternalfest_db_schema::SchemaStateRef;
use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
use sqlx::PgPool;

#[derive(Debug, Parser)]
pub struct DbArgs {
  #[clap(subcommand)]
  command: DbCommand,
}

#[derive(Debug, Parser)]
pub enum DbCommand {
  /// Check the state of the database
  #[clap(name = "check")]
  Check,
  /// Upgrade to the latest version of the database
  #[clap(name = "upgrade")]
  Upgrade,
}

pub async fn db(args: &DbArgs) -> Result<(), AnyError> {
  let config = eternalfest_config::find_config(std::env::current_dir().unwrap()).unwrap();
  let admin_database: PgPool = PgPoolOptions::new()
    .max_connections(5)
    .connect_with(
      PgConnectOptions::new()
        .host(&config.db.host)
        .port(config.db.port)
        .database(&config.db.name)
        .username(&config.db.admin_user)
        .password(&config.db.admin_password),
    )
    .await
    .unwrap();

  match args.command {
    DbCommand::Check => {
      let state: SchemaStateRef = eternalfest_db_schema::get_state(&admin_database).await.unwrap();
      eprintln!("Database state: {state:?}");
    }
    DbCommand::Upgrade => {
      todo!()
    }
  }

  admin_database.close().await;
  Ok(())
}
