use crate::cmd::clean::CleanArgs;
use crate::cmd::db::DbArgs;
use crate::cmd::dump::DumpArgs;
use clap::Parser;
use eternalfest_core::types::AnyError;

pub mod cmd {
  pub mod clean;
  pub mod db;
  pub mod dump;
}

#[derive(Debug, Parser)]
#[clap(author = "Eternaltwin")]
pub struct CliArgs {
  #[clap(subcommand)]
  command: CliCommand,
}

#[derive(Debug, Parser)]
pub enum CliCommand {
  #[clap(name = "clean")]
  Clean(CleanArgs),
  #[clap(name = "db")]
  Db(DbArgs),
  #[clap(name = "dump")]
  Dump(DumpArgs),
}

pub async fn run(args: &CliArgs) -> Result<(), AnyError> {
  match &args.command {
    CliCommand::Clean(ref args) => crate::cmd::clean::run(args).await,
    CliCommand::Db(ref args) => crate::cmd::db::db(args).await,
    CliCommand::Dump(ref args) => crate::cmd::dump::dump(args).await,
  }
}
