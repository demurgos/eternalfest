use async_trait::async_trait;
use eternalfest_core::blob::{
  Blob, BlobDigest, BlobId, BlobStore, CreateBlobError, CreateBlobOptions, CreateUploadSessionError,
  CreateUploadSessionOptions, GetBlobDataError, GetBlobDataOptions, GetBlobError, GetBlobOptions, GetBlobsError,
  GetBlobsOptions, MediaType, UploadError, UploadOptions, UploadSession, UploadSessionId,
};
use eternalfest_core::buffer::{BufferId, BufferStore};
use eternalfest_core::clock::Clock;
use eternalfest_core::core::Instant;
use eternalfest_core::digest::{DigestSha2, DigestSha3};
use eternalfest_core::pg_num::PgU32;
use eternalfest_core::types::{AnyError, ApiRef};
use eternalfest_core::uuid::UuidGenerator;
use futures::TryStreamExt;
use sqlx::postgres::{PgPool, PgQueryResult};
use std::collections::HashMap;
use std::convert::TryFrom;
use std::time::Duration;
use uuid::Uuid;

pub struct PgBlobStore<TyBufferStore, TyClock, TyDatabase, TyUuidGenerator>
where
  TyBufferStore: BufferStore,
  TyClock: Clock,
  TyDatabase: ApiRef<PgPool>,
  TyUuidGenerator: UuidGenerator,
{
  buffer_store: TyBufferStore,
  clock: TyClock,
  database: TyDatabase,
  uuid_generator: TyUuidGenerator,
}

impl<TyBufferStore, TyClock, TyDatabase, TyUuidGenerator>
  PgBlobStore<TyBufferStore, TyClock, TyDatabase, TyUuidGenerator>
where
  TyBufferStore: BufferStore,
  TyClock: Clock,
  TyDatabase: ApiRef<PgPool>,
  TyUuidGenerator: UuidGenerator,
{
  pub fn new(
    buffer_store: TyBufferStore,
    clock: TyClock,
    database: TyDatabase,
    uuid_generator: TyUuidGenerator,
  ) -> Self {
    Self {
      buffer_store,
      clock,
      database,
      uuid_generator,
    }
  }

  pub async fn mark_unused_blobs_for_deletion(&self) -> Result<(), AnyError> {
    let mut tx = self.database.as_ref().begin().await?;
    let queued_rows = {
      // `ON CONFLICT` makes the insertion idempotent (should not be needed in practice)
      let res: PgQueryResult = sqlx::query(
        r"
        WITH unused_blobs AS (
          SELECT blob_id, media_type, byte_size, buffer_id, created_at, updated_at
          FROM blobs
          WHERE NOT EXISTS (
            SELECT 1 FROM files WHERE files.blob_id = blobs.blob_id
          )
        )
        INSERT INTO blob_deletion_queue(blob_id, media_type, byte_size, buffer_id, created_at, updated_at, deleted_at)
        SELECT blob_id, media_type, byte_size, buffer_id, created_at, updated_at, NOW() AS deleted_at FROM unused_blobs
        ON CONFLICT (blob_id) DO NOTHING;
      ",
      )
      .execute(&mut tx)
      .await?;
      res.rows_affected()
    };
    let deleted_rows = {
      let res: PgQueryResult = sqlx::query(
        r"
        DELETE FROM blobs
      WHERE blob_id IN (SELECT blob_id FROM blob_deletion_queue);
      ",
      )
      .execute(&mut tx)
      .await?;
      res.rows_affected()
    };
    assert_eq!(deleted_rows, queued_rows);
    tx.commit().await?;
    Ok(())
  }

  pub async fn exec_blob_deletions(&self) -> Result<(), AnyError> {
    let mut tx = self.database.as_ref().begin().await?;
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      buffer_id: String,
    }
    let rows: Vec<Row> = sqlx::query_as(
      r"
        SELECT buffer_id FROM blob_deletion_queue;
      ",
    )
    .fetch_all(self.database.as_ref())
    .await?;

    let deleted_buffers = u64::try_from(rows.len()).expect("OverflowOnDeletedBuffers");

    for row in rows {
      self
        .buffer_store
        .delete_buffer_if_exists(row.buffer_id.parse().unwrap())
        .await?;
    }

    let deleted_rows = {
      let res: PgQueryResult = sqlx::query(
        r"
        DELETE FROM blob_deletion_queue;
      ",
      )
      .execute(&mut tx)
      .await?;
      res.rows_affected()
    };
    assert_eq!(deleted_rows, deleted_buffers);
    tx.commit().await?;
    Ok(())
  }
}

const MAX_BLOB_SIZE: u32 = 100 * 1024 * 1024;
const UPLOAD_SESSION_MAX_AGE: Duration = Duration::from_secs(24 * 3600);

#[async_trait]
impl<TyBufferStore, TyClock, TyDatabase, TyUuidGenerator> BlobStore
  for PgBlobStore<TyBufferStore, TyClock, TyDatabase, TyUuidGenerator>
where
  TyBufferStore: BufferStore,
  TyClock: Clock,
  TyDatabase: ApiRef<PgPool>,
  TyUuidGenerator: UuidGenerator,
{
  fn has_immutable_blobs(&self) -> bool {
    true
  }

  async fn create_blob(&self, options: &CreateBlobOptions) -> Result<Blob, CreateBlobError> {
    let input_len = u32::try_from(options.data.len()).map_err(|_| CreateBlobError::MaxSize)?;
    if input_len > MAX_BLOB_SIZE {
      return Err(CreateBlobError::MaxSize);
    }
    let now = self.clock.now();

    let digest = BlobDigest::digest(options.data.as_slice());

    let buffer_id = self
      .buffer_store
      .create_buffer(input_len)
      .await
      .map_err(CreateBlobError::other)?;
    self
      .buffer_store
      .write_bytes(buffer_id, 0, options.data.as_slice())
      .await
      .map_err(CreateBlobError::other)?;

    let blob_id = BlobId::from_uuid(self.uuid_generator.next());
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      blob_id: BlobId,
    }
    let row: Row = sqlx::query_as(
      r"
        INSERT
        INTO blobs(blob_id, media_type, byte_size, created_at, updated_at, buffer_id, digest_sha2_256, digest_sha3_256)
        VALUES ($1::BLOB_ID, $2::MEDIA_TYPE, $3::U32, $4::INSTANT, $4::INSTANT, $5::BUFFER_ID, $6::BYTEA, $7::BYTEA)
        RETURNING blob_id;
      ",
    )
    .bind(blob_id)
    .bind(&options.media_type)
    .bind(PgU32::from(input_len))
    .bind(now)
    .bind(buffer_id.to_hex().as_str())
    .bind(digest.sha2_256.as_slice())
    .bind(digest.sha3_256.as_slice())
    .fetch_one(self.database.as_ref())
    .await
    .map_err(CreateBlobError::other)?;
    assert_eq!(row.blob_id, blob_id);

    Ok(Blob {
      id: blob_id,
      media_type: options.media_type.clone(),
      byte_size: input_len,
      digest,
    })
  }

  async fn get_blob(&self, options: &GetBlobOptions) -> Result<Blob, GetBlobError> {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      byte_size: PgU32,
      media_type: MediaType,
      digest_sha2_256: Option<Vec<u8>>,
      digest_sha3_256: Option<Vec<u8>>,
    }
    let row: Row = sqlx::query_as::<_, Row>(
      r"
      SELECT byte_size, media_type, digest_sha2_256, digest_sha3_256
      FROM blobs
      WHERE blobs.blob_id = $1::BLOB_ID;
    ",
    )
    .bind(options.id)
    .fetch_optional(self.database.as_ref())
    .await
    .map_err(|e| GetBlobError::Other(e.into()))?
    .ok_or(GetBlobError::NotFound(options.id))?;

    Ok(Blob {
      id: options.id,
      media_type: row.media_type,
      byte_size: row.byte_size.into(),
      digest: BlobDigest {
        sha2_256: DigestSha2::from_bytes(row.digest_sha2_256.as_deref().unwrap_or(&[0; 32]))
          .expect("Digest2 from database is valid"),
        sha3_256: DigestSha3::from_bytes(row.digest_sha3_256.as_deref().unwrap_or(&[0; 32]))
          .expect("Digest3 from database is valid"),
      },
    })
  }

  async fn get_blobs(
    &self,
    options: &GetBlobsOptions,
  ) -> Result<HashMap<BlobId, Result<Blob, GetBlobError>>, GetBlobsError> {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      blob_id: BlobId,
      byte_size: PgU32,
      media_type: MediaType,
      digest_sha2_256: Option<Vec<u8>>,
      digest_sha3_256: Option<Vec<u8>>,
    }
    let ids: Vec<Uuid> = options.id.iter().map(|id| id.into_uuid()).collect();
    let mut blobs: HashMap<BlobId, Result<Blob, GetBlobError>> = HashMap::new();

    // language=PostgreSQL
    let mut rows = sqlx::query_as::<_, Row>(
      r"
      SELECT blob_id, byte_size, media_type, digest_sha2_256, digest_sha3_256
      FROM blobs
      WHERE blobs.blob_id = ANY($1::BLOB_ID[]);
      ",
    )
    .bind(ids.as_slice())
    .fetch(self.database.as_ref());

    while let Some(row) = rows.try_next().await.map_err(|e| GetBlobsError::Other(e.into()))? {
      blobs.insert(
        row.blob_id,
        Ok(Blob {
          id: row.blob_id,
          media_type: row.media_type,
          byte_size: row.byte_size.into(),
          digest: BlobDigest {
            sha2_256: DigestSha2::from_bytes(row.digest_sha2_256.as_deref().unwrap_or(&[0; 32]))
              .expect("Digest2 from database is valid"),
            sha3_256: DigestSha3::from_bytes(row.digest_sha3_256.as_deref().unwrap_or(&[0; 32]))
              .expect("Digest3 from database is valid"),
          },
        }),
      );
    }

    for id in &options.id {
      if !blobs.contains_key(id) {
        blobs.insert(*id, Err(GetBlobError::NotFound(*id)));
      }
    }

    Ok(blobs)
  }

  async fn get_blob_data(&self, options: &GetBlobDataOptions) -> Result<Vec<u8>, GetBlobDataError> {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      buffer_id: String,
    }
    let row: Row = sqlx::query_as::<_, Row>(
      r"
      SELECT buffer_id
      FROM blobs
      WHERE blobs.blob_id = $1::BLOB_ID;
    ",
    )
    .bind(options.id)
    .fetch_optional(self.database.as_ref())
    .await
    .map_err(|e| GetBlobDataError::Other(e.into()))?
    .ok_or(GetBlobDataError::NotFound(options.id))?;

    let buffer_id = BufferId::from_uuid(row.buffer_id.parse().unwrap());

    let buffer_content = self
      .buffer_store
      .read_stream(buffer_id)
      .await
      .map_err(|e| GetBlobDataError::Other(e.into()))?;

    Ok(buffer_content)
  }

  async fn create_upload_session(
    &self,
    options: &CreateUploadSessionOptions,
  ) -> Result<UploadSession, CreateUploadSessionError> {
    let input_len = options.byte_size;
    if input_len > MAX_BLOB_SIZE {
      return Err(CreateUploadSessionError::MaxSize);
    }
    let now = self.clock.now();
    let upload_session_id = UploadSessionId::from_uuid(self.uuid_generator.next());
    let buffer_id = self
      .buffer_store
      .create_buffer(input_len)
      .await
      .map_err(CreateUploadSessionError::other)?;

    {
      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        upload_session_id: UploadSessionId,
      }
      let row: Row = sqlx::query_as(
        r"
        INSERT INTO upload_sessions(
          upload_session_id, media_type, byte_size, written_bytes, created_at, updated_at, buffer_id, blob_id
        )
        VALUES ($1::UPLOAD_SESSION_ID, $2::MEDIA_TYPE, $3::U32, 0, $4::INSTANT, $4::INSTANT, $5::BUFFER_ID, NULL)
        RETURNING upload_session_id;
      ",
      )
      .bind(upload_session_id)
      .bind(options.media_type.as_str())
      .bind(PgU32::from(input_len))
      .bind(now)
      .bind(buffer_id.to_hex().as_str())
      .fetch_one(self.database.as_ref())
      .await
      .map_err(CreateUploadSessionError::other)?;
      assert_eq!(row.upload_session_id, upload_session_id);
    }
    Ok(UploadSession {
      id: upload_session_id,
      expires_at: now + chrono::Duration::from_std(UPLOAD_SESSION_MAX_AGE).unwrap(),
      remaining_range: 0..input_len,
      blob: None,
    })
  }

  async fn upload(&self, options: &UploadOptions) -> Result<UploadSession, UploadError> {
    if options.data.is_empty() {
      return Err(UploadError::EmptyInputData);
    }
    let input_len = u32::try_from(options.data.len()).map_err(|_| UploadError::Overflow)?;
    let now = self.clock.now();
    let mut tx = self.database.as_ref().begin().await.map_err(UploadError::other)?;

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      byte_size: PgU32,
      written_bytes: PgU32,
      created_at: Instant,
      media_type: MediaType,
      buffer_id: String,
    }
    let row: Row = sqlx::query_as(
      r"
      SELECT byte_size, written_bytes, created_at, media_type, buffer_id
      FROM upload_sessions
      WHERE upload_session_id = $1::UPLOAD_SESSION_ID;
      ",
    )
    .bind(options.upload_session_id)
    .fetch_optional(&mut tx)
    .await
    .map_err(|e| UploadError::Other(e.into()))?
    .ok_or(UploadError::NotFound(options.upload_session_id))?;

    let buffer_id = BufferId::from_uuid(row.buffer_id.parse().unwrap());

    let expires_at = row.created_at + chrono::Duration::from_std(UPLOAD_SESSION_MAX_AGE).unwrap();
    if now >= expires_at {
      return Err(UploadError::Expired(options.upload_session_id));
    }
    let written_bytes = u32::from(row.written_bytes);
    let byte_size = u32::from(row.byte_size);
    let new_written_bytes = written_bytes.checked_add(input_len).ok_or(UploadError::Overflow)?;
    if new_written_bytes > byte_size {
      return Err(UploadError::Overflow);
    }
    if options.offset != written_bytes {
      return Err(UploadError::BadOffset {
        actual: options.offset,
        expected: written_bytes,
      });
    }
    self
      .buffer_store
      .write_bytes(buffer_id, options.offset, &options.data)
      .await
      .map_err(UploadError::other)?;

    let blob = if new_written_bytes == byte_size {
      let data = self
        .buffer_store
        .read_stream(buffer_id)
        .await
        .expect("buffer is readable");
      let digest = BlobDigest::digest(data.as_slice());

      let blob_id = BlobId::from_uuid(self.uuid_generator.next());
      {
        #[derive(Debug, sqlx::FromRow)]
        struct Row {
          blob_id: BlobId,
        }
        let row: Row = sqlx::query_as(
          r"
        INSERT
        INTO blobs(blob_id, media_type, byte_size, created_at, updated_at, buffer_id, digest_sha2_256, digest_sha3_256)
        VALUES ($1::BLOB_ID, $2::MEDIA_TYPE, $3::U32, $4::INSTANT, $4::INSTANT, $5::BUFFER_ID, $6::BYTEA, $7::BYTEA)
        RETURNING blob_id;
      ",
        )
        .bind(blob_id)
        .bind(&row.media_type)
        .bind(PgU32::from(byte_size))
        .bind(now)
        .bind(buffer_id.to_hex().as_str())
        .bind(digest.sha2_256.as_slice())
        .bind(digest.sha3_256.as_slice())
        .fetch_one(&mut tx)
        .await
        .map_err(UploadError::other)?;
        assert_eq!(row.blob_id, blob_id);
      }
      Some(Blob {
        id: blob_id,
        byte_size,
        media_type: row.media_type.clone(),
        digest,
      })
    } else {
      None
    };
    {
      let res: PgQueryResult = sqlx::query(
        r"
        UPDATE upload_sessions
        SET written_bytes = $3::U32, updated_at = $4::INSTANT, blob_id = $5::BLOB_ID
        WHERE upload_session_id = $1::UPLOAD_SESSION_ID AND written_bytes = $2::U32 AND blob_id IS NULL;
      ",
      )
      .bind(options.upload_session_id)
      .bind(PgU32::from(written_bytes))
      .bind(PgU32::from(new_written_bytes))
      .bind(now)
      .bind(blob.as_ref().map(|b| b.id))
      .execute(&mut tx)
      .await
      .map_err(UploadError::other)?;
      assert_eq!(res.rows_affected(), 1);
    }
    tx.commit().await.map_err(UploadError::other)?;

    Ok(UploadSession {
      id: options.upload_session_id,
      expires_at,
      remaining_range: new_written_bytes..byte_size,
      blob,
    })
  }
}

#[cfg(feature = "neon")]
impl<TyBufferStore, TyClock, TyDatabase, TyUuidGenerator> neon::prelude::Finalize
  for PgBlobStore<TyBufferStore, TyClock, TyDatabase, TyUuidGenerator>
where
  TyBufferStore: BufferStore,
  TyClock: Clock,
  TyDatabase: ApiRef<PgPool>,
  TyUuidGenerator: UuidGenerator,
{
}

#[cfg(test)]
mod test {
  use super::PgBlobStore;
  use crate::test::TestApi;
  use eternalfest_buffer_store::fs::FsBufferStore;
  use eternalfest_core::blob::BlobStore;
  use eternalfest_core::clock::VirtualClock;
  use eternalfest_core::core::Instant;
  use eternalfest_core::uuid::Uuid4Generator;
  use eternalfest_db_schema::force_create_latest;
  use serial_test::serial;
  use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
  use sqlx::PgPool;
  use std::sync::Arc;

  async fn make_test_api() -> TestApi<Arc<dyn BlobStore>, Arc<VirtualClock>> {
    let config = eternalfest_config::find_config(std::env::current_dir().unwrap()).unwrap();
    let admin_database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.db.host)
          .port(config.db.port)
          .database(&config.db.name)
          .username(&config.db.admin_user)
          .password(&config.db.admin_password),
      )
      .await
      .unwrap();
    force_create_latest(&admin_database, true).await.unwrap();
    admin_database.close().await;

    let database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.db.host)
          .port(config.db.port)
          .database(&config.db.name)
          .username(&config.db.user)
          .password(&config.db.password),
      )
      .await
      .unwrap();
    let database = Arc::new(database);

    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let uuid_generator = Arc::new(Uuid4Generator);
    let data_root = config.data.root.to_file_path().expect("InvalidDataRoot");
    let buffer_store = Arc::new(FsBufferStore::new(uuid_generator.clone(), data_root).await);
    let blob_service: Arc<dyn BlobStore> =
      Arc::new(PgBlobStore::new(buffer_store, clock.clone(), database, uuid_generator));

    TestApi { blob_service, clock }
  }

  test_blob_store!(
    #[serial]
    || make_test_api().await
  );
}
