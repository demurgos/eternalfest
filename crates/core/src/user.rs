use crate::core::Instant;
use async_trait::async_trait;
use auto_impl::auto_impl;
use etwin_core::types::AnyError;
pub use etwin_core::user::UserDisplayName;
pub use etwin_core::user::UserId;
pub use etwin_core::user::UserIdParseError;
pub use etwin_core::user::UserIdRef;
#[cfg(feature = "serde")]
use etwin_serde_tools::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};

#[cfg_attr(
  feature = "serde",
  derive(Serialize, Deserialize),
  serde(tag = "type", rename = "User")
)]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct User {
  pub id: UserId,
  pub display_name: UserDisplayName,
  pub created_at: Instant,
  pub updated_at: Instant,
  pub identities: Vec<HfestIdentity>,
  pub is_administrator: bool,
  pub is_tester: bool,
}

#[cfg_attr(
  feature = "serde",
  derive(Serialize, Deserialize),
  serde(tag = "type", rename = "User")
)]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ShortUser {
  pub id: UserId,
  pub display_name: UserDisplayName,
}

impl ShortUser {
  pub const fn as_ref(&self) -> UserIdRef {
    UserIdRef::new(self.id)
  }
}

impl From<etwin_core::user::ShortUser> for ShortUser {
  fn from(user: etwin_core::user::ShortUser) -> Self {
    Self {
      id: user.id,
      display_name: user.display_name.current.value,
    }
  }
}

/// TODO: Remove from Eternalfest (redirect to Eternaltwin archive)
#[cfg_attr(
  feature = "serde",
  derive(Serialize, Deserialize),
  serde(tag = "type", rename = "Hfest")
)]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct HfestIdentity {
  // This is only a stub, it will be removed from Eternalfest in a future version.
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UserListing {
  pub offset: u32,
  pub limit: u32,
  pub count: u32,
  pub items: Vec<User>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetUserOptions {
  pub id: UserId,
  pub now: Instant,
  pub time: Option<Instant>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct GetUsersOptions {
  pub id: HashSet<UserId>,
  pub now: Instant,
  pub time: Option<Instant>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UpdateUserOptions {
  pub user_id: UserId,
  pub is_tester: bool,
}

#[async_trait]
#[auto_impl(&, Arc)]
pub trait UserStore: Send + Sync {
  async fn upsert_from_etwin(&self, user: &ShortUser) -> Result<User, AnyError>;

  async fn get_user(&self, options: &GetUserOptions) -> Result<User, AnyError>;

  async fn get_short_user(&self, options: &GetUserOptions) -> Result<ShortUser, AnyError>;

  async fn get_short_users(&self, options: &GetUsersOptions) -> Result<HashMap<UserId, ShortUser>, AnyError>;

  async fn get_users(&self) -> Result<UserListing, AnyError>;

  async fn update_user(&self, options: &UpdateUserOptions) -> Result<User, AnyError>;
}
