use eternalfest_core::auth::{
  AuthContext, AuthScope, AuthStore, CreateSessionOptions, GuestAuthContext, Session, SessionId, UserAuthContext,
};
use eternalfest_core::types::AnyError;
use eternalfest_core::user::{GetUserOptions, ShortUser, UserId, UserStore};
use etwin_client::{EtwinAuth, EtwinClient};
use etwin_core::auth::AuthContext as EtwinAuthContext;
use etwin_core::clock::Clock;
use etwin_core::password::Password;
use etwin_core::user::UserDisplayName;
use headers::authorization::Credentials;
use headers::HeaderValue;
use serde::{Deserialize, Serialize};
use std::sync::Arc;

#[derive(Debug, Serialize, Deserialize)]
pub struct AuthenticateHttpOptions {
  authorization_header: Option<String>,
  session_cookie: Option<String>,
}

pub struct AuthService<TyAuthStore, TyClock, TyEtwinClient, TyUserStore>
where
  TyAuthStore: AuthStore,
  TyClock: Clock,
  TyEtwinClient: EtwinClient,
  TyUserStore: UserStore,
{
  auth_store: TyAuthStore,
  clock: TyClock,
  etwin_client: TyEtwinClient,
  user_store: TyUserStore,
}

pub type DynAuthService = AuthService<Arc<dyn AuthStore>, Arc<dyn Clock>, Arc<dyn EtwinClient>, Arc<dyn UserStore>>;

impl<TyAuthStore, TyClock, TyEtwinClient, TyUserStore> AuthService<TyAuthStore, TyClock, TyEtwinClient, TyUserStore>
where
  TyAuthStore: AuthStore,
  TyClock: Clock,
  TyEtwinClient: EtwinClient,
  TyUserStore: UserStore,
{
  pub fn new(auth_store: TyAuthStore, clock: TyClock, etwin_client: TyEtwinClient, user_store: TyUserStore) -> Self {
    Self {
      auth_store,
      clock,
      etwin_client,
      user_store,
    }
  }

  pub async fn authenticate_http(&self, options: &AuthenticateHttpOptions) -> Result<AuthContext, AnyError> {
    if let Some(auth_header) = options.authorization_header.as_ref() {
      self.authenticate_authorization_header(auth_header.as_str()).await
    } else if let Some(session_cookie) = options.session_cookie.as_ref() {
      self.authenticate_session(session_cookie.as_str()).await
    } else {
      Ok(AuthContext::Guest(GuestAuthContext {
        scope: AuthScope::Default,
      }))
    }
  }

  pub async fn etwin_oauth(&self, id: UserId, display_name: &UserDisplayName) -> Result<AuthContext, AnyError> {
    let user = self
      .user_store
      .upsert_from_etwin(&ShortUser {
        id,
        display_name: display_name.clone(),
      })
      .await?;
    Ok(AuthContext::User(UserAuthContext {
      scope: AuthScope::Default,
      user: ShortUser {
        id: user.id,
        display_name: user.display_name,
      },
      is_administrator: user.is_administrator,
      is_tester: user.is_tester,
    }))
  }

  pub async fn session(&self, session_id: SessionId) -> Result<AuthContext, AnyError> {
    let now = self.clock.now();
    let session = self.auth_store.get_and_touch_session(session_id).await?;
    let session = match session {
      Some(s) => s,
      None => return Err("SessionNotFound".into()),
    };

    let user = self
      .user_store
      .get_user(&GetUserOptions {
        id: session.user.id,
        now,
        time: None,
      })
      .await?;

    Ok(AuthContext::User(UserAuthContext {
      scope: AuthScope::Default,
      user: ShortUser {
        id: user.id,
        display_name: user.display_name,
      },
      is_administrator: user.is_administrator,
      is_tester: user.is_tester,
    }))
  }

  pub async fn create_session(&self, user_id: UserId) -> Result<Session, AnyError> {
    let session = self
      .auth_store
      .create_session(&CreateSessionOptions { user: user_id.into() })
      .await?;
    let session = Session {
      id: session.id,
      user: session.user,
      created_at: session.created_at,
      updated_at: session.updated_at,
    };

    Ok(session)
  }

  async fn authenticate_authorization_header(&self, header: &str) -> Result<AuthContext, AnyError> {
    use headers::authorization::Basic;

    let header = HeaderValue::from_str(header)?;
    if header.as_bytes().starts_with(Basic::SCHEME.as_bytes())
      && header.as_bytes().get(Basic::SCHEME.len()) == Some(&b' ')
    {
      let credentials = Basic::decode(&header).ok_or_else::<AnyError, _>(|| "MalformedHeader".into())?;
      let acx = self
        .etwin_client
        .get_self(&EtwinAuth::Credentials {
          username: credentials
            .username()
            .parse()
            .map_err::<AnyError, _>(|_| "InvalidLogin".into())?,
          password: Password(credentials.password().as_bytes().to_vec()),
        })
        .await
        .map_err::<AnyError, _>(|_| "BadCredentials".into())?;
      match acx {
        EtwinAuthContext::User(acx) => {
          let user = self.user_store.upsert_from_etwin(&acx.user.into()).await?;
          Ok(AuthContext::User(UserAuthContext {
            scope: AuthScope::Default,
            user: ShortUser {
              id: user.id,
              display_name: user.display_name,
            },
            is_administrator: user.is_administrator,
            is_tester: user.is_tester,
          }))
        }
        acx => Err(format!("UnexpectedAuthContextType: {acx:?}").into()),
      }
    } else {
      Err("MalformedHeader".into())
    }
  }

  async fn authenticate_session(&self, session_key: &str) -> Result<AuthContext, AnyError> {
    let session_id: SessionId = session_key
      .parse()
      .map_err::<AnyError, _>(|_| "InvalidSessionKey".into())?;
    self.session(session_id).await
  }
}

#[cfg(feature = "neon")]
impl<TyAuthStore, TyClock, TyEtwinClient, TyUserStore> neon::prelude::Finalize
  for AuthService<TyAuthStore, TyClock, TyEtwinClient, TyUserStore>
where
  TyAuthStore: AuthStore,
  TyClock: Clock,
  TyEtwinClient: EtwinClient,
  TyUserStore: UserStore,
{
}
