use eternalfest_core::auth::AuthContext;
use eternalfest_core::blob::BlobStore;
use eternalfest_core::clock::Clock;
use eternalfest_core::game::store::GetStoreGame;
use eternalfest_core::game::{GameChannelPermission, GameId, GameIdRef, GameModeKey, GameRef, GameStore};
use eternalfest_core::run::{
  CreateRunOptions, GetUserItems, Leaderboard, LeaderboardEntry, LeaderboardEntryRun, Run, RunId, RunIdRef, RunStart,
  RunStore, SetRunResultOptions, StoreCreateRun, StoreGetLeaderboard, StoreGetRun, StoreLeaderboard, StoreSetRunResult,
  StoreStartRun,
};
use eternalfest_core::types::AnyError;
use eternalfest_core::user::{GetUserOptions, GetUsersOptions, UserStore};
use etwin_core::hammerfest::HammerfestItemId;
use etwin_core::user::{UserId, UserIdRef};
use std::collections::{BTreeMap, HashSet};
use std::sync::Arc;

pub struct RunService<TyBlobStore, TyClock, TyGameStore, TyRunStore, TyUserStore>
where
  TyBlobStore: BlobStore,
  TyClock: Clock,
  TyGameStore: GameStore,
  TyRunStore: RunStore,
  TyUserStore: UserStore,
{
  #[allow(unused)]
  blob_store: TyBlobStore,
  clock: TyClock,
  game_store: TyGameStore,
  run_store: TyRunStore,
  user_store: TyUserStore,
}

pub type DynRunService =
  RunService<Arc<dyn BlobStore>, Arc<dyn Clock>, Arc<dyn GameStore>, Arc<dyn RunStore>, Arc<dyn UserStore>>;

impl<TyBlobStore, TyClock, TyGameStore, TyRunStore, TyUserStore>
  RunService<TyBlobStore, TyClock, TyGameStore, TyRunStore, TyUserStore>
where
  TyBlobStore: BlobStore,
  TyClock: Clock,
  TyGameStore: GameStore,
  TyRunStore: RunStore,
  TyUserStore: UserStore,
{
  pub fn new(
    blob_store: TyBlobStore,
    clock: TyClock,
    game_store: TyGameStore,
    run_store: TyRunStore,
    user_store: TyUserStore,
  ) -> Self {
    Self {
      blob_store,
      clock,
      game_store,
      run_store,
      user_store,
    }
  }

  pub async fn create_run(&self, acx: &AuthContext, req: &CreateRunOptions) -> Result<Run, AnyError> {
    let now = self.clock.now();
    let (actor, is_tester) = match acx {
      AuthContext::User(u) => (u.user.as_ref(), u.is_tester || u.is_administrator),
      _ => return Err("Unauthorized".into()),
    };
    let game = self
      .game_store
      .get_game(&GetStoreGame {
        actor: Some(actor),
        is_tester,
        now,
        time: None,
        game: req.game.into(),
        channel: Some(req.channel.clone()),
      })
      .await?;
    if game.channels.active.key != req.channel
      || !game.channels.active.is_enabled
      || game.channels.active.build.version != req.version
    {
      return Err("ChannelNotFound".into());
    }
    if !(is_tester || game.owner == actor || game.channels.active.default_permission >= GameChannelPermission::Play) {
      return Err("Unauthorized".into());
    }
    let run = self
      .run_store
      .create_run(&StoreCreateRun {
        game: game.id.into(),
        channel: req.channel.clone(),
        version: req.version,
        user: actor,
        mode: req.game_mode.clone(),
        options: req.game_options.clone(),
        settings: req.settings,
      })
      .await?;

    // TODO: Reuse the auth-context instead of querying the DB
    let user = self
      .user_store
      .get_short_user(&GetUserOptions {
        id: run.user.id,
        time: None,
        now,
      })
      .await?;

    Ok(Run {
      id: run.id,
      created_at: run.created_at,
      started_at: run.started_at,
      result: run.result,
      game: run.game,
      user,
      game_mode: run.mode,
      game_options: run.options,
      settings: run.settings,
    })
  }

  pub async fn get_run(&self, acx: &AuthContext, run_id: RunId) -> Result<Run, AnyError> {
    let now = self.clock.now();
    let (actor, is_tester) = match acx {
      AuthContext::User(u) => (u.user.as_ref(), u.is_tester || u.is_administrator),
      _ => return Err("Unauthorized".into()),
    };
    let run = self
      .run_store
      .get_run(&StoreGetRun {
        run: RunIdRef::new(run_id),
      })
      .await?;
    let game = self
      .game_store
      .get_game(&GetStoreGame {
        actor: Some(actor),
        is_tester,
        now,
        time: None,
        game: GameRef::Id(run.game),
        channel: Some(run.channel.clone()),
      })
      .await?;
    if game.channels.active.key != run.channel || !game.channels.active.is_enabled {
      return Err("ChannelNotFound".into());
    }
    let user = self
      .user_store
      .get_short_user(&GetUserOptions {
        id: run.user.id,
        time: None,
        now,
      })
      .await?;
    Ok(Run {
      id: run.id,
      created_at: run.created_at,
      started_at: run.started_at,
      result: run.result,
      game: run.game,
      user,
      game_mode: run.mode,
      game_options: run.options,
      settings: run.settings,
    })
  }

  pub async fn start_run(&self, acx: &AuthContext, run_id: RunId) -> Result<RunStart, AnyError> {
    let actor = match acx {
      AuthContext::User(u) => u.user.as_ref(),
      _ => return Err("Unauthorized".into()),
    };
    let run_start = self
      .run_store
      .start_run(&StoreStartRun {
        run: RunIdRef::new(run_id),
        if_user: actor,
      })
      .await?;
    Ok(run_start)
  }

  pub async fn set_run_result(
    &self,
    acx: &AuthContext,
    run_id: RunId,
    result: SetRunResultOptions,
  ) -> Result<Run, AnyError> {
    let now = self.clock.now();
    let actor = match acx {
      AuthContext::User(u) => u.user.as_ref(),
      _ => return Err("Unauthorized".into()),
    };

    let run = self
      .run_store
      .set_run_result(&StoreSetRunResult {
        run: RunIdRef::new(run_id),
        if_user: actor,
        is_victory: result.is_victory,
        max_level: result.max_level,
        scores: result.scores,
        items: result.items,
        stats: result.stats,
      })
      .await?;

    // TODO: Reuse the auth-context instead of querying the DB
    let user = self
      .user_store
      .get_short_user(&GetUserOptions {
        id: run.user.id,
        time: None,
        now,
      })
      .await?;

    Ok(Run {
      id: run.id,
      created_at: run.created_at,
      started_at: run.started_at,
      result: run.result,
      game: run.game,
      user,
      game_mode: run.mode,
      game_options: run.options,
      settings: run.settings,
    })
  }

  pub async fn get_user_items(
    &self,
    acx: &AuthContext,
    user_id: UserId,
    game_id: GameId,
  ) -> Result<BTreeMap<HammerfestItemId, u32>, AnyError> {
    let now = self.clock.now();
    let (actor, is_tester) = match acx {
      AuthContext::User(u) => (u.user.as_ref(), u.is_tester || u.is_administrator),
      _ => return Err("Unauthorized".into()),
    };
    let game = self
      .game_store
      .get_game(&GetStoreGame {
        actor: Some(actor),
        is_tester,
        now,
        time: None,
        game: GameRef::id(game_id),
        channel: None,
      })
      .await?;
    if !game.channels.active.is_enabled {
      return Err("ChannelNotFound".into());
    }
    let items = self
      .run_store
      .get_user_items(&GetUserItems {
        user: UserIdRef::new(user_id),
        channel: game.channels.active.key,
        game: GameIdRef::new(game_id),
      })
      .await?;
    Ok(items)
  }

  pub async fn get_leaderboard(
    &self,
    acx: &AuthContext,
    game_id: GameId,
    game_mode: GameModeKey,
  ) -> Result<Leaderboard, AnyError> {
    let now = self.clock.now();
    let (actor, is_tester) = match acx {
      AuthContext::User(u) => (u.user.as_ref(), u.is_tester || u.is_administrator),
      _ => return Err("Unauthorized".into()),
    };
    let game = self
      .game_store
      .get_game(&GetStoreGame {
        actor: Some(actor),
        is_tester,
        now,
        time: None,
        game: GameRef::id(game_id),
        channel: None,
      })
      .await?;
    if !game.channels.active.is_enabled {
      return Err("ChannelNotFound".into());
    }
    let leaderboard: StoreLeaderboard = self
      .run_store
      .get_leaderboard(&StoreGetLeaderboard {
        game: GameIdRef::new(game.id),
        channel: game.channels.active.key,
        mode: game_mode,
      })
      .await?;
    let mut users: HashSet<UserId> = HashSet::new();
    for result in &leaderboard.results {
      users.insert(result.user.id);
    }
    let users = self
      .user_store
      .get_short_users(&GetUsersOptions {
        id: users,
        time: None,
        now,
      })
      .await?;
    Ok(Leaderboard {
      game: leaderboard.game,
      channel: leaderboard.channel,
      mode: leaderboard.mode,
      results: leaderboard
        .results
        .into_iter()
        .map(|store_entry| LeaderboardEntry {
          score: store_entry.score,
          user: users.get(&store_entry.user.id).expect("user must be resolved").clone(),
          run: LeaderboardEntryRun {
            id: store_entry.run.id,
            max_level: store_entry.run.max_level,
            game_options: store_entry.run.game_options,
          },
        })
        .collect(),
    })
  }
}

#[cfg(feature = "neon")]
impl<TyBlobStore, TyClock, TyGameStore, TyRunStore, TyUserStore> neon::prelude::Finalize
  for RunService<TyBlobStore, TyClock, TyGameStore, TyRunStore, TyUserStore>
where
  TyBlobStore: BlobStore,
  TyClock: Clock,
  TyGameStore: GameStore,
  TyRunStore: RunStore,
  TyUserStore: UserStore,
{
}
