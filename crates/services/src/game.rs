use eternalfest_core::auth::AuthContext;
use eternalfest_core::blob::{Blob, BlobId, BlobStore, GetBlobsOptions};
use eternalfest_core::clock::Clock;
use eternalfest_core::core::Listing;
use eternalfest_core::game::requests::{
  CreateGame, CreateGameBuild, GetGame, GetGames, SetGameFavorite, UpdateGameChannel,
};
use eternalfest_core::game::store::{
  CreateBuild, CreateStoreGame, GetStoreGame, GetStoreShortGames, StoreSetGameFavorite, UpdateStoreGameChannel,
};
use eternalfest_core::game::{
  ActiveGameChannel, CustomGameEngine, Game, GameBuildI18n, GameChannelListing, GameEngine, GameListItem, GamePatcher,
  GameResource, GameRevision, GameStore, InputGameEngine, ShortGame, ShortGameBuildI18n, ShortGameChannel,
  ShortGameRevision, StoreShortGame,
};
use eternalfest_core::types::AnyError;
use eternalfest_core::user::{GetUserOptions, GetUsersOptions, UserStore};
use etwin_core::user::{UserId, UserIdRef};
use std::collections::{HashMap, HashSet};
use std::ops::ControlFlow;
use std::sync::Arc;

pub struct GameService<TyBlobStore, TyClock, TyGameStore, TyUserStore>
where
  TyBlobStore: BlobStore,
  TyClock: Clock,
  TyGameStore: GameStore,
  TyUserStore: UserStore,
{
  #[allow(unused)]
  blob_store: TyBlobStore,
  clock: TyClock,
  game_store: TyGameStore,
  user_store: TyUserStore,
}

pub type DynGameService = GameService<Arc<dyn BlobStore>, Arc<dyn Clock>, Arc<dyn GameStore>, Arc<dyn UserStore>>;

impl<TyBlobStore, TyClock, TyGameStore, TyUserStore> GameService<TyBlobStore, TyClock, TyGameStore, TyUserStore>
where
  TyBlobStore: BlobStore,
  TyClock: Clock,
  TyGameStore: GameStore,
  TyUserStore: UserStore,
{
  pub fn new(blob_store: TyBlobStore, clock: TyClock, game_store: TyGameStore, user_store: TyUserStore) -> Self {
    Self {
      blob_store,
      clock,
      game_store,
      user_store,
    }
  }

  pub async fn create_game(&self, acx: &AuthContext, req: &CreateGame) -> Result<Game, AnyError> {
    let authorized_owner: Option<UserIdRef> = match acx {
      AuthContext::System(_) => match req.owner {
        Some(o) => Some(o),
        None => return Err("Missing owner for system creation".into()),
      },
      AuthContext::Guest(_) => None,
      AuthContext::User(ref acx) => {
        let owner: UserIdRef = req.owner.unwrap_or_else(|| UserIdRef::from(acx.user.id));
        if acx.is_administrator || acx.user.id == owner.id {
          Some(owner)
        } else {
          None
        }
      }
    };
    let owner = match authorized_owner {
      Some(o) => o,
      None => return Err("Unauthorized".into()),
    };
    let options = CreateStoreGame {
      owner,
      key: req.key.clone(),
      build: req.build.clone(),
      channels: req.channels.clone(),
    };
    let game = self.game_store.create_game(&options).await?;
    let now = game.created_at;
    let owner = self
      .user_store
      .get_short_user(&GetUserOptions {
        id: game.owner.id,
        now,
        time: None,
      })
      .await?;

    let mut blobs = HashSet::<BlobId>::new();
    game.for_each_blob(|blob| {
      blobs.insert(blob.id);
      ControlFlow::<()>::Continue(())
    });
    let blobs = self
      .blob_store
      .get_blobs(&GetBlobsOptions {
        id: blobs,
        now,
        time: Some(now),
      })
      .await?;
    let blobs: HashMap<BlobId, Blob> = blobs
      .into_iter()
      .map(|(blob_id, blob_result)| (blob_id, blob_result.expect("blob referenced by game must exist")))
      .collect();

    let store_channel = game.channels.active;
    let channels = GameChannelListing {
      offset: game.channels.offset,
      limit: game.channels.limit,
      count: game.channels.count,
      is_count_exact: game.channels.is_count_exact,
      active: ActiveGameChannel {
        key: store_channel.key,
        is_enabled: store_channel.is_enabled,
        is_pinned: store_channel.is_pinned,
        publication_date: store_channel.publication_date,
        sort_update_date: store_channel.sort_update_date,
        default_permission: store_channel.default_permission,
        build: GameRevision {
          version: store_channel.build.version,
          created_at: now,
          git_commit_ref: store_channel.build.git_commit_ref,
          main_locale: store_channel.build.main_locale,
          display_name: store_channel.build.display_name,
          description: store_channel.build.description,
          icon: store_channel
            .build
            .icon
            .map(|blob| blobs.get(&blob.id).expect("icon was retrieved").clone()),
          loader: store_channel.build.loader,
          engine: match store_channel.build.engine {
            InputGameEngine::V96 => GameEngine::V96,
            InputGameEngine::Custom(engine) => {
              GameEngine::custom(blobs.get(&engine.blob.id).expect("engine was retrieved").clone())
            }
          },
          patcher: store_channel.build.patcher.map(|p| GamePatcher {
            blob: blobs.get(&p.blob.id).expect("patcher was retrieved").clone(),
            framework: p.framework,
            meta: p.meta,
          }),
          debug: store_channel
            .build
            .debug
            .map(|blob| blobs.get(&blob.id).expect("debug was retrieved").clone()),
          content: store_channel
            .build
            .content
            .map(|blob| blobs.get(&blob.id).expect("content was retrieved").clone()),
          content_i18n: store_channel
            .build
            .content_i18n
            .map(|blob| blobs.get(&blob.id).expect("i18n_bundle was retrieved").clone()),
          musics: store_channel
            .build
            .musics
            .into_iter()
            .map(|gr| GameResource {
              blob: blobs.get(&gr.blob.id).expect("music was retrieved").clone(),
              display_name: gr.display_name,
            })
            .collect(),
          modes: store_channel.build.modes,
          families: store_channel.build.families,
          category: store_channel.build.category,
          i18n: store_channel
            .build
            .i18n
            .into_iter()
            .map(|(l, store_i18n)| {
              (
                l,
                GameBuildI18n {
                  display_name: store_i18n.display_name,
                  description: store_i18n.description,
                  icon: store_i18n
                    .icon
                    .map(|blob| blobs.get(&blob.id).expect("blob was retrieved").clone()),
                  content_i18n: store_i18n
                    .content_i18n
                    .map(|blob| blobs.get(&blob.id).expect("blob was retrieved").clone()),
                  modes: store_i18n.modes,
                },
              )
            })
            .collect(),
        },
      },
      items: game
        .channels
        .items
        .into_iter()
        .map(|chan| {
          chan.map(|store_channel| ShortGameChannel {
            key: store_channel.key,
            is_enabled: store_channel.is_enabled,
            is_pinned: store_channel.is_pinned,
            publication_date: store_channel.publication_date,
            sort_update_date: store_channel.sort_update_date,
            default_permission: store_channel.default_permission,
            build: ShortGameRevision {
              version: store_channel.build.version,
              git_commit_ref: store_channel.build.git_commit_ref,
              main_locale: store_channel.build.main_locale,
              display_name: store_channel.build.display_name,
              description: store_channel.build.description,
              icon: store_channel
                .build
                .icon
                .map(|blob| blobs.get(&blob.id).expect("blob was retrieved").clone()),
              i18n: store_channel
                .build
                .i18n
                .into_iter()
                .map(|(l, store_i18n)| {
                  (
                    l,
                    ShortGameBuildI18n {
                      display_name: store_i18n.display_name,
                      description: store_i18n.description,
                      icon: store_i18n
                        .icon
                        .map(|blob| blobs.get(&blob.id).expect("blob was retrieved").clone()),
                    },
                  )
                })
                .collect(),
            },
          })
        })
        .collect(),
    };
    Ok(Game {
      id: game.id,
      created_at: game.created_at,
      key: game.key,
      owner,
      channels,
    })
  }

  pub async fn create_build(&self, acx: &AuthContext, req: &CreateGameBuild) -> Result<GameRevision, AnyError> {
    let if_owner: Option<UserIdRef> = match acx {
      AuthContext::System(_) => None,
      AuthContext::Guest(_) => return Err("Unauthorized".into()),
      AuthContext::User(ref acx) => {
        if acx.is_administrator {
          None
        } else {
          Some(acx.user.as_ref())
        }
      }
    };
    let options = CreateBuild {
      game: req.game.clone(),
      if_owner,
      build: req.build.clone(),
    };
    let store_build = self.game_store.create_version(&options).await?;
    let now = store_build.created_at;
    let mut blobs = HashSet::<BlobId>::new();
    store_build.for_each_blob(|blob| {
      blobs.insert(blob.id);
      ControlFlow::<()>::Continue(())
    });
    let blobs = self
      .blob_store
      .get_blobs(&GetBlobsOptions {
        id: blobs,
        now,
        time: Some(now),
      })
      .await?;
    let blobs: HashMap<BlobId, Blob> = blobs
      .into_iter()
      .map(|(blob_id, blob_result)| (blob_id, blob_result.expect("blob referenced by game must exist")))
      .collect();

    let build = GameRevision {
      version: store_build.version,
      created_at: now,
      git_commit_ref: store_build.git_commit_ref,
      main_locale: store_build.main_locale,
      display_name: store_build.display_name,
      description: store_build.description,
      icon: store_build
        .icon
        .map(|blob| blobs.get(&blob.id).expect("icon was retrieved").clone()),
      loader: store_build.loader,
      engine: match store_build.engine {
        InputGameEngine::V96 => GameEngine::V96,
        InputGameEngine::Custom(engine) => {
          GameEngine::custom(blobs.get(&engine.blob.id).expect("engine was retrieved").clone())
        }
      },
      patcher: store_build.patcher.map(|p| GamePatcher {
        blob: blobs.get(&p.blob.id).expect("patcher was retrieved").clone(),
        framework: p.framework,
        meta: p.meta,
      }),
      debug: store_build
        .debug
        .map(|blob| blobs.get(&blob.id).expect("debug was retrieved").clone()),
      content: store_build
        .content
        .map(|blob| blobs.get(&blob.id).expect("content was retrieved").clone()),
      content_i18n: store_build
        .content_i18n
        .map(|blob| blobs.get(&blob.id).expect("i18n_bundle was retrieved").clone()),
      musics: store_build
        .musics
        .into_iter()
        .map(|gr| GameResource {
          blob: blobs.get(&gr.blob.id).expect("music was retrieved").clone(),
          display_name: gr.display_name,
        })
        .collect(),
      modes: store_build.modes,
      families: store_build.families,
      category: store_build.category,
      i18n: store_build
        .i18n
        .into_iter()
        .map(|(l, store_i18n)| {
          (
            l,
            GameBuildI18n {
              display_name: store_i18n.display_name,
              description: store_i18n.description,
              icon: store_i18n
                .icon
                .map(|blob| blobs.get(&blob.id).expect("blob was retrieved").clone()),
              content_i18n: store_i18n
                .content_i18n
                .map(|blob| blobs.get(&blob.id).expect("blob was retrieved").clone()),
              modes: store_i18n.modes,
            },
          )
        })
        .collect(),
    };
    Ok(build)
  }

  pub async fn update_channel(
    &self,
    acx: &AuthContext,
    req: &UpdateGameChannel,
  ) -> Result<ActiveGameChannel, AnyError> {
    // TODO: Add some check to restrict changes to channels that are public (or will be public)
    let (actor, if_owner): (UserIdRef, Option<UserIdRef>) = match acx {
      AuthContext::System(_) => return Err("Unauthorized".into()),
      AuthContext::Guest(_) => return Err("Unauthorized".into()),
      AuthContext::User(ref acx) => {
        let actor = acx.user.as_ref();
        if acx.is_administrator {
          (actor, None)
        } else {
          (actor, Some(actor))
        }
      }
    };
    let store_command = UpdateStoreGameChannel {
      actor,
      game: req.game.clone(),
      channel_key: req.channel_key.clone(),
      if_owner,
      patches: req.patches.clone(),
    };
    let (store_channel, now) = self.game_store.update_game_channel(&store_command).await?;
    let mut blobs = HashSet::<BlobId>::new();
    store_channel.for_each_blob(|blob| {
      blobs.insert(blob.id);
      ControlFlow::<()>::Continue(())
    });
    let blobs = self
      .blob_store
      .get_blobs(&GetBlobsOptions {
        id: blobs,
        now,
        time: Some(now),
      })
      .await?;
    let blobs: HashMap<BlobId, Blob> = blobs
      .into_iter()
      .map(|(blob_id, blob_result)| (blob_id, blob_result.expect("blob referenced by game must exist")))
      .collect();

    let channel = ActiveGameChannel {
      key: store_channel.key,
      is_enabled: store_channel.is_enabled,
      is_pinned: store_channel.is_pinned,
      publication_date: store_channel.publication_date,
      sort_update_date: store_channel.sort_update_date,
      default_permission: store_channel.default_permission,
      build: {
        let store_build = store_channel.build;
        GameRevision {
          version: store_build.version,
          created_at: now,
          git_commit_ref: store_build.git_commit_ref,
          main_locale: store_build.main_locale,
          display_name: store_build.display_name,
          description: store_build.description,
          icon: store_build
            .icon
            .map(|blob| blobs.get(&blob.id).expect("icon was retrieved").clone()),
          loader: store_build.loader,
          engine: match store_build.engine {
            InputGameEngine::V96 => GameEngine::V96,
            InputGameEngine::Custom(engine) => {
              GameEngine::custom(blobs.get(&engine.blob.id).expect("engine was retrieved").clone())
            }
          },
          patcher: store_build.patcher.map(|p| GamePatcher {
            blob: blobs.get(&p.blob.id).expect("patcher was retrieved").clone(),
            framework: p.framework,
            meta: p.meta,
          }),
          debug: store_build
            .debug
            .map(|blob| blobs.get(&blob.id).expect("debug was retrieved").clone()),
          content: store_build
            .content
            .map(|blob| blobs.get(&blob.id).expect("content was retrieved").clone()),
          content_i18n: store_build
            .content_i18n
            .map(|blob| blobs.get(&blob.id).expect("i18n_bundle was retrieved").clone()),
          musics: store_build
            .musics
            .into_iter()
            .map(|gr| GameResource {
              blob: blobs.get(&gr.blob.id).expect("music was retrieved").clone(),
              display_name: gr.display_name,
            })
            .collect(),
          modes: store_build.modes,
          families: store_build.families,
          category: store_build.category,
          i18n: store_build
            .i18n
            .into_iter()
            .map(|(l, store_i18n)| {
              (
                l,
                GameBuildI18n {
                  display_name: store_i18n.display_name,
                  description: store_i18n.description,
                  icon: store_i18n
                    .icon
                    .map(|blob| blobs.get(&blob.id).expect("blob was retrieved").clone()),
                  content_i18n: store_i18n
                    .content_i18n
                    .map(|blob| blobs.get(&blob.id).expect("blob was retrieved").clone()),
                  modes: store_i18n.modes,
                },
              )
            })
            .collect(),
        }
      },
    };
    Ok(channel)
  }

  // TODO: Handle missing games by returning an error
  pub async fn get_game(&self, acx: &AuthContext, req: &GetGame) -> Result<Option<Game>, AnyError> {
    let now = self.clock.now();
    let (actor, is_tester) = match acx {
      AuthContext::User(u) => (Some(u.user.as_ref()), u.is_tester || u.is_administrator),
      _ => (None, false),
    };

    let store_game = self
      .game_store
      .get_game(&GetStoreGame {
        actor,
        now,
        is_tester,
        time: req.time,
        game: req.game.clone(),
        channel: req.channel.clone(),
      })
      .await?;

    let mut users = HashSet::<UserId>::new();
    users.insert(store_game.owner.id);
    let users = self
      .user_store
      .get_short_users(&GetUsersOptions {
        id: users,
        now,
        time: req.time,
      })
      .await?;

    let mut blobs = HashSet::<BlobId>::new();
    store_game.for_each_blob(|blob| {
      blobs.insert(blob.id);
      ControlFlow::<()>::Continue(())
    });
    let blobs = self
      .blob_store
      .get_blobs(&GetBlobsOptions {
        id: blobs,
        now,
        time: Some(now),
      })
      .await?;
    let blobs: HashMap<BlobId, Blob> = blobs
      .into_iter()
      .map(|(blob_id, blob_result)| (blob_id, blob_result.expect("blob referenced by game must exist")))
      .collect();

    Ok(Some(Game {
      id: store_game.id,
      created_at: store_game.created_at,
      key: store_game.key,
      owner: users.get(&store_game.owner.id).expect("owner is resolved").clone(),
      channels: GameChannelListing {
        offset: store_game.channels.offset,
        limit: store_game.channels.limit,
        count: store_game.channels.count,
        is_count_exact: store_game.channels.is_count_exact,
        active: ActiveGameChannel {
          key: store_game.channels.active.key,
          is_enabled: store_game.channels.active.is_enabled,
          is_pinned: store_game.channels.active.is_pinned,
          publication_date: store_game.channels.active.publication_date,
          sort_update_date: store_game.channels.active.sort_update_date,
          default_permission: store_game.channels.active.default_permission,
          build: {
            let build = store_game.channels.active.build;
            GameRevision {
              version: build.version,
              created_at: build.created_at,
              git_commit_ref: build.git_commit_ref,
              main_locale: build.main_locale,
              display_name: build.display_name,
              description: build.description,
              icon: build.icon.map(|b| blobs.get(&b.id).expect("icon was resolved").clone()),
              loader: build.loader,
              engine: match build.engine {
                InputGameEngine::V96 => GameEngine::V96,
                InputGameEngine::Custom(engine) => GameEngine::Custom(CustomGameEngine {
                  blob: blobs.get(&engine.blob.id).expect("engine was resolved").clone(),
                }),
              },
              patcher: build.patcher.map(|p| GamePatcher {
                blob: blobs.get(&p.blob.id).expect("patcher was resolved").clone(),
                framework: p.framework,
                meta: p.meta,
              }),
              debug: build
                .debug
                .map(|b| blobs.get(&b.id).expect("debug was resolved").clone()),
              content: build
                .content
                .map(|b| blobs.get(&b.id).expect("content was resolved").clone()),
              content_i18n: build
                .content_i18n
                .map(|b| blobs.get(&b.id).expect("i18n_bundle was resolved").clone()),
              musics: build
                .musics
                .into_iter()
                .map(|gr| GameResource {
                  blob: blobs.get(&gr.blob.id).expect("music was resolved").clone(),
                  display_name: gr.display_name,
                })
                .collect(),
              modes: build.modes,
              families: build.families,
              category: build.category,
              i18n: build
                .i18n
                .into_iter()
                .map(|(l, i18n)| {
                  (
                    l,
                    GameBuildI18n {
                      display_name: i18n.display_name,
                      description: i18n.description,
                      icon: i18n
                        .icon
                        .map(|b| blobs.get(&b.id).expect("i18n.icon was resolved").clone()),
                      content_i18n: i18n
                        .content_i18n
                        .map(|b| blobs.get(&b.id).expect("i18n.i18n_bundle was resolved").clone()),
                      modes: i18n.modes,
                    },
                  )
                })
                .collect(),
            }
          },
        },
        items: vec![],
      },
    }))
  }

  pub async fn get_games(&self, acx: &AuthContext, req: &GetGames) -> Result<Listing<GameListItem>, AnyError> {
    let now = self.clock.now();
    let (actor, is_tester) = match acx {
      AuthContext::User(u) => (Some(u.user.as_ref()), u.is_tester || u.is_administrator),
      _ => (None, false),
    };

    let store_games = self
      .game_store
      .get_short_games(&GetStoreShortGames {
        actor,
        is_tester,
        favorite: req.favorite,
        offset: req.offset,
        limit: req.limit,
        now,
        time: req.time,
      })
      .await?;

    let mut users = HashSet::<UserId>::new();
    for store_game in store_games.items.iter().flatten() {
      users.insert(store_game.owner.id);
    }
    let users = self
      .user_store
      .get_short_users(&GetUsersOptions {
        id: users,
        now,
        time: req.time,
      })
      .await?;

    let mut blobs = HashSet::<BlobId>::new();
    for store_game in store_games.items.iter().flatten() {
      store_game.for_each_blob(|blob| {
        blobs.insert(blob.id);
        ControlFlow::<()>::Continue(())
      });
    }
    let blobs = self
      .blob_store
      .get_blobs(&GetBlobsOptions {
        id: blobs,
        now,
        time: Some(now),
      })
      .await?;
    let blobs: HashMap<BlobId, Blob> = blobs
      .into_iter()
      .map(|(blob_id, blob_result)| (blob_id, blob_result.expect("blob referenced by game must exist")))
      .collect();

    let mut items: Vec<GameListItem> = Vec::new();
    for store_game in store_games.items.into_iter() {
      let store_game: StoreShortGame = match store_game {
        None => {
          items.push(None);
          continue;
        }
        Some(store_game) => store_game,
      };

      let channels = store_game.channels.map(|store_channel| ShortGameChannel {
        key: store_channel.key,
        is_enabled: store_channel.is_enabled,
        is_pinned: store_channel.is_pinned,
        publication_date: store_channel.publication_date,
        sort_update_date: store_channel.sort_update_date,
        default_permission: store_channel.default_permission,
        build: ShortGameRevision {
          version: store_channel.build.version,
          git_commit_ref: store_channel.build.git_commit_ref,
          main_locale: store_channel.build.main_locale,
          display_name: store_channel.build.display_name,
          description: store_channel.build.description,
          icon: store_channel
            .build
            .icon
            .map(|blob| blobs.get(&blob.id).expect("blob was retrieved").clone()),
          i18n: store_channel
            .build
            .i18n
            .into_iter()
            .map(|(l, store_i18n)| {
              (
                l,
                ShortGameBuildI18n {
                  display_name: store_i18n.display_name,
                  description: store_i18n.description,
                  icon: store_i18n
                    .icon
                    .map(|blob| blobs.get(&blob.id).expect("blob was retrieved").clone()),
                },
              )
            })
            .collect(),
        },
      });

      let item = ShortGame {
        id: store_game.id,
        created_at: store_game.created_at,
        key: store_game.key,
        owner: users
          .get(&store_game.owner.id)
          .expect("user should be resolved")
          .clone(),
        channels,
      };
      items.push(Some(item));
    }

    Ok(Listing {
      offset: store_games.offset,
      limit: store_games.limit,
      count: store_games.count,
      is_count_exact: store_games.is_count_exact,
      items,
    })
  }

  pub async fn set_game_favorite(&self, acx: &AuthContext, req: &SetGameFavorite) -> Result<bool, AnyError> {
    let user = match acx {
      AuthContext::User(ref acx) => {
        if acx.is_administrator {
          None
        } else {
          Some(acx.user.as_ref())
        }
      }
      _ => None,
    };
    let user = user.ok_or_else(|| AnyError::from("Unauthorized"))?;

    let options = StoreSetGameFavorite {
      user,
      game: req.game.clone(),
      favorite: req.favorite,
    };
    self.game_store.set_game_favorite(&options).await?;
    Ok(req.favorite)
  }
}

#[cfg(feature = "neon")]
impl<TyBlobStore, TyClock, TyGameStore, TyUserStore> neon::prelude::Finalize
  for GameService<TyBlobStore, TyClock, TyGameStore, TyUserStore>
where
  TyBlobStore: BlobStore,
  TyClock: Clock,
  TyGameStore: GameStore,
  TyUserStore: UserStore,
{
}
