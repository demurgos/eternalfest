use eternalfest_core::auth::AuthContext;
use eternalfest_core::blob::{Blob, BlobStore, CreateBlobOptions};
use eternalfest_core::file::{
  CreateDirectoryError, CreateDirectoryOptions, CreateFileError, CreateFileOptions, CreateStoreDirectoryOptions,
  CreateStoreFileOptions, DeleteItemOptions, Directory, Drive, DriveItem, File, FileStore, GetDirectoryOptions,
  GetDriveByOwnerOptions, GetDriveOptions, GetFileOptions, GetItemByPathOptions,
};
use eternalfest_core::types::{to_any_error, AnyError};
use eternalfest_core::user::UserStore;
use std::sync::Arc;

pub struct FileService<TyBlobStore, TyFileStore, TyUserStore>
where
  TyBlobStore: BlobStore,
  TyFileStore: FileStore,
  TyUserStore: UserStore,
{
  #[allow(unused)]
  blob_store: TyBlobStore,
  file_store: TyFileStore,
  #[allow(unused)]
  user_store: TyUserStore,
}

pub type DynFileService = FileService<Arc<dyn BlobStore>, Arc<dyn FileStore>, Arc<dyn UserStore>>;

impl<TyBlobStore, TyFileStore, TyUserStore> FileService<TyBlobStore, TyFileStore, TyUserStore>
where
  TyBlobStore: BlobStore,
  TyFileStore: FileStore,
  TyUserStore: UserStore,
{
  pub fn new(blob_store: TyBlobStore, file_store: TyFileStore, user_store: TyUserStore) -> Self {
    Self {
      blob_store,
      file_store,
      user_store,
    }
  }

  pub async fn get_drive(&self, _acx: &AuthContext, options: &GetDriveOptions) -> Result<Drive, AnyError> {
    self.file_store.get_drive(options).await.map_err(to_any_error)
  }

  pub async fn get_drive_by_owner(
    &self,
    acx: &AuthContext,
    options: &GetDriveByOwnerOptions,
  ) -> Result<Drive, AnyError> {
    let authorized = match acx {
      AuthContext::System(_) => true,
      AuthContext::Guest(_) => false,
      AuthContext::User(ref acx) => acx.is_administrator || acx.user.id == options.id,
    };
    if !authorized {
      return Err("Unauthorized".into());
    }
    self.file_store.get_drive_by_owner(options).await.map_err(to_any_error)
  }

  pub async fn get_item_by_path(
    &self,
    _acx: &AuthContext,
    options: &GetItemByPathOptions,
  ) -> Result<DriveItem, AnyError> {
    self.file_store.get_item_by_path(options).await.map_err(to_any_error)
  }

  pub async fn create_directory(
    &self,
    acx: &AuthContext,
    options: &CreateDirectoryOptions,
  ) -> Result<Directory, AnyError> {
    let check_owner = match acx {
      AuthContext::System(_) => None,
      AuthContext::Guest(_) => return Err("Unauthorized".into()),
      AuthContext::User(ref acx) => {
        if acx.is_administrator {
          None
        } else {
          Some(acx.user.id)
        }
      }
    };

    self
      .file_store
      .create_directory(&CreateStoreDirectoryOptions {
        parent_id: options.parent_id,
        display_name: options.display_name.clone(),
        check_owner,
      })
      .await
      .map_err(|e| match e {
        CreateDirectoryError::ParentNotFound => "Unauthorized".into(),
        CreateDirectoryError::NotOwner => "Unauthorized".into(),
        e @ CreateDirectoryError::Other(_) => to_any_error(e),
      })
  }

  pub async fn get_directory(&self, _acx: &AuthContext, options: &GetDirectoryOptions) -> Result<Directory, AnyError> {
    self.file_store.get_directory(options).await.map_err(to_any_error)
  }

  pub async fn get_directory_children(
    &self,
    _acx: &AuthContext,
    options: &GetDirectoryOptions,
  ) -> Result<Vec<DriveItem>, AnyError> {
    self
      .file_store
      .get_directory_children(options)
      .await
      .map_err(to_any_error)
  }

  pub async fn create_file(&self, acx: &AuthContext, options: &CreateFileOptions) -> Result<File, AnyError> {
    let check_owner = match acx {
      AuthContext::System(_) => None,
      AuthContext::Guest(_) => return Err("Unauthorized".into()),
      AuthContext::User(ref acx) => {
        if acx.is_administrator {
          None
        } else {
          Some(acx.user.id)
        }
      }
    };

    self
      .file_store
      .create_file(&CreateStoreFileOptions {
        parent_id: options.parent_id,
        blob_id: options.blob_id,
        display_name: options.display_name.clone(),
        check_owner,
      })
      .await
      .map_err(|e| match e {
        CreateFileError::ParentNotFound => "Unauthorized".into(),
        CreateFileError::NotOwner => "Unauthorized".into(),
        e @ CreateFileError::Other(_) => to_any_error(e),
      })
  }

  pub async fn get_file(&self, _acx: &AuthContext, options: &GetFileOptions) -> Result<File, AnyError> {
    self.file_store.get_file(options).await.map_err(to_any_error)
  }

  pub async fn get_file_data(&self, _acx: &AuthContext, options: &GetFileOptions) -> Result<Vec<u8>, AnyError> {
    self.file_store.get_file_data(options).await.map_err(to_any_error)
  }

  pub async fn delete_item(&self, _acx: &AuthContext, options: &DeleteItemOptions) -> Result<(), AnyError> {
    self.file_store.delete_item(options).await.map_err(to_any_error)
  }

  pub async fn create_blob(&self, _acx: &AuthContext, options: &CreateBlobOptions) -> Result<Blob, AnyError> {
    self.blob_store.create_blob(options).await.map_err(to_any_error)
  }
}

#[cfg(feature = "neon")]
impl<TyBlobStore, TyFileStore, TyUserStore> neon::prelude::Finalize
  for FileService<TyBlobStore, TyFileStore, TyUserStore>
where
  TyBlobStore: BlobStore,
  TyFileStore: FileStore,
  TyUserStore: UserStore,
{
}
