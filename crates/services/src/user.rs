use eternalfest_core::auth::AuthContext;
use eternalfest_core::types::AnyError;
use eternalfest_core::user::{
  GetUserOptions, GetUsersOptions, ShortUser, UpdateUserOptions, User, UserId, UserListing, UserStore,
};
use std::collections::HashMap;
use std::sync::Arc;

pub struct UserService<TyUserStore>
where
  TyUserStore: UserStore,
{
  user_store: TyUserStore,
}

pub type DynUserService = UserService<Arc<dyn UserStore>>;

impl<TyUserStore> UserService<TyUserStore>
where
  TyUserStore: UserStore,
{
  pub fn new(user_store: TyUserStore) -> Self {
    Self { user_store }
  }

  pub async fn upsert_from_etwin(&self, _acx: &AuthContext, user: &ShortUser) -> Result<User, AnyError> {
    self.user_store.upsert_from_etwin(user).await
  }

  pub async fn get_user(&self, _acx: &AuthContext, options: &GetUserOptions) -> Result<User, AnyError> {
    self.user_store.get_user(options).await
  }

  pub async fn get_user_ref(&self, _acx: &AuthContext, options: &GetUserOptions) -> Result<ShortUser, AnyError> {
    self.user_store.get_short_user(options).await
  }

  pub async fn get_user_refs(
    &self,
    _acx: &AuthContext,
    options: &GetUsersOptions,
  ) -> Result<HashMap<UserId, ShortUser>, AnyError> {
    self.user_store.get_short_users(options).await
  }

  pub async fn get_users(&self, _acx: &AuthContext) -> Result<UserListing, AnyError> {
    self.user_store.get_users().await
  }

  pub async fn update_user(&self, acx: &AuthContext, options: &UpdateUserOptions) -> Result<User, AnyError> {
    let authorized = match acx {
      AuthContext::System(_) => true,
      AuthContext::Guest(_) => false,
      AuthContext::User(ref user) => user.is_administrator,
    };
    if !authorized {
      return Err("Unauthorized".into());
    }
    self.user_store.update_user(options).await
  }
}

#[cfg(feature = "neon")]
impl<TyUserStore> neon::prelude::Finalize for UserService<TyUserStore> where TyUserStore: UserStore {}
