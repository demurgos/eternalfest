use serde::{Deserialize, Serialize};
use std::fs;
use std::io;
use std::path::{Path, PathBuf};
use url::Url;

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct Config {
  pub eternalfest: EternalfestConfig,
  pub db: DbConfig,
  pub data: DataConfig,
  pub etwin: EtwinConfig,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct RawConfig {
  pub eternalfest: EternalfestConfig,
  pub db: DbConfig,
  pub data: DataRawConfig,
  pub etwin: EtwinConfig,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct EternalfestConfig {
  pub http_port: u16,
  pub external_uri: Url,
  pub secret: String,
  pub cookie_key: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct DbConfig {
  pub host: String,
  pub port: u16,
  pub name: String,
  pub admin_user: String,
  pub admin_password: String,
  pub user: String,
  pub password: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct DataConfig {
  /// URL to the root of the data directory
  pub root: Url,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct DataRawConfig {
  /// URL to the root of the data directory
  pub root: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct EtwinConfig {
  pub uri: Url,
  pub oauth_client_id: String,
  pub oauth_client_secret: String,
}

#[derive(Debug)]
pub enum FindConfigFileError {
  NotFound(PathBuf),
  Other(PathBuf, io::Error),
}

#[derive(Debug, thiserror::Error)]
pub enum FindConfigError {
  #[error("config file not found, searching from {:?}", .0)]
  NotFound(PathBuf),
  #[error("config file parse error")]
  ParseError(#[source] toml::de::Error),
  #[error("I/O config file error, searching from {:?}", .0)]
  Other(PathBuf, #[source] io::Error),
}

fn find_config_file(dir: PathBuf) -> Result<(PathBuf, String), FindConfigFileError> {
  for d in dir.ancestors() {
    let config_path = d.join("eternalfest.toml");
    match fs::read_to_string(&config_path) {
      Ok(toml) => return Ok((config_path, toml)),
      Err(e) if e.kind() == io::ErrorKind::NotFound => continue,
      Err(e) => return Err(FindConfigFileError::Other(dir, e)),
    }
  }
  Err(FindConfigFileError::NotFound(dir))
}

pub fn parse_config(_file: &Path, config_toml: &str) -> Result<RawConfig, toml::de::Error> {
  let raw: RawConfig = toml::from_str(config_toml)?;
  Ok(raw)
}

pub fn find_config(dir: PathBuf) -> Result<Config, FindConfigError> {
  match find_config_file(dir) {
    Ok((file, config_toml)) => match parse_config(&file, &config_toml) {
      Ok(raw) => {
        let config_uri = Url::from_file_path(file.as_path()).expect("InvalidConfigPath");
        let config = Config {
          eternalfest: raw.eternalfest,
          db: raw.db,
          data: DataConfig {
            root: Url::options()
              .base_url(Some(&config_uri))
              .parse(raw.data.root.as_str())
              .expect("InvalidDataRoot"),
          },
          etwin: raw.etwin,
        };
        Ok(config)
      }
      Err(e) => Err(FindConfigError::ParseError(e)),
    },
    Err(FindConfigFileError::NotFound(dir)) => Err(FindConfigError::NotFound(dir)),
    Err(FindConfigFileError::Other(dir, cause)) => Err(FindConfigError::Other(dir, cause)),
  }
}

// pub static DEFAULT: Lazy<Config> = Lazy::new(|| Config::default());

// #[cfg(test)]
// mod test {
//   use crate::{parse_config, ConfigSource, DEFAULT};
//
//   #[test]
//   fn test_default_config() {
//     const INPUT: &str = "";
//     let path = std::env::current_dir().unwrap().join("etwin.toml");
//     let actual = parse_config(&path, INPUT).unwrap();
//     let actual = actual.resolve(&*DEFAULT, &ConfigSource::Default);
//     let expected = DEFAULT.clone();
//     assert_eq!(actual, expected);
//   }
// }
