// use once_cell::sync::Lazy;
// use serde::Deserialize;
// use std::fs;
// use std::io;
// use std::path::{Path, PathBuf};
// use url::Url;
//
// #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
// pub struct ConfigItem<T> {
//   pub value: T,
//   pub source: ConfigSource,
// }
//
// impl<T> ConfigItem<T> {
//   pub fn default(value: T) -> Self {
//     Self {
//       value,
//       source: ConfigSource::Default,
//     }
//   }
// }
//
// #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
// pub enum ConfigSource {
//   Default,
//   File(PathBuf),
// }
//
// #[derive(Default, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
// pub struct Config {
//   pub eternalfest: EternalfestConfig,
//   pub db: DbConfig,
//   pub data: DataConfig,
//   pub etwin: EtwinConfig,
// }
//
// #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
// pub struct EternalfestConfig {
//   pub http_port: ConfigItem<u16>,
//   pub external_uri: ConfigItem<Url>,
//   pub secret: ConfigItem<String>,
//   pub cookie_key: ConfigItem<String>,
// }
//
// impl Default for EternalfestConfig {
//   fn default() -> Self {
//     Self {
//       http_port: ConfigItem::default(50313),
//       external_uri: ConfigItem::default(Url::parse("http://localhost:50313").unwrap()),
//       secret: ConfigItem::default("dev".to_string()),
//       cookie_key: ConfigItem::default("dev".to_string()),
//     }
//   }
// }
//
// #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
// pub struct DbConfig {
//   pub host: ConfigItem<String>,
//   pub port: ConfigItem<u16>,
//   pub name: ConfigItem<String>,
//   pub admin_user: ConfigItem<String>,
//   pub admin_password: ConfigItem<String>,
//   pub user: ConfigItem<String>,
//   pub password: ConfigItem<String>,
// }
//
// impl Default for DbConfig {
//   fn default() -> Self {
//     Self {
//       host: ConfigItem::default("localhost".to_string()),
//       port: ConfigItem::default(5432),
//       name: ConfigItem::default("eternalfest.dev".to_string()),
//       admin_user: ConfigItem::default("eternalfest.dev.admin".to_string()),
//       admin_password: ConfigItem::default("dev".to_string()),
//       user: ConfigItem::default("eternalfest.dev.admin".to_string()),
//       password: ConfigItem::default("dev".to_string()),
//     }
//   }
// }
//
// #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
// pub struct DataConfig {
//   /// URL to the root of the data directory
//   pub root: ConfigItem<String>,
// }
//
// impl DataConfig {
//   pub fn resolved_root(&self) -> PathBuf {
//     let base_dir = match &self.root.source {
//       ConfigSource::Default => std::env::current_dir().unwrap(),
//       ConfigSource::File(conf_file) => conf_file.join(".."),
//     };
//     base_dir.join(self.root.value.as_str())
//   }
// }
//
// impl Default for DataConfig {
//   fn default() -> Self {
//     Self {
//       root: ConfigItem::default("./data".to_string()),
//     }
//   }
// }
//
// #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
// pub struct EtwinConfig {
//   pub uri: ConfigItem<Url>,
//   pub oauth_client_id: ConfigItem<String>,
//   pub oauth_client_secret: ConfigItem<String>,
// }
//
// impl Default for EtwinConfig {
//   fn default() -> Self {
//     Self {
//       uri: ConfigItem::default(Url::parse("http://localhost:50320").unwrap()),
//       oauth_client_id: ConfigItem::default("eternalfest@clients".to_string()),
//       oauth_client_secret: ConfigItem::default("dev".to_string()),
//     }
//   }
// }
//
// #[derive(Default, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
// pub struct PartialRawConfig {
//   eternalfest: Option<EternalfestPartialRawConfig>,
//   db: Option<DbPartialRawConfig>,
//   data: Option<DataPartialRawConfig>,
//   etwin: Option<EtwinPartialRawConfig>,
// }
//
// #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
// pub struct EternalfestPartialRawConfig {
//   pub http_port: Option<u16>,
//   pub external_uri: Option<Url>,
//   pub secret: Option<String>,
//   pub cookie_key: Option<String>,
// }
//
// #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
// pub struct DbPartialRawConfig {
//   pub host: Option<String>,
//   pub port: Option<u16>,
//   pub name: Option<String>,
//   pub admin_user: Option<String>,
//   pub admin_password: Option<String>,
//   pub user: Option<String>,
//   pub password: Option<String>,
// }
//
// #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
// pub struct DataPartialRawConfig {
//   /// URL to the root of the data directory
//   pub root: Option<String>,
// }
//
// #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
// pub struct EtwinPartialRawConfig {
//   pub uri: Option<Url>,
//   pub oauth_client_id: Option<String>,
//   pub oauth_client_secret: Option<String>,
// }
//
// macro_rules! resolve_group {
//   ($this: ident, $name: ident, $base: ident, $source: ident) => {
//     $this
//       .$name
//       .map(|value| value.resolve(&$base.$name, $source))
//       .unwrap_or_else(|| $base.$name.clone())
//   };
// }
//
// macro_rules! resolve_field {
//   ($this: ident, $name: ident, $base: ident, $source: ident) => {
//     $this
//       .$name
//       .map(|value| ConfigItem {
//         value,
//         source: $source.clone(),
//       })
//       .unwrap_or_else(|| $base.$name.clone())
//   };
// }
//
// impl PartialRawConfig {
//   pub fn resolve(self, base: &Config, source: &ConfigSource) -> Config {
//     Config {
//       eternalfest: resolve_group!(self, eternalfest, base, source),
//       db: resolve_group!(self, db, base, source),
//       data: resolve_group!(self, data, base, source),
//       etwin: resolve_group!(self, etwin, base, source),
//     }
//   }
// }
//
// impl EternalfestPartialRawConfig {
//   pub fn resolve(self, base: &EternalfestConfig, source: &ConfigSource) -> EternalfestConfig {
//     EternalfestConfig {
//       http_port: resolve_field!(self, http_port, base, source),
//       external_uri: resolve_field!(self, external_uri, base, source),
//       secret: resolve_field!(self, secret, base, source),
//       cookie_key: resolve_field!(self, cookie_key, base, source),
//     }
//   }
// }
//
// impl DbPartialRawConfig {
//   pub fn resolve(self, base: &DbConfig, source: &ConfigSource) -> DbConfig {
//     DbConfig {
//       host: resolve_field!(self, host, base, source),
//       port: resolve_field!(self, port, base, source),
//       name: resolve_field!(self, name, base, source),
//       admin_user: resolve_field!(self, admin_user, base, source),
//       admin_password: resolve_field!(self, admin_password, base, source),
//       user: resolve_field!(self, user, base, source),
//       password: resolve_field!(self, password, base, source),
//     }
//   }
// }
//
// impl DataPartialRawConfig {
//   pub fn resolve(self, base: &DataConfig, source: &ConfigSource) -> DataConfig {
//     DataConfig {
//       root: resolve_field!(self, root, base, source),
//     }
//   }
// }
//
// impl EtwinPartialRawConfig {
//   pub fn resolve(self, base: &EtwinConfig, source: &ConfigSource) -> EtwinConfig {
//     EtwinConfig {
//       uri: resolve_field!(self, uri, base, source),
//       oauth_client_id: resolve_field!(self, oauth_client_id, base, source),
//       oauth_client_secret: resolve_field!(self, oauth_client_secret, base, source),
//     }
//   }
// }
//
// #[derive(Debug)]
// pub enum FindConfigFileError {
//   NotFound(PathBuf),
//   Other(PathBuf, io::Error),
// }
//
// #[derive(Debug)]
// pub enum FindConfigError {
//   NotFound(PathBuf),
//   ParseError(toml::de::Error),
//   Other(PathBuf, io::Error),
// }
//
// fn find_config_file(dir: PathBuf) -> Result<(PathBuf, String), FindConfigFileError> {
//   for d in dir.ancestors() {
//     let config_path = d.join("etwin.toml");
//     match fs::read_to_string(&config_path) {
//       Ok(toml) => return Ok((config_path, toml)),
//       Err(e) if e.kind() == io::ErrorKind::NotFound => continue,
//       Err(e) => return Err(FindConfigFileError::Other(dir, e)),
//     }
//   }
//   Err(FindConfigFileError::NotFound(dir))
// }
//
// pub fn parse_config(_file: &Path, config_toml: &str) -> Result<PartialRawConfig, toml::de::Error> {
//   let raw: PartialRawConfig = toml::from_str(&config_toml)?;
//   Ok(raw)
// }
//
// pub fn find_config(dir: PathBuf) -> Result<Config, FindConfigError> {
//   match find_config_file(dir) {
//     Ok((file, config_toml)) => match parse_config(&file, &config_toml) {
//       Ok(config) => Ok(config.resolve(&*DEFAULT, &ConfigSource::File(file))),
//       Err(e) => Err(FindConfigError::ParseError(e)),
//     },
//     Err(FindConfigFileError::NotFound(dir)) => Err(FindConfigError::NotFound(dir)),
//     Err(FindConfigFileError::Other(dir, cause)) => Err(FindConfigError::Other(dir, cause)),
//   }
// }
//
// pub static DEFAULT: Lazy<Config> = Lazy::new(|| Config::default());
//
// #[cfg(test)]
// mod test {
//   use crate::{parse_config, ConfigSource, DEFAULT};
//
//   #[test]
//   fn test_default_config() {
//     const INPUT: &str = "";
//     let path = std::env::current_dir().unwrap().join("etwin.toml");
//     let actual = parse_config(&path, INPUT).unwrap();
//     let actual = actual.resolve(&*DEFAULT, &ConfigSource::Default);
//     let expected = DEFAULT.clone();
//     assert_eq!(actual, expected);
//   }
// }
