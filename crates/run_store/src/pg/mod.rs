use async_trait::async_trait;
use eternalfest_core::clock::Clock;
use eternalfest_core::core::{Instant, SimpleSemVer};
use eternalfest_core::game::{FamiliesString, GameChannelKey, GameId, GameIdRef, GameModeKey, GameOptionKey};
use eternalfest_core::pg_num::PgU32;
use eternalfest_core::run::{
  GetUserItems, GetUserItemsError, HammerfestItemId, LeaderboardEntryRun, RunId, RunIdRef, RunResult, RunSettings,
  RunStart, RunStore, StoreCreateRun, StoreCreateRunError, StoreGetLeaderboard, StoreGetLeaderboardError, StoreGetRun,
  StoreGetRunError, StoreLeaderboard, StoreLeaderboardEntry, StoreRun, StoreSetRunResult, StoreSetRunResultError,
  StoreStartRun, StoreStartRunError,
};
use eternalfest_core::types::{AnyError, ApiRef};
use eternalfest_core::user::{UserId, UserIdRef};
use eternalfest_core::uuid::UuidGenerator;
use eternalfest_core::LocaleId;
use serde_json::Value as JsonValue;
use sqlx::{PgPool, Postgres, Transaction};
use std::collections::BTreeMap;
use std::str::FromStr;
use uuid::Uuid;

pub struct PgRunStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: Clock,
  TyDatabase: ApiRef<PgPool>,
  TyUuidGenerator: UuidGenerator,
{
  clock: TyClock,
  database: TyDatabase,
  uuid_generator: TyUuidGenerator,
}

impl<TyClock, TyDatabase, TyUuidGenerator> PgRunStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: Clock,
  TyDatabase: ApiRef<PgPool>,
  TyUuidGenerator: UuidGenerator,
{
  pub async fn new(clock: TyClock, database: TyDatabase, uuid_generator: TyUuidGenerator) -> Self {
    Self {
      clock,
      database,
      uuid_generator,
    }
  }
}
#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> RunStore for PgRunStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: Clock,
  TyDatabase: ApiRef<PgPool>,
  TyUuidGenerator: UuidGenerator,
{
  async fn get_user_items(&self, req: &GetUserItems) -> Result<BTreeMap<HammerfestItemId, u32>, GetUserItemsError> {
    let mut tx = self.database.as_ref().begin().await.map_err(GetUserItemsError::other)?;
    let items = get_user_items(&mut tx, req.user, req.game, &req.channel)
      .await
      .map_err(GetUserItemsError::Other)?;
    tx.commit().await.map_err(GetUserItemsError::other)?;
    Ok(items)
  }

  async fn create_run(&self, req: &StoreCreateRun) -> Result<StoreRun, StoreCreateRunError> {
    let now = self.clock.now();
    let mut tx = self
      .database
      .as_ref()
      .begin()
      .await
      .map_err(StoreCreateRunError::other)?;
    let run = create_run(&mut tx, &self.uuid_generator, now, req.clone()).await?;
    tx.commit().await.map_err(StoreCreateRunError::other)?;
    Ok(run)
  }

  async fn get_run(&self, req: &StoreGetRun) -> Result<StoreRun, StoreGetRunError> {
    let mut tx = self.database.as_ref().begin().await.map_err(StoreGetRunError::other)?;
    let run = get_run(&mut tx, req.run).await?;
    tx.commit().await.map_err(StoreGetRunError::other)?;
    Ok(run)
  }

  async fn start_run(&self, req: &StoreStartRun) -> Result<RunStart, StoreStartRunError> {
    let now = self.clock.now();
    let mut tx = self
      .database
      .as_ref()
      .begin()
      .await
      .map_err(StoreStartRunError::other)?;
    let run = start_run(&mut tx, now, req.clone()).await?;
    tx.commit().await.map_err(StoreStartRunError::other)?;
    Ok(run)
  }

  async fn set_run_result(&self, req: &StoreSetRunResult) -> Result<StoreRun, StoreSetRunResultError> {
    let now = self.clock.now();
    let mut tx = self
      .database
      .as_ref()
      .begin()
      .await
      .map_err(StoreSetRunResultError::other)?;
    set_run_result(&mut tx, now, req.clone()).await?;
    let run = get_run(&mut tx, req.run).await.map_err(StoreSetRunResultError::other)?;
    tx.commit().await.map_err(StoreSetRunResultError::other)?;
    Ok(run)
  }

  async fn get_leaderboard(&self, req: &StoreGetLeaderboard) -> Result<StoreLeaderboard, StoreGetLeaderboardError> {
    let mut tx = self
      .database
      .as_ref()
      .begin()
      .await
      .map_err(StoreGetLeaderboardError::other)?;
    let leaderboard = get_leaderboard(&mut tx, req.clone()).await?;
    tx.commit().await.map_err(StoreGetLeaderboardError::other)?;
    Ok(leaderboard)
  }
}

#[cfg(feature = "neon")]
impl<TyClock, TyDatabase, TyUuidGenerator> neon::prelude::Finalize for PgRunStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: Clock,
  TyDatabase: ApiRef<PgPool>,
  TyUuidGenerator: UuidGenerator,
{
}

async fn get_user_items(
  tx: &mut Transaction<'_, Postgres>,
  user: UserIdRef,
  game: GameIdRef,
  channel: &GameChannelKey,
) -> Result<BTreeMap<HammerfestItemId, u32>, AnyError> {
  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    item_id: String,
    item_count: i32,
  }

  // language=PostgreSQL
  let rows: Vec<Row> = sqlx::query_as::<_, Row>(
    r"
    SELECT item_id, SUM(run_items.item_count)::INT AS item_count
    FROM (
      SELECT
      run_id, item.key::TEXT AS item_id, item.value::TEXT::INT AS item_count
      FROM (
        SELECT run_id, items
        FROM run_results
        INNER JOIN runs USING (run_id)
        WHERE user_id = $1::USER_ID AND game_id = $2::GAME_ID AND game_channel_key = $3::GAME_CHANNEL_KEY
      ) AS run_infos,
      LATERAL json_each(run_infos.items) AS item
    ) AS run_items
    GROUP BY item_id
    ORDER BY item_id;
  ",
  )
  .bind(user.id)
  .bind(game.id)
  .bind(channel)
  .fetch_all(tx)
  .await?;

  let mut items: BTreeMap<HammerfestItemId, u32> = BTreeMap::new();
  for row in rows {
    let item_id = HammerfestItemId::from_str(row.item_id.as_str())?;
    let old = items.insert(item_id, u32::try_from(row.item_count).unwrap_or(0));
    debug_assert!(old.is_none());
  }
  Ok(items)
}

async fn create_run(
  tx: &mut Transaction<'_, Postgres>,
  uuid_generator: &impl UuidGenerator,
  now: Instant,
  command: StoreCreateRun,
) -> Result<StoreRun, AnyError> {
  let run_id = RunId::from_uuid(uuid_generator.next());

  let options = serde_json::to_string(&command.options).expect("option serialize should succeed");

  // language=PostgreSQL
  let res = sqlx::query(
    r"
    INSERT INTO runs(
      run_id, game_id, user_id,
      created_at, started_at,
      game_mode, game_options,
      detail, music, shake,
      sound, volume, locale,
      game_channel_key, version_major, version_minor, version_patch
    )
    VALUES (
      $1::RUN_ID, $2::GAME_ID, $3::USER_ID,
      $4::INSTANT, NULL,
      $5::GAME_MODE_KEY, $6::JSON,
      $7::BOOLEAN, $8::BOOLEAN, $9::BOOLEAN,
      $10::BOOLEAN, $11::INT4, $12::LOCALE_ID,
      $13::GAME_CHANNEL_KEY, $14::U32, $15::U32, $16::U32);
  ",
  )
  .bind(run_id)
  .bind(command.game.id)
  .bind(command.user.id)
  .bind(now)
  .bind(&command.mode)
  .bind(options)
  .bind(command.settings.detail)
  .bind(command.settings.music)
  .bind(command.settings.shake)
  .bind(command.settings.sound)
  .bind(command.settings.volume)
  .bind(command.settings.locale)
  .bind(&command.channel)
  .bind(PgU32::from(command.version.major))
  .bind(PgU32::from(command.version.minor))
  .bind(PgU32::from(command.version.patch))
  .execute(tx)
  .await?;

  debug_assert_eq!(res.rows_affected(), 1);

  Ok(StoreRun {
    id: run_id,
    created_at: now,
    started_at: None,
    result: None,
    game: command.game,
    channel: command.channel,
    version: command.version,
    user: command.user,
    mode: command.mode,
    options: command.options,
    settings: command.settings,
  })
}

async fn get_run(tx: &mut Transaction<'_, Postgres>, run: RunIdRef) -> Result<StoreRun, StoreGetRunError> {
  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    run_id: RunId,
    game_id: GameId,
    user_id: UserId,
    created_at: Instant,
    started_at: Option<Instant>,
    game_mode: GameModeKey,
    game_options: JsonValue,
    detail: bool,
    music: bool,
    shake: bool,
    sound: bool,
    volume: i32,
    locale: LocaleId,
    game_channel_key: GameChannelKey,
    version_major: PgU32,
    version_minor: PgU32,
    version_patch: PgU32,
    run_result_created_at: Option<Instant>,
    is_victory: Option<bool>,
    max_level: Option<i32>,
    scores: Option<JsonValue>,
    items: Option<JsonValue>,
    stats: Option<JsonValue>,
  }

  // language=PostgreSQL
  let row: Option<Row> = sqlx::query_as::<_, Row>(
    r"
    SELECT
      run_id, game_id, user_id,
      runs.created_at, started_at,
      game_mode, game_options,
      detail, music, shake,
      sound, volume, locale,
      game_channel_key, version_major, version_minor, version_patch,
      run_results.created_at AS run_result_created_at,
      is_victory, max_level, scores, items, stats
    FROM runs
      LEFT OUTER JOIN run_results USING (run_id)
    WHERE run_id = $1::RUN_ID;
  ",
  )
  .bind(run.id)
  .fetch_optional(tx)
  .await
  .map_err(StoreGetRunError::other)?;

  let row = row.ok_or(StoreGetRunError::NotFound)?;

  let options: Vec<GameOptionKey> = serde_json::from_value(row.game_options).map_err(StoreGetRunError::other)?;

  Ok(StoreRun {
    id: row.run_id,
    created_at: row.created_at,
    started_at: row.started_at,
    result: match (
      row.run_result_created_at,
      row.is_victory,
      row.max_level,
      row.scores,
      row.items,
      row.stats,
    ) {
      (Some(created_at), Some(is_victory), Some(max_level), Some(scores), Some(items), Some(stats)) => {
        Some(RunResult {
          created_at,
          is_victory,
          max_level: u32::try_from(max_level).unwrap_or(0),
          scores: serde_json::from_value(scores).map_err(StoreGetRunError::other)?,
          items: serde_json::from_value(items).map_err(StoreGetRunError::other)?,
          stats: serde_json::from_value(stats).map_err(StoreGetRunError::other)?,
        })
      }
      (None, None, None, None, None, None) => None,
      _ => return Err(StoreGetRunError::Other("corrupted_run_result".into())),
    },
    game: GameIdRef::new(row.game_id),
    channel: row.game_channel_key,
    version: SimpleSemVer {
      major: u32::from(row.version_major),
      minor: u32::from(row.version_minor),
      patch: u32::from(row.version_patch),
    },
    user: UserIdRef::new(row.user_id),
    mode: row.game_mode,
    options,
    settings: RunSettings {
      detail: row.detail,
      shake: row.shake,
      sound: row.sound,
      music: row.music,
      volume: row.volume,
      locale: row.locale,
    },
  })
}

async fn start_run(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  req: StoreStartRun,
) -> Result<RunStart, StoreStartRunError> {
  // language=PostgreSQL
  let res = sqlx::query(
    r"
    UPDATE runs
    SET started_at = $2::INSTANT
    WHERE run_id = $1::RUN_ID AND started_at IS NULL;
  ",
  )
  .bind(req.run.id)
  .bind(now)
  .execute(&mut *tx)
  .await
  .map_err(StoreStartRunError::other)?;

  debug_assert!(res.rows_affected() <= 1);

  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    user_id: UserId,
    game_id: GameId,
    game_channel_key: GameChannelKey,
    families: FamiliesString,
  }
  // language=PostgreSQL
  let row: Option<Row> = sqlx::query_as::<_, Row>(
    r"
    SELECT
      user_id, game_id, game_channel_key, families
    FROM runs
      INNER JOIN game_version USING (game_id, version_major, version_minor, version_patch)
      INNER JOIN game_build USING (game_build_id)
    WHERE run_id = $1::RUN_ID;
  ",
  )
  .bind(req.run.id)
  .fetch_optional(&mut *tx)
  .await
  .map_err(StoreStartRunError::other)?;
  let row = row.ok_or(StoreStartRunError::NotFound)?;

  let user = UserIdRef::new(row.user_id);
  if user != req.if_user {
    return Err(StoreStartRunError::InvalidUser {
      created_by: user,
      started_by: req.if_user,
    });
  }

  let items = get_user_items(tx, user, GameIdRef::new(row.game_id), &row.game_channel_key).await?;

  Ok(RunStart {
    run: req.run,
    key: Uuid::from_str("00000000-0000-0000-0000-000000000000").unwrap(),
    families: row.families,
    items,
  })
}

async fn set_run_result(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  req: StoreSetRunResult,
) -> Result<(), StoreSetRunResultError> {
  let scores = serde_json::to_string(&req.scores).map_err(StoreSetRunResultError::other)?;
  let items = serde_json::to_string(&req.items).map_err(StoreSetRunResultError::other)?;
  let stats = serde_json::to_string(&req.stats).map_err(StoreSetRunResultError::other)?;
  // language=PostgreSQL
  let res = sqlx::query(
    r"
    INSERT INTO run_results (
      run_id, created_at, is_victory, max_level, scores, items, stats
    )
    VALUES (
      $1::RUN_ID, $2::INSTANT, $3::BOOLEAN, $4::INT4, $5::JSON, $6::JSON, $7::JSON
    );
  ",
  )
  .bind(req.run.id)
  .bind(now)
  .bind(req.is_victory)
  .bind(i32::try_from(req.max_level).unwrap_or(i32::MAX))
  .bind(scores)
  .bind(items)
  .bind(stats)
  .execute(tx)
  .await
  .map_err(StoreSetRunResultError::other)?;

  debug_assert_eq!(res.rows_affected(), 1);

  Ok(())
}

async fn get_leaderboard(
  tx: &mut Transaction<'_, Postgres>,
  req: StoreGetLeaderboard,
) -> Result<StoreLeaderboard, StoreGetLeaderboardError> {
  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    user_id: UserId,
    run_id: RunId,
    best_score: i64,
    max_level: i32,
    game_options: JsonValue,
  }
  // language=PostgreSQL
  let rows: Vec<Row> = sqlx::query_as::<_, Row>(
    r"
    WITH
      runs_with_score AS (
        SELECT
          (SELECT SUM(score) FROM (SELECT json_array_elements(scores)::TEXT::INT as score) AS items) as total_score,
          run_id, user_id, run_results.created_at
        FROM run_results
          INNER JOIN runs USING (run_id)
        WHERE game_id = $1::GAME_ID
          AND game_channel_key = $2::GAME_CHANNEL_KEY
          AND game_mode = $3::GAME_MODE_KEY
      ),
      best_runs AS (
        SELECT user_id,
          first_value(run_id) over w AS run_id,
          first_value(total_score) over w AS total_score,
          first_value(created_at) over w AS created_at,
          row_number() over w AS rn
        FROM runs_with_score
        WINDOW w AS (PARTITION BY user_id ORDER BY total_score DESC, created_at ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
      )
      SELECT users.user_id, runs.run_id, total_score AS best_score, max_level, game_options
      FROM best_runs
      INNER JOIN users USING (user_id)
      INNER JOIN run_results USING (run_id)
      INNER JOIN runs USING (run_id)
      WHERE best_runs.rn = 1
      ORDER BY best_runs.total_score DESC, best_runs.created_at ASC, best_runs.user_id;
  ",
  )
  .bind(req.game.id)
  .bind(&req.channel)
  .bind(&req.mode)
  .fetch_all(&mut *tx)
  .await
  .map_err(StoreGetLeaderboardError::other)?;

  Ok(StoreLeaderboard {
    game: req.game,
    channel: req.channel,
    mode: req.mode,
    results: rows
      .into_iter()
      .map(|row| -> Result<StoreLeaderboardEntry, StoreGetLeaderboardError> {
        Ok(StoreLeaderboardEntry {
          score: i32::try_from(row.best_score).unwrap_or(i32::MAX),
          user: UserIdRef::from(row.user_id),
          run: LeaderboardEntryRun {
            id: row.run_id,
            max_level: u32::try_from(row.max_level).unwrap_or(0),
            game_options: serde_json::from_value(row.game_options).map_err(StoreGetLeaderboardError::other)?,
          },
        })
      })
      .collect::<Result<Vec<_>, _>>()?,
  })
}
