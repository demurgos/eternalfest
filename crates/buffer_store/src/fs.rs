use async_trait::async_trait;
use eternalfest_core::buffer::{
  BufferId, BufferStore, CreateBufferError, DeleteBufferError, ReadStreamError, WriteBytesError,
};
use eternalfest_core::types::AnyError;
use eternalfest_core::uuid::UuidGenerator;
use std::convert::TryFrom;
use std::io::ErrorKind;
use std::path::PathBuf;
use tokio::fs;
use tokio::fs::OpenOptions;
use tokio::io::{AsyncSeekExt, AsyncWriteExt, SeekFrom};

pub struct FsBufferStore<TyUuidGenerator: UuidGenerator> {
  pub(crate) uuid_generator: TyUuidGenerator,
  pub(crate) root: PathBuf,
  pub(crate) tmp_dir: PathBuf,
}

impl<TyUuidGenerator> FsBufferStore<TyUuidGenerator>
where
  TyUuidGenerator: UuidGenerator,
{
  pub async fn new(uuid_generator: TyUuidGenerator, root: PathBuf) -> Self {
    let tmp_dir = root.join(".tmp");
    fs::create_dir_all(root.as_path())
      .await
      .unwrap_or_else(|_| panic!("failed to create FsBufferStore root: {}", root.display()));
    fs::create_dir_all(tmp_dir.as_path())
      .await
      .unwrap_or_else(|_| panic!("failed to create FsBufferStore tmp dir: {}", root.display()));
    Self {
      uuid_generator,
      root,
      tmp_dir,
    }
  }

  fn get_alloc_path(&self, id: BufferId) -> PathBuf {
    self.tmp_dir.join(id.to_hex().as_str())
  }

  fn get_writable_path(&self, id: BufferId) -> PathBuf {
    let id = id.to_hex();
    let mut path = self.root.clone();
    path.push(&id[0..2]);
    path.push(&id[2..4]);
    path.push(id.as_str()); // TODO: Prefix with dot
    path
  }

  fn get_readable_path(&self, id: BufferId) -> PathBuf {
    let id = id.to_hex();
    let mut path = self.root.clone();
    path.push(&id[0..2]);
    path.push(&id[2..4]);
    path.push(id.as_str());
    path
  }
}

#[async_trait]
impl<TyUuidGenerator> BufferStore for FsBufferStore<TyUuidGenerator>
where
  TyUuidGenerator: UuidGenerator,
{
  async fn create_buffer(&self, size: u32) -> Result<BufferId, CreateBufferError> {
    let id = BufferId::from_uuid(self.uuid_generator.next());

    let alloc_file_path = self.get_alloc_path(id);
    let mut alloc_file = OpenOptions::new()
      .create_new(true)
      .append(true)
      .open(alloc_file_path.as_path())
      .await
      .map_err(CreateBufferError::other)?;
    alloc_file
      .set_len(size.into())
      .await
      .map_err(CreateBufferError::other)?;
    alloc_file.flush().await.map_err(CreateBufferError::other)?;
    alloc_file.sync_all().await.map_err(CreateBufferError::other)?;

    let writable_file_path = self.get_writable_path(id);

    fs::create_dir_all(writable_file_path.parent().unwrap())
      .await
      .map_err(CreateBufferError::other)?;
    fs::hard_link(alloc_file_path.as_path(), writable_file_path.as_path())
      .await
      .map_err(CreateBufferError::other)?;
    fs::remove_file(alloc_file_path.as_path())
      .await
      .map_err(CreateBufferError::other)?;

    Ok(id)
  }

  async fn delete_buffer(&self, buffer_id: BufferId) -> Result<(), DeleteBufferError> {
    // The order (readable then writable) is important to prevent race conditions with `upload`
    let writable_file = self.get_writable_path(buffer_id);
    let found = match fs::remove_file(writable_file).await {
      Err(e) if e.kind() != ErrorKind::NotFound => return Err(DeleteBufferError::Other(e.into())),
      r => r.is_ok(),
    };
    let readable_file = self.get_readable_path(buffer_id);
    let found = match fs::remove_file(readable_file).await {
      Err(e) if e.kind() != ErrorKind::NotFound => return Err(DeleteBufferError::Other(e.into())),
      r => found || r.is_ok(),
    };
    if found {
      Ok(())
    } else {
      Err(DeleteBufferError::NotFound(buffer_id))
    }
  }

  async fn delete_buffer_if_exists(&self, buffer_id: BufferId) -> Result<(), AnyError> {
    let res = self.delete_buffer(buffer_id).await;
    match res {
      Ok(()) => Ok(()),
      Err(DeleteBufferError::NotFound(_)) => Ok(()),
      Err(DeleteBufferError::Other(e)) => Err(e),
    }
  }

  async fn write_bytes(&self, buffer_id: BufferId, offset: u32, bytes: &[u8]) -> Result<(), WriteBytesError> {
    // TODO: Hashmap from buffer id to file locks? What about multiple server processes?
    let writable_file_path = self.get_writable_path(buffer_id);
    let mut writable_file = OpenOptions::new()
      .write(true)
      .open(writable_file_path.as_path())
      .await
      .map_err(|e| match e.kind() {
        ErrorKind::NotFound => WriteBytesError::NotFound(buffer_id),
        _ => WriteBytesError::Other(e.into()),
      })?;
    let buf_len = writable_file.metadata().await.map_err(WriteBytesError::other)?.len();
    let input_len = u32::try_from(bytes.len()).map_err(|_| WriteBytesError::MaxSize)?;
    let end_offset = offset.checked_add(input_len).ok_or(WriteBytesError::BufferOverflow)?;
    if u64::from(end_offset) > buf_len {
      return Err(WriteBytesError::BufferOverflow);
    }
    writable_file
      .seek(SeekFrom::Start(offset.into()))
      .await
      .map_err(WriteBytesError::other)?;
    writable_file.write_all(bytes).await.map_err(WriteBytesError::other)?;
    writable_file.flush().await.map_err(WriteBytesError::other)?;
    writable_file.sync_all().await.map_err(WriteBytesError::other)?;
    Ok(())
  }

  async fn read_stream(&self, buffer_id: BufferId) -> Result<Vec<u8>, ReadStreamError> {
    let readable_file_path = self.get_readable_path(buffer_id);
    match fs::read(readable_file_path.as_path()).await {
      Ok(buf) => Ok(buf),
      Err(e) if e.kind() == ErrorKind::NotFound => return Err(ReadStreamError::NotFound(buffer_id)),
      Err(e) => return Err(ReadStreamError::Other(e.into())),
    }
  }
}

#[cfg(feature = "neon")]
impl<TyUuidGenerator> neon::prelude::Finalize for FsBufferStore<TyUuidGenerator> where TyUuidGenerator: UuidGenerator {}

#[cfg(test)]
mod test {
  use super::FsBufferStore;
  use crate::test::TestApi;
  use eternalfest_core::buffer::BufferStore;
  use eternalfest_core::uuid::Uuid4Generator;
  use serial_test::serial;
  use std::sync::Arc;

  async fn make_test_api() -> TestApi<Arc<dyn BufferStore>> {
    let config = eternalfest_config::find_config(std::env::current_dir().unwrap()).unwrap();
    let uuid_generator = Arc::new(Uuid4Generator);
    let data_root = config.data.root.to_file_path().expect("InvalidDataRoot");
    let buffer_store: Arc<dyn BufferStore> = Arc::new(FsBufferStore::new(uuid_generator, data_root).await);

    TestApi { buffer_store }
  }

  test_buffer_store!(
    #[serial]
    || make_test_api().await
  );
}
