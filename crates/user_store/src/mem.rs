use async_trait::async_trait;
use eternalfest_core::clock::Clock;
use eternalfest_core::core::Instant;
use eternalfest_core::types::AnyError;
use eternalfest_core::user::{
  GetUserOptions, GetUsersOptions, ShortUser, UpdateUserOptions, User, UserDisplayName, UserId, UserListing, UserStore,
};
use std::collections::HashMap;
use std::sync::RwLock;

struct StoreState {
  users: HashMap<UserId, RawUser>,
}

#[derive(Debug)]
struct RawUser {
  id: UserId,
  display_name: UserDisplayName,
  created_at: Instant,
  updated_at: Instant,
  is_administrator: bool,
  is_tester: bool,
}

impl StoreState {
  fn new() -> Self {
    Self { users: HashMap::new() }
  }

  pub(crate) fn upsert_from_etwin(&mut self, now: Instant, user: &ShortUser) -> Result<User, AnyError> {
    let is_administrator = self.users.is_empty();
    let user = self.users.entry(user.id).or_insert_with(|| RawUser {
      id: user.id,
      display_name: user.display_name.clone(),
      created_at: now,
      updated_at: now,
      is_administrator,
      is_tester: is_administrator,
    });

    Ok(User {
      id: user.id,
      display_name: user.display_name.clone(),
      created_at: user.created_at,
      updated_at: user.updated_at,
      identities: Vec::new(),
      is_administrator: user.is_administrator,
      is_tester: user.is_tester,
    })
  }
}

pub struct MemUserStore<TyClock>
where
  TyClock: Clock,
{
  clock: TyClock,
  state: RwLock<StoreState>,
}

impl<TyClock> MemUserStore<TyClock>
where
  TyClock: Clock,
{
  pub fn new(clock: TyClock) -> Self {
    Self {
      clock,
      state: RwLock::new(StoreState::new()),
    }
  }
}

#[async_trait]
impl<TyClock> UserStore for MemUserStore<TyClock>
where
  TyClock: Clock,
{
  async fn upsert_from_etwin(&self, user: &ShortUser) -> Result<User, AnyError> {
    let now = self.clock.now();
    let mut state = self.state.write().unwrap();
    state.upsert_from_etwin(now, user)
  }

  async fn get_user(&self, _options: &GetUserOptions) -> Result<User, AnyError> {
    todo!()
  }

  async fn get_short_user(&self, _options: &GetUserOptions) -> Result<ShortUser, AnyError> {
    todo!()
  }

  async fn get_short_users(&self, _options: &GetUsersOptions) -> Result<HashMap<UserId, ShortUser>, AnyError> {
    todo!()
  }

  async fn get_users(&self) -> Result<UserListing, AnyError> {
    todo!()
  }

  async fn update_user(&self, _options: &UpdateUserOptions) -> Result<User, AnyError> {
    todo!()
  }
}

#[cfg(feature = "neon")]
impl<TyClock> neon::prelude::Finalize for MemUserStore<TyClock> where TyClock: Clock {}
