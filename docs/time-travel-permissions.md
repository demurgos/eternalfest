# Time Travel and Permissions

The API exposes full time travel: you can query the system at any point in time.
How to deal with permissions and permission revocation?


Coucou, je me suis remis sur les permissions
En ce moment je réfléchis à un problème : comment concilier révocation de permissions et voyage dans le temps
Un truc que j'ajoute des les APIs Rust, c'est le contrôle du temps. Tu peux passer ?time=... pour voir les données à instant t (voyage dans le temps)
C'est pratique pour deux raisons.
Premièrement ça permet d'avoir un peu le fonctionnement de webarchive / voir la progression des jeux / joueurs au fil du temps.
Mais aussi permet d'avoir une pagination sans race condition: tu peux traverser la liste des jeux sans subir les réordonnements causés par les modifs qui peuvent survenir en parallèle
Sky
—
Today at 3:46 PM
et quel est le problème avec les révocations de permissions ?
Demurgos
—
Today at 3:46 PM
Sauf que du coup ça rend la question "quels jeux le joueurs a-t-il le droit de voir" un poil plus subtile
Sky
—
Today at 3:46 PM
j'ai envie de dire pas de voyage dans le temps pour ça
Demurgos
—
Today at 3:47 PM
Les permissions ont pour but de permettre de controler un peu plus finement les autorisations. Par exemple assiette peut voir "épopée spatiale" avant qu'elle soit publique pour beta tester
Sky
—
Today at 3:47 PM
exemple : sans faire expres j'ai donné à un instant t les droits à nassimou de voir mon futur projet
je l'enleve à l'instant t+1
je veux pas qu'il puisse voir meme l'état passé
Demurgos
—
Today at 3:47 PM
Mais si maxdefolsch ajoute assiette au temps x puis le vire au temps y. Si assiette veut lister les jeux au temps x, voit-il l'épopée spatiale ?
Sky
—
Today at 3:48 PM
s'il a décidé de le virer de l'épopée j'aurai envie de dire que prob non
mais ça dépend vraiment du contexte
Demurgos
—
Today at 3:48 PM
Oui, c'est un peu ma question Sky
Sky
—
Today at 3:48 PM
je pense pas qu'il existe de réponse strictement toujours juste
Demurgos
—
Today at 3:48 PM
Mais du coup, je pense qu'une solution OK pour le début c'est que faut avoir la permissions au temps x et dans le présent
Sky
—
Today at 3:49 PM
mais à défaut d'implémenter toute une gestion compliquée dont je pense sera pratiquement jamais utile en pratique, +1 à ce que tu dis
cad il faut avoir les droits à l'instant t et dans le passé
au strict minimum à l'instant t
Demurgos
—
Today at 3:49 PM
Oui
Y'a deux effets à ce fonctionnement
Le premier c'est que c'est pas rétroactif
Sky
—
Today at 3:50 PM
yup
Demurgos
—
Today at 3:50 PM
Donc si on invite Blessed au groupe Eternalfest au temps t, il peut pas voir les games en beta que y'avait dans le passé (si ils ont été supprimés depuis)
Sky
—
Today at 3:51 PM
mais sur le coup ça me parait "pas bien grave"
Demurgos
—
Today at 3:51 PM
C'est "normal" parce qu'il avait pas la permission à cette époque
Oui, je trouve ça OK aussi, c'est équivalent au fonctionnement actuel
(Où les permissions ne fonctionne que à partir du moment donné vu que tu peux pas revenir dans le passé)
L'équivalent Discord c'est si on invite qqun, est-ce qu'il peut lire les messages avant son arrivée dans le serveur ou pas
Le deuxième truc c'est que si lister les jeux dépend des permissions présentes, ça veut dire que tu redeviens sensible aux shifts dans la liste si t'itères sur les pages dessus (c'est plus exactement une snapshot).
Sky
—
Today at 3:54 PM
ça me parait pas grave non plus
moulins
—
Today at 3:54 PM
+1 pour le comportement qui demande les perms maintenant et dans le passé
Sky
—
Today at 3:54 PM
les game vont pas pop toutes les 5s
Demurgos
—
Today at 3:54 PM
Si le seul problème c'est d'éviter que les trucs bougent, je pense qu'une solution simple serait de juste mettre des "tombstone"
moulins
—
Today at 3:55 PM
et ouais, les shift dans les pages ça me paraît pas trop grave dans l'absolu
et +1 pour la tombstone, genre ça décompte pour la pagination mais ça apparaît pas sur la page visible
Demurgos
—
Today at 3:55 PM
Oui, pour les games c'est pas important ; ça bouge pas trop. Mais je préfère avoir un framework unique qui pourrait aussi marcher pour des trucs comme comme les runs ou pour Eternaltwin
moulins
—
Today at 3:56 PM
(ça veut dire que t'as certaines pages avec moins de résultats mais osef)
Demurgos
—
Today at 3:56 PM
ça serait un peu comme sur Google
"certains résultats ne peuvent pas être affichés pour des raisons légales"
Enfin là ce serait juste "les permissions ont changés depuis et certains jeux ne vous sont plus accessibles"
moulins
—
Today at 3:57 PM
donc genre l'API renverrai

{
results: [...],
page: 42,
total_pages: 69,
masked_results: 5,
}

Demurgos
—
Today at 3:57 PM
(en vrai je pense que je mettrais même pas de message dans le front :calim~2:)
moulins
—
Today at 3:58 PM
ça dépend je dirais
genre est-ce que t'affiche résultats 50 à 75 pour chaque page?
si oui il faut indiquer que y'a des résultats qui sont plus visible pour être cohérent
Demurgos
—
Today at 3:59 PM
Pour le moment je calque la pagination sur les forum eternaltwin
moulins
—
Today at 3:59 PM
genre résultats 50 à 75 (dont 5 masqués)
Demurgos
—
Today at 3:59 PM
Donc l'API a cette forme :

#[cfg_attr(feature = "_serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Listing<T> {
pub offset: u32,
pub limit: u32,
/// Count with the current permissions
pub count: u32,
/// If `true`, the count is guaranteed to be exact (there are no restrictions
/// on viewing all the items). If `false`, the count may or may not be complete.
pub is_full_count: bool,
pub items: Vec<T>,
}


Mais l'interface affiche juste "page 1", "page 2", etc.
moulins
—
Today at 3:59 PM
'k
dans quels cas tu peux avoir is_full_count: false?
Demurgos
—
Today at 4:00 PM
(par contre si les message ont été supprimés, ils sont affichés sur la page comme message supprimé (y'a pas de permissions, mais y'a des suppressions)
moulins
—
Today at 4:00 PM
dès que t'as pas les perms pour tout voir en théorie?
Demurgos
—
Today at 4:00 PM
Je pense à une situation comme "lister la liste des versions de l'épopée spatiale"
Assiette et les gens d'Eternalfest ont true, tous les autres ont false
C'est pour savoir si count est un nombre exact ou une borne inférieure
moulins
—
Today at 4:01 PM
okay
mais donc, avec les tombstones
Demurgos
—
Today at 4:02 PM
Les tombstones sont dans count
moulins
—
Today at 4:02 PM
si t'as count < limit, comment tu sais si t'es à la fin de la liste, ou si c'est parce que y'a des tombstones?
ah, ok
donc on a items.len() <= count <= limit
Demurgos
—
Today at 4:04 PM
offset + limit >= count c'est que t'es au bout
moulins
—
Today at 4:04 PM
et t'es à la fin de la liste ssi count < limit
Demurgos
—
Today at 4:04 PM
offset/limit fonctionne comme en SQL
(et items c'est tes rows)
moulins
—
Today at 4:04 PM
yep ok
Demurgos
—
Today at 4:04 PM
count c'est COUNT(*), ça dépend pas de offset/limit
moulins
—
Today at 4:05 PM
ah ok
mais donc je maintiens, comment tu fais la différence entre les tombstones et la fin de la liste?
les tombstones sont dans items comme des nulls?
Demurgos
—
Today at 4:05 PM
Oups, j'ai corrigé
offset + limit >= count
moulins
—
Today at 4:06 PM
ok
Demurgos
—
Today at 4:06 PM
(c'est plus logique)
Oui, c'est le dernier truc à voir
moulins
—
Today at 4:06 PM
perso j'incluerai pas du tout les tombstones dans la liste des résultats
Demurgos
—
Today at 4:06 PM
Je me dis que le plus logique est de mettre les tombstone dans items directement
Parce que tu peux faire offset = x, limit = 1 et itérer pour trouver la position exacte de toute manière
(donc oui, en tant que null)
moulins
—
Today at 4:07 PM
vu que si tu veux savoir si y'a des tombstones tu peux juste comparer min(offset + limit, count) - offset et items.len()
ah, pas faux ce que tu dis
Demurgos
—
Today at 4:07 PM
Un dernier truc, faut une option pour juste ignorer les tombstones
C'est un truc utile pour les APIs si tu veux éviter que ça bouge
moulins
—
Today at 4:08 PM
c'est à dire "ignorer"?
Demurgos
—
Today at 4:08 PM
C'est à dire que par défaut y'a pas de tombstone et on laisse les choses bouger si y'a des modifs concurrentes
moulins
—
Today at 4:09 PM
c'est quoi l'utilité de laisser les choses bouger?
backwards comp?
Demurgos
—
Today at 4:09 PM
C'est plus pour le cas de l'utilisateur normal qui en à rien à faire d'avoir une snapshot consistante et préfère avoir le même nombre d'items par page
moulins
—
Today at 4:11 PM
mouais, perso je m'emmerderai pas avec ça
Demurgos
—
Today at 4:11 PM
Enfin, là c'est plus pour le cas du forum eternaltwin où ça sert à rien de voir une page avec 10 "message supprimé"
Oui, t'as raison
moulins
—
Today at 4:11 PM
parce que le nombre d'items qui change ça va se voir seulement si t'essayes de voir dans le passé?
Demurgos
—
Today at 4:11 PM
ça fait une option de moins
moulins
—
Today at 4:11 PM
donc c'est déjà qqchose d'assez rare
Demurgos
—
Today at 4:11 PM
Y'a deux raisons : suppression et perte de permission
Je sais pas encore si je veux traiter ça pareil
maxdefolsch
—
Today at 4:12 PM
Oui Demu essaye de minimiser le nombre de trucs à implémenter qui concrètement va être remarqué par personne stp :calnoel~1:
moulins
—
Today at 4:12 PM
ah aussi, ptet que tu devrais mettre limit: u8 dans l'API?
comme ça ça limite naturellement le nombre d'items qu'on peut demander en une fois
Demurgos
—
Today at 4:12 PM
Je crois que j'ai une limite de genre 100
moulins
—
Today at 4:13 PM
'k
Demurgos
—
Today at 4:13 PM
Enfin merci bien de la discussion @Sky et @moulins
Je vais du coup màj l'API, mais sans encore ajouter de nouvelles fonctionnalités ; juste que les types soient plus flexibles
Du coup ça va, l'algo est assez simple :
1. Récupérer les infos au temps t
2. Pour chaque item, vérifier si les permissions sont toujours valables ; si non -> remplacer par null
