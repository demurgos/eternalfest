# Eternalfest

Main Eternalfest repository.

This README is not up-to-date and needs some work...

Upgrade crates using [cargo-edit](https://github.com/killercup/cargo-edit):

```
cargo upgrade --workspace --exclude eternalfest_core
```

## Get Started

```sh
npm run start
```

- `yarn up '*' '@!(eternalfest)/*'`: Update all Typescript dependencies.

## Create a DB user

```sh
# Run as the Postgres user
createuser --encrypted --interactive --pwprompt
```

Example

```sh
postgres@host $ createuser --encrypted --interactive --pwprompt
Enter name of role to add: eternalfest
Shall the new role be a superuser? (y/n) n
Shall the new role be allowed to create databases? (y/n) y
Shall the new role be allowed to create more new roles? (y/n) n
```

## Create a DB

```sh
createdb --owner=dbuser dbname
psql dbname
ALTER SCHEMA public OWNER TO dbuser;
```

Example:

```sh
$ createdb --owner=eternalfest eternalfestdb
$ psql eternalfestdb
psql (9.6.10)
Type "help" for help.

eternalfestdb=# ALTER SCHEMA public OWNER TO eternalfest;
```
