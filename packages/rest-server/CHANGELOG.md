# 0.15.2 (2021-03-29)

- **[Fix]** Republished due to borked previous publish.

# 0.15.1 (2021-03-29)

- **[Fix]** Republished due to borked previous publish.

# 0.15.0 (2021-03-29)

- **[Breaking change]** Update to `api-core@0.15`.

# 0.14.0 (2020-06-01)

- **[Breaking change]** Update to `api-core@0.14`.

# 0.13.0 (2020-04-27)

- **[Breaking change]** Use native ESM.
- **[Breaking change]** Update to `api-core@0.13`.

# 0.12.3 (2020-02-05)

- **[Fix]** Update dependencies.
- **[Fix]** Mark `koa-mount` as a regular dependency.

# 0.12.2 (2020-01-30)

- **[Fix]** Remove unused dependencies.
- **[Fix]** Update dependencies.

# 0.12.1 (2019-12-31)

- **[Fix]** Update dependencies.

# 0.12.0 (2019-12-04)

- **[Breaking change]** Update to `api-core@0.12`.

# 0.11.1 (2019-10-23)

- **[Fix]** Update to `api-core@0.11.2`.

# 0.11.0 (2019-10-22)

- **[Breaking change]** Update to `api-core@0.11`.
- **[Fix]** Fix support for pre-releases in CI script.

# 0.10.0 (2019-08-22)

- **[Breaking change]** Update to `api-core@0.10`.

# 0.9.0 (2019-05-17)

- **[Breaking change]** Update to `api-core@0.9`.
- **[Fix]** Improve error messages for games and runs.
- **[Fix]** Update dependencies.

# 0.8.0 (2019-05-12)

- **[Breaking change]** Update to `api-core@0.8`.

# 0.5.1 (2019-03-31)

- **[Features]** Add CORS support for local development
- **[Features]** Add `GET /self/auth`.
- **[Fix]** Add credentials support for local development.
- **[Fix]** Update `api-backend`, fix icon updates

# 0.5.0 (2019-01-10)

- **[Breaking change]** Update to `api-core@0.7`.
- **[Feature]** Add `/upload-sessions` router.
- **[Internal]** Use a single CI stage.

# 0.4.0 (2019-01-03)

- **[Breaking change]** Update to `api-core@0.6`.

# 0.3.0 (2018-12-31)

- **[Breaking change]** Update to `api-core@0.5`, and support for run results.

# 0.2.3 (2018-12-24)

- **[Feature]** Add support for updating games.

# 0.2.2 (2018-12-24)

- **[Feature]** Add support for updating users.

# 0.2.1 (2018-12-22)

- **[Feature]** Drop runtime dependency on `api-backend`.

# 0.2.0 (2018-12-20)

- **[Breaking change]** Update to `api-core@0.4.1` and `api-backend@0.2.0`.

# 0.1.1 (2018-12-16)

- **[Feature]** Add `/runs` router.

# 0.1.0 (2018-12-16)

- **[Feature]** First release.
