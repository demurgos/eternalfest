import {Database} from "@eternal-twin/pg-db";
import {Config, getLocalConfig} from "@eternalfest/local-config";
import {PgAuthStore} from "@eternalfest/native/auth-store";
import {PgBlobStore} from "@eternalfest/native/blob-store";
import {FsBufferStore} from "@eternalfest/native/buffer-store";
import {SystemClock} from "@eternalfest/native/clock";
import {Database as NativeDatabase} from "@eternalfest/native/database";
import {HttpEtwinClient} from "@eternalfest/native/etwin-client";
import {PgFileStore} from "@eternalfest/native/file-store";
import {PgGameStore} from "@eternalfest/native/game-store";
import {PgRunStore} from "@eternalfest/native/run-store";
import {NativeAuthService} from "@eternalfest/native/services/auth";
import {NativeFileService} from "@eternalfest/native/services/file";
import {NativeGameService} from "@eternalfest/native/services/game";
import {NativeRunService} from "@eternalfest/native/services/run";
import {NativeUserService} from "@eternalfest/native/services/user";
import {PgUserStore} from "@eternalfest/native/user-store";
import {Uuid4Generator} from "@eternalfest/native/uuid";
import koaCors from "@koa/cors";
import Router from "@koa/router";
import Koa from "koa";
import koaLogger from "koa-logger";
import koaMount from "koa-mount";
import pg from "pg";

import {createApiRouter} from "../lib/create-api-router.js";
import {KoaAuth} from "../lib/koa-auth.js";

async function main(): Promise<void> {
  const config: Config = await getLocalConfig();

  const poolConfig: pg.PoolConfig = {
    user: config.db.user,
    password: config.db.password,
    host: config.db.host,
    port: config.db.port,
    database: config.db.name,
    max: 10,
    idleTimeoutMillis: 1000,
  };

  const pool: pg.Pool = new pg.Pool(poolConfig);
  const db: Database = new Database(pool);

  const nativeDb = await NativeDatabase.create({
    host: config.db.host,
    port: config.db.port,
    name: config.db.name,
    user: config.db.user,
    password: config.db.password,
  });

  const clock = new SystemClock();
  const uuidGenerator = new Uuid4Generator();
  const bufferStore = await FsBufferStore.create({uuidGenerator, root: config.data.root});
  const blobStore = await PgBlobStore.create({bufferStore, clock, database: nativeDb, uuidGenerator});
  const authStore = await PgAuthStore.create({clock, database: nativeDb, uuidGenerator});
  const userStore = await PgUserStore.create({clock, database: nativeDb});
  const fileStore = await PgFileStore.create({blobStore, clock, database: nativeDb, uuidGenerator});

  const etwinClient = await HttpEtwinClient.create({clock, root: config.etwin.uri.toString()});

  const user: NativeUserService = await NativeUserService.create({userStore});
  const file: NativeFileService = await NativeFileService.create({blobStore, fileStore, userStore});
  const gameStore = await PgGameStore.create({clock, database: nativeDb, uuidGenerator});
  const game2 = await NativeGameService.create({blobStore, clock, gameStore, userStore});
  const runStore = await PgRunStore.create({clock, database: nativeDb, uuidGenerator});
  const run = await NativeRunService.create({blobStore, clock, gameStore, runStore, userStore});
  const auth: NativeAuthService = await NativeAuthService.create({authStore, clock, etwinClient, userStore});
  const koaAuth: KoaAuth = new KoaAuth(auth);
  const apiRouter: Router = createApiRouter({
    auth,
    blob: blobStore,
    file,
    game,
    game2,
    koaAuth,
    run,
    leaderboard,
    user
  });

  const app: Koa = new Koa();
  const port: number = 50313;

  app.use(koaLogger());
  app.use(koaCors({origin: "http://localhost:4200", credentials: true}));
  app.use(koaMount("/api/v1", apiRouter.routes()));
  app.use(koaMount("/api/v1", apiRouter.allowedMethods()));

  app.listen(port, () => {
    console.log(`Listening on http://localhost:${port}`);
  });
}

main()
  .catch((err: Error): never => {
    console.error(err.stack);
    return process.exit(1) as never;
  });
