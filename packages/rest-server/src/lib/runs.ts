import { AuthContext } from "@eternalfest/api-core/auth/auth-context";
import { $CreateRunOptions, CreateRunOptions } from "@eternalfest/api-core/run/create-run-options";
import { $Run, Run } from "@eternalfest/api-core/run/run";
import { $RunStart, RunStart } from "@eternalfest/api-core/run/run-start";
import { RunService } from "@eternalfest/api-core/run/service";
import { $SetRunResultOptions, SetRunResultOptions } from "@eternalfest/api-core/run/set-run-result-options";
import Router from "@koa/router";
import Koa from "koa";
import koaBodyParser from "koa-bodyparser";
import koaCompose from "koa-compose";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";
import { JSON_VALUE_READER } from "kryo-json/json-value-reader";
import { JSON_VALUE_WRITER } from "kryo-json/json-value-writer";
import { QS_VALUE_READER } from "kryo-qs/qs-value-reader";

import { KoaAuth } from "./koa-auth.js";

export interface Api {
  koaAuth: KoaAuth;
  run: RunService;
}

export function createRunsRouter(api: Api): Router {
  const router: Router = new Router();

  router.post("/", koaCompose([koaBodyParser(), createRun]));

  async function createRun(cx: Koa.Context): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(cx);
    let options: CreateRunOptions;
    try {
      options = $CreateRunOptions.read(JSON_VALUE_READER, cx.request.body);
    } catch (err) {
      console.error(err);
      cx.response.status = 422;
      cx.response.body = {error: "InvalidRequest"};
      return;
    }
    const run: Run = await api.run.createRun(auth, options);
    cx.response.body = $Run.write(JSON_VALUE_WRITER, run);
  }

  router.get("/:run_id", getRunById);

  async function getRunById(cx: Koa.Context): Promise<void> {
    const rawRunId = cx.params["run_id"];
    const auth: AuthContext = await api.koaAuth.auth(cx);
    let runId: UuidHex;
    try {
      runId = $UuidHex.read(QS_VALUE_READER, rawRunId);
    } catch (err) {
      cx.response.status = 422;
      cx.response.body = {error: "InvalidRequest", message: "Invalid run id"};
      return;
    }

    let run: Run;
    try {
      run = await api.run.getRunById(auth, runId);
    } catch (err) {
      switch (err instanceof Error ? err.name : undefined) {
        case "RunNotFound":
          cx.response.status = 404;
          cx.response.body = {error: "ResourceNotFound"};
          break;
        case "Unauthenticated":
          cx.response.status = 401;
          cx.response.body = {error: "Unauthorized"};
          break;
        case "Forbidden":
          cx.response.status = 403;
          cx.response.body = {error: "Forbidden"};
          break;
        default:
          console.error("getRunById");
          console.error(err);
          cx.response.status = 500;
          break;
      }
      return;
    }

    cx.response.body = $Run.write(JSON_VALUE_WRITER, run);
  }

  router.post("/:run_id/start", startRun);

  async function startRun(cx: Koa.Context): Promise<void> {
    const rawRunId = cx.params["run_id"];
    const auth: AuthContext = await api.koaAuth.auth(cx);
    const runId: UuidHex = $UuidHex.read(QS_VALUE_READER, rawRunId);
    const runStart: RunStart = await api.run.startRun(auth, runId);
    cx.response.body = $RunStart.write(JSON_VALUE_WRITER, runStart);
  }

  router.post("/:run_id/result", koaCompose([koaBodyParser(), setRunResult]));

  async function setRunResult(cx: Koa.Context): Promise<void> {
    const rawRunId = cx.params["run_id"];
    const auth: AuthContext = await api.koaAuth.auth(cx);
    let runId: UuidHex;
    let options: SetRunResultOptions;
    try {
      runId = $UuidHex.read(QS_VALUE_READER, rawRunId);
      if (Reflect.get(cx.request.body as any, "flash") === "true") {
        for (const [key, value] of Object.entries(cx.request.body as any)) {
          Reflect.set(cx.request.body as any, key, JSON.parse(value as any));
        }
      }
      options = $SetRunResultOptions.read(JSON_VALUE_READER, cx.request.body);
    } catch (err) {
      console.error(err);
      cx.response.status = 422;
      cx.response.body = {error: "InvalidRequest"};
      return;
    }
    const run: Run = await api.run.setRunResult(auth, runId, options);
    cx.response.body = $Run.write(JSON_VALUE_WRITER, run);
  }

  return router;
}
