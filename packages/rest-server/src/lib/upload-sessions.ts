import { AuthContext } from "@eternalfest/api-core/auth/auth-context";
import {
  $CreateUploadSessionOptions,
  CreateUploadSessionOptions
} from "@eternalfest/api-core/blob/create-upload-session-options";
import { BlobService } from "@eternalfest/api-core/blob/service";
import { $UploadBytesOptions, UploadBytesOptions } from "@eternalfest/api-core/blob/upload-bytes-options";
import { $UploadSession, UploadSession } from "@eternalfest/api-core/blob/upload-session";
import Router from "@koa/router";
import Koa from "koa";
import koaBodyParser from "koa-bodyparser";
import koaCompose from "koa-compose";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";
import { JSON_VALUE_READER } from "kryo-json/json-value-reader";
import { JSON_VALUE_WRITER } from "kryo-json/json-value-writer";
import { QS_VALUE_READER } from "kryo-qs/qs-value-reader";

import { KoaAuth } from "./koa-auth.js";

export interface Api {
  koaAuth: KoaAuth;
  blob: BlobService;
}

export function createUploadSessionsRouter(api: Api): Router {
  const router: Router = new Router();

  router.post("/", koaCompose([koaBodyParser(), createUploadSession]));

  async function createUploadSession(ctx: Koa.Context): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(ctx);
    const options: CreateUploadSessionOptions = $CreateUploadSessionOptions.read(JSON_VALUE_READER, ctx.request.body);
    const uploadSession: UploadSession = await api.blob.createUploadSession(auth, options);
    ctx.response.body = $UploadSession.write(JSON_VALUE_WRITER, uploadSession);
  }

  router.patch("/:upload_session_id", koaCompose([koaBodyParser(), uploadBytes]));

  async function uploadBytes(cx: Koa.Context): Promise<void> {
    const rawUploadSessionId = cx.params["upload_session_id"];
    const auth: AuthContext = await api.koaAuth.auth(cx);
    let uploadSessionId: UuidHex;
    let options: UploadBytesOptions;
    try {
      uploadSessionId = $UuidHex.read(QS_VALUE_READER, rawUploadSessionId);
      options = $UploadBytesOptions.read(JSON_VALUE_READER, cx.request.body);
    } catch (err) {
      console.error(err);
      cx.response.status = 422;
      cx.response.body = {error: "InvalidRequest"};
      return;
    }
    const uploadSession: UploadSession = await api.blob.uploadBytes(auth, {uploadSessionId, ...options});
    cx.response.body = $UploadSession.write(JSON_VALUE_WRITER, uploadSession);
  }

  return router;
}
