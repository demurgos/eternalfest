import {AuthService} from "@eternalfest/api-core/auth/service";
import {BlobService} from "@eternalfest/api-core/blob/service";
import {FileService} from "@eternalfest/api-core/file/service";
import {Game2Service} from "@eternalfest/api-core/game2/service";
import {RunService} from "@eternalfest/api-core/run/service";
import {UserService} from "@eternalfest/api-core/user/service";
import Router from "@koa/router";
import Koa from "koa";

import {createBlobsRouter} from "./blobs.js";
import {createFilesRouter} from "./files.js";
import {createGamesRouter} from "./games.js";
import {KoaAuth} from "./koa-auth.js";
import {createRunsRouter} from "./runs.js";
import {createSelfRouter} from "./self.js";
import {createUploadSessionsRouter} from "./upload-sessions.js";
import {createUsersRouter} from "./users.js";

export interface Api {
  auth: AuthService;
  blob: BlobService;
  file: FileService;
  game2: Game2Service;
  koaAuth: KoaAuth;
  run: RunService;
  user: UserService;
}

export function createApiRouter(api: Api): Router {
  const router: Router = new Router();

  const blobs = createBlobsRouter(api);
  router.use("/blobs", blobs.routes(), blobs.allowedMethods());
  const files = createFilesRouter(api);
  router.use("/files", files.routes(), files.allowedMethods());
  const games = createGamesRouter(api);
  router.use("/games", games.routes(), games.allowedMethods());
  const runs = createRunsRouter(api);
  router.use("/runs", runs.routes(), runs.allowedMethods());
  const self = createSelfRouter(api);
  router.use("/self", self.routes(), self.allowedMethods());
  const uploadSessions = createUploadSessionsRouter(api);
  router.use("/upload-sessions", uploadSessions.routes(), uploadSessions.allowedMethods());
  const users = createUsersRouter(api);
  router.use("/users", users.routes(), users.allowedMethods());
  router.use((cx: Koa.Context) => {
    cx.response.status = 404;
    cx.body = {error: "ResourceNotFound"};
  });

  return router;
}
