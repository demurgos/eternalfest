import { AuthContext } from "@eternalfest/api-core/auth/auth-context";
import {
  $CreateFileOptions,
  CreateFileOptions
} from "@eternalfest/api-core/file/create-file-options";
import { $File, File } from "@eternalfest/api-core/file/file";
import { FileService } from "@eternalfest/api-core/file/service";
import Router from "@koa/router";
import Koa from "koa";
import koaBodyParser from "koa-bodyparser";
import koaCompose from "koa-compose";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";
import { JSON_VALUE_READER } from "kryo-json/json-value-reader";
import { JSON_VALUE_WRITER } from "kryo-json/json-value-writer";
import { QS_VALUE_READER } from "kryo-qs/qs-value-reader";

import { KoaAuth } from "./koa-auth.js";

export interface Api {
  koaAuth: KoaAuth;
  file: FileService;
}

export function createFilesRouter(api: Api): Router {
  const router: Router = new Router();

  router.post("/", koaCompose([koaBodyParser(), createFile]));

  async function createFile(ctx: Koa.Context): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(ctx);
    const options: CreateFileOptions = $CreateFileOptions.read(JSON_VALUE_READER, ctx.request.body);
    const file: File = await api.file.createFile(auth, options);
    ctx.response.body = $File.write(JSON_VALUE_WRITER, file);
  }

  router.get("/:file_id", getFileById);

  async function getFileById(cx: Koa.Context): Promise<void> {
    const rawFileId = cx.params["file_id"];
    const auth: AuthContext = await api.koaAuth.auth(cx);
    const fileId: UuidHex = $UuidHex.read(QS_VALUE_READER, rawFileId);
    const file: File = await api.file.getFileById(auth, fileId);
    cx.response.body = $File.write(JSON_VALUE_WRITER, file);
  }

  router.get("/:file_id/raw", getRawFileById);

  async function getRawFileById(cx: Koa.Context): Promise<void> {
    const rawFileId = cx.params["file_id"];
    const auth: AuthContext = await api.koaAuth.auth(cx);
    const fileId: UuidHex = $UuidHex.read(QS_VALUE_READER, rawFileId);
    const file: File = await api.file.getFileById(auth, fileId);
    cx.response.set("Content-Length", file.byteSize.toString(10));
    cx.response.set("Content-Type", file.mediaType);
    cx.response.body = await api.file.readFileContent(auth, fileId);
  }

  return router;
}
