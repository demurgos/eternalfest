import { AuthContext } from "@eternalfest/api-core/auth/auth-context";
import { $Blob, Blob } from "@eternalfest/api-core/blob/blob";
import {
  $CreateBlobOptions,
  CreateBlobOptions
} from "@eternalfest/api-core/blob/create-blob-options";
import { BlobService } from "@eternalfest/api-core/blob/service";
import Router from "@koa/router";
import Koa from "koa";
import koaBodyParser from "koa-bodyparser";
import koaCompose from "koa-compose";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";
import { JSON_VALUE_READER } from "kryo-json/json-value-reader";
import { JSON_VALUE_WRITER } from "kryo-json/json-value-writer";
import { QS_VALUE_READER } from "kryo-qs/qs-value-reader";

import { KoaAuth } from "./koa-auth.js";

export interface Api {
  koaAuth: KoaAuth;
  blob: BlobService;
}

export function createBlobsRouter(api: Api): Router {
  const router: Router = new Router();

  router.post("/", koaCompose([koaBodyParser(), createBlob]));

  async function createBlob(cx: Koa.Context): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(cx);
    const options: CreateBlobOptions = $CreateBlobOptions.read(JSON_VALUE_READER, cx.request.body);
    const blob: Blob = await api.blob.createBlob(auth, options);
    cx.response.body = $Blob.write(JSON_VALUE_WRITER, blob);
  }

  router.get("/:blob_id", getFileById);

  async function getFileById(cx: Koa.Context): Promise<void> {
    const rawBlobId = cx.params["blob_id"];
    const auth: AuthContext = await api.koaAuth.auth(cx);
    const blobId: UuidHex = $UuidHex.read(QS_VALUE_READER, rawBlobId);
    const blob: Blob = await api.blob.getBlobById(auth, {id: blobId});
    cx.response.body = $Blob.write(JSON_VALUE_WRITER, blob);
  }

  router.get("/:blob_id/raw", getRawBlobById);

  async function getRawBlobById(cx: Koa.Context): Promise<void> {
    const rawBlobId = cx.params["blob_id"];
    const auth: AuthContext = await api.koaAuth.auth(cx);
    const blobId: UuidHex = $UuidHex.read(QS_VALUE_READER, rawBlobId);
    const blob: Blob = await api.blob.getBlobById(auth, {id: blobId});
    cx.response.set("Content-Length", blob.byteSize.toString(10));
    cx.response.set("Content-Type", blob.mediaType);
    if (api.blob.hasImmutableBlobs) {
      cx.response.set("Cache-Control", "public,max-age=31536000,immutable");
    }
    cx.response.body = await api.blob.readBlobData(auth, {id: blobId});
  }

  return router;
}
