/**
 * 2 min timeout per test.
 */
export const TIMEOUT = 120000;
