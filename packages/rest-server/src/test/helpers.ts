import incident from "incident";
import { IoType } from "kryo";
import { JSON_VALUE_READER } from "kryo-json/json-value-reader";

export function readJsonResponse<T>(type: IoType<T>, raw: any): T {
  if (typeof raw.error === "string") {
    throw new incident.Incident(raw.error, raw);
  } else {
    return type.read(JSON_VALUE_READER, raw);
  }
}
