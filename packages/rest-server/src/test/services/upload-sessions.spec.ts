import { $CreateUploadSessionOptions } from "@eternalfest/api-core/blob/create-upload-session-options";
import { $UploadBytesOptions } from "@eternalfest/api-core/blob/upload-bytes-options";
import { $UploadSession, UploadSession } from "@eternalfest/api-core/blob/upload-session";
import chai from "chai";
import chaiHttp from "chai-http";
import { JSON_VALUE_WRITER } from "kryo-json/json-value-writer";

import { readJsonResponse } from "../helpers.js";
import { populateWithUsers } from "../populate/users.js";
import { withTestServer } from "../test-server.js";
import { TIMEOUT } from "../timeout.js";

chai.use(chaiHttp);

describe("/upload-sessions", function () {
  it("Create an upload session", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(async server => {
      const {aliceAgent} = await populateWithUsers(server);

      const startTime: number = Date.now();

      const uploadSessionRes: ChaiHttp.Response = await aliceAgent.post("/upload-sessions")
        .send($CreateUploadSessionOptions.write(JSON_VALUE_WRITER, {mediaType: "text/plain", byteSize: 13}));
      const uploadSession: UploadSession = readJsonResponse($UploadSession, uploadSessionRes.body);

      chai.assert.isString(uploadSession.id);
      chai.assert.deepEqual(uploadSession.remainingRange, {start: 0, end: 13});
      chai.assert.isTrue(uploadSession.expiresAt.getTime() >= startTime);
      chai.assert.isNull(uploadSession.blob);
    });
  });

  it("Create an upload session and upload some bytes", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(async server => {
      const {aliceAgent} = await populateWithUsers(server);

      let uploadSession: UploadSession;
      {
        const res: ChaiHttp.Response = await aliceAgent.post("/upload-sessions")
          .send($CreateUploadSessionOptions.write(JSON_VALUE_WRITER, {mediaType: "text/plain", byteSize: 13}));
        uploadSession = readJsonResponse($UploadSession, res.body);
      }

      const data: Buffer = Buffer.from("Hello", "UTF-8" as any);
      let uploadSession2: UploadSession;
      {
        const res: ChaiHttp.Response = await aliceAgent.patch(`/upload-sessions/${uploadSession.id}`)
          .send($UploadBytesOptions.write(JSON_VALUE_WRITER, {data, offset: 0}));
        uploadSession2 = readJsonResponse($UploadSession, res.body);
      }

      chai.assert.strictEqual(uploadSession2.id, uploadSession.id);
      chai.assert.deepEqual(uploadSession2.remainingRange, {start: 5, end: 13});
      chai.assert.isTrue(uploadSession2.expiresAt.getTime() >= uploadSession.expiresAt.getTime());
      chai.assert.isNull(uploadSession2.blob);
    });
  });

  it("Create an upload session and upload all chunks", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(async server => {
      const {aliceAgent} = await populateWithUsers(server);

      let uploadSession: UploadSession;
      {
        const res: ChaiHttp.Response = await aliceAgent.post("/upload-sessions")
          .send($CreateUploadSessionOptions.write(JSON_VALUE_WRITER, {mediaType: "text/plain", byteSize: 13}));
        uploadSession = readJsonResponse($UploadSession, res.body);
      }

      let uploadSession2: UploadSession;
      {
        const data: Buffer = Buffer.from("Hello", "UTF-8" as any);
        const res: ChaiHttp.Response = await aliceAgent.patch(`/upload-sessions/${uploadSession.id}`)
          .send($UploadBytesOptions.write(JSON_VALUE_WRITER, {data, offset: 0}));
        uploadSession2 = readJsonResponse($UploadSession, res.body);
      }

      let uploadSession3: UploadSession;
      {
        const data: Buffer = Buffer.from(", World!", "UTF-8" as any);
        const res: ChaiHttp.Response = await aliceAgent.patch(`/upload-sessions/${uploadSession2.id}`)
          .send($UploadBytesOptions.write(JSON_VALUE_WRITER, {data, offset: 5}));
        uploadSession3 = readJsonResponse($UploadSession, res.body);
      }

      chai.assert.strictEqual(uploadSession3.id, uploadSession2.id);
      chai.assert.deepEqual(uploadSession3.remainingRange, {start: 13, end: 13});
      chai.assert.isTrue(uploadSession3.expiresAt.getTime() >= uploadSession2.expiresAt.getTime());
      chai.assert.isDefined(uploadSession3.blob, "expected `uploadSession3.blob` to be defined");
      chai.assert.isString(uploadSession3.blob!.id);
      chai.assert.strictEqual(uploadSession3.blob!.byteSize, 13);
      chai.assert.strictEqual(uploadSession3.blob!.mediaType, "text/plain");
    });
  });

  it("Create an upload session and upload all chunks", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(async server => {
      const {aliceAgent} = await populateWithUsers(server);

      let uploadSession: UploadSession;
      {
        const res: ChaiHttp.Response = await aliceAgent.post("/upload-sessions")
          .send($CreateUploadSessionOptions.write(JSON_VALUE_WRITER, {mediaType: "text/plain", byteSize: 13}));
        uploadSession = readJsonResponse($UploadSession, res.body);
      }

      let uploadSession2: UploadSession;
      {
        const data: Buffer = Buffer.from("Hello", "UTF-8" as any);
        const res: ChaiHttp.Response = await aliceAgent.patch(`/upload-sessions/${uploadSession.id}`)
          .send($UploadBytesOptions.write(JSON_VALUE_WRITER, {data, offset: 0}));
        uploadSession2 = readJsonResponse($UploadSession, res.body);
      }

      let uploadSession3: UploadSession;
      {
        const data: Buffer = Buffer.from(", World!", "UTF-8" as any);
        const res: ChaiHttp.Response = await aliceAgent.patch(`/upload-sessions/${uploadSession2.id}`)
          .send($UploadBytesOptions.write(JSON_VALUE_WRITER, {data, offset: 5}));
        uploadSession3 = readJsonResponse($UploadSession, res.body);
      }

      chai.assert.strictEqual(uploadSession3.id, uploadSession2.id);
      chai.assert.deepEqual(uploadSession3.remainingRange, {start: 13, end: 13});
      chai.assert.isTrue(uploadSession3.expiresAt.getTime() >= uploadSession2.expiresAt.getTime());
      chai.assert.isDefined(uploadSession3.blob, "expected `session3.blob` to be defined");
      chai.assert.isString(uploadSession3.blob!.id);
      chai.assert.strictEqual(uploadSession3.blob!.byteSize, 13);
      chai.assert.strictEqual(uploadSession3.blob!.mediaType, "text/plain");
    });
  });

  it("Create a blob with an upload session and read it", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(async server => {
      const {aliceAgent} = await populateWithUsers(server);

      let uploadSession: UploadSession;
      {
        const res: ChaiHttp.Response = await aliceAgent.post("/upload-sessions")
          .send($CreateUploadSessionOptions.write(JSON_VALUE_WRITER, {mediaType: "text/plain", byteSize: 13}));
        uploadSession = readJsonResponse($UploadSession, res.body);
      }

      {
        const data: Buffer = Buffer.from("Hello", "UTF-8" as any);
        await aliceAgent.patch(`/upload-sessions/${uploadSession.id}`)
          .send($UploadBytesOptions.write(JSON_VALUE_WRITER, {data, offset: 0}));
      }

      let completeSession: UploadSession;
      {
        const data: Buffer = Buffer.from(", World!", "UTF-8" as any);
        const res: ChaiHttp.Response = await aliceAgent.patch(`/upload-sessions/${uploadSession.id}`)
          .send($UploadBytesOptions.write(JSON_VALUE_WRITER, {data, offset: 5}));
        completeSession = readJsonResponse($UploadSession, res.body);
      }

      chai.assert.isDefined(completeSession.blob, "expected `completeSession.blob` to be defined");

      const blobRes: ChaiHttp.Response = await aliceAgent.get(`/blobs/${completeSession.blob!.id}/raw`);

      chai.assert.strictEqual(blobRes.type, "text/plain");
      chai.assert.deepEqual(blobRes.text, "Hello, World!");
    });
  });
});
