import {$Blob, Blob} from "@eternalfest/api-core/blob/blob";
import {$CreateBlobOptions} from "@eternalfest/api-core/blob/create-blob-options";
import {MediaType} from "@eternalfest/api-core/blob/media-type";
import {$Drive, Drive} from "@eternalfest/api-core/file/drive";
import {GameCategory} from "@eternalfest/api-core/game/game-category";
import {$ActiveGameChannel, ActiveGameChannel} from "@eternalfest/api-core/game2/active-game-channel";
import {$CreateGameOptions, CreateGameOptions} from "@eternalfest/api-core/game2/create-game-options";
import {$Game, Game} from "@eternalfest/api-core/game2/game";
import {GameChannelPermission} from "@eternalfest/api-core/game2/game-channel-permission";
import {GameEngineType} from "@eternalfest/api-core/game2/game-engine-type";
import {$InputGameBuild} from "@eternalfest/api-core/game2/input-game-build";
import {$ShortGameListing, ShortGameListing} from "@eternalfest/api-core/game2/short-game-listing";
import {$Leaderboard, Leaderboard} from "@eternalfest/api-core/leaderboard/leaderboard";
import {$CreateRunOptions, CreateRunOptions} from "@eternalfest/api-core/run/create-run-options";
import {$Run, Run} from "@eternalfest/api-core/run/run";
import {$RunItems, RunItems} from "@eternalfest/api-core/run/run-items";
import {$SetRunResultOptions} from "@eternalfest/api-core/run/set-run-result-options";
import {ObjectType} from "@eternalfest/api-core/types/object-type";
import chai from "chai";
import chaiHttp from "chai-http";
import {UuidHex} from "kryo/uuid-hex";
import {JSON_VALUE_WRITER} from "kryo-json/json-value-writer";

import {$UpdateGameChannelBody} from "../../lib/games.js";
import {getTestResource} from "../get-test-resource.js";
import {readJsonResponse} from "../helpers.js";
import {populateWithUsers} from "../populate/users.js";
import {withTestServer} from "../test-server.js";
import {TIMEOUT} from "../timeout.js";

chai.use(chaiHttp);

describe("/games", function () {
  it("From an empty server, upload `sous-la-colline`", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(async server => {
      const {alice: aliceUser, aliceAgent} = await populateWithUsers(server);
      const aliceDrive: Drive = await getDriveByOwnerId(aliceAgent, aliceUser.id);

      const {
        icon: iconBlob,
        gameSwf: engineBlob,
        gameXml: contentBlob,
        music: musicBlob,
      } = await uploadSousLaCollineBlobs(aliceAgent, aliceDrive.root.id);

      const options: CreateGameOptions = {
        build: {
          version: "1.0.0",
          gitCommitRef: null,
          mainLocale: "fr-FR",
          displayName: "Sous la colline",
          description: "Contrée emblématique d'Eternalfest qui ravira les débutants et nostalgiques !",
          icon: {
            type: ObjectType.Blob,
            id: iconBlob.id
          },
          loader: "3.0.0",
          engine: {
            type: GameEngineType.Custom,
            blob: {
              type: ObjectType.Blob,
              id: engineBlob.id
            }
          },
          musics: [
            {
              displayName: "music.mp3",
              blob:
                {
                  type: ObjectType.Blob,
                  id: musicBlob.id
                }
            },
            {
              displayName: "music.mp3",
              blob:
                {
                  type: ObjectType.Blob,
                  id: musicBlob.id
                }
            },
            {
              displayName: "music.mp3",
              blob:
                {
                  type: ObjectType.Blob,
                  id: musicBlob.id
                }
            },
          ],
          patcher: null,
          debug: null,
          content: {
            type: ObjectType.Blob,
            id: contentBlob.id
          },
          contentI18n: null,
          modes: new Map([
            ["solo", {
              displayName: "Aventure",
              isVisible: true,
              options: new Map([
                ["mirror", {
                  displayName: "Miroir",
                  isVisible: true,
                  isEnabled: true,
                  defaultValue: false
                }],
                ["nightmare", {
                  displayName: "Cauchemar",
                  isVisible: true,
                  isEnabled: true,
                  defaultValue: false
                }],
                ["ninja", {
                  displayName: "Ninjutsu",
                  isVisible: true,
                  isEnabled: true,
                  defaultValue: false
                }],
                ["bombexpert", {
                  displayName: "Explosifs Instables",
                  isVisible: true,
                  isEnabled: true,
                  defaultValue: false
                }],
                ["boost", {
                  displayName: "Tornade",
                  isVisible: true,
                  isEnabled: true,
                  defaultValue: false
                }],
              ]),
            }],
            ["multicoop", {
              displayName: "Multi coopératif",
              isVisible: true,
              options: new Map([
                ["mirrormulti", {
                  displayName: "Miroir",
                  isVisible: true,
                  isEnabled: true,
                  defaultValue: false
                }],
                ["nightmaremulti", {
                  displayName: "Cauchemar",
                  isVisible: true,
                  isEnabled: true,
                  defaultValue: false
                }],
                ["lifesharing", {
                  displayName: "Partage de vies",
                  isVisible: true,
                  isEnabled: true,
                  defaultValue: false
                }],
                ["bombotopia", {
                  displayName: "Bombotopia",
                  isVisible: true,
                  isEnabled: true,
                  defaultValue: false
                }],
              ]),
            }],
          ]),
          families: "0,1,2,3,4,10,11,12,19,100,101,102,103,104,105,106,107,108,109,110,111,112,113,1000,1001,1002,1003,1004,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015,1016,1017,1018,1023,1025,1026,1030,5000,5001,5002,5003,5004,5005,5006,5007,5008,5009,5010,5012,5014",
          category: GameCategory.Small,
          i18n: new Map(),
        },
        channels: [
          {
            key: "main",
            isEnabled: true,
            defaultPermission: GameChannelPermission.None,
            isPinned: true,
            publicationDate: null,
            sortUpdateDate: null,
            version: "1.0.0",
            patches: [],
          }
        ]
      };
      const res: ChaiHttp.Response = await aliceAgent.post("/games")
        .send($CreateGameOptions.write(JSON_VALUE_WRITER, options));
      const actual: Game = readJsonResponse($Game, res.body);

      chai.assert.isString(actual.id);
      chai.assert.instanceOf(actual.createdAt, Date);
      chai.assert.instanceOf(actual.channels.active.sortUpdateDate, Date);
      chai.assert.isTrue(actual.channels.active.sortUpdateDate.getTime() >= actual.createdAt.getTime());

      const expected: Game = {
        id: actual.id,
        createdAt: actual.createdAt,
        owner: {
          type: ObjectType.User,
          id: aliceUser.id,
          displayName: aliceUser.displayName,
        },
        key: null,
        type: ObjectType.Game,
        channels: {
          offset: 0,
          count: 1,
          limit: 1,
          isCountExact: true,
          active: {
            type: ObjectType.GameChannel,
            key: "main",
            isEnabled: true,
            defaultPermission: GameChannelPermission.None,
            isPinned: true,
            publicationDate: null,
            sortUpdateDate: actual.createdAt,
            build: {
              createdAt: actual.createdAt,
              version: "1.0.0",
              gitCommitRef: null,
              mainLocale: "fr-FR",
              displayName: "Sous la colline",
              description: "Contrée emblématique d'Eternalfest qui ravira les débutants et nostalgiques !",
              icon: iconBlob,
              loader: "3.0.0",
              engine: {
                type: GameEngineType.Custom,
                blob: engineBlob
              },
              musics: [
                {
                  displayName: "music.mp3",
                  blob: musicBlob
                },
                {
                  displayName: "music.mp3",
                  blob: musicBlob
                },
                {
                  displayName: "music.mp3",
                  blob: musicBlob
                },
              ],
              patcher: null,
              debug: null,
              content: contentBlob,
              contentI18n: null,
              modes: new Map([
                ["solo", {
                  displayName: "Aventure",
                  isVisible: true,
                  options: new Map([
                    ["mirror", {
                      displayName: "Miroir",
                      isVisible: true,
                      isEnabled: true,
                      defaultValue: false
                    }],
                    ["nightmare", {
                      displayName: "Cauchemar",
                      isVisible: true,
                      isEnabled: true,
                      defaultValue: false
                    }],
                    ["ninja", {
                      displayName: "Ninjutsu",
                      isVisible: true,
                      isEnabled: true,
                      defaultValue: false
                    }],
                    ["bombexpert", {
                      displayName: "Explosifs Instables",
                      isVisible: true,
                      isEnabled: true,
                      defaultValue: false
                    }],
                    ["boost", {
                      displayName: "Tornade",
                      isVisible: true,
                      isEnabled: true,
                      defaultValue: false
                    }],
                  ]),
                }],
                ["multicoop", {
                  displayName: "Multi coopératif",
                  isVisible: true,
                  options: new Map([
                    ["mirrormulti", {
                      displayName: "Miroir",
                      isVisible: true,
                      isEnabled: true,
                      defaultValue: false
                    }],
                    ["nightmaremulti", {
                      displayName: "Cauchemar",
                      isVisible: true,
                      isEnabled: true,
                      defaultValue: false
                    }],
                    ["lifesharing", {
                      displayName: "Partage de vies",
                      isVisible: true,
                      isEnabled: true,
                      defaultValue: false
                    }],
                    ["bombotopia", {
                      displayName: "Bombotopia",
                      isVisible: true,
                      isEnabled: true,
                      defaultValue: false
                    }],
                  ]),
                }],
              ]),
              families: "0,1,2,3,4,10,11,12,19,100,101,102,103,104,105,106,107,108,109,110,111,112,113,1000,1001,1002,1003,1004,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015,1016,1017,1018,1023,1025,1026,1030,5000,5001,5002,5003,5004,5005,5006,5007,5008,5009,5010,5012,5014",
              category: GameCategory.Small,
              i18n: new Map(),
            }
          },
          items: [
            {
              type: ObjectType.GameChannel,
              key: "main",
              isEnabled: true,
              defaultPermission: GameChannelPermission.None,
              isPinned: true,
              publicationDate: null,
              sortUpdateDate: actual.createdAt,
              build: {
                version: "1.0.0",
                gitCommitRef: null,
                mainLocale: "fr-FR",
                displayName: "Sous la colline",
                description: "Contrée emblématique d'Eternalfest qui ravira les débutants et nostalgiques !",
                icon: iconBlob,
                i18n: new Map(),
              }
            },
          ],
        }
      };

      chai.assert.deepEqual(actual, expected);

      const gamesRes: ChaiHttp.Response = await aliceAgent.get("/games");
      const actualGames: ShortGameListing = readJsonResponse($ShortGameListing, gamesRes.body);
      const expectedGames: ShortGameListing = {
        offset: 0,
        limit: 30,
        count: 1,
        isCountExact: false,
        items: [{
          type: ObjectType.Game,
          id: actual.id,
          key: null,
          createdAt: actual.createdAt,
          owner: {
            type: ObjectType.User,
            id: aliceUser.id,
            displayName: aliceUser.displayName,
          },
          channels: {
            offset: 0,
            limit: 1,
            count: 1,
            isCountExact: false,
            items: [{
              type: ObjectType.GameChannel,
              key: "main",
              isEnabled: true,
              publicationDate: null,
              defaultPermission: GameChannelPermission.None,
              isPinned: true,
              sortUpdateDate: actual.createdAt,
              build: {
                version: "1.0.0",
                gitCommitRef: null,
                mainLocale: "fr-FR",
                displayName: "Sous la colline",
                description: "Contrée emblématique d'Eternalfest qui ravira les débutants et nostalgiques !",
                icon: {
                  type: ObjectType.Blob,
                  id: iconBlob.id,
                  byteSize: iconBlob.byteSize,
                  mediaType: iconBlob.mediaType,
                },
                i18n: new Map(),
              }
            }]
          }
        }]
      };
      chai.assert.deepEqual(actualGames, expectedGames);
    });
  });

  it("Authors should be able to edit games (musics)", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(async server => {
      const {bob, bobAgent} = await populateWithUsers(server);
      const {alpha} = await createSampleGames(bobAgent);
      const bobDrive: Drive = await getDriveByOwnerId(bobAgent, bob.id);

      chai.assert.isEmpty(alpha.channels.active.build.musics);

      const {
        icon: iconBlob,
        gameSwf: engineBlob,
        gameXml: contentBlob,
        music: musicBlob,
      } = await uploadSousLaCollineBlobs(bobAgent, bobDrive.root.id);
      const actual = {
        updatedAt: new Date()
      };

      {
        await bobAgent.post(`/games/${alpha.id}/builds`)
          .send($InputGameBuild.write(JSON_VALUE_WRITER, {
            version: "1.0.1",
            gitCommitRef: null,
            mainLocale: "fr-FR",
            displayName: "Sous la colline",
            description: "Contrée emblématique d'Eternalfest qui ravira les débutants et nostalgiques !",
            icon: {
              type: ObjectType.Blob,
              id: iconBlob.id
            },
            loader: "3.0.0",
            engine: {
              type: GameEngineType.Custom,
              blob: {
                type: ObjectType.Blob,
                id: engineBlob.id
              }
            },
            musics: [
              {
                displayName: "music.mp3",
                blob:
                  {
                    type: ObjectType.Blob,
                    id: musicBlob.id
                  }
              },
              {
                displayName: "music.mp3",
                blob:
                  {
                    type: ObjectType.Blob,
                    id: musicBlob.id
                  }
              },
              {
                displayName: "music.mp3",
                blob:
                  {
                    type: ObjectType.Blob,
                    id: musicBlob.id
                  }
              },
            ],
            patcher: null,
            debug: null,
            content: {
              type: ObjectType.Blob,
              id: contentBlob.id
            },
            contentI18n: null,
            modes: new Map([
              ["solo", {
                displayName: "Aventure",
                isVisible: true,
                options: new Map([
                  ["mirror", {
                    displayName: "Miroir",
                    isVisible: true,
                    isEnabled: true,
                    defaultValue: false
                  }],
                  ["nightmare", {
                    displayName: "Cauchemar",
                    isVisible: true,
                    isEnabled: true,
                    defaultValue: false
                  }],
                  ["ninja", {
                    displayName: "Ninjutsu",
                    isVisible: true,
                    isEnabled: true,
                    defaultValue: false
                  }],
                  ["bombexpert", {
                    displayName: "Explosifs Instables",
                    isVisible: true,
                    isEnabled: true,
                    defaultValue: false
                  }],
                  ["boost", {
                    displayName: "Tornade",
                    isVisible: true,
                    isEnabled: true,
                    defaultValue: false
                  }],
                ]),
              }],
              ["multicoop", {
                displayName: "Multi coopératif",
                isVisible: true,
                options: new Map([
                  ["mirrormulti", {
                    displayName: "Miroir",
                    isVisible: true,
                    isEnabled: true,
                    defaultValue: false
                  }],
                  ["nightmaremulti", {
                    displayName: "Cauchemar",
                    isVisible: true,
                    isEnabled: true,
                    defaultValue: false
                  }],
                  ["lifesharing", {
                    displayName: "Partage de vies",
                    isVisible: true,
                    isEnabled: true,
                    defaultValue: false
                  }],
                  ["bombotopia", {
                    displayName: "Bombotopia",
                    isVisible: true,
                    isEnabled: true,
                    defaultValue: false
                  }],
                ]),
              }],
            ]),
            families: "0,1,2,3,4,10,11,12,19,100,101,102,103,104,105,106,107,108,109,110,111,112,113,1000,1001,1002,1003,1004,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015,1016,1017,1018,1023,1025,1026,1030,5000,5001,5002,5003,5004,5005,5006,5007,5008,5009,5010,5012,5014",
            category: GameCategory.Small,
            i18n: new Map(),
          }));
      }

      let updatedAlpha: ActiveGameChannel;
      {
        const res: ChaiHttp.Response = await bobAgent.patch(`/games/${alpha.id}/channels/main`)
          .send($UpdateGameChannelBody.write(JSON_VALUE_WRITER, {
            patches: [{
              period: {start: null, end: null},
              isEnabled: true,
              defaultPermission: GameChannelPermission.Play,
              isPinned: false,
              publicationDate: actual.updatedAt,
              sortUpdateDate: actual.updatedAt,
              version: "1.0.1"
            }]
          }));
        updatedAlpha = readJsonResponse($ActiveGameChannel, res.body);
      }

      const expectedUpdatedAlpha: ActiveGameChannel = {
        type: ObjectType.GameChannel,
        key: "main",
        isEnabled: true,
        defaultPermission: GameChannelPermission.Play,
        isPinned: false,
        publicationDate: actual.updatedAt,
        sortUpdateDate: actual.updatedAt,
        build: {
          createdAt: updatedAlpha.build.createdAt,
          version: "1.0.1",
          gitCommitRef: null,
          mainLocale: "fr-FR",
          displayName: "Sous la colline",
          description: "Contrée emblématique d'Eternalfest qui ravira les débutants et nostalgiques !",
          icon: iconBlob,
          loader: "3.0.0",
          engine: {
            type: GameEngineType.Custom,
            blob: engineBlob
          },
          musics: [
            {
              displayName: "music.mp3",
              blob: musicBlob
            },
            {
              displayName: "music.mp3",
              blob: musicBlob
            },
            {
              displayName: "music.mp3",
              blob: musicBlob
            },
          ],
          patcher: null,
          debug: null,
          content: contentBlob,
          contentI18n: null,
          modes: new Map([
            ["solo", {
              displayName: "Aventure",
              isVisible: true,
              options: new Map([
                ["mirror", {
                  displayName: "Miroir",
                  isVisible: true,
                  isEnabled: true,
                  defaultValue: false
                }],
                ["nightmare", {
                  displayName: "Cauchemar",
                  isVisible: true,
                  isEnabled: true,
                  defaultValue: false
                }],
                ["ninja", {
                  displayName: "Ninjutsu",
                  isVisible: true,
                  isEnabled: true,
                  defaultValue: false
                }],
                ["bombexpert", {
                  displayName: "Explosifs Instables",
                  isVisible: true,
                  isEnabled: true,
                  defaultValue: false
                }],
                ["boost", {
                  displayName: "Tornade",
                  isVisible: true,
                  isEnabled: true,
                  defaultValue: false
                }],
              ]),
            }],
            ["multicoop", {
              displayName: "Multi coopératif",
              isVisible: true,
              options: new Map([
                ["mirrormulti", {
                  displayName: "Miroir",
                  isVisible: true,
                  isEnabled: true,
                  defaultValue: false
                }],
                ["nightmaremulti", {
                  displayName: "Cauchemar",
                  isVisible: true,
                  isEnabled: true,
                  defaultValue: false
                }],
                ["lifesharing", {
                  displayName: "Partage de vies",
                  isVisible: true,
                  isEnabled: true,
                  defaultValue: false
                }],
                ["bombotopia", {
                  displayName: "Bombotopia",
                  isVisible: true,
                  isEnabled: true,
                  defaultValue: false
                }],
              ]),
            }],
          ]),
          families: "0,1,2,3,4,10,11,12,19,100,101,102,103,104,105,106,107,108,109,110,111,112,113,1000,1001,1002,1003,1004,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015,1016,1017,1018,1023,1025,1026,1030,5000,5001,5002,5003,5004,5005,5006,5007,5008,5009,5010,5012,5014",
          category: GameCategory.Small,
          i18n: new Map(),
        }
      };

      chai.assert.deepEqual(updatedAlpha, expectedUpdatedAlpha);
    });
  });

  it.skip("Administrators can set the `publicationDate` value", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(async server => {
      const {aliceAgent} = await populateWithUsers(server);
      const {alpha} = await createSampleGames(aliceAgent);

      chai.assert.propertyVal(alpha, "publicationDate", null);
      const now: Date = new Date();
      let updatedAlpha: Game;
      {
        const res: ChaiHttp.Response = await aliceAgent.patch(`/games/${alpha.id}`)
          .send($UpdateGameChannelBody.write(JSON_VALUE_WRITER, {
            patches: [{
              period: {start: null, end: null},
              isEnabled: true,
              defaultPermission: GameChannelPermission.Play,
              isPinned: false,
              publicationDate: now,
              sortUpdateDate: now,
              version: "1.0.0"
            }]
          }));
        updatedAlpha = readJsonResponse($Game, res.body);
      }
      chai.assert.deepEqual(updatedAlpha.id, alpha.id);
      chai.assert.deepEqual(updatedAlpha.channels.active.publicationDate, now);
      let updatedAlpha2: Game;
      {
        const res: ChaiHttp.Response = await aliceAgent.patch(`/games/${alpha.id}`)
          .send($UpdateGameChannelBody.write(JSON_VALUE_WRITER, {
            patches: [{
              period: {start: null, end: null},
              isEnabled: true,
              defaultPermission: GameChannelPermission.None,
              isPinned: false,
              publicationDate: null,
              sortUpdateDate: now,
              version: "1.0.0"
            }]
          }));
        updatedAlpha2 = readJsonResponse($Game, res.body);
      }
      chai.assert.deepEqual(updatedAlpha2.id, alpha.id);
      chai.assert.deepEqual(updatedAlpha2.channels.active.publicationDate, null);
    });
  });

  it("Can give the leaderboard of a game", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(async server => {
      const {alice, aliceAgent} = await populateWithUsers(server);
      const {alpha} = await createSampleGames(aliceAgent);

      // TODO: Replace with a run populate function when there is one.
      const createRunOptions: CreateRunOptions = {
        user: {type: ObjectType.User, id: alice.id},
        game: {id: alpha.id},
        channel: "main",
        version: "1.0.0",
        gameMode: "solo",
        gameOptions: ["boost"],
        settings: {
          detail: false,
          music: true,
          shake: false,
          sound: false,
          volume: 100,
          locale: "fr-FR",
        },
      };
      const runRes: ChaiHttp.Response = await aliceAgent.post("/runs")
        .send($CreateRunOptions.write(JSON_VALUE_WRITER, createRunOptions));
      const actualRun: Run = readJsonResponse($Run, runRes.body);
      await aliceAgent.post(`/runs/${actualRun.id}/start`).send({});
      await aliceAgent.post(`/runs/${actualRun.id}/result`)
        .send($SetRunResultOptions.write(JSON_VALUE_WRITER, {
          isVictory: true,
          maxLevel: 10,
          scores: [1500],
          items: new Map(),
          stats: {}
        }));

      const res: ChaiHttp.Response = await aliceAgent.get(`/games/${alpha.id}/leaderboard?game_mode=solo`);
      const leaderboard: Leaderboard = readJsonResponse($Leaderboard, res.body);

      const expectedLeaderboard: Leaderboard = {
        game: {id: alpha.id},
        channel: "main",
        mode: "solo",
        results: [{
          score: 1500,
          user: {type: ObjectType.User, id: alice.id, displayName: "Alice"},
          run: {id: actualRun.id, maxLevel: 10, gameOptions: ["boost"]}
        }]
      };
      chai.assert.deepEqual(leaderboard, expectedLeaderboard);
    });
  });

  it("Can give the items of a game for a particular user", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(async server => {
      const {alice, aliceAgent} = await populateWithUsers(server);
      const {alpha} = await createSampleGames(aliceAgent);

      // TODO: Replace with a run populate function when there is one.
      const createRunOptions: CreateRunOptions = {
        user: {type: ObjectType.User, id: alice.id},
        game: {id: alpha.id},
        channel: "main",
        version: "1.0.0",
        gameMode: "solo",
        gameOptions: ["boost"],
        settings: {
          detail: false,
          music: true,
          shake: false,
          sound: false,
          volume: 100,
          locale: "fr-FR",
        },
      };

      const runItems: RunItems = new Map([["1000", 42], ["1235", 1]]);

      const runRes: ChaiHttp.Response = await aliceAgent.post("/runs")
        .send($CreateRunOptions.write(JSON_VALUE_WRITER, createRunOptions));
      const actualRun: Run = readJsonResponse($Run, runRes.body);
      await aliceAgent.post(`/runs/${actualRun.id}/start`).send({});
      await aliceAgent.post(`/runs/${actualRun.id}/result`)
        .send($SetRunResultOptions.write(JSON_VALUE_WRITER, {
          isVictory: true,
          maxLevel: 10,
          scores: [1500],
          items: runItems,
          stats: {}
        }));

      const res: ChaiHttp.Response = await aliceAgent.get(`/users/${alice.id}/profiles/${alpha.id}/items`);
      const actualItems: RunItems = readJsonResponse($RunItems, res.body);
      chai.assert.deepEqual(actualItems, runItems);
    });
  });
});

interface SampleGames {
  alpha: Game;
  beta: Game;
}

// TODO: Remove this function and use `populateWithGames`
async function createSampleGames(agent: ChaiHttp.Agent): Promise<SampleGames> {
  let alpha: Game;
  {
    const options: CreateGameOptions = {
      build: {
        version: "1.0.0",
        gitCommitRef: null,
        mainLocale: "fr-FR",
        displayName: "Alpha",
        description: "Alpha game description.",
        icon: null,
        loader: "3.0.0",
        engine: {
          type: GameEngineType.V96
        },
        musics: [],
        contentI18n: null,
        patcher: null,
        debug: null,
        content: null,
        modes: new Map([
          ["solo", {
            displayName: "Aventure",
            isVisible: true,
            options: new Map(),
          }],
        ]),
        families: "1,2,4",
        category: GameCategory.Big,
        i18n: new Map(),
      },
      channels: [
        {
          key: "main",
          isEnabled: true,
          defaultPermission: GameChannelPermission.None,
          isPinned: true,
          publicationDate: null,
          sortUpdateDate: null,
          version: "1.0.0",
          patches: [],
        }
      ]
    };
    const res: ChaiHttp.Response = await agent.post("/games")
      .send($CreateGameOptions.write(JSON_VALUE_WRITER, options));
    alpha = readJsonResponse($Game, res.body);
  }
  let beta: Game;
  {
    const options: CreateGameOptions = {
      build: {
        version: "1.0.0",
        gitCommitRef: null,
        mainLocale: "fr-FR",
        displayName: "Beta",
        description: "Beta game description.",
        icon: null,
        loader: "3.0.0",
        engine: {
          type: GameEngineType.V96
        },
        musics: [],
        contentI18n: null,
        patcher: null,
        debug: null,
        content: null,
        modes: new Map([
          ["solo", {
            displayName: "Aventure",
            isVisible: true,
            options: new Map(),
          }],
        ]),
        families: "10,20,40",
        category: GameCategory.Big,
        i18n: new Map(),
      },
      channels: [
        {
          key: "main",
          isEnabled: true,
          defaultPermission: GameChannelPermission.None,
          isPinned: true,
          publicationDate: null,
          sortUpdateDate: null,
          version: "1.0.0",
          patches: [],
        }
      ],
    };
    const res: ChaiHttp.Response = await agent.post("/games")
      .send($CreateGameOptions.write(JSON_VALUE_WRITER, options));
    beta = readJsonResponse($Game, res.body);
  }
  return {alpha, beta};
}

interface SousLaCollineBlobs {
  icon: Blob;
  gameSwf: Blob;
  gameXml: Blob;
  music: Blob;
}

async function uploadSousLaCollineBlobs(
  agent: ChaiHttp.Agent,
  _directoryId: UuidHex,
): Promise<SousLaCollineBlobs> {
  async function createFile(components: string[], mediaType: MediaType): Promise<Blob> {
    const buffer: Buffer = await getTestResource(["games", "sous-la-colline", ...components]);
    const blobRes: ChaiHttp.Response = await agent.post("/blobs")
      .send($CreateBlobOptions.write(JSON_VALUE_WRITER, {mediaType, data: buffer}));
    const blob: Blob = readJsonResponse($Blob, blobRes.body);
    return blob;
  }

  const icon: Blob = await createFile(["icon.png"], "image/png");
  const gameSwf: Blob = await createFile(["game.swf"], "application/x-shockwave-flash");
  const gameXml: Blob = await createFile(["game.xml"], "application/xml");
  const music: Blob = await createFile(["music", "rourou.mp3"], "audio/mp3");

  return {icon, gameSwf, gameXml, music};
}

async function getDriveByOwnerId(agent: ChaiHttp.Agent, userId: string): Promise<Drive> {
  const res: ChaiHttp.Response = await agent.get(`/users/${userId}/drive`);
  return readJsonResponse($Drive, res.body);
}
