import { $Blob, Blob } from "@eternalfest/api-core/blob/blob";
import { $CreateBlobOptions } from "@eternalfest/api-core/blob/create-blob-options";
import { $CreateFileOptions } from "@eternalfest/api-core/file/create-file-options";
import { $Drive, Drive } from "@eternalfest/api-core/file/drive";
import { DriveItemType } from "@eternalfest/api-core/file/drive-item-type";
import { $File, File } from "@eternalfest/api-core/file/file";
import {ObjectType} from "@eternalfest/api-core/types/object-type";
import { $ShortUser } from "@eternalfest/api-core/user/short-user";
import { $User, User } from "@eternalfest/api-core/user/user";
import chai from "chai";
import chaiHttp from "chai-http";
import { JSON_VALUE_WRITER } from "kryo-json/json-value-writer";

import { getTestResource } from "../get-test-resource.js";
import { readJsonResponse } from "../helpers.js";
import { withTestServer } from "../test-server.js";
import { TIMEOUT } from "../timeout.js";

chai.use(chaiHttp);

describe("/files", function () {
  it("Upload sous-la-colline/icon.png", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);

    return withTestServer(async server => {
      const aliceAgent: ChaiHttp.Agent = chai.request.agent(server);
      let aliceUser: User;
      {
        const res: ChaiHttp.Response = await aliceAgent.post("/users").send($ShortUser.write(
          JSON_VALUE_WRITER,
          {type: ObjectType.User, displayName: "Alice", id: "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"},
        ));
        aliceUser = readJsonResponse($User, res.body);
      }
      let aliceDrive: Drive;
      {
        const res: ChaiHttp.Response = await aliceAgent.get(`/users/${aliceUser.id}/drive`);
        aliceDrive = readJsonResponse($Drive, res.body);
      }

      const buffer: Buffer = await getTestResource(["games", "sous-la-colline", "icon.png"]);
      const blobRes: ChaiHttp.Response = await aliceAgent.post("/blobs")
        .send($CreateBlobOptions.write(JSON_VALUE_WRITER, {mediaType: "image/png", data: buffer}));
      const blob: Blob = readJsonResponse($Blob, blobRes.body);

      const fileRes: ChaiHttp.Response = await aliceAgent.post("/files")
        .send($CreateFileOptions.write(
          JSON_VALUE_WRITER,
          {parentId: aliceDrive.root.id, displayName: "icon.png", blobId: blob.id},
        ));
      const actual: File = readJsonResponse($File, fileRes.body);

      const expected: File = {
        type: DriveItemType.File,
        id: actual.id,
        createdAt: actual.createdAt,
        updatedAt: actual.updatedAt,
        displayName: "icon.png",
        byteSize: 80805,
        mediaType: "image/png",
      };

      chai.assert.deepEqual(actual, expected);
    });
  });

  it("Retrieve sous-la-colline/icon.png", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);

    return withTestServer(async server => {
      const aliceAgent: ChaiHttp.Agent = chai.request.agent(server);
      let aliceUser: User;
      {
        const res: ChaiHttp.Response = await aliceAgent.post("/users").send($ShortUser.write(
          JSON_VALUE_WRITER,
          {type: ObjectType.User, displayName: "Alice", id: "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"},
        ));
        aliceUser = readJsonResponse($User, res.body);
      }
      let aliceDrive: Drive;
      {
        const res: ChaiHttp.Response = await aliceAgent.get(`/users/${aliceUser.id}/drive`);
        aliceDrive = readJsonResponse($Drive, res.body);
      }
      let iconFile: File;
      {
        const buffer: Buffer = await getTestResource(["games", "sous-la-colline", "icon.png"]);
        const blobRes: ChaiHttp.Response = await aliceAgent.post("/blobs")
          .send($CreateBlobOptions.write(JSON_VALUE_WRITER, {mediaType: "image/png", data: buffer}));
        const blob: Blob = readJsonResponse($Blob, blobRes.body);

        const fileRes: ChaiHttp.Response = await aliceAgent.post("/files")
          .send($CreateFileOptions.write(
            JSON_VALUE_WRITER,
            {parentId: aliceDrive.root.id, displayName: "icon.png", blobId: blob.id},
          ));
        iconFile = readJsonResponse($File, fileRes.body);
      }

      const res: ChaiHttp.Response = await aliceAgent.get(`/files/${iconFile.id}`);
      const actual: File = readJsonResponse($File, res.body);

      const expected: File = {
        type: DriveItemType.File,
        id: iconFile.id,
        createdAt: iconFile.createdAt,
        updatedAt: iconFile.updatedAt,
        displayName: "icon.png",
        byteSize: 80805,
        mediaType: "image/png",
      };

      chai.assert.deepEqual(actual, expected);
    });
  });

  it("Download sous-la-colline/icon.png", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);

    return withTestServer(async server => {
      const aliceAgent: ChaiHttp.Agent = chai.request.agent(server);
      let aliceUser: User;
      {
        const res: ChaiHttp.Response = await aliceAgent.post("/users").send($ShortUser.write(
          JSON_VALUE_WRITER,
          {type: ObjectType.User, displayName: "Alice", id: "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"},
        ));
        aliceUser = readJsonResponse($User, res.body);
      }
      let aliceDrive: Drive;
      {
        const res: ChaiHttp.Response = await aliceAgent.get(`/users/${aliceUser.id}/drive`);
        aliceDrive = readJsonResponse($Drive, res.body);
      }
      const iconBuffer: Buffer = await getTestResource(["games", "sous-la-colline", "icon.png"]);
      let iconFile: File;
      {
        const blobRes: ChaiHttp.Response = await aliceAgent.post("/blobs")
          .send($CreateBlobOptions.write(JSON_VALUE_WRITER, {mediaType: "image/png", data: iconBuffer}));
        const blob: Blob = readJsonResponse($Blob, blobRes.body);

        const fileRes: ChaiHttp.Response = await aliceAgent.post("/files")
          .send($CreateFileOptions.write(
            JSON_VALUE_WRITER,
            {parentId: aliceDrive.root.id, displayName: "icon.png", blobId: blob.id},
          ));
        iconFile = readJsonResponse($File, fileRes.body);
      }

      const res: ChaiHttp.Response = await aliceAgent.get(`/files/${iconFile.id}/raw`);

      chai.assert.strictEqual(res.type, "image/png");
      chai.assert.deepEqual(res.body, iconBuffer);
    });
  });
});
