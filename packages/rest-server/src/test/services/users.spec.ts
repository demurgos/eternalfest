import {ObjectType} from "@eternalfest/api-core/types/object-type";
import { $ShortUser } from "@eternalfest/api-core/user/short-user";
import { $UpdateUserOptions } from "@eternalfest/api-core/user/update-user-options";
import { $User, User } from "@eternalfest/api-core/user/user";
import chai from "chai";
import chaiHttp from "chai-http";
import { JSON_VALUE_WRITER } from "kryo-json/json-value-writer";

import { readJsonResponse } from "../helpers.js";
import { withTestServer } from "../test-server.js";
import { TIMEOUT } from "../timeout.js";

chai.use(chaiHttp);

describe("/users", function () {
  it("should be possible to create the first user", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(async server => {
      const agent: ChaiHttp.Agent = chai.request.agent(server);
      {
        const res: ChaiHttp.Response = await agent.get("/users");
        chai.assert.deepEqual(res.body, []);
      }
      {
        const res: ChaiHttp.Response = await agent.post("/users").send($ShortUser.write(
          JSON_VALUE_WRITER,
          {type: ObjectType.User, displayName: "Alice", id: "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"},
        ));

        const user: User = readJsonResponse($User, res.body);

        const expected: User = {
          id: user.id,
          type: ObjectType.User,
          displayName: "Alice",
          identities: [],
          isAdministrator: true,
          isTester: true,
          createdAt: user.createdAt,
          updatedAt: user.updatedAt,
        };

        chai.assert.deepEqual(user, expected);

        await agent.get(`/users/${user.id}`).send();
      }
    });
  });

  it("Administrators can set the `isTester` value for users", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(async server => {
      const aliceAgent: ChaiHttp.Agent = chai.request.agent(server);
      const bobAgent: ChaiHttp.Agent = chai.request.agent(server);
      {
        await aliceAgent.post("/users")
          .send($ShortUser.write(
            JSON_VALUE_WRITER,
            {type: ObjectType.User, displayName: "Alice", id: "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"},
          ));
      }
      let bobUser: User;
      {
        const res: ChaiHttp.Response = await bobAgent.post("/users")
          .send($ShortUser.write(
            JSON_VALUE_WRITER,
            {type: ObjectType.User, displayName: "Bob", id: "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb"},
          ));
        bobUser = readJsonResponse($User, res.body);
      }

      chai.assert.isFalse(bobUser.isTester);

      let updatedBob: User;
      {
        const res: ChaiHttp.Response = await aliceAgent.patch(`/users/${bobUser.id}`)
          .send($UpdateUserOptions.write(
            JSON_VALUE_WRITER,
            {userId: bobUser.id, isTester: true},
          ));
        updatedBob = readJsonResponse($User, res.body);
      }

      chai.assert.strictEqual(updatedBob.id, bobUser.id);
      chai.assert.isTrue(updatedBob.isTester);

      let updatedBob2: User;
      {
        const res: ChaiHttp.Response = await aliceAgent.patch(`/users/${bobUser.id}`)
          .send($UpdateUserOptions.write(
            JSON_VALUE_WRITER,
            {userId: bobUser.id, isTester: false},
          ));
        updatedBob2 = readJsonResponse($User, res.body);
      }

      chai.assert.strictEqual(updatedBob2.id, bobUser.id);
      chai.assert.isFalse(updatedBob2.isTester);
    });
  });

  it("The `isTester` value cannot be set to `false` for administrators", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(async server => {
      const aliceAgent: ChaiHttp.Agent = chai.request.agent(server);
      let aliceUser: User;
      {
        const res: ChaiHttp.Response = await aliceAgent.post("/users")
          .send($ShortUser.write(
            JSON_VALUE_WRITER,
            {type: ObjectType.User, displayName: "Alice", id: "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"},
          ));
        aliceUser = readJsonResponse($User, res.body);
      }
      const res: ChaiHttp.Response = await aliceAgent.patch(`/users/${aliceUser.id}`)
        .send($UpdateUserOptions.write(JSON_VALUE_WRITER, {userId: aliceUser.id, isTester: false}));
      chai.assert.strictEqual(res.status, 500);
    });
  });

  it("The `isTester` value cannot be set to `false` for administrators", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(async server => {
      const bobAgent: ChaiHttp.Agent = chai.request.agent(server);
      {
        const aliceAgent: ChaiHttp.Agent = chai.request.agent(server);
        await aliceAgent.post("/users")
          .send($ShortUser.write(
            JSON_VALUE_WRITER,
            {type: ObjectType.User, displayName: "Alice", id: "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"},
          ));
      }
      let bobUser: User;
      {
        const res: ChaiHttp.Response = await bobAgent.post("/users")
          .send($ShortUser.write(
            JSON_VALUE_WRITER,
            {type: ObjectType.User, displayName: "Bob", id: "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb"},
          ));
        bobUser = readJsonResponse($User, res.body);
      }
      const res: ChaiHttp.Response = await bobAgent.patch(`/users/${bobUser.id}`)
        .send($UpdateUserOptions.write(JSON_VALUE_WRITER, {userId: bobUser.id, isTester: true}));
      chai.assert.strictEqual(res.status, 500);
    });
  });
});
