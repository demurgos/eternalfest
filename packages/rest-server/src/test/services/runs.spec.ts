import { $CreateRunOptions, CreateRunOptions } from "@eternalfest/api-core/run/create-run-options";
import { $Run, Run } from "@eternalfest/api-core/run/run";
import { $RunStart, RunStart } from "@eternalfest/api-core/run/run-start";
import { $SetRunResultOptions } from "@eternalfest/api-core/run/set-run-result-options";
import {ObjectType} from "@eternalfest/api-core/types/object-type";
import chai from "chai";
import chaiHttp from "chai-http";
import { JSON_VALUE_WRITER } from "kryo-json/json-value-writer";

import { readJsonResponse } from "../helpers.js";
import { populateWithGames } from "../populate/games.js";
import { populateWithUsers } from "../populate/users.js";
import { withTestServer } from "../test-server.js";
import { TIMEOUT } from "../timeout.js";

chai.use(chaiHttp);

describe("/runs", function () {
  it("Start a \"Sous la colline\" run", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(async server => {
      const {alice, aliceAgent} = await populateWithUsers(server);
      const {alpha} = await populateWithGames(aliceAgent);

      console.log(alpha);

      const createRunOptions: CreateRunOptions = {
        user: {type: ObjectType.User, id: alice.id},
        game: {id: alpha.id},
        channel: "main",
        version: "1.0.0",
        gameMode: "solo",
        gameOptions: ["boost"],
        settings: {
          detail: false,
          music: true,
          shake: false,
          sound: true,
          volume: 100,
          locale: "fr-FR",
        },
      };

      const runRes: ChaiHttp.Response = await aliceAgent.post("/runs")
        .send($CreateRunOptions.write(JSON_VALUE_WRITER, createRunOptions));
      const actualRun: Run = readJsonResponse($Run, runRes.body);

      const expectedRun: Run = {
        id: actualRun.id,
        createdAt: actualRun.createdAt,
        startedAt: null,
        result: null,
        user: {id: alice.id, type: ObjectType.User, displayName: alice.displayName},
        game: {id: alpha.id},
        gameMode: "solo",
        gameOptions: ["boost"],
        settings: {
          detail: false,
          music: true,
          shake: false,
          sound: true,
          volume: 100,
          locale: "fr-FR",
        },
      };

      chai.assert.deepEqual(actualRun, expectedRun);

      const runStartRes: ChaiHttp.Response = await aliceAgent.post(`/runs/${actualRun.id}/start`).send({});
      const actualRunStart: RunStart = readJsonResponse($RunStart, runStartRes.body);

      const expectedRunStart: RunStart = {
        run: {
          id: actualRun.id,
        },
        key: actualRunStart.key,
        families: "1,2,4",
        items: new Map(),
      };

      chai.assert.deepEqual(actualRunStart, expectedRunStart);

      const runWithResultRes: ChaiHttp.Response = await aliceAgent.post(`/runs/${actualRun.id}/result`)
        .send($SetRunResultOptions.write(
          JSON_VALUE_WRITER,
          {isVictory: true, maxLevel: 10, scores: [1500], items: new Map([["1000", 10]]), stats: {jumps: 10}}),
        );
      const actualRunWithResult: Run = readJsonResponse($Run, runWithResultRes.body);

      const expectedRunWithResult: Run = {
        id: actualRun.id,
        createdAt: actualRun.createdAt,
        startedAt: actualRunWithResult.startedAt,
        result: {
          createdAt: actualRunWithResult.result!.createdAt,
          isVictory: true,
          maxLevel: 10,
          scores: [1500],
          items: new Map([["1000", 10]]),
          stats: {jumps: 10},
        },
        user: {id: alice.id, type: ObjectType.User, displayName: alice.displayName},
        game: {id: alpha.id},
        gameMode: "solo",
        gameOptions: ["boost"],
        settings: {
          detail: false,
          music: true,
          shake: false,
          sound: true,
          volume: 100,
          locale: "fr-FR",
        },
      };

      chai.assert.deepEqual(actualRunWithResult, expectedRunWithResult);
    });
  });
});
