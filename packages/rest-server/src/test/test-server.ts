import {Session} from "@eternalfest/api-core/types/session";
import {ActorType} from "@eternalfest/api-core/types/user/actor-type";
import {$ShortUser, ShortUser} from "@eternalfest/api-core/user/short-user";
import {$User, User} from "@eternalfest/api-core/user/user";
import {PgAuthStore} from "@eternalfest/native/auth-store";
import {PgBlobStore} from "@eternalfest/native/blob-store";
import {FsBufferStore} from "@eternalfest/native/buffer-store";
import {SystemClock} from "@eternalfest/native/clock";
import {HttpEtwinClient} from "@eternalfest/native/etwin-client";
import {PgFileStore} from "@eternalfest/native/file-store";
import {PgGameStore} from "@eternalfest/native/game-store";
import {PgRunStore} from "@eternalfest/native/run-store";
import {NativeAuthService} from "@eternalfest/native/services/auth";
import {NativeFileService} from "@eternalfest/native/services/file";
import {NativeGameService} from "@eternalfest/native/services/game";
import {NativeRunService} from "@eternalfest/native/services/run";
import {NativeUserService} from "@eternalfest/native/services/user";
import {PgUserStore} from "@eternalfest/native/user-store";
import {Uuid4Generator} from "@eternalfest/native/uuid";
import Router from "@koa/router";
import http from "http";
import Koa from "koa";
import koaBodyParser from "koa-bodyparser";
import koaCompose from "koa-compose";
import koaMount from "koa-mount";
import {JSON_VALUE_READER} from "kryo-json/json-value-reader";
import {JSON_VALUE_WRITER} from "kryo-json/json-value-writer";

import {createApiRouter} from "../lib/create-api-router.js";
import {KoaAuth, SESSION_COOKIE} from "../lib/koa-auth.js";
import {StateService, withTestState} from "./test-db.js";

export async function withTestServer<R>(handler: (server: http.Server) => Promise<R>): Promise<R> {
  return withTestState(async ({bufferRoot, database}: StateService): Promise<R> => {
    const clock = new SystemClock();
    const uuidGenerator = new Uuid4Generator();
    const bufferStore = await FsBufferStore.create({uuidGenerator, root: bufferRoot});
    const blobStore: PgBlobStore = await PgBlobStore.create({bufferStore, clock, database, uuidGenerator});

    const authStore = await PgAuthStore.create({clock, database, uuidGenerator});
    const userStore = await PgUserStore.create({clock, database});
    const user = await NativeUserService.create({userStore});
    const fileStore: PgFileStore = await PgFileStore.create({blobStore, clock, database, uuidGenerator});
    const gameStore: PgGameStore = await PgGameStore.create({clock, database, uuidGenerator});
    const file: NativeFileService = await NativeFileService.create({blobStore, fileStore, userStore});
    const runStore = await PgRunStore.create({clock, database, uuidGenerator});

    const etwinClient = await HttpEtwinClient.create({clock, root: "http://localhost:50320"});

    const auth: NativeAuthService = await NativeAuthService.create({authStore, clock, etwinClient, userStore});
    const game2: NativeGameService = await NativeGameService.create({blobStore, clock, gameStore, userStore});
    const run = await NativeRunService.create({blobStore, clock, gameStore, runStore, userStore});
    const koaAuth: KoaAuth = new KoaAuth(auth);

    const app: Koa = new Koa();

    const testRouter = new Router();
    const apiRouter: Router = createApiRouter({
      auth,
      blob: blobStore,
      file,
      game2,
      koaAuth,
      run,
      user
    });

    testRouter.post("/users", koaCompose([koaBodyParser(), createUser]));

    async function createUser(ctx: Koa.Context): Promise<void> {
      const options: ShortUser = $ShortUser.read(JSON_VALUE_READER, ctx.request.body);
      const newUser: User = await user.getOrCreateUserWithEtwin({type: ActorType.System}, options.id, options.displayName);
      const newSession: Session = await auth.createSession({type: ActorType.System}, newUser.id);
      ctx.cookies.set(SESSION_COOKIE, newSession.id);
      ctx.response.body = $User.write(JSON_VALUE_WRITER, newUser);
    }

    app.use(koaMount("/", testRouter.routes()));
    app.use(koaMount("/", testRouter.allowedMethods()));
    app.use(koaMount("/", apiRouter.routes()));
    app.use(koaMount("/", apiRouter.allowedMethods()));

    const server: http.Server = http.createServer(app.callback());

    async function closeServer(): Promise<void> {
      return new Promise<void>(resolve => {
        server.close(() => {
          resolve();
        });
      });
    }

    return new Promise<R>((resolve, reject): void => {
      async function onListening(): Promise<void> {
        server.removeListener("error", onError);

        let result: R;
        try {
          result = await handler(server);
        } catch (err) {
          await closeServer();
          reject(err);
          return;
        }
        await closeServer();
        resolve(result);
      }

      function onError(err: Error): void {
        server.removeListener("listening", onListening);
        reject(err);
      }

      server.once("listening", onListening);
      server.once("error", onError);

      server.listen();
    });
  });
}
