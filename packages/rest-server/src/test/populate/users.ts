import {ObjectType} from "@eternalfest/api-core/types/object-type";
import {$ShortUser} from "@eternalfest/api-core/user/short-user";
import {$UpdateUserOptions} from "@eternalfest/api-core/user/update-user-options";
import {$User, User} from "@eternalfest/api-core/user/user";
import chai from "chai";
import http from "http";
import {JSON_VALUE_WRITER} from "kryo-json/json-value-writer";

import {readJsonResponse} from "../helpers.js";

export interface PopulateUsers {
  alice: User;
  aliceAgent: ChaiHttp.Agent;
  bob: User;
  bobAgent: ChaiHttp.Agent;
  charlie: User;
  charlieAgent: ChaiHttp.Agent;
}

export async function populateWithUsers(server: http.Server): Promise<PopulateUsers> {
  const aliceAgent: ChaiHttp.Agent = chai.request.agent(server);
  let alice: User;
  {
    const res: ChaiHttp.Response = await aliceAgent.post("/users")
      .send($ShortUser.write(
        JSON_VALUE_WRITER,
        {type: ObjectType.User, displayName: "Alice", id: "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"},
      ));

    alice = readJsonResponse($User, res.body);
  }
  const bobAgent: ChaiHttp.Agent = chai.request.agent(server);
  let bob: User;
  {
    const res: ChaiHttp.Response = await bobAgent.post("/users")
      .send($ShortUser.write(
        JSON_VALUE_WRITER,
        {type: ObjectType.User, displayName: "Bob", id: "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb"},
      ));

    const simpleBob: User = readJsonResponse($User, res.body);
    {
      const res: ChaiHttp.Response = await aliceAgent.patch(`/users/${simpleBob.id}`)
        .send($UpdateUserOptions.write(JSON_VALUE_WRITER, {userId: simpleBob.id, isTester: true}));
      bob = readJsonResponse($User, res.body);
    }
  }
  const charlieAgent: ChaiHttp.Agent = chai.request.agent(server);
  let charlie: User;
  {
    const res: ChaiHttp.Response = await charlieAgent.post("/users")
      .send($ShortUser.write(
        JSON_VALUE_WRITER,
        {type: ObjectType.User, displayName: "Charlie", id: "cccccccc-cccc-cccc-cccc-cccccccccccc"},
      ));

    charlie = readJsonResponse($User, res.body);
  }

  return {alice, aliceAgent, bob, bobAgent, charlie, charlieAgent};
}
