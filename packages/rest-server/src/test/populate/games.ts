import {GameCategory} from "@eternalfest/api-core/game/game-category";
import {$CreateGameOptions} from "@eternalfest/api-core/game2/create-game-options";
import {$Game, Game} from "@eternalfest/api-core/game2/game";
import {GameChannelPermission} from "@eternalfest/api-core/game2/game-channel-permission";
import {GameEngineType} from "@eternalfest/api-core/game2/game-engine-type";
import {JSON_VALUE_WRITER} from "kryo-json/json-value-writer";

import {readJsonResponse} from "../helpers.js";

export interface SampleGames {
  alpha: Game;
  beta: Game;
}

export async function populateWithGames(agent: ChaiHttp.Agent): Promise<SampleGames> {
  let alpha: Game;
  {
    const res: ChaiHttp.Response = await agent.post("/games")
      .send($CreateGameOptions.write(
        JSON_VALUE_WRITER,
        {
          build: {
            version: "1.0.0",
            gitCommitRef: null,
            mainLocale: "fr-FR",
            displayName: "Alpha",
            description: "Alpha game description.",
            icon: null,
            loader: "3.0.0",
            engine: {
              type: GameEngineType.V96,
            },
            musics: [],
            patcher: null,
            debug: null,
            content: null,
            contentI18n: null,
            modes: new Map([
              ["solo", {
                displayName: "Aventure",
                isVisible: true,
                options: new Map(),
              }],
            ]),
            families: "1,2,4",
            category: GameCategory.Big,
            i18n: new Map(),
          },
          channels: [
            {
              key: "main",
              isEnabled: true,
              defaultPermission: GameChannelPermission.None,
              isPinned: true,
              publicationDate: null,
              sortUpdateDate: null,
              version: "1.0.0",
              patches: [],
            }
          ]
        },
      ));

    alpha = readJsonResponse($Game, res.body);
  }
  let beta: Game;
  {
    const res: ChaiHttp.Response = await agent.post("/games")
      .send($CreateGameOptions.write(
        JSON_VALUE_WRITER,
        {
          build: {
            version: "1.0.0",
            gitCommitRef: null,
            mainLocale: "fr-FR",
            displayName: "Beta",
            description: "Beta game description.",
            icon: null,
            loader: "3.0.0",
            engine: {
              type: GameEngineType.V96,
            },
            musics: [],
            patcher: null,
            debug: null,
            content: null,
            contentI18n: null,
            modes: new Map([
              ["solo", {
                displayName: "Aventure",
                isVisible: true,
                options: new Map(),
              }],
            ]),
            families: "10,20,40",
            category: GameCategory.Big,
            i18n: new Map(),
          },
          channels: [
            {
              key: "main",
              isEnabled: true,
              defaultPermission: GameChannelPermission.None,
              isPinned: true,
              publicationDate: null,
              sortUpdateDate: null,
              version: "1.0.0",
              patches: [],
            }
          ]
        },
      ));

    beta = readJsonResponse($Game, res.body);
  }

  return {alpha, beta};
}
