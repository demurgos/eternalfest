import { Database } from "@eternal-twin/pg-db";
import { Url } from "@eternalfest/api-core/types/url";
import { forceCreateLatest } from "@eternalfest/eternalfest-db/index";
import { Config, getLocalConfig } from "@eternalfest/native/config";
import { Database as NativeDatabase } from "@eternalfest/native/database";
import pg from "pg";

async function withTestDbPool<R>(handler: (pool: pg.Pool) => Promise<R>): Promise<R> {
  const config: Config = await getLocalConfig();

  const poolConfig: pg.PoolConfig = {
    user: config.db.user,
    password: config.db.password,
    host: config.db.host,
    port: config.db.port,
    database: config.db.name,
    max: 10,
    idleTimeoutMillis: 1000,
  };

  const pool: pg.Pool = new pg.Pool(poolConfig);

  let result: R;
  try {
    result = await handler(pool);
  } catch (err) {
    await pool.end();
    throw err;
  }
  await pool.end();
  return result;
}

async function withNativeDatabase<R>(handler: (db: NativeDatabase) => Promise<R>): Promise<R> {
  const config: Config = await getLocalConfig();

  const db = await NativeDatabase.create({
    host: config.db.host,
    port: config.db.port,
    name: config.db.name,
    user: config.db.user,
    password: config.db.password,
  });

  let result: R;
  try {
    result = await handler(db);
  } catch (err) {
    await db.close();
    throw err;
  }
  await db.close();
  return result;
}

export enum DbState {
  Void,
  Empty,
}

export async function withTestDb<R>(handler: (db: Database) => Promise<R>): Promise<R> {
  return withTestDbPool(async (pool: pg.Pool): Promise<R> => {
    const db: Database = new Database(pool);
    await forceCreateLatest(db);
    return handler(db);
  });
}

export interface StateService {
  db: Database;
  database: NativeDatabase;
  bufferRoot: Url;
}

export async function withTestState<R>(handler: (state: StateService) => Promise<R>): Promise<R> {
  return withTestDb(async (db: Database): Promise<R> => {
    return withNativeDatabase(async (database: NativeDatabase): Promise<R> => {
      return withTestFsBufferRoot(async (bufferRoot: Url): Promise<R> => {
        return handler({db, database, bufferRoot});
      });
    });
  });
}

async function withTestFsBufferRoot<R>(handler: (root: Url) => Promise<R>): Promise<R> {
  const config: Config = await getLocalConfig();
  const dataRoot: Url = config.data.root;
  return handler(dataRoot);
}
