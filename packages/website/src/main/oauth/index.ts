import { AuthContext as EtwinAuthContext } from "@eternal-twin/core/auth/auth-context";
import { AuthType } from "@eternal-twin/core/auth/auth-type";
import { EtwinClientService } from "@eternal-twin/core/etwin-client/service";
import { RfcOauthClient } from "@eternal-twin/oauth-client-http/rfc-oauth-client";
import { $AuthContext, AuthContext } from "@eternalfest/api-core/auth/auth-context";
import { AuthService } from "@eternalfest/api-core/auth/service";
import { OauthAccessToken } from "@eternalfest/api-core/oauth/oauth-access-token";
import { Session } from "@eternalfest/api-core/types/session";
import { ActorType } from "@eternalfest/api-core/types/user/actor-type";
import { KoaAuth, SESSION_COOKIE } from "@eternalfest/rest-server/koa-auth";
import Router  from "@koa/router";
import Koa from "koa";
import { JSON_VALUE_WRITER } from "kryo-json/json-value-writer";

export interface Api {
  auth: AuthService;
  etwinClient: EtwinClientService;
  koaAuth: KoaAuth;
  oauthClient: RfcOauthClient;
}

export function createOauthRouter(api: Api): Router {
  const router: Router = new Router();

  router.get("/callback", onAuthorizationGrant);

  async function onAuthorizationGrant(cx: Koa.Context): Promise<void> {
    if (cx.request.query.error !== undefined) {
      cx.response.body = {error: cx.request.query.error};
    }
    const code: unknown = cx.request.query.code;
    const state: unknown = cx.request.query.state;
    if (typeof code !== "string" || typeof state !== "string") {
      cx.response.body = {error: "InvalidRequest: Both `code` and `state` are required."};
      return;
    }

    let accessToken: OauthAccessToken;
    try {
      accessToken = await api.oauthClient.getAccessToken(code);
    } catch (err: unknown) {
      const e: any = err;
      console.error(e);
      console.log(e.status);
      console.log(e.response?.body);
      if (e.status === 401 && e.response.body && e.response.body && e.response.body.error === "Unauthorized") {
        cx.response.body = {
          error: "Unauthorized",
          cause: e.response.body.cause === "CodeExpiredError" ? "CodeExpiredError" : "AuthorizationServerError",
        };
        cx.response.status = 401;
      } else {
        console.error(e);
        cx.response.status = 503;
      }
      return;
    }

    const etwinAuth: EtwinAuthContext = await api.etwinClient.getAuthSelf(accessToken.accessToken);
    if (etwinAuth.type !== AuthType.AccessToken) {
      throw new Error(`UnexpectedEtwinAuthType: ${etwinAuth.type}`);
    }

    const auth: AuthContext = await api.auth.etwinOauth(etwinAuth.user.id, etwinAuth.user.displayName.current.value);
    switch (auth.type) {
      case ActorType.User: {
        const session: Session = await api.auth.createSession(auth, auth.user.id);
        cx.cookies.set(SESSION_COOKIE, session.id);
        cx.response.body = $AuthContext.write(JSON_VALUE_WRITER, auth);
        break;
      }
      default:
        throw new Error("UnexpectedAuthType");
    }

    cx.redirect("/");
  }

  return router;
}
