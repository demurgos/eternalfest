import { RfcOauthClient } from "@eternal-twin/oauth-client-http/rfc-oauth-client";
import { AuthService } from "@eternalfest/api-core/auth/service";
import { UserService } from "@eternalfest/api-core/user/service";
import { KoaAuth, SESSION_COOKIE } from "@eternalfest/rest-server/koa-auth";
import Router  from "@koa/router";
import Koa from "koa";

import { createLoginEtwinRouter } from "./login-etwin.js";

export interface Api {
  auth: AuthService;
  koaAuth: KoaAuth;
  oauthClient: RfcOauthClient;
  tokenSecret: Buffer;
  user: UserService;
}

export function createActionsRouter(api: Api): Router {
  const router: Router = new Router();

  router.post("/logout", deleteSession);

  async function deleteSession(cx: Koa.Context): Promise<void> {
    // let auth: AuthContext;
    // try {
    //   auth = await api.koaAuth.auth(ctx);
    // } catch (err) {
    //   console.error(err);
    //   auth = GUEST_AUTH_CONTEXT;
    // }

    // try {
    //   const sessionId: string | undefined = ctx.cookies.get(SESSION_COOKIE);
    //   if (sessionId !== undefined && $UuidHex.test(sessionId)) {
    //     await api.auth.deleteSessionById(auth, sessionId);
    //   }
    // } finally {
    cx.cookies.set(SESSION_COOKIE, undefined);
    cx.response.redirect("/");
    // }
  }

  const etwinRouter = createLoginEtwinRouter(api);
  router.use("/login/etwin", etwinRouter.routes());
  router.use("/login/etwin", etwinRouter.allowedMethods());

  return router;
}
