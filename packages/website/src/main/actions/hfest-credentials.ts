import { $HfestLogin, HfestLogin } from "@eternalfest/api-core/hfest-identity/hfest-login";
import { $HfestServer, HfestServer } from "@eternalfest/api-core/hfest-identity/hfest-server";
import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";
import { Ucs2StringType } from "kryo/ucs2-string";

export interface HfestCredentials {
  server: HfestServer;
  login: HfestLogin;
  password: string;
}

export const $HfestCredentials: RecordIoType<HfestCredentials> = new RecordType<HfestCredentials>({
  properties: {
    server: {type: $HfestServer},
    login: {type: $HfestLogin},
    password: {type: new Ucs2StringType({maxLength: 50})},
  },
  changeCase: CaseStyle.SnakeCase,
});
