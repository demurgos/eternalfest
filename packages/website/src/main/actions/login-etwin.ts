import { Url } from "@eternal-twin/core/core/url";
import { RfcOauthClient } from "@eternal-twin/oauth-client-http/rfc-oauth-client";
import { AuthService } from "@eternalfest/api-core/auth/service";
import { KoaAuth } from "@eternalfest/rest-server/koa-auth";
import Router  from "@koa/router";
import jsonWebToken from "jsonwebtoken";
import Koa from "koa";
import koaBodyParser from "koa-bodyparser";
import koaCompose from "koa-compose";

export interface Api {
  auth: AuthService;
  koaAuth: KoaAuth;
  oauthClient: RfcOauthClient;
  tokenSecret: Buffer,
}

export function createLoginEtwinRouter(api: Api): Router {
  const router: Router = new Router();

  router.post("/", koaCompose([koaBodyParser(), loginWithEtwin]));

  async function loginWithEtwin(cx: Koa.Context): Promise<void> {
    const csrfToken: string = await api.koaAuth.getOrCreateCsrfToken(cx);
    const payload = {
      authorizationServer: "eternal-twin.net",
      requestForgeryProtection: csrfToken,
    };
    const state: string = await jsonWebToken.sign(
      payload,
      api.tokenSecret,
      {
        algorithm: "HS256",
        expiresIn: "1d",
      },
    );

    const reqUrl: Url = await api.oauthClient.getAuthorizationUri("base", state);
    cx.response.redirect(reqUrl.toString());
  }

  return router;
}
