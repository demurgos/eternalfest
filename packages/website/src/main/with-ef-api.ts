import {EtwinClientService} from "@eternal-twin/core/etwin-client/service";
import {RfcOauthClient} from "@eternal-twin/oauth-client-http/rfc-oauth-client";
import {Database} from "@eternal-twin/pg-db";
import {AuthService} from "@eternalfest/api-core/auth/service";
import {BlobService} from "@eternalfest/api-core/blob/service";
import {FileService} from "@eternalfest/api-core/file/service";
import {Game2Service} from "@eternalfest/api-core/game2/service";
import {RunService} from "@eternalfest/api-core/run/service";
import {Url} from "@eternalfest/api-core/types/url";
import {UserService} from "@eternalfest/api-core/user/service";
import {Config, getLocalConfig} from "@eternalfest/local-config";
import {PgAuthStore} from "@eternalfest/native/auth-store";
import {PgBlobStore} from "@eternalfest/native/blob-store";
import {FsBufferStore} from "@eternalfest/native/buffer-store";
import {SystemClock} from "@eternalfest/native/clock";
import {Database as NativeDatabase} from "@eternalfest/native/database";
import {HttpEtwinClient} from "@eternalfest/native/etwin-client";
import {PgFileStore} from "@eternalfest/native/file-store";
import {NativeGameStore, PgGameStore} from "@eternalfest/native/game-store";
import {PgRunStore} from "@eternalfest/native/run-store";
import {NativeAuthService} from "@eternalfest/native/services/auth";
import {NativeFileService} from "@eternalfest/native/services/file";
import {NativeGameService} from "@eternalfest/native/services/game";
import {NativeRunService} from "@eternalfest/native/services/run";
import {NativeUserService} from "@eternalfest/native/services/user";
import {PgUserStore} from "@eternalfest/native/user-store";
import {Uuid4Generator} from "@eternalfest/native/uuid";
import {KoaAuth} from "@eternalfest/rest-server/koa-auth";
import pg from "pg";
import urljoin from "url-join";

export interface Api {
  auth: AuthService;
  blob: BlobService;
  etwinClient: EtwinClientService;
  file: FileService;
  game2: Game2Service;
  koaAuth: KoaAuth;
  oauthClient: RfcOauthClient;
  run: RunService;
  tokenSecret: Buffer;
  user: UserService;
}

export async function withEfApi<R>(handler: (api: Api) => Promise<R>): Promise<R> {
  // TODO: Only load config once (it is currently loaded again in `withEfState`)
  const config: Config = await getLocalConfig();

  return withEfState(async ({bufferRoot, database}): Promise<R> => {
    const clock = new SystemClock();
    const uuidGenerator = new Uuid4Generator();
    const bufferStore = await FsBufferStore.create({uuidGenerator, root: bufferRoot});
    const blobStore: PgBlobStore = await PgBlobStore.create({bufferStore, clock, database, uuidGenerator});


    const userStore = await PgUserStore.create({clock, database});
    const user = await NativeUserService.create({userStore});
    const fileStore: PgFileStore = await PgFileStore.create({blobStore, clock, database, uuidGenerator});
    const file: NativeFileService = await NativeFileService.create({blobStore, fileStore, userStore});
    const authStore = await PgAuthStore.create({clock, database, uuidGenerator});
    const runStore = await PgRunStore.create({clock, database, uuidGenerator});

    const etwinClient = await HttpEtwinClient.create({clock, root: config.etwin.uri.toString()});

    const auth: NativeAuthService = await NativeAuthService.create({clock, authStore, etwinClient, userStore});
    const gameStore: NativeGameStore = await PgGameStore.create({clock, database, uuidGenerator});
    const game2: NativeGameService = await NativeGameService.create({blobStore, clock, gameStore, userStore});
    const run = await NativeRunService.create({blobStore, clock, gameStore, runStore, userStore});
    const kAuth: KoaAuth = new KoaAuth(auth);
    const oauthClient: RfcOauthClient = new RfcOauthClient({
      authorizationEndpoint: new Url(urljoin(config.etwin.uri.toString(), "oauth/authorize")),
      tokenEndpoint: new Url(urljoin(config.etwin.uri.toString(), "oauth/token")),
      callbackEndpoint: new Url(urljoin(config.eternalfest.externalUri.toString(), "oauth/callback")),
      clientId: config.etwin.oauthClientId,
      clientSecret: config.etwin.oauthClientSecret,
    });
    const tokenSecret: Buffer = Buffer.from(config.eternalfest.secret);
    const api: Api = {
      auth,
      blob: blobStore,
      file,
      game2,
      koaAuth: kAuth,
      run,
      user,
      oauthClient,
      etwinClient,
      tokenSecret
    };
    return handler(api);
  });
}

export interface EfState {
  bufferRoot: Url;
  db: Database;
  database: NativeDatabase;
}

export async function withEfState<R>(handler: (state: EfState) => Promise<R>): Promise<R> {
  return withEfDb(async (db: Database): Promise<R> => {
    return withNativeDatabase(async (database: NativeDatabase): Promise<R> => {
      return withEfFsBufferRoot(async (root: Url): Promise<R> => {
        return handler({db, database, bufferRoot: root});
      });
    });
  });
}

async function withEfFsBufferRoot<R>(handler: (root: Url) => Promise<R>): Promise<R> {
  const {data} = await getLocalConfig();
  const fsBufferRoot: Url = data.root;
  return handler(fsBufferRoot);
}

export async function withEfDb<R>(handler: (db: Database) => Promise<R>): Promise<R> {
  return withEfDbPool(async (pool: pg.Pool): Promise<R> => {
    const db: Database = new Database(pool);
    return handler(db);
  });
}

async function withEfDbPool<R>(handler: (pool: pg.Pool) => Promise<R>): Promise<R> {
  const config: Config = await getLocalConfig();

  const poolConfig: pg.PoolConfig = {
    user: config.db.user,
    password: config.db.password,
    host: config.db.host,
    port: config.db.port,
    database: config.db.name,
    max: 10,
    idleTimeoutMillis: 1000,
  };

  const pool: pg.Pool = new pg.Pool(poolConfig);

  let result: R;
  try {
    result = await handler(pool);
  } catch (err) {
    await pool.end();
    throw err;
  }
  await pool.end();
  return result;
}

async function withNativeDatabase<R>(handler: (db: NativeDatabase) => Promise<R>): Promise<R> {
  const config: Config = await getLocalConfig();

  const db = await NativeDatabase.create({
    host: config.db.host,
    port: config.db.port,
    name: config.db.name,
    user: config.db.user,
    password: config.db.password,
  });

  let result: R;
  try {
    result = await handler(db);
  } catch (err) {
    await db.close();
    throw err;
  }
  await db.close();
  return result;
}
