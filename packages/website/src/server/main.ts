import "zone.js/node";
import "@angular/localize/init";

import { APP_BASE_HREF } from "@angular/common";
import { StaticProvider } from "@angular/core";
import { AuthContext } from "@eternalfest/api-core/auth/auth-context";
import { AuthScope } from "@eternalfest/api-core/auth/auth-scope";
import { ActorType } from "@eternalfest/api-core/types/user/actor-type";
import * as furi from "furi";
import Koa from "koa";
import Router = require("@koa/router");
import koaStaticCache from "koa-static-cache";

import { AppServerModule } from "../app/app.server.module";
import { ROUTES } from "../routes";
import { ServerAppConfig } from "./config";
import { NgKoaEngine } from "./ng-koa-engine";
import { AUTH_CONTEXT, EF_API } from "./tokens";

function resolveServerOptions(options?: Partial<ServerAppConfig>): ServerAppConfig {
  let isProduction: boolean = false;
  if (options === undefined) {
    options = {};
  } else {
    isProduction = options.isProduction === true;
  }
  const externalBaseUri: URL | undefined = options.externalBaseUri;
  const isIndexNextToServerMain: boolean = options.isIndexNextToServerMain === true;
  if (isProduction) {
    if (externalBaseUri === undefined) {
      throw new Error("Aborting: Missing server option `externalBaseUri` in production mode");
    }
    if (!isIndexNextToServerMain) {
      throw new Error("Aborting: Index.html must be located next to server's main in production mode");
    }
  }
  const efApi: any = options.efApi;
  return {externalBaseUri, isIndexNextToServerMain, isProduction, efApi};
}

/**
 * Resolves the fully qualified URL from the path and query
 */
function fullyQualifyUrl(options: ServerAppConfig, pathAndQuery: string): URL {
  if (options.externalBaseUri !== undefined) {
    return new URL(pathAndQuery, options.externalBaseUri);
  } else {
    return new URL(pathAndQuery, "http://localhost/");
  }
}

export async function app(options?: Partial<ServerAppConfig>): Promise<Koa> {
  const config: ServerAppConfig = resolveServerOptions(options);

  const serverDir = furi.fromSysPath(__dirname);
  const indexFuri = config.isIndexNextToServerMain
    ? furi.join(serverDir, "index.html")
    : furi.join(serverDir, "../../browser", furi.basename(serverDir), "index.html");
  const staticFuri = furi.join(serverDir, "../../browser", furi.basename(serverDir));
  const app = new Koa();

  if (config.efApi === undefined) {
    throw new Error("Server-Side rendering requires `efApi` option");
  }
  const efApi: any = config.efApi;

  const providers: StaticProvider[] = [];
  if (config.externalBaseUri !== undefined) {
    providers.push({provide: APP_BASE_HREF, useValue: config.externalBaseUri.toString()});
  }
  providers.push({provide: EF_API, useValue: efApi});

  const engine: NgKoaEngine = await NgKoaEngine.create({
    indexFuri,
    bootstrap: AppServerModule,
    providers,
    staticFuri,
  });

  const router = new Router();
  // TODO: Fix `koajs/router` type definitions to accept a readonly ROUTES.
  router.get([...ROUTES], ngRender);
  app.use(router.routes());
  app.use(router.allowedMethods());

  async function ngRender(cx: Koa.Context): Promise<void> {
    const GUEST_AUTH_CONTEXT: AuthContext = {
      type: ActorType.Guest,
      scope: AuthScope.Default,
    };

    let acx: AuthContext;
    try {
      acx = await efApi.koaAuth.auth(cx);
    } catch (err) {
      console.error(err);
      acx = GUEST_AUTH_CONTEXT;
    }
    const reqUrl: URL = fullyQualifyUrl(config, cx.request.originalUrl);
    cx.response.body = await engine.render({
      url: reqUrl,
      providers: [
        {provide: AUTH_CONTEXT, useValue: acx},
      ],
    });
  }

  if (!config.isIndexNextToServerMain) {
    const browserDir = furi.join(serverDir, "../../browser", furi.basename(serverDir));
    const ONE_DAY: number = 24 * 3600;
    app.use(koaStaticCache(furi.toSysPath(browserDir), {maxAge: ONE_DAY}));
  }

  return app;
}

// async function run() {
//   const port = process.env.PORT || 4000;
//
//   // Start up the Node server
//   const server = await app();
//   server.listen(port, () => {
//     console.log(`Node server listening on http://localhost:${port}`);
//   });
// }
//
// // Webpack will replace 'require' with '__webpack_require__'
// // '__non_webpack_require__' is a proxy to Node 'require'
// // The below code is to ensure that the server is run only when not requiring the bundle.
// declare const __non_webpack_require__: NodeRequire;
// const mainModule = __non_webpack_require__.main;
// const moduleFilename = mainModule && mainModule.filename || "";
// if (moduleFilename === __filename || moduleFilename.includes("iisnode")) {
//   run();
// }
