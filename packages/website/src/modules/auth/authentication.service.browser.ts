import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { TransferState } from "@angular/platform-browser";
import { $AuthContext, AuthContext, UserAuthContext } from "@eternalfest/api-core/auth/auth-context";
import { AuthScope } from "@eternalfest/api-core/auth/auth-scope";
import { ActorType } from "@eternalfest/api-core/types/user/actor-type";
import { JSON_VALUE_READER } from "kryo-json/json-value-reader";
import { BehaviorSubject, firstValueFrom, Observable } from "rxjs";

import { SelfService } from "../api/self.service";
import { Authentication } from "./authentication.service";
import { AUTH_CONTEXT_KEY, RawAuthContext } from "./state-keys";

const GUEST_AUTH_CONTEXT: AuthContext = {type: ActorType.Guest, scope: AuthScope.Default};

@Injectable()
export class BrowserAuthentication extends Authentication {
  private readonly http: HttpClient;
  private readonly self: SelfService;
  private readonly auth$: BehaviorSubject<AuthContext>;

  constructor(transferState: TransferState, http: HttpClient, self: SelfService) {
    super();
    const transferredAuth: RawAuthContext | undefined = transferState.get(AUTH_CONTEXT_KEY, undefined);
    if (transferredAuth !== undefined) {
      let auth: AuthContext = GUEST_AUTH_CONTEXT;
      try {
        auth = $AuthContext.read(JSON_VALUE_READER, transferredAuth);
      } catch (err) {
        console.error(err);
      }
      this.auth$ = new BehaviorSubject<AuthContext>(auth);
    } else {
      this.auth$ = new BehaviorSubject<AuthContext>(GUEST_AUTH_CONTEXT);
      firstValueFrom(self.getAuth()).then((auth) => this.auth$.next(auth));
    }
    this.http = http;
    this.self = self;
  }

  getAuth(): Observable<AuthContext> {
    return this.auth$;
  }

  async logout(): Promise<void> {
    this.auth$.next(GUEST_AUTH_CONTEXT);
  }
}
