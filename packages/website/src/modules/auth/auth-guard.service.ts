import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot } from "@angular/router";
import { Observable, of as rxOf } from "rxjs";

import { Authorization } from "./authorization.service";

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
  private authorization: Authorization;
  private router: Router;

  constructor(authorization: Authorization, router: Router) {
    this.authorization = authorization;
    this.router = router;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    // TODO(demurgos): /user/:id/settings
    if (state.url === "/settings") {
      return this.authorization.editUserSettings();
    }
    return rxOf(true);
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(route, state);
  }
}
