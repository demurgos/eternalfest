import { Injectable } from "@angular/core";
import { ActorType } from "@eternalfest/api-core/types/user/actor-type";
import { Observable } from "rxjs";
import { map as rxMap } from "rxjs/operators";

import { Authentication } from "./authentication.service";

@Injectable()
export class Authorization {
  private authentication: Authentication;

  constructor(authenticationService: Authentication) {
    this.authentication = authenticationService;
  }

  editUserSettings(): Observable<boolean> {
    return this.authentication.getAuth().pipe(rxMap((auth) => auth.type === ActorType.User));
  }
}
