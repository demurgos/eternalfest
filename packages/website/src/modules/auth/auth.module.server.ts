import { NgModule } from "@angular/core";

import { ServerApiModule } from "../api/api.module.server";
import { AuthGuard } from "./auth-guard.service";
import { Authentication } from "./authentication.service";
import { ServerAuthentication } from "./authentication.service.server";
import { Authorization } from "./authorization.service";

@NgModule({
  providers: [
    AuthGuard,
    {provide: Authentication, useClass: ServerAuthentication},
    Authorization,
  ],
  imports: [
    ServerApiModule,
  ],
})
export class ServerAuthModule {
}
