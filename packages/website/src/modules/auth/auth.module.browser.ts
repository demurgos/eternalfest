import { NgModule } from "@angular/core";

import { BrowserApiModule } from "../api/api.module.browser";
import { AuthGuard } from "./auth-guard.service";
import { Authentication } from "./authentication.service";
import { BrowserAuthentication } from "./authentication.service.browser";
import { Authorization } from "./authorization.service";

@NgModule({
  providers: [
    AuthGuard,
    {provide: Authentication, useClass: BrowserAuthentication},
    Authorization,
  ],
  imports: [
    BrowserApiModule,
  ],
})
export class BrowserAuthModule {
}
