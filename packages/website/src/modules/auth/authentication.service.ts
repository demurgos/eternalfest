import { Injectable } from "@angular/core";
import { AuthContext, UserAuthContext } from "@eternalfest/api-core/auth/auth-context";
import { Observable } from "rxjs";

@Injectable()
export abstract class Authentication {
  abstract getAuth(): Observable<AuthContext>;

  abstract logout(): Promise<void>;
}
