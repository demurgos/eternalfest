import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { GameModeKey } from "@eternalfest/api-core/game/game-mode-key";
import { $ShortGame, ShortGame } from "@eternalfest/api-core/game/short-game";
import { $Game, Game } from "@eternalfest/api-core/game2/game";
import { $ShortGameListing,ShortGameListing } from "@eternalfest/api-core/game2/short-game-listing";
import { $Leaderboard, Leaderboard } from "@eternalfest/api-core/leaderboard/leaderboard";
import { Url } from "@eternalfest/api-core/types/url";
import { ArrayIoType, ArrayType } from "kryo/array";
import { UuidHex } from "kryo/uuid-hex";
import { JSON_VALUE_READER } from "kryo-json/json-value-reader";
import { Observable } from "rxjs";
import { map as rxMap } from "rxjs/operators";

import {GameService, SetFavorite} from "./game.service";
import { apiUri } from "./utils/api-uri";

const $ShortGameArray: ArrayIoType<ShortGame> = new ArrayType({
  itemType: $ShortGame,
  maxLength: Infinity,
});

@Injectable()
export class BrowserGameService extends GameService {
  private http: HttpClient;

  constructor(http: HttpClient) {
    super();
    this.http = http;
  }

  getGames(page0: number, favorite: boolean): Observable<ShortGameListing> {
    return this.http
      .get(apiUri("games"), {withCredentials: true, params: {favorite, offset: page0 * 20, limit: 20}})
      .pipe(rxMap((raw) => $ShortGameListing.read(JSON_VALUE_READER, raw)));
  }

  async getGameById(gameId: UuidHex): Promise<Game | undefined> {
    return this.http
      .get(apiUri("games", gameId), {withCredentials: true})
      .pipe(rxMap((raw) => $Game.read(JSON_VALUE_READER, raw)))
      .toPromise();
  }

  async setFavorite(options: SetFavorite): Promise<null> {
    const reqBody: any = {favorite: options.favorite};
    await this.http.put(apiUri("games", options.game.id, "favorite"), reqBody).toPromise();
    return null;
  }

  async getLeaderboardByGameId(gameId: UuidHex, gameMode: GameModeKey): Promise<Leaderboard | undefined> {
    const leaderboardUri: Url = new Url(apiUri("games", gameId, "leaderboard"));
    leaderboardUri.searchParams.set("game_mode", gameMode);
    return this.http
      .get(leaderboardUri.toString(), {withCredentials: true})
      .pipe(rxMap((raw) => $Leaderboard.read(JSON_VALUE_READER, raw)))
      .toPromise();
  }
}
