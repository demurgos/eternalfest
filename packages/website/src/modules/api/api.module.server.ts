import { NgModule } from "@angular/core";

import { GameService } from "./game.service";
import { ServerGameService } from "./game.service.server";
import { RunService } from "./run.service";
import { ServerRunService } from "./run.service.server";
import { SelfService } from "./self.service";
import { ServerSelfService } from "./self.service.server";
import { UserService } from "./user.service";
import { ServerUserService } from "./user.service.server";

@NgModule({
  providers: [
    {provide: GameService, useClass: ServerGameService},
    {provide: RunService, useClass: ServerRunService},
    {provide: SelfService, useClass: ServerSelfService},
    {provide: UserService, useClass: ServerUserService},
  ],
})
export class ServerApiModule {
}
