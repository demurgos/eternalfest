import {Injectable} from "@angular/core";
import {RunItems} from "@eternalfest/api-core/run/run-items";
import {UpdateUserOptions} from "@eternalfest/api-core/user/update-user-options";
import {User} from "@eternalfest/api-core/user/user";
import {UuidHex} from "kryo/uuid-hex";

@Injectable()
export abstract class UserService {
  abstract getUserById(userId: UuidHex): Promise<User | undefined>;

  abstract getProfile(userId: UuidHex, gameId: UuidHex): Promise<RunItems | undefined>;

  abstract updateUser(options: UpdateUserOptions): Promise<User>;
}
