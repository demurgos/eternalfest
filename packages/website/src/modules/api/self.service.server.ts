import { Injectable } from "@angular/core";
import { AuthContext, UserAuthContext } from "@eternalfest/api-core/auth/auth-context";
import { Incident } from "incident";
import { Observable } from "rxjs";

import { SelfService } from "./self.service";

@Injectable()
export class ServerSelfService extends SelfService {
  getAuth(): Observable<AuthContext> {
    throw new Incident("NotSupported", {action: "ServerSelfService#getAuth"});
  }
}
