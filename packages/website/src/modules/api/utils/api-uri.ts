import { environment } from "../../../environments/environment";

export function apiUri(...components: string[]): string {
  return `${environment.apiBase}/${components.map(encodeURIComponent).join("/")}`;
}
