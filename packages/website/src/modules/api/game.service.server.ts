import { Inject, Injectable, NgZone } from "@angular/core";
import { AuthContext } from "@eternalfest/api-core/auth/auth-context";
import { GameModeKey } from "@eternalfest/api-core/game/game-mode-key";
import { Game } from "@eternalfest/api-core/game2/game";
import { ShortGameListing } from "@eternalfest/api-core/game2/short-game-listing";
import { Leaderboard } from "@eternalfest/api-core/leaderboard/leaderboard";
import { Api as EfApi } from "@eternalfest/rest-server/create-api-router";
import { Incident } from "incident";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";
import { from as rxFrom,Observable } from "rxjs";

import { AUTH_CONTEXT, EF_API } from "../../server/tokens";
import {GameService, SetFavorite} from "./game.service";
import { runUnzoned } from "./utils/run-unzoned.server";

@Injectable()
export class ServerGameService extends GameService {
  private readonly auth: AuthContext;
  private readonly efApi: EfApi;
  private readonly ngZone: NgZone;

  constructor(
    @Inject(AUTH_CONTEXT) auth: AuthContext,
    @Inject(EF_API) efApi: EfApi,
      ngZone: NgZone,
  ) {
    super();
    this.auth = auth;
    this.efApi = efApi;
    this.ngZone = ngZone;
  }

  getGames(page0: number, favorite: boolean): Observable<ShortGameListing> {
    return rxFrom(runUnzoned(this.ngZone, "ServerGameService#getGames", () => this._getGames(page0, favorite)));
  }

  async getGameById(gameId: UuidHex): Promise<Game | undefined> {
    return runUnzoned(this.ngZone, "ServerGameService#getGameById", () => this._getGameById(gameId));
  }

  async getLeaderboardByGameId(gameId: UuidHex, gameMode: GameModeKey): Promise<Leaderboard | undefined> {
    return runUnzoned(this.ngZone, "ServerGameService#getLeaderboardByGameId", () => this._getLeaderboardByGameId(gameId, gameMode));
  }

  async setFavorite(options: SetFavorite): Promise<null> {
    throw new Incident("NotSupported", {action: "ServerGameService#setFavorite"});
  }

  private async _getGames(page0: number, favorite: boolean): Promise<ShortGameListing> {
    const PER_PAGE = 20;
    const offset = page0 * PER_PAGE;
    return this.efApi.game2.getGames(this.auth, {favorite, offset, limit: PER_PAGE});
  }

  private async _getGameById(gameId: UuidHex): Promise<Game | undefined> {
    const error: Error | undefined = $UuidHex.testError(gameId);
    if (error !== undefined) {
      throw error;
    }
    const result = await this.efApi.game2.getGame(this.auth, {game: {id: gameId}, channelOffset: 0, channelLimit: 10});
    return result !== null ? result : undefined;
  }

  private async _getLeaderboardByGameId(gameId: UuidHex, gameMode: GameModeKey): Promise<Leaderboard | undefined> {
    const error: Error | undefined = $UuidHex.testError(gameId);
    if (error !== undefined) {
      throw error;
    }
    return this.efApi.run.getLeaderboard(this.auth, gameId, gameMode);
  }
}
