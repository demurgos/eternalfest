import { Injectable } from "@angular/core";
import { CreateRunOptions } from "@eternalfest/api-core/run/create-run-options";
import { Run } from "@eternalfest/api-core/run/run";
import { UuidHex } from "kryo/uuid-hex";

@Injectable()
export abstract class RunService {
  abstract createRun(options: CreateRunOptions): Promise<Run>;

  abstract getRunById(gameId: UuidHex): Promise<Run | undefined>;
}
