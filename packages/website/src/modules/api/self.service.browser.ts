import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { $AuthContext, $UserAuthContext, AuthContext } from "@eternalfest/api-core/auth/auth-context";
import { JSON_VALUE_READER } from "kryo-json/json-value-reader";
import { Observable } from "rxjs";
import { map as rxMap } from "rxjs/operators";

import { SelfService } from "./self.service";
import { apiUri } from "./utils/api-uri";

@Injectable()
export class BrowserSelfService extends SelfService {
  private http: HttpClient;

  constructor(http: HttpClient) {
    super();
    this.http = http;
  }

  getAuth(): Observable<AuthContext> {
    return this.http.get(apiUri("self", "auth"), {withCredentials: true})
      .pipe(
        rxMap((raw) => $AuthContext.read(JSON_VALUE_READER, raw)),
      );
  }
}
