import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {$RunItems,RunItems} from "@eternalfest/api-core/run/run-items";
import {$UpdateUserOptions, UpdateUserOptions} from "@eternalfest/api-core/user/update-user-options";
import {$User, User} from "@eternalfest/api-core/user/user";
import {UuidHex} from "kryo/uuid-hex";
import {JSON_VALUE_READER} from "kryo-json/json-value-reader";
import {JSON_VALUE_WRITER} from "kryo-json/json-value-writer";

import {UserService} from "./user.service";
import {apiUri} from "./utils/api-uri";

@Injectable()
export class BrowserUserService extends UserService {
  private http: HttpClient;

  constructor(http: HttpClient) {
    super();
    this.http = http;
  }

  async getUserById(userId: UuidHex): Promise<User | undefined> {
    const raw: any = await this.http.get(apiUri("users", userId)).toPromise();
    return $User.read(JSON_VALUE_READER, raw);
  }

  async getProfile(userId: UuidHex, gameId: UuidHex): Promise<RunItems | undefined> {
    const raw: any = await this.http.get(apiUri("users", userId, "profiles", gameId, "items")).toPromise();
    try {
      return $RunItems.read(JSON_VALUE_READER, raw);
    } catch (e) {
      console.error(e);
      return undefined;
    }
  }

  async updateUser(options: UpdateUserOptions): Promise<User> {
    const reqBody: any = $UpdateUserOptions.write(JSON_VALUE_WRITER, options);
    const raw: any = await this.http.patch(apiUri("users", options.userId!), reqBody).toPromise();
    return $User.read(JSON_VALUE_READER, raw);
  }
}
