import { NgModule } from "@angular/core";

import { GameService } from "./game.service";
import { BrowserGameService } from "./game.service.browser";
import { RunService } from "./run.service";
import { BrowserRunService } from "./run.service.browser";
import { SelfService } from "./self.service";
import { BrowserSelfService } from "./self.service.browser";
import { UserService } from "./user.service";
import { BrowserUserService } from "./user.service.browser";

@NgModule({
  providers: [
    {provide: GameService, useClass: BrowserGameService},
    {provide: RunService, useClass: BrowserRunService},
    {provide: SelfService, useClass: BrowserSelfService},
    {provide: UserService, useClass: BrowserUserService},
  ],
})
export class BrowserApiModule {
}
