import { Injectable } from "@angular/core";
import { AuthContext } from "@eternalfest/api-core/auth/auth-context";
import { Observable } from "rxjs";

@Injectable()
export abstract class SelfService {
  abstract getAuth(): Observable<AuthContext>;
}
