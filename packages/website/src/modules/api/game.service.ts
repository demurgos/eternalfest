import {Injectable} from "@angular/core";
import {GameModeKey} from "@eternalfest/api-core/game/game-mode-key";
import {Game} from "@eternalfest/api-core/game2/game";
import {GameIdRef} from "@eternalfest/api-core/game2/game-ref";
import {ShortGameListing} from "@eternalfest/api-core/game2/short-game-listing";
import {Leaderboard} from "@eternalfest/api-core/leaderboard/leaderboard";
import {UuidHex} from "kryo/uuid-hex";
import {Observable} from "rxjs";

export interface SetFavorite {
  game: GameIdRef;
  favorite: boolean;
}

@Injectable()
export abstract class GameService {
  abstract getGames(page0: number, favorite: boolean): Observable<ShortGameListing>;

  abstract getGameById(gameId: UuidHex): Promise<Game | undefined>;

  abstract setFavorite(options: SetFavorite): Promise<null>;

  abstract getLeaderboardByGameId(gameId: UuidHex, gameMode: GameModeKey): Promise<Leaderboard | undefined>;
}
