import { NgModule } from "@angular/core";

import { HfestUriPipe } from "./hfest-uri.pipe";
import { ScorePipe } from "./score.pipe";
import { ShortNumberPipe } from "./short-number.pipe";

@NgModule({
  declarations: [
    HfestUriPipe,
    ScorePipe,
    ShortNumberPipe,
  ],
  exports: [
    HfestUriPipe,
    ScorePipe,
    ShortNumberPipe,
  ],
})
export class HelpersModule {
}
