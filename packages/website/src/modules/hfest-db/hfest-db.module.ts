import { NgModule } from "@angular/core";

import { HfestItemNamePipe } from "./hfest-item-name.pipe";
import { HfestItemsDbService } from "./hfest-items-db.service";

@NgModule({
  declarations: [
    HfestItemNamePipe,
  ],
  exports: [
    HfestItemNamePipe,
  ],
  providers: [
    HfestItemsDbService,
  ],
})
export class HfestDbModule {
}
