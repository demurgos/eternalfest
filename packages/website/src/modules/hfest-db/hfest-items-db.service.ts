import { Inject, Injectable, LOCALE_ID } from "@angular/core";

interface DbItem {
  id: string;
  name: {
    fr: string;
    en: string;
    es: string;
  };
  // TODO(demurgos): Rename to unlock
  required?: number;
}

const itemsDb: Map<string, DbItem> = new Map();

declare function require(id: string): any;

// tslint:disable-next-line:no-var-requires
const DB = require("./items-db.json") as DbItem[];
for (const dbItem of DB) {
  itemsDb.set(dbItem.id, dbItem);
}

@Injectable()
export class HfestItemsDbService {
  readonly #localeId: "en" | "es" | "fr";

  constructor(@Inject(LOCALE_ID) localeId: string) {
    console.log(localeId);

    if (/^fr(?:-|$)/.test(localeId)) {
      this.#localeId = "fr";
    } else if (/^es(?:-|$)/.test(localeId)) {
      this.#localeId = "es";
    } else {
      this.#localeId = "en";
    }
  }

  hasItem(itemId: string): boolean {
    return itemsDb.has(itemId);
  }

  getItemName(itemId: string): string | undefined {
    const dbItem: DbItem | undefined = itemsDb.get(itemId);
    if (dbItem === undefined) {
      return undefined;
    }
    return dbItem.name[this.#localeId];
  }

  getItemRarity(itemId: number): number {
    // TODO(demurgos): Implement this function
    return 6;
  }

  getItemUnlock(itemId: number): number {
    const dbItem: DbItem | undefined = itemsDb.get("" + itemId);
    if (dbItem === undefined) {
      console.warn("Item not found: " + itemId);
      return 10;
    }
    return typeof dbItem.required === "number" ? dbItem.required : 10;
  }

  getItemFamilyId(itemId: number): number {
    // TODO(demurgos): Implement this function
    return 1;
  }
}
