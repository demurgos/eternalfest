import { NgModule } from "@angular/core";

import { ConsoleService } from "./console.service";

@NgModule({
  providers: [
    ConsoleService,
  ],
})
export class ConsoleModule {
}
