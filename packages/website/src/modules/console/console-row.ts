export interface ConsoleRowBase {
  type: "text" | "flash-object";
  level: "log" | "warning" | "error";
}

export interface TextRow extends ConsoleRowBase {
  type: "text";
  text: string;
}

export interface FlashObjectRow extends ConsoleRowBase {
  type: "flash-object";
  runtime: any;
  object: any;
}

export type ConsoleRow = TextRow | FlashObjectRow;
