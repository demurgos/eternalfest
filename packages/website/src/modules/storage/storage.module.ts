import { NgModule } from "@angular/core";

import { LocalStorageService } from "./local-storage.service";

@NgModule({
  providers: [
    {provide: LocalStorageService},
  ],
})
export class StorageModule {
}
