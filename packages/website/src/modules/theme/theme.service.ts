import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable()
export class ThemeService {
  private readonly goodbyeTheme$: BehaviorSubject<boolean>;

  constructor() {
    this.goodbyeTheme$ = new BehaviorSubject<boolean>(false);
  }

  enableGoodbyeTheme(): void {
    if (!this.goodbyeTheme$.value) {
      this.goodbyeTheme$.next(true);
    }
  }

  disableGoodbyeTheme(): void {
    if (this.goodbyeTheme$.value) {
      this.goodbyeTheme$.next(false);
    }
  }

  observeGoodbye(): Observable<boolean> {
    return this.goodbyeTheme$;
  }
}
