import {isPlatformBrowser} from "@angular/common";
import {Component, Inject, LOCALE_ID, OnInit, PLATFORM_ID} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {Game} from "@eternalfest/api-core/game2/game";
import { GameBuild } from "@eternalfest/api-core/game2/game-build";
import {ActorType} from "@eternalfest/api-core/types/user/actor-type";
import {Observable} from "rxjs";
import {map as rxMap} from "rxjs/operators";

import {apiUri} from "../../modules/api/utils/api-uri";
import {Authentication} from "../../modules/auth/authentication.service";
import {HfestItemsDbService} from "../../modules/hfest-db/hfest-items-db.service";
import {GameProfile} from "./games-routing.module";

export interface Item {
  id: string;
  image: string | null;
  displayName: string;
  count: number;
}

@Component({
  selector: "ef-profile",
  templateUrl: "./game-profile.component.html",
})
export class GameProfileComponent implements OnInit {
  readonly #isBrowser: boolean;
  readonly #localeId: string;
  readonly #authentication: Authentication;
  readonly #route: ActivatedRoute;
  readonly #hfestItemsDb: HfestItemsDbService;
  #customItemNamesById: Map<string, string> | null;


  public profile: GameProfile | undefined;
  public isAuthenticated$: Observable<boolean>;

  constructor(
    @Inject(PLATFORM_ID) platformId: Object,
    @Inject(LOCALE_ID) localeId: string,
      authentication: Authentication,
      route: ActivatedRoute,
      hfestItemsDb: HfestItemsDbService
  ) {
    this.#isBrowser = isPlatformBrowser(platformId);
    this.#localeId = localeId;
    this.#authentication = authentication;
    this.#route = route;
    this.#hfestItemsDb = hfestItemsDb;
    this.#customItemNamesById = null;

    this.profile = undefined;
    this.isAuthenticated$ = authentication.getAuth().pipe(
      rxMap((auth) => auth.type === ActorType.User),
    );
  }

  ngOnInit(): void {
    const routeData$: Observable<{ profile: GameProfile }> = this.#route.data as any;
    routeData$
      .subscribe(async (data: { profile: GameProfile }) => {
        this.profile = data.profile;
        this.#customItemNamesById = await this.loadCustomItemNames();
      });
  }

  getItems(): Item[] {
    const result: Item[] = [];
    if (this.profile !== undefined) {
      for (const [id, count] of this.profile.items) {
        result.push(this.getItem(id, count));
      }
    }
    result.sort(compareItemIds);
    return result;
  }

  private getItem(id: string, count: number): Item {
    let displayName: string = `#${id}`;
    let image: string | null = null;

    if (this.#hfestItemsDb.hasItem(id)) {
      displayName = this.#hfestItemsDb.getItemName(id)!;
      image = `/assets/items/${id}.png`;
    }

    const customName = this.#customItemNamesById?.get(id);
    if (customName !== undefined) {
      displayName = customName;
    }

    return { id, image, displayName, count };
  }

  private async loadCustomItemNames(): Promise<Map<string, string> | null> {
    if (!this.#isBrowser || this.profile === undefined) {
      // Don't load custom items from XML in SSR mode.
      return null;
    }

    const game: Game = this.profile.game;
    const gameBuild: GameBuild = game.channels.active.build;

    // Select which content file to load, depending on the locale.
    let contentBlobId = gameBuild.contentI18n?.id;
    const localizedI18n = gameBuild.i18n.get(this.#localeId);
    if (localizedI18n !== undefined && localizedI18n.contentI18n !== undefined) {
      contentBlobId = localizedI18n.contentI18n.id;
    }

    let isFullGameContent = false;
    if (contentBlobId === undefined) {
      if (gameBuild.content === null) {
        return null;
      }
      // Legacy fallback if no localized content exist.
      contentBlobId = gameBuild.content.id;
      isFullGameContent = true;
    }

    // Fetch and parse the game's contents
    try {
      const contentResp = await window.fetch(apiUri("blobs", contentBlobId, "raw"));
      if (!contentResp.ok) {
        throw new Error("Failed to fetch game contents: " + contentResp.statusText);
      }

      const parser = new DOMParser();
      const xml = parser.parseFromString(await contentResp.text(), "application/xml");

      if (isFullGameContent) {
        const langs = xml.getElementsByTagName("lang");
        return langs.length > 0 ? parseItemNamesFromXml(langs[0]) : null;
      } else {
        return parseItemNamesFromXml(xml.documentElement);
      }
    } catch (e) {
      console.error(e);
      return null;
    }
  }
}

function compareItemIds(a: Item, b: Item): number {
  const aNum = parseInt(a.id, 10);
  const bNum = parseInt(b.id, 10);
  if (!isNaN(aNum)) {
    if (!isNaN(bNum)) {
      const aHf = (0 <= aNum && aNum <= 117) || (1000 <= aNum && aNum <= 1238);
      const bHf = (0 <= bNum && bNum <= 117) || (1000 <= bNum && bNum <= 1238);
      if (aHf !== bHf) {
        return aHf ? 1 : -1;
      } else {
        return a < b ? -1 : 1;
      }
    } else {
      return -1;
    }
  } else {
    if (!isNaN(bNum)) {
      return 1;
    } else {
      return a < b ? -1 : 1;
    }
  }
}

function parseItemNamesFromXml(xml: Element): Map<string, string> {
  const customNames: Map<string, string> = new Map();

  const itemLists = xml.getElementsByTagName("items");
  // No iterators for XML collections :c
  for (let i = 0; i < itemLists.length; i++) {
    const items = itemLists[i].getElementsByTagName("item");

    for (let j = 0; j < items.length; j++) {
      const item = items[j];
      const id = item.getAttribute("id");
      const name = item.getAttribute("name");
      if (id !== null && name !== null) {
        customNames.set(id, name);
      }
    }
  }

  return customNames;
}
