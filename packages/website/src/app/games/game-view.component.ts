import {Component, NgZone, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthContext} from "@eternalfest/api-core/auth/auth-context";
import {GameModeKey} from "@eternalfest/api-core/game/game-mode-key";
import {GameModeState} from "@eternalfest/api-core/game/game-mode-state";
import {GameOptionKey} from "@eternalfest/api-core/game/game-option-key";
import {GameOptionState} from "@eternalfest/api-core/game/game-option-state";
import {Game} from "@eternalfest/api-core/game2/game";
import {GameBuild} from "@eternalfest/api-core/game2/game-build";
import {GameModeSpec} from "@eternalfest/api-core/game2/game-mode-spec";
import {GameIdRef} from "@eternalfest/api-core/game2/game-ref";
import {CreateRunOptions} from "@eternalfest/api-core/run/create-run-options";
import {Run} from "@eternalfest/api-core/run/run";
import {$RunSettings, RunSettings} from "@eternalfest/api-core/run/run-settings";
import {LocaleId} from "@eternalfest/api-core/types/locale-id";
import {ObjectType} from "@eternalfest/api-core/types/object-type";
import {ActorType} from "@eternalfest/api-core/types/user/actor-type";
import {firstValueFrom, Observable} from "rxjs";
import {map as rxMap} from "rxjs/operators";

import {GameService} from "../../modules/api/game.service";
import {RunService} from "../../modules/api/run.service";
import {Authentication} from "../../modules/auth/authentication.service";
import {LocalStorageKey, LocalStorageService} from "../../modules/storage/local-storage.service";

const GAME_SETTINGS_KEY: LocalStorageKey<RunSettings> = new LocalStorageKey("user/game-settings", $RunSettings);

@Component({
  selector: "ef-game-view",
  templateUrl: "./game-view.component.html",
  styleUrls: ["./game-view.component.scss"],
})
export class GameViewComponent implements OnInit {
  public GameModeState: typeof GameModeState = GameModeState;
  public GameOptionState: typeof GameOptionState = GameOptionState;
  public game: Game | undefined;

  public get gameModeDisplayName(): string | undefined {
    return this.gameMode !== undefined ? this.gameMode.displayName : undefined;
  }

  public set gameModeKey(newVal: string | undefined) {
    if (this.game !== undefined) {
      for (const [modeKey, modeSpec] of this.game.channels.active.build.modes) {
        if (modeKey === newVal) {
          const selectedOptions: Set<GameOptionKey> = new Set();
          for (const [optionKey, optionSpec] of modeSpec.options) {
            if (!optionSpec.isEnabled && optionSpec.defaultValue) {
              selectedOptions.add(optionKey);
            }
          }
          this.#modeKey = modeKey;
          this.gameMode = modeSpec;
          this.options = selectedOptions;
          return;
        }
      }
    }
    this.gameMode = undefined;
  }

  public get gameModeKey(): string | undefined {
    return this.#modeKey;
  }

  #modeKey: GameModeKey | undefined;
  public gameMode: GameModeSpec | undefined;
  public options: Set<GameOptionKey>;
  public detail: boolean;
  public shake: boolean;
  public sound: boolean;
  public music: boolean;
  public locale: LocaleId;
  public pending: boolean;
  public newPublicationDate: string;
  public isAdmin$: Observable<boolean>;
  public isAuthenticated$: Observable<boolean>;
  public favorite: boolean;
  public availableLocales: string[];

  readonly #authentication: Authentication;
  readonly #gameService: GameService;
  readonly #ngZone: NgZone;
  readonly #route: ActivatedRoute;
  readonly #router: Router;
  readonly #runService: RunService;
  readonly #localStorageService: LocalStorageService;

  constructor(
    authentication: Authentication,
    gameService: GameService,
    ngZone: NgZone,
    route: ActivatedRoute,
    router: Router,
    runService: RunService,
    localStorageService: LocalStorageService,
  ) {
    this.#authentication = authentication;
    this.#gameService = gameService;
    this.#ngZone = ngZone;
    this.#route = route;
    this.#router = router;
    this.#runService = runService;
    this.#localStorageService = localStorageService;
    this.isAdmin$ = authentication.getAuth()
      .pipe(rxMap((auth) => auth.type === ActorType.User && auth.isAdministrator));
    this.isAuthenticated$ = authentication.getAuth().pipe(
      rxMap((auth) => auth.type === ActorType.User),
    );

    this.pending = false;
    this.game = undefined;
    this.gameMode = undefined;
    this.options = new Set();
    this.detail = true;
    this.shake = true;
    this.sound = true;
    this.music = true;
    this.availableLocales = ["fr-FR"];
    this.locale = "fr-FR";
    this.favorite = false;
    this.newPublicationDate = (new Date()).toISOString();

    const settings: RunSettings | null = this.#localStorageService.read(GAME_SETTINGS_KEY);
    if (settings !== null) {
      this.detail = settings.detail;
      this.shake = settings.shake;
      this.sound = settings.sound;
      this.music = settings.music;
      this.locale = settings.locale ?? ("fr-FR" as const);
    }
  }

  ngOnInit(): void {
    const routeData$: Observable<{ game: Game }> = this.#route.data as any;
    routeData$
      .subscribe((data: { game: Game }) => {
        this.game = data.game;
        const build: GameBuild = data.game.channels.active.build;
        this.availableLocales = [build.mainLocale, ...build.i18n.keys()];
        console.log(build);
        console.log(this.availableLocales);
        for (const [modeKey, modeSpec] of this.game.channels.active.build.modes) {
          if (modeSpec.isVisible) {
            this.gameModeKey = modeKey;
            return;
          }
        }
        this.gameModeKey = undefined;
      });
  }

  async addToFavorite(event: Event): Promise<void> {
    event.preventDefault();
    const game: GameIdRef = {id: this.game!.id};
    try {
      await this.#gameService.setFavorite({game, favorite: true});
      this.favorite = true;
    } catch (err) {
      console.error(err);
    }
  }

  async onSubmit(event: Event): Promise<void> {
    event.preventDefault();
    const createRunOptions: CreateRunOptions = await this.read();
    if (this.pending) {
      return;
    }
    this.pending = true;
    this.#localStorageService.store(GAME_SETTINGS_KEY, createRunOptions.settings);
    try {
      const run: Run = await this.#runService.createRun(createRunOptions);
      await this.#ngZone.run(() => this.#router.navigateByUrl(`/runs/${run.id}`));
    } catch (err) {
      console.error(err);
    }
    this.pending = false;
  }

  onOptionChange(event: Event): void {
    event.preventDefault();
    const target: HTMLInputElement = event.target as any;
    if (target.checked) {
      this.options.add(target.value);
    } else {
      this.options.delete(target.value);
    }
  }

  private async read(): Promise<CreateRunOptions> {
    const auth: AuthContext = await firstValueFrom(this.#authentication.getAuth());
    if (auth.type !== ActorType.User) {
      throw new Error("NotAuthenticated");
    }
    return {
      user: {type: ObjectType.User, id: auth.user.id},
      game: {id: this.game!.id},
      gameMode: this.#modeKey!,
      gameOptions: [...this.options],
      channel: this.game!.channels.active.key,
      version: this.game!.channels.active.build.version,
      settings: {
        detail: this.detail,
        music: this.music,
        shake: this.shake,
        sound: this.sound,
        volume: 100,
        locale: this.locale,
      },
    };
  }
}
