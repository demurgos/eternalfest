import {Pipe, PipeTransform} from "@angular/core";
import {NullableBlob} from "@eternalfest/api-core/blob/blob";

import {apiUri} from "../../modules/api/utils/api-uri";

const DEFAULT_ICON_URI: string = "/assets/accounts/ef.x64.png";

@Pipe({
  name: "gameIconUri",
  pure: true,
})
export class GameIconUriPipe implements PipeTransform {
  transform(iconBlob: NullableBlob): string {
    if (iconBlob === null || iconBlob === undefined) {
      return DEFAULT_ICON_URI;
    }
    return apiUri("blobs", iconBlob.id, "raw");
  }
}
