import {Component, Input} from "@angular/core";
import {GameId} from "@eternalfest/api-core/game2/game-id";
import {ShortGame} from "@eternalfest/api-core/game2/short-game";
import {ShortGameChannel} from "@eternalfest/api-core/game2/short-game-channel";

@Component({
  selector: "ef-game-list-item",
  templateUrl: "./game-list-item.component.html",
})
export class GameListItemComponent {
  #game: ShortGame | null = null;

  @Input("game")
  set game(game: ShortGame | null | undefined) {
    if (game === undefined) {
      game = null;
    }
    this.#game = game;
    this.#refresh();
  }

  get game(): ShortGame | null {
    return this.#game;
  }

  gameId: GameId | null = null;
  channel: ShortGameChannel | null = null;
  channelChanged: boolean = false;

  constructor() {}

  #refresh() {
    if (this.#game !== null) {
      this.gameId = this.#game.id;
      this.channel = this.#game.channels.items[0];
      this.channelChanged = this.#game.channels.offset !== 0;
    } else {
      this.gameId = null;
      this.channel = null;
      this.channelChanged = false;
    }
  }
}
