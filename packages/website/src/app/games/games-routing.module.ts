import {Injectable, NgModule, NgZone} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, Router, RouterModule, RouterStateSnapshot, Routes} from "@angular/router";
import {UserId} from "@eternal-twin/core/user/user-id";
import {GameModeKey} from "@eternalfest/api-core/game/game-mode-key";
import {Game} from "@eternalfest/api-core/game2/game";
import {ShortGameListing} from "@eternalfest/api-core/game2/short-game-listing";
import {Leaderboard} from "@eternalfest/api-core/leaderboard/leaderboard";
import {RunItems} from "@eternalfest/api-core/run/run-items";
import {ActorType} from "@eternalfest/api-core/types/user/actor-type";
import {firstValueFrom, Observable} from "rxjs";
import {map as rxMap} from "rxjs/operators";

import {GameService} from "../../modules/api/game.service";
import {UserService} from "../../modules/api/user.service";
import {Authentication} from "../../modules/auth/authentication.service";
import {GameLeaderboardComponent} from "./game-leaderboard.component";
import {GameListComponent} from "./game-list.component";
import {GameProfileComponent} from "./game-profile.component";
import {GameViewComponent} from "./game-view.component";

@Injectable()
export class GameResolverService implements Resolve<Game> {
  private readonly game: GameService;
  private readonly ngZone: NgZone;
  private readonly router: Router;

  constructor(game: GameService, ngZone: NgZone, router: Router) {
    this.game = game;
    this.ngZone = ngZone;
    this.router = router;
  }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<Game | never> {
    const gameId: string | null = route.paramMap.get("game_id");
    if (gameId !== null) {
      const game: Game | undefined = await this.game.getGameById(gameId);
      if (game !== undefined) {
        return game;
      }
    }

    await this.ngZone.run(() => this.router.navigateByUrl("/games"));
    return undefined as never;
  }
}

export interface GameProfile {
  game: Game;
  items: RunItems;
}

@Injectable()
export class GameProfileResolverService implements Resolve<GameProfile> {
  readonly #authentication: Authentication;
  readonly #game: GameService;
  readonly #user: UserService;
  readonly #ngZone: NgZone;
  readonly #router: Router;

  constructor(authentication: Authentication, game: GameService, user: UserService, ngZone: NgZone, router: Router) {
    this.#authentication = authentication;
    this.#game = game;
    this.#user = user;
    this.#ngZone = ngZone;
    this.#router = router;
  }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<GameProfile | never> {
    const gameId: string | null = route.paramMap.get("game_id");
    if (gameId === null) {
      await this.#ngZone.run(() => this.#router.navigateByUrl("/games"));
      return undefined as never;
    }
    const userId$: Observable<UserId | undefined> = this.#authentication.getAuth().pipe(
      rxMap((auth) => auth.type === ActorType.User ? auth.user.id : undefined),
    );
    const userP = firstValueFrom(userId$);
    const gameP: Promise<Game | undefined> = this.#game.getGameById(gameId);
    const [user, game] = await Promise.all([userP, gameP]);
    if (user === undefined || game === undefined) {
      await this.#ngZone.run(() => this.#router.navigateByUrl("/games"));
      return undefined as never;
    }
    const items = await this.#user.getProfile(user, gameId);
    if (items === undefined) {
      await this.#ngZone.run(() => this.#router.navigateByUrl("/games"));
      return undefined as never;
    }
    return {game, items};
  }
}

@Injectable()
export class GamesResolverService implements Resolve<ShortGameListing> {
  readonly #game: GameService;
  readonly #router: Router;

  constructor(router: Router, game: GameService) {
    this.#router = router;
    this.#game = game;
  }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<ShortGameListing> {
    const favorite = route.queryParamMap.get("favorite");
    const pageStr = route.queryParamMap.get("p");
    const page: number = pageStr !== null ? parseInt(pageStr, 10) : 1;
    return firstValueFrom(this.#game.getGames(Math.max(0, page - 1), favorite === "true"));
  }
}

@Injectable()
export class GameLeaderboardResolverService implements Resolve<Leaderboard> {
  private readonly game: GameService;
  private readonly ngZone: NgZone;
  private readonly router: Router;

  constructor(game: GameService, ngZone: NgZone, router: Router) {
    this.ngZone = ngZone;
    this.router = router;
    this.game = game;
  }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<Leaderboard | never> {
    const gameId: string | null = route.paramMap.get("game_id");
    if (gameId !== null) {
      const game: Game | undefined = await this.game.getGameById(gameId);
      if (game !== undefined) {
        // TODO: Local tests have no mode, but have runs... So to remove once it's not possible to have no mode.
        // TODO: How to choose a different mode?
        const gameMode: GameModeKey = game.channels.active.build.modes.keys().next().value ?? "solo";
        const leaderboard: Leaderboard | undefined = await this.game.getLeaderboardByGameId(gameId, gameMode);
        if (leaderboard !== undefined) {
          return leaderboard;
        }
      }
    }

    await this.ngZone.run(() => this.router.navigateByUrl("/games/" + gameId + "/leaderboard"));
    return undefined as never;
  }
}

const routes: Routes = [
  {
    path: "",
    component: GameListComponent,
    pathMatch: "full",
    runGuardsAndResolvers: "paramsOrQueryParamsChange",
    resolve: {
      games: GamesResolverService,
    },
  },
  {
    path: ":game_id",
    component: GameViewComponent,
    pathMatch: "full",
    resolve: {
      game: GameResolverService,
    },
  },
  {
    path: ":game_id/leaderboard",
    component: GameLeaderboardComponent,
    pathMatch: "full",
    resolve: {
      leaderboard: GameLeaderboardResolverService,
    },
  },
  {
    path: ":game_id/profile",
    component: GameProfileComponent,
    pathMatch: "full",
    resolve: {
      profile: GameProfileResolverService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [GameResolverService, GamesResolverService, GameProfileResolverService, GameLeaderboardResolverService],
})
export class GamesRoutingModule {
}
