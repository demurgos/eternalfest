import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";

import {HelpersModule} from "../../modules/helpers/helpers.module";
import {HfestDbModule} from "../../modules/hfest-db/hfest-db.module";
import {GameIconUriPipe} from "./game-icon-uri.pipe";
import {GameLeaderboardComponent} from "./game-leaderboard.component";
import {GameListComponent} from "./game-list.component";
import {GameListItemComponent} from "./game-list-item.component";
import {GameModeNameComponent} from "./game-mode-name.component";
import {GameOptionNameComponent} from "./game-option-name.component";
import {GamePaginationComponent} from "./game-pagination.component";
import {GameProfileComponent} from "./game-profile.component";
import {GameViewComponent} from "./game-view.component";
import {GamesRoutingModule} from "./games-routing.module";

@NgModule({
  declarations: [
    GameIconUriPipe,
    GameLeaderboardComponent,
    GameProfileComponent,
    GameListComponent,
    GameListItemComponent,
    GameModeNameComponent,
    GameOptionNameComponent,
    GamePaginationComponent,
    GameViewComponent,
  ],
  imports: [
    HelpersModule,
    HfestDbModule,
    CommonModule,
    FormsModule,
    GamesRoutingModule,
  ],
})
export class GamesModule {
}
