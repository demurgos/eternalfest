import { Component, Input } from "@angular/core";

@Component({
  selector: "ef-game-mode-name",
  templateUrl: "./game-mode-name.component.html",
  styleUrls: ["./game-mode-name.component.scss"],
})
export class GameModeNameComponent {
  @Input("mode")
    mode: string | undefined;
}
