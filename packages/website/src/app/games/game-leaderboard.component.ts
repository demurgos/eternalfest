import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {Leaderboard} from "@eternalfest/api-core/leaderboard/leaderboard";
import {ActorType} from "@eternalfest/api-core/types/user/actor-type";
import {Observable} from "rxjs";
import {map as rxMap} from "rxjs/operators";

import {Authentication} from "../../modules/auth/authentication.service";

@Component({
  selector: "ef-leaderboard",
  templateUrl: "./game-leaderboard.component.html",
  styleUrls: ["./game-leaderboard.component.scss"],
})
export class GameLeaderboardComponent implements OnInit {
  private readonly authentication: Authentication;
  private readonly route: ActivatedRoute;
  public leaderboard: Leaderboard | undefined;
  public isAuthenticated$: Observable<boolean>;

  constructor(authentication: Authentication, route: ActivatedRoute) {
    this.authentication = authentication;
    this.route = route;
    this.leaderboard = undefined;
    this.isAuthenticated$ = authentication.getAuth().pipe(
      rxMap((auth) => auth.type === ActorType.User),
    );
  }

  ngOnInit(): void {
    const routeData$: Observable<{ leaderboard: Leaderboard }> = this.route.data as any;
    routeData$
      .subscribe((data: { leaderboard: Leaderboard }) => {
        this.leaderboard = data.leaderboard;
      });
  }
}
