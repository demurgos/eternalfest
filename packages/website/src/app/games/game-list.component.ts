import {Component} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {ShortGameListing} from "@eternalfest/api-core/game2/short-game-listing";
import {ActorType} from "@eternalfest/api-core/types/user/actor-type";
import {NEVER as RX_NEVER, Observable} from "rxjs";
import {map as rxMap} from "rxjs/operators";

import {Authentication} from "../../modules/auth/authentication.service";

@Component({
  selector: "ef-game-list",
  templateUrl: "./game-list.component.html",
})
export class GameListComponent {
  readonly #route: ActivatedRoute;
  readonly #authentication: Authentication;

  public games$: Observable<ShortGameListing>;
  public isAuthenticated$: Observable<boolean>;

  constructor(
    authentication: Authentication,
    route: ActivatedRoute,
  ) {
    this.#authentication = authentication;
    this.#route = route;
    this.games$ = RX_NEVER;
    this.isAuthenticated$ = RX_NEVER;
  }

  ngOnInit(): void {
    interface RouteData {
      games: ShortGameListing;
    }

    const routeData$: Observable<RouteData> = this.#route.data as any;
    this.games$ = routeData$.pipe(rxMap(({games}: RouteData) => games));
    this.isAuthenticated$ = this.#authentication.getAuth().pipe(
      rxMap((auth) => auth.type === ActorType.User),
    );
  }
}
