import {CommonModule} from "@angular/common";
import {HttpClientModule} from "@angular/common/http";
import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";

import {ConsoleModule} from "../modules/console/console.module";
import {HfestDbModule} from "../modules/hfest-db/hfest-db.module";
import {ThemeModule} from "../modules/theme/theme.module";
import {AppComponent} from "./app.component";
import {AppRoutingModule} from "./app-routing.module";
import {HomeView} from "./home/home.component";

@NgModule({
  imports: [
    AppRoutingModule,
    BrowserModule.withServerTransition({appId: "eternalfest"}),
    CommonModule,
    ConsoleModule,
    HfestDbModule,
    HttpClientModule,
    ThemeModule,
  ],
  declarations: [AppComponent, HomeView],
  exports: [AppComponent],
})
export class AppModule {
}
