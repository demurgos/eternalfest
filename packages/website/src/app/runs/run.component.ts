import { Component, NgZone, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthContext } from "@eternalfest/api-core/auth/auth-context";
import { CreateRunOptions } from "@eternalfest/api-core/run/create-run-options";
import { Run } from "@eternalfest/api-core/run/run";
import {ObjectType} from "@eternalfest/api-core/types/object-type";
import { ActorType } from "@eternalfest/api-core/types/user/actor-type";
import { firstValueFrom, Observable, Subscription } from "rxjs";

import { RunService } from "../../modules/api/run.service";
import { Authentication } from "../../modules/auth/authentication.service";
import { ThemeService } from "../../modules/theme/theme.service";

@Component({
  selector: "ef-run",
  templateUrl: "./run.component.html",
  styleUrls: ["./run.component.scss"],
})
export class RunComponent implements OnInit, OnDestroy {
  public run: Run | undefined;
  public pending: boolean;

  private readonly authentication: Authentication;
  private readonly ngZone: NgZone;
  private readonly route: ActivatedRoute;
  private readonly router: Router;
  private readonly runService: RunService;
  private readonly themeService: ThemeService;

  private readonly subscriptions: Subscription[];

  constructor(
    authentication: Authentication,
    ngZone: NgZone,
    route: ActivatedRoute,
    router: Router,
    runService: RunService,
    themeService: ThemeService,
  ) {
    this.authentication = authentication;
    this.ngZone = ngZone;
    this.route = route;
    this.router = router;
    this.runService = runService;
    this.themeService = themeService;
    this.subscriptions = [];

    this.run = undefined;
    this.pending = false;
  }

  ngOnInit(): void {
    const routeData$: Observable<{run: Run}> = this.route.data as any;
    this.subscriptions.push(routeData$
      .subscribe((data: {run: Run}) => {
        // if (data.run.game.displayName === "Goodbye") {
        //   this.themeService.enableGoodbyeTheme();
        // } else if (this.run !== undefined) {
        //   this.themeService.disableGoodbyeTheme();
        // }
        this.run = data.run;
      }));
  }

  ngOnDestroy(): void {
    // if (this.run !== undefined && this.run.game.displayName === "Goodbye") {
    //   this.themeService.disableGoodbyeTheme();
    // }
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
    this.subscriptions.length = 0;
  }

  async onSubmit(event: Event): Promise<void> {
    event.preventDefault();
    const createRunOptions: CreateRunOptions = await this.read();
    if (this.pending) {
      return;
    }
    this.pending = true;
    try {
      const run: Run = await this.runService.createRun(createRunOptions);
      await this.ngZone.run(() => this.router.navigateByUrl(`/runs/${run.id}`));
    } catch (err) {
      // this.errorMessage = err.message;
      console.error(err);
    }
    this.pending = false;
  }

  public sumScores(scores: readonly number[]): number {
    let result: number = 0;
    for (const score of scores) {
      result += score;
    }
    return result;
  }

  private async read(): Promise<CreateRunOptions> {
    const auth: AuthContext = await firstValueFrom(this.authentication.getAuth());
    if (auth.type !== ActorType.User) {
      throw new Error("NotAuthenticated");
    }
    return {
      user: {type: ObjectType.User, id: auth.user.id},
      game: {id: this.run!.game.id},
      gameMode: this.run!.gameMode,
      gameOptions: [...this.run!.gameOptions],
      channel: "main",
      version: "1.0.0",
      settings: {
        detail: this.run!.settings.detail,
        music: this.run!.settings.music,
        shake: this.run!.settings.shake,
        sound: this.run!.settings.sound,
        volume: this.run!.settings.volume,
        locale: this.run!.settings.locale,
      },
    };
  }
}
