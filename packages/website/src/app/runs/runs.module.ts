import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { HelpersModule } from "../../modules/helpers/helpers.module";
import { GameComponent } from "./game.component";
import { RunComponent } from "./run.component";
import { RunsRoutingModule } from "./runs-routing.module";

@NgModule({
  declarations: [
    GameComponent,
    RunComponent,
  ],
  imports: [
    HelpersModule,
    CommonModule,
    FormsModule,
    RunsRoutingModule,
  ],
})
export class RunsModule {
}
