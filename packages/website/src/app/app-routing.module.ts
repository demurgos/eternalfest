import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { AuthGuard } from "../modules/auth/auth-guard.service";
import { HomeView } from "./home/home.component";

const routes: Routes = [
  {path: "", component: HomeView, pathMatch: "full"},
  {path: "login", loadChildren: () => import("./auth/auth.module").then((m) => m.AuthModule)},
  {path: "games", loadChildren: () => import("./games/games.module").then((m) => m.GamesModule)},
  {path: "runs", loadChildren: () => import("./runs/runs.module").then((m) => m.RunsModule)},
  {
    path: "settings",
    canActivate: [AuthGuard],
    loadChildren: () => import("./settings/settings.module").then((m) => m.SettingsModule),
  },
  {path: "users", loadChildren: () => import("./users/users.module").then((m) => m.UsersModule)},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
