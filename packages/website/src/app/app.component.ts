import { Component, HostBinding } from "@angular/core";
import { Router } from "@angular/router";
import { ActorType } from "@eternalfest/api-core/types/user/actor-type";
import { $Boolean } from "kryo/boolean";
import { UuidHex } from "kryo/uuid-hex";
import { Observable, of as rxOf } from "rxjs";
import { map as rxMap } from "rxjs/operators";

import { Authentication } from "../modules/auth/authentication.service";
import { LocalStorageKey, LocalStorageService } from "../modules/storage/local-storage.service";
import { ThemeService } from "../modules/theme/theme.service";

const DISABLE_SNOW_KEY: LocalStorageKey<boolean> = new LocalStorageKey("app/snow", $Boolean);

@Component({
  selector: "ef-app",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  public readonly localStorageService: LocalStorageService;
  public readonly themeService: ThemeService;
  public isAuthenticated$: Observable<boolean>;
  public userId$: Observable<UuidHex | undefined>;
  public userDisplayName$: Observable<string | undefined>;

  @HostBinding("class.theme-goodbye")
  public isGoodbye: boolean = false;

  @HostBinding("class.theme-xmas")
  public isXmas: boolean = false;

  @HostBinding("class.theme-xmas-snow")
  public isXmasSnow: boolean;

  constructor(
    authentication: Authentication,
    localStorageService: LocalStorageService,
    router: Router,
    themeService: ThemeService,
  ) {
    this.localStorageService = localStorageService;
    const storedDisableSnow: boolean | null = localStorageService.read(DISABLE_SNOW_KEY);
    this.isXmasSnow = storedDisableSnow !== null ? !storedDisableSnow : true;

    this.isAuthenticated$ = authentication.getAuth().pipe(
      rxMap((auth) => auth.type === ActorType.User),
    );
    this.userId$ = authentication.getAuth().pipe(
      rxMap((auth) => auth.type === ActorType.User ? auth.user.id : undefined),
    );
    this.userDisplayName$ = authentication.getAuth().pipe(
      rxMap((auth) => auth.type === ActorType.User ? auth.user.displayName : undefined),
    );

    this.themeService = themeService;

    this.themeService.observeGoodbye().subscribe((isEnabled: boolean): void => {
      // TODO: Find a proper fix for `ExpressionChangedAfterItHasBeenCheckedError` instead of
      //       wrapping in an immediate timeout
      setTimeout(() => {
        this.isGoodbye = isEnabled;
      }, 0);
    });
  }

  public toggleSnow(): void {
    this.isXmasSnow = !this.isXmasSnow;
    this.localStorageService.store(DISABLE_SNOW_KEY, !this.isXmasSnow);
  }
}
