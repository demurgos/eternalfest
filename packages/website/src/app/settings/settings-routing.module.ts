import { Injectable, NgModule, NgZone } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, Router, RouterModule, RouterStateSnapshot, Routes } from "@angular/router";
import { AuthContext } from "@eternalfest/api-core/auth/auth-context";
import { ActorType } from "@eternalfest/api-core/types/user/actor-type";
import { User } from "@eternalfest/api-core/user/user";
import { EMPTY as RX_EMPTY, from as rxFrom, Observable, of as rxOf } from "rxjs";
import { first as rxFirst, flatMap as rxFlatMap } from "rxjs/operators";

import { UserService } from "../../modules/api/user.service";
import { Authentication } from "../../modules/auth/authentication.service";
import { SettingsComponent } from "./settings.component";

@Injectable()
export class SelfUserResolverService implements Resolve<User> {
  private readonly auth: Authentication;
  private readonly ngZone: NgZone;
  private readonly router: Router;
  private readonly user: UserService;

  constructor(auth: Authentication, ngZone: NgZone, router: Router, user: UserService) {
    this.auth = auth;
    this.ngZone = ngZone;
    this.router = router;
    this.user = user;
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User | never> {
    return this.auth.getAuth().pipe(
      rxFirst(),
      rxFlatMap((auth: AuthContext): Observable<User | undefined> => {
        if (auth.type === ActorType.User) {
          return rxFrom(this.user.getUserById(auth.user.id));
        } else {
          return rxOf(undefined);
        }
      }),
      rxFlatMap((user: User | undefined): Observable<User | never> => {
        if (user !== undefined) {
          return rxOf(user);
        } else {
          return rxFrom(this.ngZone.run(() => this.router.navigateByUrl("/")))
            .pipe(rxFlatMap(() => RX_EMPTY));
        }
      }),
    );
  }
}

const routes: Routes = [
  {
    path: "",
    component: SettingsComponent,
    pathMatch: "full",
    resolve: {
      user: SelfUserResolverService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [SelfUserResolverService],
})
export class SettingsRoutingModule {
}
