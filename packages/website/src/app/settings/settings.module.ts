import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { HelpersModule } from "../../modules/helpers/helpers.module";
import { SettingsComponent } from "./settings.component";
import { SettingsRoutingModule } from "./settings-routing.module";

@NgModule({
  declarations: [
    SettingsComponent,
  ],
  imports: [
    CommonModule,
    HelpersModule,
    SettingsRoutingModule,
  ],
})
export class SettingsModule {
}
