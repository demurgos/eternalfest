import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { HelpersModule } from "../../modules/helpers/helpers.module";
import { HfestDbModule } from "../../modules/hfest-db/hfest-db.module";
import { HfestIdentityComponent } from "./hfest-identity.component";
import { ItemComponent } from "./item.component";
import { UserComponent } from "./user.component";
import { UsersRoutingModule } from "./users-routing.module";

@NgModule({
  declarations: [HfestIdentityComponent, ItemComponent, UserComponent],
  imports: [
    CommonModule,
    HelpersModule,
    HfestDbModule,
    UsersRoutingModule,
  ],
})
export class UsersModule {
}
