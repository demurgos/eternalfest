import {IoType} from "kryo";

export interface SimpleRequestOptions<Res> {
  queryType?: undefined,
  query?: undefined,
  reqType?: undefined,
  req?: undefined,
  resType: IoType<Res>,
}

export interface QueryRequestOptions<Query, Res> {
  queryType: IoType<Query>,
  query: Query,
  reqType?: undefined,
  req?: undefined,
  resType: IoType<Res>,
}

export interface BodyRequestOptions<Req, Res> {
  queryType?: undefined,
  query?: undefined,
  reqType: IoType<Req>,
  req: Req,
  resType: IoType<Res>,
}

export interface CompleteRequestOptions<Query, Req, Res> {
  queryType: IoType<Query>,
  query: Query,
  reqType: IoType<Req>,
  req: Req,
  resType: IoType<Res>,
}

export type RequestOptions<Query, Req, Res> =
  SimpleRequestOptions<Res>
  | QueryRequestOptions<Query, Res>
  | BodyRequestOptions<Req, Res>
  | CompleteRequestOptions<Query, Req, Res>;
