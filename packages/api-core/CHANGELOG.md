# 0.15.2 (2021-03-29)

- **[Fix]** Republished due to borked previous publish.

# 0.15.0 (2021-03-29)

- **[Breaking change]** Use Eternal-Twin's `UserDisplayName` type.

# 0.14.0 (2020-06-01)

- **[Breaking change]** Update to `kryo@0.11`.

# 0.13.0 (2020-04-27)

- **[Breaking change]** Use native ESM.

# 0.12.2 (2020-01-24)

- **[Feature]** Add wiki types.

# 0.12.1 (2019-12-31)

- **[Fix]** Update dependencies.

# 0.12.0 (2019-12-04)

- **[Breaking change]** Add `getUserRefById` and `getUserRefsById`.
- **[Breaking change]** Rename `Unauthorized` error to `Unauthenticated`.
- **[Feature]** Add `Forbidden` error.

# 0.11.2 (2019-10-23)

- **[Fix]** Allow spaces, parenthesis and underscores in user display names.

# 0.11.1 (2019-10-22)

- **[Feature]** Add locale support for game updates.

# 0.11.0 (2019-10-22)

- **[Breaking change]** Return a list of `ShortGame` for `.getGames`.
- **[Feature]** Add locale support for games.

# 0.10.0 (2019-08-22)

- **[Breaking change]** Add `items` to `RunStart`.

# 0.9.1 (2019-05-17)

- **[Fix]** Change `GameDisplayName` and `GameDescription` min length to `1`

# 0.9.0 (2019-05-16)

- **[Breaking change]** Replace `UuidHex` with `IdRef` for resources.
- **[Breaking change]** Rename `tag` to `key` in game modes and options.
- **[Feature]** Add `game` and `user` fields to `Run`.
- **[Feature]** Add `stats` field to run results.

# 0.8.0 (2019-05-11)

- **[Breaking change]** Refactor file service.
- **[Breaking change]** Replace `AsyncIterable` with `Promise<ReadonlyArray>`.
- **[Fix]** Use `Sint32` for run scores.
- **[Fix]** Replace `AsyncIterable` with `Promise<ReadonlyArray>`.
- **[Fix]** Update dependencies.

# 0.7.0 (2019-01-09)

- **[Breaking change]** Refactor blob service.
- **[Breaking change]** Refactor game updates.

# 0.6.0 (2019-01-03)

- **[Breaking change]** Update `RunResult` to have scores, max level and items.

# 0.5.1 (2018-12-31)

- **[Feature]** Add `setRunResult` to `RunService`.
- **[Fix]** Use integer for `RunSettings.volume`.

# 0.5.0 (2018-12-31)

- **[Breaking change]** Refactor `run` structure.
- **[Feature]** Add support for run results.
- **[Fix]** Add `volume` to `RunSettings`.

# 0.4.4 (2018-12-23)

- **[Fix]** Fix incomplete `$UpdateUserOptions`.

# 0.4.3 (2018-12-22)

- **[Feature]** Add `AuthService`.

# 0.4.2 (2018-12-20)

- **[Fix]** Fix game decription max length.

# 0.4.1 (2018-12-20)

- **[Fix]** Add `gameId` to `UpdateGameOptions`.

# 0.4.0 (2018-12-20)

- **[Breaking change]** Refactor lib structure.
- **[Feature]** Add `isTester` to `UpdateUserOptions`.
- **[Feature]** Add `GameService#updateGame`.

# 0.3.0 (2018-12-18)

- **[Breaking Change]** Refactor hfest-identify, password and user services.
- **[Internal]** Add tests for user login.

# 0.2.4 (2018-12-18)

- **[Fix]** Fix out case of file interfaces.
- **[Fix]** Allow dots in media types.
- **[Fix]** Increase game description length to 1000 characters.

# 0.2.3 (2018-12-16)

- **[Feature]** Add run service.

# 0.2.2 (2018-12-15)

- **[Fix]** Fix `$MediaType` pattern.
- **[Fix]** Fix game families type.
- **[Fix]** Fix nullable `Game#publicAccessFrom` read.
- **[Internal]** Update dependencies.

# 0.2.1 (2018-12-15)

- **[Feature]** Add `kryo` types for game resources and service.
- **[Feature]** Add `kryo` types for blob service.
- **[Feature]** Add `kryo` types for file service.

# 0.2.0 (2018-12-11)

- **[Breaking change]** Rename `HammerfestIdentity` to `HfestIdentity`.
- **[Breaking change]** Use strings for Hammerfest identifiers.
- **[Breaking change]** Change method to create user from Hammerfest.
- **[Feature]** Add `kryo` types for `AuthContext`.
- **[Feature]** Add `SystemAuthContext`.
- **[Feature]** Add `SessionService`.
- **[Fix]** Type serializable documents as `RecordIoType`.

# 0.1.0 (2018-12-06)

- **[Feature]** First Release.
