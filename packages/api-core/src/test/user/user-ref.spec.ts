import {ObjectType} from "../../lib/types/object-type.js";
import {$UserRef} from "../../lib/user/user-ref.js";
import {testKryoType} from "../test-kryo.js";

describe("UserRef", function () {
  testKryoType({
    type: $UserRef,
    valid: [
      {
        value: {
          type: ObjectType.User,
          id: "00000000-0000-0000-0000-000000000000",
          displayName: "NotDemurgosForOnce",
        },
        // tslint:disable-next-line:max-line-length
        rawJson: "{\"type\":\"User\",\"id\":\"00000000-0000-0000-0000-000000000000\",\"display_name\":\"NotDemurgosForOnce\"}",
      },
    ],
    invalid: [],
  });
});
