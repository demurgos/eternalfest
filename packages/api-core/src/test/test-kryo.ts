import chai from "chai";
import fs from "fs";
import furi from "furi";
import { IoType,Type } from "kryo";
import { JSON_READER } from "kryo-json/json-reader";
import { PRETTY_JSON_WRITER } from "kryo-json/json-writer";
import objectInspect from "object-inspect";

const TEST_ROOT = furi.join(import.meta.url, "../../../../test-resources/core");

export interface InvalidValue {
  readonly value: any;
  readonly name?: string;
}

export interface ValidValue<T> {
  readonly value: T;
  readonly rawJson?: string;
  readonly name?: string;
}

export interface TestKryoTypeOptions<T> {
  readonly type: Type<T>;
  readonly valid: ReadonlyArray<ValidValue<T>>;
  readonly invalid: ReadonlyArray<InvalidValue>;
}

export function testKryoType<T = unknown>(options: TestKryoTypeOptions<T>): void {
  for (const valid of options.valid) {
    const name: string = valid.name !== undefined ? valid.name : objectInspect(valid.value);
    it(`accepts: ${name}`, () => {
      const err: Error | undefined = options.type.testError!(valid.value);
      if (err !== undefined) {
        throw err;
      }
    });

    if (valid.rawJson !== undefined) {
      it(`reads raw JSON: ${name}`, () => {
        const actual: T = options.type.read!(JSON_READER, valid.rawJson);
        chai.assert.isTrue(options.type.equals(actual, valid.value));
      });
    }
  }

  for (const invalid of options.invalid) {
    const name: string = invalid.name !== undefined ? invalid.name : objectInspect(invalid.value);
    it(`rejects: ${name}`, () => {
      chai.assert.isFalse(options.type.test(invalid.value));
    });
  }
}

export function registerJsonIoTests<T>(type: IoType<T>, group: string, items: ReadonlyMap<string, T>): void {
  const groupeUri = furi.join(TEST_ROOT, group);
  const actualItems: Set<string> = new Set();
  for (const ent of fs.readdirSync(groupeUri, {withFileTypes: true})) {
    if (!ent.isDirectory() || ent.name.startsWith(".")) {
      continue;
    }
    const name = ent.name;
    actualItems.add(name);
    const value: T | undefined = items.get(name);
    if (value === undefined) {
      throw new Error(`ValueNotFound: ${group} -> ${name}`);
    }
    const valuePath = furi.join(groupeUri, name, "value.json");
    it (`Reads ${name}`, () => {
      const valueJson: string = fs.readFileSync(valuePath, {encoding: "utf-8"});
      const actual = type.read(JSON_READER, valueJson);
      chai.assert.deepEqual(actual, value);
    });

    it (`Writes ${name}`, () => {
      const expected: string = fs.readFileSync(valuePath, {encoding: "utf-8"});
      const actual = type.write(PRETTY_JSON_WRITER, value);
      chai.assert.deepEqual(actual, expected);
    });
  }
  const extraValues: Set<string> = new Set();
  for (const name of items.keys()) {
    if (!actualItems.has(name)) {
      extraValues.add(name);
    }
  }
  if (extraValues.size > 0) {
    throw new Error(`ExtraValues: ${group} -> ${[...extraValues].join(", ")}`);
  }
}
