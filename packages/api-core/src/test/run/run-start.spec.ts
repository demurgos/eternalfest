import { $RunStart } from "../../lib/run/run-start.js";
import { testKryoType } from "../test-kryo.js";

describe("RunStart", function () {
  testKryoType({
    type: $RunStart,
    valid: [
      {
        value: {
          run: {id: "00000000-0000-0000-0000-000000000000"},
          key: "00000000-0000-0000-0000-000000000000",
          families: "100,101",
          items: new Map([["1000", 1]]),
        },
        // tslint:disable-next-line:max-line-length
        rawJson: "{\"run\":{\"id\":\"00000000-0000-0000-0000-000000000000\"},\"key\":\"00000000-0000-0000-0000-000000000000\",\"families\":\"100,101\",\"items\":{\"1000\":1}}",
      },
    ],
    invalid: [],
  });
});
