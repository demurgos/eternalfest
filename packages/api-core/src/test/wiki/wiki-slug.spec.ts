import chai from "chai";
import { JSON_READER } from "kryo-json/json-reader";

import { $GameDisplayName } from "../../lib/game/game-display-name.js";
import { $WikiSlug, WikiSlug } from "../../lib/wiki/wiki-slug.js";

describe("WikiSlug", function () {
  describe("invalid", function () {
    interface Item {
      name?: string;
      value: WikiSlug;
    }

    const items: Item[] = [
      {value: "/"},
      {value: "/a"},
      {value: "a/"},
      {value: "/a/"},
      {value: "A"},
      {value: "A/B"},
      {value: "a/B"},
      {value: "@"},
      {value: "%"},
      {name: "101 (max + 1) letters", value: new Array(101).fill("a").join("")},
    ];

    for (const item of items) {
      const valueStr: string = JSON.stringify(item.value);
      it(`should reject ${valueStr}: ${item.name ?? valueStr}`, function () {
        chai.assert.throw(() => $GameDisplayName.read(JSON_READER, item.value));
      });
    }
  });
  describe("valid", function () {
    interface Item {
      name?: string;
      value: WikiSlug;
      rawJson: string;
    }

    const items: Item[] = [
      {
        name: "empty string",
        value: "",
        rawJson: "\"\"",
      },
      {
        name: "1 letter",
        value: "a",
        rawJson: "\"a\"",
      },
      {
        name: "1 digit",
        value: "0",
        rawJson: "\"0\"",
      },
      {
        name: "1 dash",
        value: "-",
        rawJson: "\"-\"",
      },
      {
        name: "1 dot",
        value: ".",
        rawJson: "\".\"",
      },
      {
        value: "levels/10.8",
        rawJson: "\"levels/10.8\"",
      },
      {
        value: "dimensions/les-landes-violettes",
        rawJson: "\"dimensions/les-landes-violettes\"",
      },
      {
        name: "100 letters",
        value: new Array(100).fill("a").join(""),
        rawJson: `"${new Array(100).fill("a").join("")}"`,
      },
    ];

    for (const item of items) {
      const valueStr: string = JSON.stringify(item.value);
      it(`should accept ${valueStr}: ${item.name ?? valueStr}`, function () {
        const actualValue: WikiSlug = $WikiSlug.read(JSON_READER, item.rawJson);
        chai.assert.isTrue($WikiSlug.equals(actualValue, item.value));
      });
    }
  });
});
