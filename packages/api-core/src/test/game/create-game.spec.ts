import {GameCategory} from "../../lib/game/game-category.js";
import {$CreateGameOptions, CreateGameOptions} from "../../lib/game2/create-game-options.js";
import {GameChannelPermission} from "../../lib/game2/game-channel-permission.js";
import {GameEngineType} from "../../lib/game2/game-engine-type.js";
import {ObjectType} from "../../lib/types/object-type.js";
import {registerJsonIoTests} from "../test-kryo.js";

describe("CreateGame", function () {
  registerJsonIoTests<CreateGameOptions>(
    $CreateGameOptions,
    "game/create-game",
    new Map([
      ["sous-la-colline", {
        owner: {
          type: ObjectType.User,
          id: "00000000-0000-0000-0001-000000000001"
        },
        key: "hill",
        build: {
          version: "1.2.3",
          gitCommitRef: "0123456789abcdef0123456789abcdef01234567",
          mainLocale: "fr-FR",
          displayName: "Sous la colline",
          description: "Contrée emblématique d'Eternalfest qui ravira les débutants et nostalgiques\u202f!",
          icon: {
            type: ObjectType.Blob,
            id: "00000000-0000-0000-0002-000000000001"
          },
          loader: "4.1.0",
          engine: {
            type: GameEngineType.V96,
          },
          musics: [],
          contentI18n: {
            type: ObjectType.Blob,
            id: "00000000-0000-0000-0002-000000000002"
          },
          patcher: null,
          debug: null,
          content: null,
          modes: new Map([
            ["solo", {
              displayName:  "Aventure",
              isVisible: true,
              options: new Map([
                ["boost", {
                  displayName: "Tornade",
                  isVisible: true,
                  isEnabled: true,
                  defaultValue: false
                }]
              ]),
            }],
            ["multi", {
              displayName: "Multicoopératif",
              isVisible: true,
              options: new Map([
                ["lifesharing", {
                  displayName: "Partage de vies",
                  isVisible: true,
                  isEnabled: true,
                  defaultValue: false
                }]
              ]),
            }],
          ]),
          families: "10",
          category: GameCategory.Small,
          i18n: new Map([
            ["en-US", {
              displayName: "Under the hill",
              description: "Hallmark Eternalfest game for beginners and nostalgics\u202f!",
              contentI18n: {
                type: ObjectType.Blob,
                id: "00000000-0000-0000-0002-000000000002"
              },
              modes: new Map([
                ["multi", {
                  displayName: "Multiplayer",
                  options: new Map([
                    ["lifesharing", {displayName: "Life sharing"}]
                  ])
                }],
                ["solo", {
                  displayName: "Adventure",
                  options: new Map([
                    ["boost", {displayName: "Tornado"}]
                  ])
                }],
              ])
            }]
          ]),
        },
        channels: [
          {
            key: "main",
            isEnabled: false,
            defaultPermission: GameChannelPermission.None,
            isPinned: false,
            publicationDate: null,
            sortUpdateDate: null,
            version: "1.2.3",
            patches: [],
          }
        ],
      }],
    ])
  );
});
