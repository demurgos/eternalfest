import {GameChannelPermission} from "../../lib/game2/game-channel-permission.js";
import {$ShortGameListing, ShortGameListing} from "../../lib/game2/short-game-listing.js";
import {ObjectType} from "../../lib/types/object-type.js";
import {registerJsonIoTests} from "../test-kryo.js";

describe("ShortGameListing", function () {
  registerJsonIoTests<ShortGameListing>(
    $ShortGameListing,
    "game/short-game-listing",
    new Map([
      ["alpha-beta", {
        offset: 0,
        limit: 10,
        count: 3,
        isCountExact: false,
        items: [
          {
            type: ObjectType.Game,
            id: "00000000-0000-0000-0003-000000000001",
            createdAt: new Date("2022-01-01T00:00:01.000Z"),
            key: null,
            owner: {
              type: ObjectType.User,
              id: "00000000-0000-0000-0001-000000000001",
              displayName: "Alice",
            },
            channels: {
              offset: 0,
              limit: 1,
              count: 2,
              isCountExact: false,
              items: [
                {
                  type: ObjectType.GameChannel,
                  key: "main",
                  isEnabled: true,
                  isPinned: false,
                  publicationDate: null,
                  sortUpdateDate: new Date("2022-01-01T00:00:02.000Z"),
                  defaultPermission: GameChannelPermission.None,
                  build: {
                    version: "1.2.3",
                    gitCommitRef: null,
                    mainLocale: "fr-FR",
                    displayName: "Alpha",
                    description: "Alpha game",
                    icon: null,
                    i18n: new Map(),
                  }
                }
              ]
            }
          },
          {
            type: ObjectType.Game,
            id: "00000000-0000-0000-0003-000000000002",
            createdAt: new Date("2022-01-01T00:00:02.000Z"),
            key: null,
            owner: {
              type: ObjectType.User,
              id: "00000000-0000-0000-0001-000000000001",
              displayName: "Alice",
            },
            channels: {
              offset: 1,
              limit: 1,
              count: 3,
              isCountExact: false,
              items: [
                {
                  type: ObjectType.GameChannel,
                  key: "beta",
                  isEnabled: true,
                  isPinned: false,
                  publicationDate: null,
                  sortUpdateDate: new Date("2022-01-01T00:00:02.000Z"),
                  defaultPermission: GameChannelPermission.None,
                  build: {
                    version: "0.1.2",
                    gitCommitRef: null,
                    mainLocale: "fr-FR",
                    displayName: "Beta",
                    description: "Beta game",
                    icon: null,
                    i18n: new Map(),
                  }
                }
              ]
            }
          },
          null,
        ]
      }],
    ])
  );
});
