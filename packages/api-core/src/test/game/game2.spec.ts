import {GameCategory} from "../../lib/game/game-category.js";
import {$Game, Game} from "../../lib/game2/game.js";
import {GameChannelPermission} from "../../lib/game2/game-channel-permission.js";
import {GameEngineType} from "../../lib/game2/game-engine-type.js";
import {ObjectType} from "../../lib/types/object-type.js";
import {registerJsonIoTests} from "../test-kryo.js";

describe("Game", function () {
  registerJsonIoTests<Game>(
    $Game,
    "game/game",
    new Map([
      ["beta", {
        type: ObjectType.Game,
        id: "00000000-0000-0000-0003-000000000002",
        createdAt: new Date("2022-01-01T00:00:02.000Z"),
        key: null,
        owner: {
          type: ObjectType.User,
          id: "00000000-0000-0000-0001-000000000001",
          displayName: "Alice",
        },
        channels: {
          offset: 0,
          limit: 10,
          count: 3,
          isCountExact: false,
          active: {
            type: ObjectType.GameChannel,
            key: "beta",
            isEnabled: true,
            isPinned: false,
            publicationDate: null,
            sortUpdateDate: new Date("2022-01-01T00:00:02.000Z"),
            defaultPermission: GameChannelPermission.None,
            build: {
              createdAt: new Date("2022-01-01T00:00:02.000Z"),
              version: "0.1.2",
              gitCommitRef: null,
              mainLocale: "fr-FR",
              displayName: "Beta",
              description: "Beta game",
              icon: null,
              loader: "4.1.0",
              engine: {
                type: GameEngineType.V96,
              },
              patcher: null,
              debug: null,
              content: null,
              contentI18n: null,
              musics: [],
              modes: new Map(),
              families: "1,2,3",
              category: GameCategory.Big,
              i18n: new Map(),
            }
          },
          items: [
            null,
            {
              type: ObjectType.GameChannel,
              key: "beta",
              isEnabled: true,
              isPinned: false,
              publicationDate: null,
              sortUpdateDate: new Date("2022-01-01T00:00:02.000Z"),
              defaultPermission: GameChannelPermission.None,
              build: {
                version: "0.1.2",
                gitCommitRef: null,
                mainLocale: "fr-FR",
                displayName: "Beta",
                description: "Beta game",
                icon: null,
                i18n: new Map(),
              },
            },
            {
              type: ObjectType.GameChannel,
              key: "dev",
              isEnabled: true,
              isPinned: false,
              publicationDate: null,
              sortUpdateDate: new Date("2022-01-01T00:00:02.000Z"),
              defaultPermission: GameChannelPermission.None,
              build: {
                version: "0.1.2",
                gitCommitRef: null,
                mainLocale: "fr-FR",
                displayName: "Beta",
                description: "Beta game",
                icon: null,
                i18n: new Map(),
              },
            },
          ],
        },
      }],
    ])
  );
});
