import { Ucs2StringType } from "kryo/ucs2-string";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

export type OauthClientSecret = UuidHex;

export const $OauthClientSecret: Ucs2StringType = $UuidHex;
