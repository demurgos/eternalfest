import incident, { Incident } from "incident";

export type Name = "Unauthenticated";
export const name: Name = "Unauthenticated";

export type Cause = undefined;
export type UnauthenticatedError<D extends object> = Incident<D, Name, Cause>;

export function createUnauthenticatedError<D extends object>(data: D): UnauthenticatedError<D> {
  return incident.Incident(name, data);
}
