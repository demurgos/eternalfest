import { CaseStyle } from "kryo";
import { $Date } from "kryo/date";
import { $Null } from "kryo/null";
import { RecordIoType, RecordType } from "kryo/record";
import { TryUnionType } from "kryo/try-union";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

/**
 * Represents a reference to a revision (version) of a wiki page.
 */
export interface RevisionRef {
  /**
   * Revision id
   */
  id: UuidHex;

  /**
   * Date of the revision.
   */
  date: Date;
}

export const $RevisionRef: RecordIoType<RevisionRef> = new RecordType<RevisionRef>({
  properties: {
    id: {type: $UuidHex},
    date: {type: $Date},
  },
  changeCase: CaseStyle.SnakeCase,
});

export const $NullableRevisionRef: TryUnionType<null | RevisionRef> = new TryUnionType({
  variants: [$Null, $RevisionRef],
});
