import { TaggedUnionType } from "kryo/tagged-union";

import { $DeletedRevisionContent, DeletedRevisionContent } from "./deleted-revision-content.js";
import { $TextRevisionContent, TextRevisionContent } from "./text-revision-content.js";

export type RevisionContent = DeletedRevisionContent | TextRevisionContent;

export const $RevisionContent: TaggedUnionType<RevisionContent> = new TaggedUnionType<RevisionContent>({
  variants: [$DeletedRevisionContent, $TextRevisionContent],
  tag: "type",
});
