import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

export interface GetPageByIdOptions {
  pageId: UuidHex;
  revision?: UuidHex;
}

export const $GetPageByIdOptions: RecordIoType<GetPageByIdOptions> = new RecordType<GetPageByIdOptions>({
  properties: {
    pageId: {type: $UuidHex},
    revision: {type: $UuidHex, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
