import { UuidHex } from "kryo/uuid-hex";

import { AuthContext } from "../auth/auth-context.js";
import { CreatePageOptions } from "./create-page-options.js";
import { GetPageByIdOptions } from "./get-page-by-id-options.js";
import { GetPageBySlugOptions } from "./get-page-by-slug-options.js";
import { UpdatePageOptions } from "./update-page-options.js";
import { WikiPage } from "./wiki-page.js";

export interface ReadonlyWikiService {
  getPageById(auth: AuthContext, options: GetPageByIdOptions): Promise<WikiPage | undefined>;

  getPageBySlug(auth: AuthContext, options: GetPageBySlugOptions): Promise<WikiPage | undefined>;
}

export interface WikiService extends ReadonlyWikiService {
  createPage(auth: AuthContext, options: CreatePageOptions): Promise<WikiPage>;

  updatePage(auth: AuthContext, gameId: UuidHex, options: UpdatePageOptions): Promise<WikiPage>;
}
