import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

import { $FamiliesString, FamiliesString } from "../game/families-string.js";
import { $IdRef, IdRef } from "../types/id-ref.js";
import { $RunItems, RunItems } from "./run-items.js";

export interface RunStart {
  readonly run: IdRef;
  readonly key: UuidHex;
  readonly families: FamiliesString;
  readonly items: RunItems;
}

export const $RunStart: RecordIoType<RunStart> = new RecordType<RunStart>({
  properties: {
    run: {type: $IdRef},
    key: {type: $UuidHex},
    families: {type: $FamiliesString},
    items: {type: $RunItems},
  },
  changeCase: CaseStyle.SnakeCase,
});
