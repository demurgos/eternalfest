import {CaseStyle} from "kryo";
import {ArrayType} from "kryo/array";
import {RecordIoType, RecordType} from "kryo/record";

import {$GameModeKey, GameModeKey} from "../game/game-mode-key.js";
import {$GameOptionKey, GameOptionKey} from "../game/game-option-key.js";
import {$GameChannelKey, GameChannelKey} from "../game2/game-channel-key.js";
import {$GameIdRef, GameIdRef} from "../game2/game-ref.js";
import {$VersionString, VersionString} from "../types/version-string.js";
import {$UserIdRef, UserIdRef} from "../user/user-id-ref.js";
import {$RunSettings, RunSettings} from "./run-settings.js";

export interface CreateRunOptions {
  readonly game: GameIdRef;
  readonly channel: GameChannelKey;
  readonly version: VersionString;
  readonly user: UserIdRef;
  readonly gameMode: GameModeKey;
  readonly gameOptions: ReadonlyArray<GameOptionKey>;
  readonly settings: RunSettings;
}

export const $CreateRunOptions: RecordIoType<CreateRunOptions> = new RecordType<CreateRunOptions>({
  properties: {
    game: {type: $GameIdRef},
    channel: {type: $GameChannelKey},
    version: {type: $VersionString},
    user: {type: $UserIdRef},
    gameMode: {type: $GameModeKey},
    gameOptions: {type: new ArrayType({itemType: $GameOptionKey, maxLength: 10})},
    settings: {type: $RunSettings},
  },
  changeCase: CaseStyle.SnakeCase,
});
