import { CaseStyle } from "kryo";
import { $Boolean } from "kryo/boolean";
import { $Uint32 } from "kryo/integer";
import { RecordIoType, RecordType } from "kryo/record";

import { $RunItems, RunItems } from "./run-items.js";
import { $RunScores, RunScores } from "./run-scores.js";
import { $RunStats, RunStats } from "./run-stats.js";

export interface SetRunResultOptions {
  readonly isVictory: boolean;
  readonly maxLevel: number;
  readonly scores: RunScores;
  readonly items: RunItems;
  readonly stats: RunStats;
}

export const $SetRunResultOptions: RecordIoType<SetRunResultOptions> = new RecordType<SetRunResultOptions>({
  properties: {
    isVictory: {type: $Boolean},
    maxLevel: {type: $Uint32},
    scores: {type: $RunScores},
    items: {type: $RunItems},
    stats: {type: $RunStats},
  },
  changeCase: CaseStyle.SnakeCase,
});
