import {CaseStyle} from "kryo";
import {ArrayType} from "kryo/array";
import {$Date} from "kryo/date";
import {$Null} from "kryo/null";
import {RecordIoType, RecordType} from "kryo/record";
import {TryUnionType} from "kryo/try-union";
import {Ucs2StringType} from "kryo/ucs2-string";
import {$UuidHex, UuidHex} from "kryo/uuid-hex";

import {$GameModeKey, GameModeKey} from "../game/game-mode-key.js";
import {$GameIdRef, GameIdRef} from "../game2/game-ref.js";
import {$NullableDate, NullableDate} from "../types/nullable-date.js";
import {$ShortUser, ShortUser} from "../user/short-user.js";
import {$RunResult, RunResult} from "./run-result.js";
import {$RunSettings, RunSettings} from "./run-settings.js";

export interface Run {
  id: UuidHex;
  createdAt: Date;
  startedAt: NullableDate;
  result: null | RunResult;
  game: GameIdRef;
  user: ShortUser;
  gameMode: GameModeKey;
  gameOptions: ReadonlyArray<string>;
  settings: RunSettings;
}

export const $Run: RecordIoType<Run> = new RecordType<Run>({
  properties: {
    id: {type: $UuidHex},
    createdAt: {type: $Date},
    startedAt: {type: $NullableDate},
    result: {type: new TryUnionType({variants: [$Null, $RunResult]})},
    game: {type: $GameIdRef},
    user: {type: $ShortUser},
    gameMode: {type: $GameModeKey},
    gameOptions: {type: new ArrayType({itemType: new Ucs2StringType({maxLength: 50}), maxLength: 10})},
    settings: {type: $RunSettings},
  },
  changeCase: CaseStyle.SnakeCase,
});
