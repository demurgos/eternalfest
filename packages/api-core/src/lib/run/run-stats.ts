import { AnyType } from "kryo/any";

// TODO: Set to `unknown`
export type RunStats = any;

export const $RunStats: AnyType = new AnyType();
