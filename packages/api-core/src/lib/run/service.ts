import {UuidHex} from "kryo/uuid-hex";

import {AuthContext} from "../auth/auth-context.js";
import {LeaderboardService} from "../leaderboard/service.js";
import {CreateRunOptions} from "./create-run-options.js";
import {Run} from "./run.js";
import {RunItems} from "./run-items.js";
import {RunStart} from "./run-start.js";
import {SetRunResultOptions} from "./set-run-result-options.js";

export interface RunService extends LeaderboardService {
  createRun(acx: AuthContext, options: CreateRunOptions): Promise<Run>;

  getRunById(acx: AuthContext, runId: UuidHex): Promise<Run>;

  startRun(acx: AuthContext, runId: UuidHex): Promise<RunStart>;

  setRunResult(acx: AuthContext, runId: UuidHex, result: SetRunResultOptions): Promise<Run>;

  getGameUserItemsById(auth: AuthContext, userId: UuidHex, gameId: UuidHex): Promise<RunItems | undefined>;
}
