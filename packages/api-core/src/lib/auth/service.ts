import { Session } from "../types/session.js";
import { UserDisplayName } from "../user/user-display-name.js";
import { UserId } from "../user/user-id.js";
import { AuthContext } from "./auth-context.js";
import { AuthenticateHttpOptions } from "./authenticate-http-options.js";

export interface AuthService {
  authenticateHttp(options: AuthenticateHttpOptions): Promise<AuthContext>;

  session(sessionId: string): Promise<AuthContext>;

  etwinOauth(userId: UserId, userDisplayName: UserDisplayName): Promise<AuthContext>;

  createSession(_acx: AuthContext, _userId: UserId): Promise<Session>;
}
