import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";
import { $Ucs2String } from "kryo/ucs2-string";

export interface AuthenticateHttpOptions {
  authorizationHeader?: string;
  sessionCookie?: string;
}

export const $AuthenticateHttpOptions: RecordIoType<AuthenticateHttpOptions> = new RecordType<AuthenticateHttpOptions>({
  properties: {
    authorizationHeader: {type: $Ucs2String},
    sessionCookie: {type: $Ucs2String},
  },
  changeCase: CaseStyle.SnakeCase,
});
