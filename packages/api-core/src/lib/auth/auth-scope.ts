import { CaseStyle } from "kryo";
import { TsEnumType } from "kryo/ts-enum";

export enum AuthScope {
  Default,
}

export const $AuthScope: TsEnumType<AuthScope> = new TsEnumType<AuthScope>({
  enum: AuthScope,
  changeCase: CaseStyle.PascalCase,
});
