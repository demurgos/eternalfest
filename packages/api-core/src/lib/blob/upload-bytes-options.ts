import { CaseStyle } from "kryo";
import { $Bytes } from "kryo/bytes";
import { $Uint32 } from "kryo/integer";
import { RecordIoType, RecordType } from "kryo/record";

export interface UploadBytesOptions {
  offset: number;
  data: Uint8Array;
}

export const $UploadBytesOptions: RecordIoType<UploadBytesOptions> = new RecordType<UploadBytesOptions>({
  properties: {
    offset: {type: $Uint32},
    data: {type: $Bytes},
  },
  changeCase: CaseStyle.SnakeCase,
});
