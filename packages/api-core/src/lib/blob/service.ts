import stream from "stream";

import { AuthContext } from "../auth/auth-context.js";
import { Blob } from "./blob.js";
import { CreateBlobOptions } from "./create-blob-options.js";
import { CreateUploadSessionOptions } from "./create-upload-session-options.js";
import { GetBlobOptions } from "./get-blob-options.js";
import { UploadOptions } from "./upload-options.js";
import { UploadSession } from "./upload-session.js";

export interface BlobService {
  readonly hasImmutableBlobs: boolean;

  createBlob(acx: AuthContext, options: Readonly<CreateBlobOptions>): Promise<Blob>;

  getBlobById(acx: AuthContext, options: Readonly<GetBlobOptions>): Promise<Blob>;

  readBlobData(acx: AuthContext, options: Readonly<GetBlobOptions>): Promise<stream.Readable>;

  createUploadSession(acx: AuthContext, options: Readonly<CreateUploadSessionOptions>): Promise<UploadSession>;

  uploadBytes(acx: AuthContext, options: Readonly<UploadOptions>): Promise<UploadSession>;
}
