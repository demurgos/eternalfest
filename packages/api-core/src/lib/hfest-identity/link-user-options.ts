import { UuidHex } from "kryo/uuid-hex";

export interface LinkUserOptions {
  userId?: UuidHex;

  identityId: UuidHex;
}
