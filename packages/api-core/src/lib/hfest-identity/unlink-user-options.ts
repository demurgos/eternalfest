import { UuidHex } from "kryo/uuid-hex";

export interface UnlinkUserOptions {
  identityId: UuidHex;
}
