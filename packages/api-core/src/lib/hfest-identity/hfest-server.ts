import { CaseStyle } from "kryo";
import { TsEnumType } from "kryo/ts-enum";

export enum HfestServer {
  Fr,
  En,
  Es,
}

export const $HfestServer: TsEnumType<HfestServer> = new TsEnumType({
  enum: HfestServer,
  changeCase: CaseStyle.KebabCase,
});
