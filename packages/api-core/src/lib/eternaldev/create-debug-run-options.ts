import {CaseStyle} from "kryo";
import {ArrayType} from "kryo/array";
import {RecordIoType, RecordType} from "kryo/record";
import {$UuidHex, UuidHex} from "kryo/uuid-hex";

import {$GameModeKey, GameModeKey} from "../game/game-mode-key.js";
import {$GameOptionKey, GameOptionKey} from "../game/game-option-key.js";
import {$GameChannelKey, GameChannelKey} from "../game2/game-channel-key.js";
import {$RunSettings, RunSettings} from "../run/run-settings.js";
import {$LocaleId, LocaleId} from "../types/locale-id.js";
import {$VersionString, VersionString} from "../types/version-string.js";
import {$ProfileKey, ProfileKey} from "./profile-key.js";

export interface CreateDebugRunOptions {
  readonly gameId: UuidHex;
  readonly channel: GameChannelKey;
  readonly version: VersionString;
  readonly userId: UuidHex;
  readonly gameMode: GameModeKey;
  readonly gameOptions: ReadonlyArray<GameOptionKey>;
  readonly settings: RunSettings;
  readonly profile?: ProfileKey;
  readonly locale?: LocaleId;
}

export const $CreateDebugRunOptions: RecordIoType<CreateDebugRunOptions> = new RecordType<CreateDebugRunOptions>({
  properties: {
    gameId: {type: $UuidHex},
    userId: {type: $UuidHex},
    channel: {type: $GameChannelKey},
    version: {type: $VersionString},
    gameMode: {type: $GameModeKey},
    gameOptions: {type: new ArrayType({itemType: $GameOptionKey, maxLength: 10})},
    settings: {type: $RunSettings},
    profile: {type: $ProfileKey, optional: true},
    locale: {type: $LocaleId, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
