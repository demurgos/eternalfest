import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";
import { Ucs2StringType } from "kryo/ucs2-string";

export interface LocalFileRef {
  url: string;
}

export const $LocalFileRef: RecordIoType<LocalFileRef> = new RecordType<LocalFileRef>({
  properties: {
    url: {type: new Ucs2StringType({maxLength: Infinity})},
  },
  changeCase: CaseStyle.SnakeCase,
});
