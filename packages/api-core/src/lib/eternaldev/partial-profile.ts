import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

import { $RunItems, RunItems } from "../run/run-items.js";
import { $PartialUserRef, PartialUserRef } from "./partial-user-ref.js";

export interface PartialProfile {
  readonly user?: PartialUserRef;
  readonly items?: RunItems;
}

export const $PartialProfile: RecordIoType<PartialProfile> = new RecordType<PartialProfile>({
  properties: {
    user: {type: $PartialUserRef, optional: true},
    items: {type: $RunItems, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
