import { MapType } from "kryo/map";

import { $BlobId, BlobId } from "../blob/blob-id.js";
import { $LocalFileRef, LocalFileRef } from "./local-file-ref.js";

export type LocalFileMap = Map<BlobId, LocalFileRef>;

export const $LocalFileMap: MapType<BlobId, LocalFileRef> = new MapType({
  keyType: $BlobId,
  valueType: $LocalFileRef,
  maxSize: Infinity,
  assumeStringKey: true,
});
