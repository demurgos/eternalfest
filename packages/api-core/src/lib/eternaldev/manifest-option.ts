import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

import { $GameOptionDisplayName, GameOptionDisplayName } from "../game/game-option-display-name.js";
import { $GameOptionKey, GameOptionKey } from "../game/game-option-key.js";
import { $GameOptionState, GameOptionState } from "../game/game-option-state.js";
import { DeepReadonly } from "../types/deep-readonly.js";

export interface ManifestOption {
  key?: GameOptionKey;
  id?: GameOptionKey;
  displayName?: GameOptionDisplayName;
  state: GameOptionState;
}

export type ReadonlyManifestOption = DeepReadonly<ManifestOption>;

export const $ManifestOption: RecordIoType<ManifestOption> = new RecordType<ManifestOption>({
  properties: {
    key: {type: $GameOptionKey, optional: true},
    id: {type: $GameOptionKey, optional: true},
    displayName: {type: $GameOptionDisplayName, optional: true},
    state: {type: $GameOptionState},
  },
  changeCase: CaseStyle.SnakeCase,
});
