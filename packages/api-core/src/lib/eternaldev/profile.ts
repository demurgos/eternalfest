import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

import { $RunItems, RunItems } from "../run/run-items.js";
import { $UserRef, UserRef } from "../user/user-ref.js";

export interface Profile {
  readonly user: UserRef;
  readonly items: RunItems;
}

export const $Profile: RecordIoType<Profile> = new RecordType<Profile>({
  properties: {
    user: {type: $UserRef, optional: true},
    items: {type: $RunItems, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
