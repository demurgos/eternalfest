import { Ucs2StringType } from "kryo/ucs2-string";

export type UrlFragment = string;

export const $UrlFragment: Ucs2StringType = new Ucs2StringType({maxLength: Infinity});
