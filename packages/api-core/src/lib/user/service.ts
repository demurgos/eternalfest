import { UuidHex } from "kryo/uuid-hex";

import { AuthContext } from "../auth/auth-context.js";
import { UpdateUserOptions } from "./update-user-options.js";
import { User } from "./user.js";
import { UserDisplayName } from "./user-display-name.js";
import { UserListing } from "./user-listing.js";
import { UserRef } from "./user-ref.js";

export interface UserService {
  getOrCreateUserWithEtwin(auth: AuthContext, userId: UuidHex, userDisplay: UserDisplayName): Promise<User>;

  getUserById(auth: AuthContext, userId: UuidHex): Promise<User | undefined>;

  getUserRefById(auth: AuthContext, userId: UuidHex): Promise<UserRef | undefined>;

  getUserRefsById(auth: AuthContext, userIds: ReadonlySet<UuidHex>): Promise<Map<UuidHex, UserRef | undefined>>;

  getUsers(auth: AuthContext): Promise<UserListing>;

  updateUser(auth: AuthContext, options: UpdateUserOptions): Promise<User>;
}
