import {CaseStyle} from "kryo";
import {ArrayType} from "kryo/array";
import {RecordIoType, RecordType} from "kryo/record";

import {$GameModeKey, GameModeKey} from "../game/game-mode-key.js";
import {$GameChannelKey, GameChannelKey} from "../game2/game-channel-key.js";
import {$GameIdRef, GameIdRef} from "../game2/game-ref.js";
import {$LeaderboardEntry, LeaderboardEntry} from "./leaderboard-entry.js";

export interface Leaderboard {
  game: GameIdRef;
  channel: GameChannelKey;
  mode: GameModeKey;
  results: LeaderboardEntry[];
}

export const $Leaderboard: RecordIoType<Leaderboard> = new RecordType<Leaderboard>({
  properties: {
    game: {type: $GameIdRef},
    channel: {type: $GameChannelKey},
    mode: {type: $GameModeKey},
    results: {type: new ArrayType({itemType: $LeaderboardEntry, maxLength: Infinity})},
  },
  changeCase: CaseStyle.SnakeCase,
});
