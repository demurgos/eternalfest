import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

import { $GameOptionDisplayName, GameOptionDisplayName } from "./game-option-display-name.js";

export interface GameOptionLocale {
  displayName?: GameOptionDisplayName;
}

export const $GameOptionLocale: RecordIoType<GameOptionLocale> = new RecordType<GameOptionLocale>({
  properties: {
    displayName: {type: $GameOptionDisplayName, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
