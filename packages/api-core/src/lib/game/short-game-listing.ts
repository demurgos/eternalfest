import { CaseStyle } from "kryo";
import { ArrayType } from "kryo/array";
import { $Uint32 } from "kryo/integer";
import { RecordIoType, RecordType } from "kryo/record";

import { $ShortGame, ShortGame } from "./short-game.js";

export interface ShortGameListing {
  offset: number;
  limit: number;
  count: number;
  items: ShortGame[];
}

export const $ShortGameListing: RecordIoType<ShortGameListing> = new RecordType<ShortGameListing>({
  properties: {
    offset: {type: $Uint32},
    limit: {type: $Uint32},
    count: {type: $Uint32},
    items: {type: new ArrayType({itemType: $ShortGame, maxLength: 100})},
  },
  changeCase: CaseStyle.SnakeCase,
});
