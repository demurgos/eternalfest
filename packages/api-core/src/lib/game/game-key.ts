import { $Null } from "kryo/null";
import { TryUnionType } from "kryo/try-union";
import { Ucs2StringType } from "kryo/ucs2-string";

export type GameKey = string;

export const $GameKey: Ucs2StringType = new Ucs2StringType({maxLength: 32});

export type NullableGameKey = null | GameKey;

export const $NullableGameKey: TryUnionType<NullableGameKey> = new TryUnionType({variants: [$Null, $GameKey]});
