import { Ucs2StringType } from "kryo/ucs2-string";

export type GameOptionDisplayName = string;

export const $GameOptionDisplayName: Ucs2StringType = new Ucs2StringType({
  trimmed: true,
  minLength: 1,
  maxLength: 64,
});
