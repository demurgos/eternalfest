import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";
import { Ucs2StringType } from "kryo/ucs2-string";

import { $File, File } from "../file/file.js";

export interface GameResource {
  readonly file: File;
  readonly tag?: string;
  readonly priority: string;
}

export const $GameResource: RecordIoType<GameResource> = new RecordType<GameResource>({
  properties: {
    file: {type: $File},
    tag: {type: new Ucs2StringType({maxLength: 50}), optional: true},
    priority: {type: new Ucs2StringType({maxLength: 50})},
  },
  changeCase: CaseStyle.SnakeCase,
});
