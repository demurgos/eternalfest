import { Ucs2StringType } from "kryo/ucs2-string";

export type GameDisplayName = string;

export const $GameDisplayName: Ucs2StringType = new Ucs2StringType({
  trimmed: true,
  minLength: 1,
  maxLength: 64,
});
