import { CaseStyle } from "kryo";
import { TsEnumType } from "kryo/ts-enum";

export enum GameOptionState {
  /**
   * The option is displayed, unchecked and cannot be checked (constantly unchecked).
   * The option should never be active.
   */
  Disabled,

  /**
   * The option is displayed, unchecked but can be checked.
   */
  Enabled,

  /**
   * The option is displayed, checked and cannot be unchecked (constantly checked).
   */
  Locked,
}

export const $GameOptionState: TsEnumType<GameOptionState> = new TsEnumType<GameOptionState>({
  enum: GameOptionState,
  changeCase: CaseStyle.KebabCase,
});
