import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";
import { Ucs2StringType } from "kryo/ucs2-string";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

export interface InputGameResource {
  readonly fileId: UuidHex;
  readonly tag?: string;
  readonly priority?: string;
}

export const $InputGameResource: RecordIoType<InputGameResource> = new RecordType<InputGameResource>({
  properties: {
    fileId: {type: $UuidHex},
    tag: {type: new Ucs2StringType({maxLength: 50}), optional: true},
    priority: {type: new Ucs2StringType({maxLength: 50}), optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
