import { Ucs2StringType } from "kryo/ucs2-string";

export type GameModeKey = string;

export const $GameModeKey: Ucs2StringType = new Ucs2StringType({maxLength: 50});
