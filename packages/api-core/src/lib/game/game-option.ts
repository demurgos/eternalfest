import { CaseStyle } from "kryo";
import { MapType } from "kryo/map";
import { RecordIoType, RecordType } from "kryo/record";

import { DeepReadonly } from "../types/deep-readonly.js";
import { $LocaleId, LocaleId } from "../types/locale-id.js";
import { $GameModeLocale, GameModeLocale } from "./game-mode-locale.js";
import { $GameOptionDisplayName, GameOptionDisplayName } from "./game-option-display-name.js";
import { $GameOptionKey, GameOptionKey } from "./game-option-key.js";
import { $GameOptionState, GameOptionState } from "./game-option-state.js";

export interface GameOption {
  key: GameOptionKey;
  displayName: GameOptionDisplayName;
  state: GameOptionState;
  locales: Map<LocaleId, GameModeLocale>;
}

export type ReadonlyGameOption = DeepReadonly<GameOption>;

export const $GameOption: RecordIoType<GameOption> = new RecordType<GameOption>({
  properties: {
    key: {type: $GameOptionKey},
    displayName: {type: $GameOptionDisplayName},
    state: {type: $GameOptionState},
    locales: {
      type: new MapType({
        keyType: $LocaleId,
        valueType: $GameModeLocale,
        maxSize: 500,
        assumeStringKey: true,
      }),
    },
  },
  changeCase: CaseStyle.SnakeCase,
});
