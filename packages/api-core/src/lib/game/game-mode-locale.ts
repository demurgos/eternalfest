import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

import { $GameModeDisplayName, GameModeDisplayName } from "./game-mode-display-name.js";

export interface GameModeLocale {
  displayName?: GameModeDisplayName;
}

export const $GameModeLocale: RecordIoType<GameModeLocale> = new RecordType<GameModeLocale>({
  properties: {
    displayName: {type: $GameModeDisplayName, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
