import {CaseStyle} from "kryo";
import {TsEnumType} from "kryo/ts-enum";

export enum GameCategory {
  Big,
  Small,
  Puzzle,
  Challenge,
  Fun,
  Lab,
  Other,
}

export const $GameCategory: TsEnumType<GameCategory> = new TsEnumType<GameCategory>({
  enum: GameCategory,
  changeCase: CaseStyle.PascalCase,
});
