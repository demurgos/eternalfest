import {CaseStyle} from "kryo";
import {ArrayType} from "kryo/array";
import {RecordIoType, RecordType} from "kryo/record";

import {DeepReadonly} from "../types/deep-readonly.js";
import {$NullableUserIdRef, NullableUserIdRef} from "../user/user-id-ref.js";
import {$GameChannelKey, GameChannelKey} from "./game-channel-key.js";
import {$GameChannelPatch, GameChannelPatch} from "./game-channel-patch.js";
import {$GameRef, GameRef} from "./game-ref.js";

export interface UpdateGameChannelOptions {
  actor?: NullableUserIdRef;
  game: GameRef;
  channelKey: GameChannelKey;
  patches: GameChannelPatch[];
}

export type ReadonlyUpdateGameChannelOptions = DeepReadonly<UpdateGameChannelOptions>;

export const $UpdateGameChannelOptions: RecordIoType<UpdateGameChannelOptions> = new RecordType<UpdateGameChannelOptions>({
  properties: {
    actor: {type: $NullableUserIdRef, optional: true},
    game: {type: $GameRef},
    channelKey: {type: $GameChannelKey},
    patches: {type: new ArrayType({itemType: $GameChannelPatch, maxLength: 100})},
  },
  changeCase: CaseStyle.SnakeCase,
});
