import {CaseStyle} from "kryo";
import {ArrayType} from "kryo/array";
import {$Date} from "kryo/date";
import {RecordIoType, RecordType} from "kryo/record";

import {$NullableBlob, NullableBlob} from "../blob/blob.js";
import {$FamiliesString, FamiliesString} from "../game/families-string.js";
import {$GameCategory, GameCategory} from "../game/game-category.js";
import {$GameDescription, GameDescription} from "../game/game-description.js";
import {$GameDisplayName, GameDisplayName} from "../game/game-display-name.js";
import {DeepReadonly} from "../types/deep-readonly.js";
import {$NullableGitCommitRef, NullableGitCommitRef} from "../types/git-commit-ref.js";
import {$LocaleId, LocaleId} from "../types/locale-id.js";
import {$VersionString, VersionString} from "../types/version-string.js";
import {$GameBuildI18nMap, GameBuildI18nMap} from "./game-build-i18n-map.js";
import {$GameEngine, GameEngine} from "./game-engine.js";
import {$GameModeSpecMap, GameModeSpecMap} from "./game-mode-spec-map.js";
import {$NullableGamePatcher, NullableGamePatcher} from "./game-patcher.js";
import {$GameResource, GameResource} from "./game-resource.js";

export interface GameBuild {
  version: VersionString;
  createdAt: Date,
  gitCommitRef: NullableGitCommitRef;
  mainLocale: LocaleId;
  displayName: GameDisplayName;
  description: GameDescription;
  // Blob id ref
  icon: NullableBlob;
  loader: VersionString;
  engine: GameEngine;
  patcher: NullableGamePatcher;
  debug: NullableBlob;
  content: NullableBlob;
  contentI18n: NullableBlob;
  musics: GameResource[];
  modes: GameModeSpecMap;
  families: FamiliesString;
  category: GameCategory;
  i18n: GameBuildI18nMap;
}

export type ReadonlyGameBuild = DeepReadonly<GameBuild>;

export const $GameBuild: RecordIoType<GameBuild> = new RecordType<GameBuild>({
  properties: {
    version: {type: $VersionString},
    createdAt: {type: $Date},
    gitCommitRef: {type: $NullableGitCommitRef},
    mainLocale: {type: $LocaleId},
    displayName: {type: $GameDisplayName},
    description: {type: $GameDescription},
    icon: {type: $NullableBlob},
    loader: {type: $VersionString},
    engine: {type: $GameEngine},
    patcher: {type: $NullableGamePatcher},
    debug: {type: $NullableBlob},
    content: {type: $NullableBlob},
    contentI18n: {type: $NullableBlob},
    musics: {type: new ArrayType({itemType: $GameResource, maxLength: 50})},
    modes: {type: $GameModeSpecMap},
    families: {type: $FamiliesString},
    category: {type: $GameCategory},
    i18n: {type: $GameBuildI18nMap},
  },
  changeCase: CaseStyle.SnakeCase,
});
