import {CaseStyle, IoType} from "kryo";
import {$Date} from "kryo/date";
import {LiteralType} from "kryo/literal";
import {$Null} from "kryo/null";
import {RecordIoType, RecordType} from "kryo/record";
import {TryUnionType} from "kryo/try-union";

import {$NullableGameKey, NullableGameKey} from "../game/game-key.js";
import {DeepReadonly} from "../types/deep-readonly.js";
import {$Listing, Listing} from "../types/listing.js";
import {$ObjectType, ObjectType} from "../types/object-type.js";
import {$ShortUser, ShortUser} from "../user/short-user.js";
import {$GameId, GameId} from "./game-id.js";
import {$ShortGameChannel, ShortGameChannel} from "./short-game-channel.js";

export interface ShortGame {
  type: ObjectType.Game;
  id: GameId;
  createdAt: Date;
  key: NullableGameKey;
  owner: ShortUser;
  channels: Listing<ShortGameChannel>;
}

export type ReadonlyShortGame = DeepReadonly<ShortGame>;

export const $ShortGame: RecordIoType<ShortGame> = new RecordType<ShortGame>({
  properties: {
    type: {type: new LiteralType({type: $ObjectType, value: ObjectType.Game as ObjectType.Game})},
    id: {type: $GameId},
    createdAt: {type: $Date},
    key: {type: $NullableGameKey},
    owner: {type: $ShortUser},
    channels: {type: $Listing.apply($ShortGameChannel) as IoType<Listing<ShortGameChannel>>},
  },
  changeCase: CaseStyle.SnakeCase,
});

export type NullableShortGame = null | ShortGame;

export const $NullableShortGame: TryUnionType<NullableShortGame> = new TryUnionType({variants: [$Null, $ShortGame]});
