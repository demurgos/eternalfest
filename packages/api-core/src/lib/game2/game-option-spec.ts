import { CaseStyle } from "kryo";
import {$Boolean} from "kryo/boolean";
import { RecordIoType, RecordType } from "kryo/record";

import { $GameOptionDisplayName, GameOptionDisplayName } from "../game/game-option-display-name.js";
import { DeepReadonly } from "../types/deep-readonly.js";

export interface GameOptionSpec {
  displayName: GameOptionDisplayName;
  isVisible: boolean;
  isEnabled: boolean;
  defaultValue: boolean;
}

export type ReadonlyGameOptionSpec = DeepReadonly<GameOptionSpec>;

export const $GameOptionSpec: RecordIoType<GameOptionSpec> = new RecordType<GameOptionSpec>({
  properties: {
    displayName: {type: $GameOptionDisplayName},
    isVisible: {type: $Boolean},
    isEnabled: {type: $Boolean},
    defaultValue: {type: $Boolean},
  },
  changeCase: CaseStyle.SnakeCase,
});
