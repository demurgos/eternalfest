import {CaseStyle} from "kryo";
import {TsEnumType} from "kryo/ts-enum";

export enum GameChannelPermission {
  None,
  View,
  Play,
  Debug,
  Manage,
}

export const $GameChannelPermission: TsEnumType<GameChannelPermission> = new TsEnumType<GameChannelPermission>({
  enum: GameChannelPermission,
  changeCase: CaseStyle.PascalCase,
});
