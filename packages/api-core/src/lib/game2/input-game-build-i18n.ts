import {CaseStyle} from "kryo";
import {RecordIoType, RecordType} from "kryo/record";

import {$BlobIdRef, BlobIdRef} from "../blob/blob-id-ref.js";
import {$GameDescription, GameDescription} from "../game/game-description.js";
import {$GameDisplayName, GameDisplayName} from "../game/game-display-name.js";
import {DeepReadonly} from "../types/deep-readonly.js";
import {$GameModeSpecI18nMap, GameModeSpecI18nMap} from "./game-mode-spec-i18n-map.js";

export interface InputGameBuildI18n {
  displayName?: GameDisplayName;
  description?: GameDescription;
  icon?: BlobIdRef;
  contentI18n?: BlobIdRef;
  modes?: GameModeSpecI18nMap;
}

export type ReadonlyInputGameBuildI18n = DeepReadonly<InputGameBuildI18n>;

export const $InputGameBuildI18n: RecordIoType<InputGameBuildI18n> = new RecordType<InputGameBuildI18n>({
  properties: {
    displayName: {type: $GameDisplayName, optional: true},
    description: {type: $GameDescription, optional: true},
    icon: {type: $BlobIdRef, optional: true},
    contentI18n: {type: $BlobIdRef, optional: true},
    modes: {type: $GameModeSpecI18nMap, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
