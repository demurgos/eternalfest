import {CaseStyle} from "kryo";
import {RecordIoType, RecordType} from "kryo/record";

import {$Blob, Blob} from "../blob/blob.js";
import {$GameDescription, GameDescription} from "../game/game-description.js";
import {$GameDisplayName, GameDisplayName} from "../game/game-display-name.js";
import {DeepReadonly} from "../types/deep-readonly.js";

export interface ShortGameBuildI18n {
  displayName?: GameDisplayName,
  description?: GameDescription,
  icon?: Blob,
}

export type ReadonlyShortGameBuildI18n = DeepReadonly<ShortGameBuildI18n>;

export const $ShortGameBuildI18n: RecordIoType<ShortGameBuildI18n> = new RecordType<ShortGameBuildI18n>({
  properties: {
    displayName: {type: $GameDisplayName, optional: true},
    description: {type: $GameDescription, optional: true},
    icon: {type: $Blob, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
