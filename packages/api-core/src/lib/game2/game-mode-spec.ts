import { CaseStyle } from "kryo";
import {$Boolean} from "kryo/boolean";
import { RecordIoType, RecordType } from "kryo/record";

import { $GameModeDisplayName, GameModeDisplayName } from "../game/game-mode-display-name.js";
import { DeepReadonly } from "../types/deep-readonly.js";
import {$GameOptionSpecMap, GameOptionSpecMap} from "./game-option-spec-map.js";

export interface GameModeSpec {
  displayName: GameModeDisplayName;
  isVisible: boolean;
  options: GameOptionSpecMap;
}

export type ReadonlyGameModeSpec = DeepReadonly<GameModeSpec>;

export const $GameModeSpec: RecordIoType<GameModeSpec> = new RecordType<GameModeSpec>({
  properties: {
    displayName: {type: $GameModeDisplayName},
    isVisible: {type: $Boolean},
    options: {type: $GameOptionSpecMap},
  },
  changeCase: CaseStyle.SnakeCase,
});
