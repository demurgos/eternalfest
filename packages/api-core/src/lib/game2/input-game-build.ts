import {CaseStyle} from "kryo";
import {ArrayType} from "kryo/array";
import {RecordIoType, RecordType} from "kryo/record";

import {$NullableBlobIdRef, NullableBlobIdRef} from "../blob/blob-id-ref.js";
import {$FamiliesString, FamiliesString} from "../game/families-string.js";
import {$GameCategory, GameCategory} from "../game/game-category.js";
import {$GameDescription, GameDescription} from "../game/game-description.js";
import {$GameDisplayName, GameDisplayName} from "../game/game-display-name.js";
import {DeepReadonly} from "../types/deep-readonly.js";
import {$NullableGitCommitRef, NullableGitCommitRef} from "../types/git-commit-ref.js";
import {$LocaleId, LocaleId} from "../types/locale-id.js";
import {$VersionString, VersionString} from "../types/version-string.js";
import {$GameModeSpecMap, GameModeSpecMap} from "./game-mode-spec-map.js";
import {$InputGameBuildI18nMap, InputGameBuildI18nMap} from "./input-game-build-i18n-map.js";
import {$InputGameEngine, InputGameEngine} from "./input-game-engine.js";
import {$NullableInputGamePatcher, NullableInputGamePatcher} from "./input-game-patcher.js";
import {$InputGameResource, InputGameResource} from "./input-game-resource.js";

export interface InputGameBuild {
  version: VersionString;
  gitCommitRef?: NullableGitCommitRef;
  mainLocale: LocaleId;
  displayName: GameDisplayName;
  description: GameDescription;
  // Blob id ref
  icon: NullableBlobIdRef;
  loader: VersionString;
  engine: InputGameEngine;
  patcher: NullableInputGamePatcher;
  debug: NullableBlobIdRef;
  content: NullableBlobIdRef;
  contentI18n?: NullableBlobIdRef;
  musics: InputGameResource[];
  modes: GameModeSpecMap;
  families: FamiliesString;
  category: GameCategory;
  i18n?: InputGameBuildI18nMap;
}

export type ReadonlyInputGameBuild = DeepReadonly<InputGameBuild>;

export const $InputGameBuild: RecordIoType<InputGameBuild> = new RecordType<InputGameBuild>({
  properties: {
    version: {type: $VersionString},
    gitCommitRef: {type: $NullableGitCommitRef, optional: true},
    mainLocale: {type: $LocaleId},
    displayName: {type: $GameDisplayName},
    description: {type: $GameDescription},
    icon: {type: $NullableBlobIdRef},
    loader: {type: $VersionString},
    engine: {type: $InputGameEngine},
    patcher: {type: $NullableInputGamePatcher, optional: true},
    debug: {type: $NullableBlobIdRef},
    content: {type: $NullableBlobIdRef},
    contentI18n: {type: $NullableBlobIdRef},
    musics: {type: new ArrayType({itemType: $InputGameResource, maxLength: 50})},
    modes: {type: $GameModeSpecMap},
    families: {type: $FamiliesString},
    category: {type: $GameCategory},
    i18n: {type: $InputGameBuildI18nMap, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
