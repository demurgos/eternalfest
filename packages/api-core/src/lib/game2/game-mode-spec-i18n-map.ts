import {MapType} from "kryo/map";

import {$GameModeKey, GameModeKey} from "../game/game-mode-key.js";
import {$GameModeSpecI18n, GameModeSpecI18n} from "./game-mode-spec-i18n.js";

export type GameModeSpecI18nMap = Map<GameModeKey, GameModeSpecI18n>;

export const $GameModeSpecI18nMap: MapType<GameModeKey, GameModeSpecI18n> = new MapType({
  keyType: $GameModeKey,
  valueType: $GameModeSpecI18n,
  maxSize: 500,
  assumeStringKey: true,
});
