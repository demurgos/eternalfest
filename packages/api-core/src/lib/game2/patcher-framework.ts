import {CaseStyle} from "kryo";
import {RecordIoType, RecordType} from "kryo/record";
import {$Ucs2String} from "kryo/ucs2-string";

import {DeepReadonly} from "../types/deep-readonly.js";
import {$VersionString, VersionString} from "../types/version-string.js";

export interface PatcherFramework {
  name: string;
  version: VersionString;
}

export type ReadonlyPatcherFramework = DeepReadonly<PatcherFramework>;

export const $PatcherFramework: RecordIoType<PatcherFramework> = new RecordType<PatcherFramework>({
  properties: {
    name: {type: $Ucs2String},
    version: {type: $VersionString},
  },
  changeCase: CaseStyle.SnakeCase,
});
