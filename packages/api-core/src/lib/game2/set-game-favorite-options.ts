import {CaseStyle} from "kryo";
import {$Boolean} from "kryo/boolean";
import {RecordIoType, RecordType} from "kryo/record";

import {DeepReadonly} from "../types/deep-readonly.js";
import {$GameRef, GameRef} from "./game-ref.js";

export interface SetGameFavoriteOptions {
  game: GameRef;
  favorite: boolean;
}

export type ReadonlySetGameFavoriteOptions = DeepReadonly<SetGameFavoriteOptions>;

export const $SetGameFavoriteOptions: RecordIoType<SetGameFavoriteOptions> = new RecordType<SetGameFavoriteOptions>({
  properties: {
    game: {type: $GameRef },
    favorite: {type: $Boolean },
  },
  changeCase: CaseStyle.SnakeCase,
});
