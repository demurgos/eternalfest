import {MapType} from "kryo/map";

import {$GameModeKey, GameModeKey} from "../game/game-mode-key.js";
import {$GameModeSpec, GameModeSpec} from "./game-mode-spec.js";

export type GameModeSpecMap = Map<GameModeKey, GameModeSpec>;

export const $GameModeSpecMap: MapType<GameModeKey, GameModeSpec> = new MapType({
  keyType: $GameModeKey,
  valueType: $GameModeSpec,
  maxSize: 500,
  assumeStringKey: true,
});
