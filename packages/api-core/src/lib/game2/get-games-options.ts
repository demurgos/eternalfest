import {CaseStyle} from "kryo";
import {$Boolean} from "kryo/boolean";
import {$Date} from "kryo/date";
import {$Uint32} from "kryo/integer";
import {RecordIoType, RecordType} from "kryo/record";

export interface GetGamesOptions {
  offset?: number,
  limit?: number,
  favorite?: boolean,
  time?: Date,
}

export const $GetGamesOptions: RecordIoType<GetGamesOptions> = new RecordType<GetGamesOptions>({
  properties: {
    offset: {type: $Uint32, optional: true},
    limit: {type: $Uint32, optional: true},
    favorite: {type: $Boolean, optional: true},
    time: {type: $Date, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
