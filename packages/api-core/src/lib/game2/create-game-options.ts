import {CaseStyle} from "kryo";
import { ArrayType } from "kryo/array";
import {RecordIoType, RecordType} from "kryo/record";

import {$NullableGameKey, NullableGameKey} from "../game/game-key.js";
import {DeepReadonly} from "../types/deep-readonly.js";
import {$NullableUserIdRef, NullableUserIdRef} from "../user/user-id-ref.js";
import {$InputGameBuild, InputGameBuild} from "./input-game-build.js";
import {$InputGameChannel, InputGameChannel} from "./input-game-channel.js";

export interface CreateGameOptions {
  owner?: NullableUserIdRef;
  key?: NullableGameKey;
  build: InputGameBuild;
  channels: InputGameChannel[];
}

export type ReadonlyCreateGameOptions = DeepReadonly<CreateGameOptions>;

export const $CreateGameOptions: RecordIoType<CreateGameOptions> = new RecordType<CreateGameOptions>({
  properties: {
    owner: {type: $NullableUserIdRef, optional: true},
    key: {type: $NullableGameKey, optional: true},
    build: {type: $InputGameBuild },
    channels: {type: new ArrayType({itemType: $InputGameChannel, minLength: 1, maxLength: 10}) },
  },
  changeCase: CaseStyle.SnakeCase,
});
