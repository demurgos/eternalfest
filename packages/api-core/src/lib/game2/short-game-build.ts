import {CaseStyle} from "kryo";
import {MapType} from "kryo/map";
import {RecordIoType, RecordType} from "kryo/record";

import {$NullableBlob, NullableBlob} from "../blob/blob.js";
import {$GameDescription, GameDescription} from "../game/game-description.js";
import {$GameDisplayName, GameDisplayName} from "../game/game-display-name.js";
import {DeepReadonly} from "../types/deep-readonly.js";
import {$NullableGitCommitRef, NullableGitCommitRef} from "../types/git-commit-ref.js";
import {$LocaleId, LocaleId} from "../types/locale-id.js";
import {$VersionString, VersionString} from "../types/version-string.js";
import {$ShortGameBuildI18n, ShortGameBuildI18n} from "./short-game-build-i18n.js";

export interface ShortGameBuild {
  version: VersionString;
  gitCommitRef: NullableGitCommitRef;
  mainLocale: LocaleId;
  displayName: GameDisplayName,
  description: GameDescription,
  icon: NullableBlob,
  i18n: Map<LocaleId, ShortGameBuildI18n>,
}

export type ReadonlyShortGameBuild = DeepReadonly<ShortGameBuild>;

export const $ShortGameBuild: RecordIoType<ShortGameBuild> = new RecordType<ShortGameBuild>({
  properties: {
    version: {type: $VersionString},
    gitCommitRef: {type: $NullableGitCommitRef},
    mainLocale: {type: $LocaleId},
    displayName: {type: $GameDisplayName},
    description: {type: $GameDescription},
    icon: {type: $NullableBlob},
    i18n: {
      type: new MapType({
        keyType: $LocaleId,
        valueType: $ShortGameBuildI18n,
        maxSize: 500,
        assumeStringKey: true,
      }),
    },
  },
  changeCase: CaseStyle.SnakeCase,
});
