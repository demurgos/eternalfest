import {AuthContext} from "../auth/auth-context.js";
import {DeepReadonly} from "../types/deep-readonly.js";
import {ActiveGameChannel} from "./active-game-channel.js";
import {CreateGameBuildOptions} from "./create-game-build-options.js";
import {CreateGameOptions} from "./create-game-options.js";
import {Game, NullableGame} from "./game.js";
import {GameBuild} from "./game-build.js";
import {GetGameOptions} from "./get-game-options.js";
import {GetGamesOptions} from "./get-games-options.js";
import {SetGameFavoriteOptions} from "./set-game-favorite-options.js";
import {ShortGameListing} from "./short-game-listing.js";
import {UpdateGameChannelOptions} from "./update-game-channel-options.js";

export interface ReadonlyGame2Service {
  createGame(acx: AuthContext, options: DeepReadonly<CreateGameOptions>): Promise<Game>;

  getGame(acx: AuthContext, options: DeepReadonly<GetGameOptions>): Promise<NullableGame>;

  getGames(acx: AuthContext, options: DeepReadonly<GetGamesOptions>): Promise<ShortGameListing>;

  setGameFavorite(acx: AuthContext, options: DeepReadonly<SetGameFavoriteOptions>): Promise<unknown>;

  createBuild(acx: AuthContext, options: DeepReadonly<CreateGameBuildOptions>): Promise<GameBuild>;

  updateChannel(acx: AuthContext, options: DeepReadonly<UpdateGameChannelOptions>): Promise<ActiveGameChannel>;
}

export interface Game2Service extends ReadonlyGame2Service {
}
