import {CaseStyle} from "kryo";
import {$Boolean} from "kryo/boolean";
import {$Date} from "kryo/date";
import {LiteralType} from "kryo/literal";
import {$Null} from "kryo/null";
import {RecordIoType, RecordType} from "kryo/record";
import {TryUnionType} from "kryo/try-union";

import {DeepReadonly} from "../types/deep-readonly.js";
import {$NullableDate, NullableDate} from "../types/nullable-date.js";
import {$ObjectType, ObjectType} from "../types/object-type.js";
import {$GameChannelKey, GameChannelKey} from "./game-channel-key.js";
import {$GameChannelPermission, GameChannelPermission} from "./game-channel-permission.js";
import {$ShortGameBuild, ShortGameBuild} from "./short-game-build.js";

export interface ShortGameChannel {
  type: ObjectType.GameChannel;
  key: GameChannelKey;
  isEnabled: boolean;
  isPinned: boolean;
  publicationDate: NullableDate;
  sortUpdateDate: Date;
  defaultPermission: GameChannelPermission;
  build: ShortGameBuild;
}

export type ReadonlyShortGameChannel = DeepReadonly<ShortGameChannel>;

export const $ShortGameChannel: RecordIoType<ShortGameChannel> = new RecordType<ShortGameChannel>({
  properties: {
    type: {type: new LiteralType({type: $ObjectType, value: ObjectType.GameChannel as ObjectType.GameChannel})},
    key: {type: $GameChannelKey},
    isEnabled: {type: $Boolean},
    isPinned: {type: $Boolean},
    publicationDate: {type: $NullableDate},
    sortUpdateDate: {type: $Date},
    defaultPermission: {type: $GameChannelPermission},
    build: {type: $ShortGameBuild},
  },
  changeCase: CaseStyle.SnakeCase,
});

export type NullableShortGameChannel = null | ShortGameChannel;

export const $NullableShortGameChannel: TryUnionType<NullableShortGameChannel> = new TryUnionType({variants: [$Null, $ShortGameChannel]});
