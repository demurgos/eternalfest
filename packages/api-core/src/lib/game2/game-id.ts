import { Ucs2StringType } from "kryo/ucs2-string";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

export type GameId = UuidHex;

export const $GameId: Ucs2StringType = $UuidHex;
