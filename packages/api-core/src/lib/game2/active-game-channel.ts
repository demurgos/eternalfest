import {CaseStyle} from "kryo";
import {$Boolean} from "kryo/boolean";
import {$Date} from "kryo/date";
import {LiteralType} from "kryo/literal";
import {RecordIoType, RecordType} from "kryo/record";

import {DeepReadonly} from "../types/deep-readonly.js";
import {$NullableDate, NullableDate} from "../types/nullable-date.js";
import {$ObjectType, ObjectType} from "../types/object-type.js";
import {$GameBuild, GameBuild} from "./game-build.js";
import {$GameChannelKey, GameChannelKey} from "./game-channel-key.js";
import {$GameChannelPermission, GameChannelPermission} from "./game-channel-permission.js";

export interface ActiveGameChannel {
  type: ObjectType.GameChannel;
  key: GameChannelKey;
  isEnabled: boolean;
  isPinned: boolean;
  publicationDate: NullableDate;
  sortUpdateDate: Date;
  defaultPermission: GameChannelPermission;
  build: GameBuild;
}

export type ReadonlyActiveGameChannel = DeepReadonly<ActiveGameChannel>;

export const $ActiveGameChannel: RecordIoType<ActiveGameChannel> = new RecordType<ActiveGameChannel>({
  properties: {
    type: {type: new LiteralType({type: $ObjectType, value: ObjectType.GameChannel as ObjectType.GameChannel})},
    key: {type: $GameChannelKey},
    isEnabled: {type: $Boolean},
    isPinned: {type: $Boolean},
    publicationDate: {type: $NullableDate},
    sortUpdateDate: {type: $Date},
    defaultPermission: {type: $GameChannelPermission},
    build: {type: $GameBuild},
  },
  changeCase: CaseStyle.SnakeCase,
});
