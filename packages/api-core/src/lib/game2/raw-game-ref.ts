import {TryUnionType} from "kryo/try-union";

import {$GameKey, GameKey} from "../game/game-key.js";
import {$GameId, GameId} from "./game-id.js";

export type RawGameRef = GameId | GameKey;

export const $RawGameRef: TryUnionType<RawGameRef> = new TryUnionType<RawGameRef>({
  variants: [$GameId, $GameKey],
});
