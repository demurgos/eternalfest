import {$Null} from "kryo/null";
import {TryUnionType} from "kryo/try-union";
import {Ucs2StringType} from "kryo/ucs2-string";

export type GameResourceDisplayName = string;

export const $GameResourceDisplayName: Ucs2StringType = new Ucs2StringType({
  maxLength: 100,
});

export type NullableGameResourceDisplayName = null | GameResourceDisplayName;

export const $NullableGameResourceDisplayName: TryUnionType<NullableGameResourceDisplayName> = new TryUnionType({variants: [$Null, $GameResourceDisplayName]});
