import {MapType} from "kryo/map";

import {$LocaleId, LocaleId} from "../types/locale-id.js";
import {$GameBuildI18n, GameBuildI18n} from "./game-build-i18n.js";

export type GameBuildI18nMap = Map<LocaleId, GameBuildI18n>;

export const $GameBuildI18nMap: MapType<LocaleId, GameBuildI18n> = new MapType({
  keyType: $LocaleId,
  valueType: $GameBuildI18n,
  maxSize: 500,
  assumeStringKey: true,
});
