import { CaseStyle } from "kryo";
import { TsEnumType } from "kryo/ts-enum";

export enum GameEngineType {
  V96,
  Custom,
}

export const $GameEngineType: TsEnumType<GameEngineType> = new TsEnumType(() => ({
  enum: GameEngineType,
  changeCase: CaseStyle.PascalCase,
}));
