import { $Null } from "kryo/null";
import { TryUnionType } from "kryo/try-union";
import { Ucs2StringType } from "kryo/ucs2-string";

export type GameChannelKey = string;

export const $GameChannelKey: Ucs2StringType = new Ucs2StringType({maxLength: 32});

export type NullableGameChannelKey = null | GameChannelKey;

export const $NullableGameChannelKey: TryUnionType<NullableGameChannelKey> = new TryUnionType({variants: [$Null, $GameChannelKey]});
