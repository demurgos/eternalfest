import {RecordIoType} from "kryo/record";

import {DeepReadonly} from "../types/deep-readonly.js";
import {$Listing, Listing} from "../types/listing.js";
import {$NullableShortGame, NullableShortGame} from "./short-game.js";

export type ShortGameListing = Listing<NullableShortGame>;

export type ReadonlyShortGameListing = DeepReadonly<ShortGameListing>;

export const $ShortGameListing: RecordIoType<ShortGameListing> = $Listing.apply($NullableShortGame) as RecordIoType<ShortGameListing>;
