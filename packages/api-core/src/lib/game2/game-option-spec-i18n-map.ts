import {MapType} from "kryo/map";

import {$GameOptionKey, GameOptionKey} from "../game/game-option-key.js";
import {$GameOptionSpecI18n, GameOptionSpecI18n} from "./game-option-spec-i18n.js";

export type GameOptionSpecI18nMap = Map<GameOptionKey, GameOptionSpecI18n>;

export const $GameOptionSpecI18nMap: MapType<GameOptionKey, GameOptionSpecI18n> = new MapType({
  keyType: $GameOptionKey,
  valueType: $GameOptionSpecI18n,
  maxSize: 500,
  assumeStringKey: true,
});
