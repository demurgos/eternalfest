import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

import { $FileId, FileId } from "./file-id.js";

export interface GetFileOptions {
  id: FileId;
}

export const $GetFileOptions: RecordIoType<GetFileOptions> = new RecordType<GetFileOptions>({
  properties: {
    id: {type: $FileId},
  },
  changeCase: CaseStyle.SnakeCase,
});
