import { Ucs2StringType } from "kryo/ucs2-string";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

export type FileId = UuidHex;

export const $FileId: Ucs2StringType = $UuidHex;
