import { Ucs2StringType } from "kryo/ucs2-string";

export type DriveItemDisplayName = string;

export const $DriveItemDisplayName: Ucs2StringType = new Ucs2StringType({
  maxLength: 100,
});
