import { RecordIoType, RecordType } from "kryo/record";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

export interface DeleteItemOptions {
  id: UuidHex;
}

// tslint:disable-next-line:max-line-length
export const $DeleteItemOptions: RecordIoType<DeleteItemOptions> = new RecordType<DeleteItemOptions>(() => ({
  properties: {
    id: {type: $UuidHex},
  },
}));
