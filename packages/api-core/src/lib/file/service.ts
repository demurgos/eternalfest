import { UuidHex } from "kryo/uuid-hex";
import stream from "stream";

import { AuthContext } from "../auth/auth-context.js";
import { CreateDirectoryOptions } from "./create-directory-options.js";
import { CreateFileOptions } from "./create-file-options.js";
import { DeleteItemOptions } from "./delete-item-options.js";
import { Directory } from "./directory.js";
import { Drive } from "./drive.js";
import { DriveItem } from "./drive-item.js";
import { File } from "./file.js";
import { GetItemByPathOptions } from "./get-item-by-path-options.js";

export interface FileService {
  /**
   * Returns the drive with this id.
   *
   * @param auth Authentication context for this operation.
   * @param driveId The ID of the drive to retrieve.
   * @return The corresponding drive
   * @throws DriveNotFound If there is no drive for the supplied id.
   * @throws Unauthorized If the auth context is not an administrator or the owner of the drive.
   */
  getDriveById(auth: AuthContext, driveId: UuidHex): Promise<Drive>;

  /**
   * Returns the main drive for the supplied owner.
   *
   * @param auth Auth context for this operation.
   * @param ownerId Id of the owner (a user).
   * @return The corresponding drive.
   * @throws OwnerNotFound If the supplied owner id does not exist.
   * @throws Unauthorized If the auth context is not an administrator or the owner.
   */
  getDriveByOwnerId(auth: AuthContext, ownerId: UuidHex): Promise<Drive>;

  /**
   * Returns the item corresponding to the supplied path of display names.
   *
   * @param auth Authentication context for this operation.
   * @param options The ID of the drive used to resolve this path and list of display names
   * corresponding to the parts of this path.
   * @return The corresponding drive item.
   * @throws DriveNotFound If there is no drive for the supplied drive id
   * @throws Unauthorized If the auth context is not an administrator or the owner.
   * @throws DriveItemNotFound If no item corresponds to the supplied path of display names
   */
  getItemByPath(auth: AuthContext, options: Readonly<GetItemByPathOptions>): Promise<DriveItem>;

  /**
   * Returns the directory corresponding to the supplied id.
   *
   * @param auth Authentication context for this operation.
   * @param directoryId The ID of the directory.
   * @return The corresponding directory.
   * @throws DirectoryNotFound If there is no directory for the supplied directory id.
   * @throws Unauthorized If the auth context is not an administrator or the owner.
   */
  getDirectoryById(auth: AuthContext, directoryId: UuidHex): Promise<Directory>;

  /**
   * Returns the drive items that are direct children of the directory with the supplied id
   *
   * @param auth Authentication context for this operation.
   * @param directoryId The ID of the directory.
   * @return The children drive items.
   * @throws DirectoryNotFound If there is no directory for the supplied directory id.
   * @throws Unauthorized If the auth context is not an administrator or the owner.
   */
  getDirectoryChildrenById(auth: AuthContext, directoryId: UuidHex): Promise<ReadonlyArray<DriveItem>>;

  /**
   * Returns the file corresponding to the supplied id.
   *
   * @param auth Authentication context for this operation.
   * @param fileId The ID of the file.
   * @return The corresponding file.
   * @throws FileNotFound If there is no file for the supplied id.
   */
  getFileById(auth: AuthContext, fileId: UuidHex): Promise<File>;

  /**
   * Creates a file in the directory corresponding to the supplied id.
   *
   * @param auth Authentication context for this operation
   * @param options new file.
   * @throws DirectoryNotFound If the parent directory does not exist.
   * @throws Unauthorized If the auth context is not an administrator or the owner.
   */
  createFile(auth: AuthContext, options: CreateFileOptions): Promise<File>;

  /**
   * Creates a sub-directory in the directory corresponding to the supplied id.
   *
   * @param auth Authentication context for this operation
   * @param options Options for the new directory
   * @return The new directory.
   * @throws DirectoryNotFound If the parent directory does not exist.
   * @throws Unauthorized If the auth context is not an administrator or the owner.
   */
  createDirectory(auth: AuthContext, options: CreateDirectoryOptions): Promise<Directory>;

  /**
   * Creates a sub-directory in the directory corresponding to the supplied id.
   *
   * @param auth Authentication context for this operation
   * @param options The id of the item to delete
   * @throws DriveItemNotFound If item does not exist.
   * @throws Unauthorized If the auth context is not an administrator or the owner; or the item is
   *                      the root directory or used.
   */
  deleteItem(auth: AuthContext, options: Readonly<DeleteItemOptions>): Promise<void>;

  /**
   * Returns a readable stream for the content of this file.
   *
   * @param auth Authentication context for this operation
   * @param fileId The id of the file to read.
   * @return Readable stream for the content of this file.
   */
  readFileContent(auth: AuthContext, fileId: UuidHex): Promise<stream.Readable>;
}
