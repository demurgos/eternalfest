import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

import { $DriveId, DriveId } from "./drive-id.js";

export interface GetDriveOptions {
  id: DriveId;
}

export const $GetDriveOptions: RecordIoType<GetDriveOptions> = new RecordType<GetDriveOptions>({
  properties: {
    id: {type: $DriveId},
  },
  changeCase: CaseStyle.SnakeCase,
});
