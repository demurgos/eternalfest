import { CaseStyle } from "kryo";
import { ArrayType } from "kryo/array";
import { $Date } from "kryo/date";
import { LiteralType } from "kryo/literal";
import { RecordIoType, RecordType } from "kryo/record";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

import { $DriveItem, DriveItem } from "./drive-item.js";
import { $DriveItemDisplayName, DriveItemDisplayName } from "./drive-item-display-name.js";
import { $DriveItemType, DriveItemType } from "./drive-item-type.js";

export interface Directory {
  type: DriveItemType.Directory;
  id: UuidHex;
  displayName: DriveItemDisplayName;
  children?: DriveItem[];
  createdAt: Date;
  updatedAt: Date;
}

export const $Directory: RecordIoType<Directory> = new RecordType<Directory>(() => ({
  properties: {
    type: {type: new LiteralType({type: $DriveItemType, value: DriveItemType.Directory as DriveItemType.Directory})},
    id: {type: $UuidHex},
    displayName: {type: $DriveItemDisplayName},
    children: {type: new ArrayType({itemType: $DriveItem, maxLength: Infinity}), optional: true},
    createdAt: {type: $Date},
    updatedAt: {type: $Date},
  },
  changeCase: CaseStyle.SnakeCase,
}));
