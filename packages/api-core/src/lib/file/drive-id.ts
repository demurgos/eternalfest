import { Ucs2StringType } from "kryo/ucs2-string";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

export type DriveId = UuidHex;

export const $DriveId: Ucs2StringType = $UuidHex;
