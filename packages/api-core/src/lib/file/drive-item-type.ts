import { CaseStyle } from "kryo";
import { TsEnumType } from "kryo/ts-enum";

export enum DriveItemType {
  Directory,
  File,
}

export const $DriveItemType: TsEnumType<DriveItemType> = new TsEnumType(() => ({
  enum: DriveItemType,
  changeCase: CaseStyle.PascalCase,
}));
