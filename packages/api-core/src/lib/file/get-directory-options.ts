import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

import { $DirectoryId, DirectoryId } from "./directory-id.js";

export interface GetDirectoryOptions {
  id: DirectoryId;
}

export const $GetDirectoryOptions: RecordIoType<GetDirectoryOptions> = new RecordType<GetDirectoryOptions>({
  properties: {
    id: {type: $DirectoryId},
  },
  changeCase: CaseStyle.SnakeCase,
});
