import { CaseStyle } from "kryo";
import { ArrayType } from "kryo/array";
import { RecordIoType, RecordType } from "kryo/record";

import { $DriveId, DriveId } from "./drive-id.js";
import { $DriveItemDisplayName, DriveItemDisplayName } from "./drive-item-display-name.js";

export interface GetItemByPathOptions {
  drive: DriveId;
  path: DriveItemDisplayName[];
}

export const $GetItemByPathOptions: RecordIoType<GetItemByPathOptions> = new RecordType<GetItemByPathOptions>({
  properties: {
    drive: {type: $DriveId},
    path: {type: new ArrayType({itemType: $DriveItemDisplayName, maxLength: 10})},
  },
  changeCase: CaseStyle.SnakeCase,
});
