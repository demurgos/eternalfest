import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

import { $UserId, UserId } from "../user/user-id.js";

export interface GetDriveByOwnerOptions {
  id: UserId;
}

export const $GetDriveByOwnerOptions: RecordIoType<GetDriveByOwnerOptions> = new RecordType<GetDriveByOwnerOptions>({
  properties: {
    id: {type: $UserId},
  },
  changeCase: CaseStyle.SnakeCase,
});
