import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

import { $DriveItemDisplayName, DriveItemDisplayName } from "./drive-item-display-name.js";

export interface CreateDirectoryOptions {
  parentId: UuidHex;

  displayName: DriveItemDisplayName;
}

// tslint:disable-next-line:max-line-length
export const $CreateDirectoryOptions: RecordIoType<CreateDirectoryOptions> = new RecordType<CreateDirectoryOptions>(() => ({
  properties: {
    parentId: {type: $UuidHex},
    displayName: {type: $DriveItemDisplayName},
  },
  changeCase: CaseStyle.SnakeCase,
}));
