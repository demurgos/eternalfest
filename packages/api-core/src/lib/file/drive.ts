import { CaseStyle } from "kryo";
import { $Date } from "kryo/date";
import { RecordIoType, RecordType } from "kryo/record";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

import { $IdRef, IdRef } from "../types/id-ref.js";

export interface Drive {
  id: UuidHex;
  owner: IdRef;
  root: IdRef;
  createdAt: Date;
  updatedAt: Date;
}

export const $Drive: RecordIoType<Drive> = new RecordType<Drive>(() => ({
  properties: {
    id: {type: $UuidHex},
    owner: {type: $IdRef},
    root: {type: $IdRef},
    createdAt: {type: $Date},
    updatedAt: {type: $Date},
  },
  changeCase: CaseStyle.SnakeCase,
}));
