import { TaggedUnionType } from "kryo/tagged-union";

import { $Directory, Directory } from "./directory.js";
import { $File, File } from "./file.js";

export type DriveItem = Directory | File;

export const $DriveItem: TaggedUnionType<DriveItem> = new TaggedUnionType<DriveItem>(() => ({
  variants: [$Directory, $File],
  tag: "type",
}));
