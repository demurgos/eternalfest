import { Ucs2StringType } from "kryo/ucs2-string";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

export type DirectoryId = UuidHex;

export const $DirectoryId: Ucs2StringType = $UuidHex;
