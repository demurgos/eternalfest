import { TaggedUnionType } from "kryo/tagged-union";

import { $HfestIdentity, HfestIdentity } from "../../hfest-identity/hfest-identity.js";
import { $EternalfestIdentity, EternalfestIdentity } from "./eternalfest-identity.js";

export type Identity = EternalfestIdentity | HfestIdentity;

export const $Identity: TaggedUnionType<Identity> = new TaggedUnionType<Identity>({
  variants: [$EternalfestIdentity, $HfestIdentity],
  tag: "type",
});
