import { CaseStyle } from "kryo";
import { $Uint32 } from "kryo/integer";
import { RecordIoType, RecordType } from "kryo/record";

export interface ItemCount {
  count: number;
  maxCount: number;
}

export const $ItemCount: RecordIoType<ItemCount> = new RecordType<ItemCount>({
  properties: {
    count: {type: $Uint32},
    maxCount: {type: $Uint32},
  },
  changeCase: CaseStyle.SnakeCase,
});
