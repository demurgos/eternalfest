import {CaseStyle, IoType} from "kryo";
import {ArrayType} from "kryo/array";
import {$Boolean} from "kryo/boolean";
import {GenericIoType, GenericType} from "kryo/generic";
import {$Uint32} from "kryo/integer";
import {RecordIoType, RecordType} from "kryo/record";

export interface Listing<T> {
  offset: number;
  limit: number;
  count: number;
  isCountExact: boolean;
  items: T[];
}

export const $Listing: GenericIoType<<T>(t: T) => Listing<T>> = new GenericType({
  apply: <T, >(t: IoType<T>): RecordIoType<Listing<T>> => new RecordType({
    properties: {
      offset: {type: $Uint32},
      limit: {type: $Uint32},
      count: {type: $Uint32},
      isCountExact: {type: $Boolean},
      items: {
        type: new ArrayType({
          itemType: t as any,
          maxLength: 100,
        })
      },
    },
    changeCase: CaseStyle.SnakeCase,
  }),
});
