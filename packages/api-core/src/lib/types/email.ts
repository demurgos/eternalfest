import { Ucs2StringType } from "kryo/ucs2-string";

export type Email = string;

export const $Email: Ucs2StringType = new Ucs2StringType({
  trimmed: true,
  minLength: 3,
  maxLength: 120,
  pattern: /@/,
});
