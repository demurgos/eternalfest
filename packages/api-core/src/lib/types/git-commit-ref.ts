import {$Null} from "kryo/null";
import {TryUnionType} from "kryo/try-union";
import {$Ucs2String, Ucs2StringType} from "kryo/ucs2-string";

export type GitCommitRef = string;

export const $GitCommitRef: Ucs2StringType = $Ucs2String;

export type NullableGitCommitRef = null | GitCommitRef;

export const $NullableGitCommitRef: TryUnionType<NullableGitCommitRef> = new TryUnionType({variants: [$Null, $GitCommitRef]});
