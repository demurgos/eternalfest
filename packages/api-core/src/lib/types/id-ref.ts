import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

export interface IdRef {
  id: UuidHex;
}

export const $IdRef: RecordIoType<IdRef> = new RecordType<IdRef>({
  properties: {
    id: {type: $UuidHex},
  },
  changeCase: CaseStyle.SnakeCase,
});
