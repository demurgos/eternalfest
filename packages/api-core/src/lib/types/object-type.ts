import { TsEnumType } from "kryo/ts-enum";

/**
 * Tag identifying the object types.
 *
 * It helps with discriminated unions and reflection.
 */
export enum ObjectType {
  Blob,
  Game,
  GameChannel,
  OauthClient,
  User,
}

export const $ObjectType: TsEnumType<ObjectType> = new TsEnumType<ObjectType>({
  enum: ObjectType,
});
