import {$Ucs2String, Ucs2StringType} from "kryo/ucs2-string";

export type VersionString = string;

export const $VersionString: Ucs2StringType = $Ucs2String;
