export interface HammerfestSession {
  readonly userId: string;
  readonly sessionId: string;
  readonly server: "fr" | "en" | "es";
}

export interface HfestApiService {
  /**
   * Verify that the session is valid: a user with the supplied id exists
   * on the corresponding server and the session hasn't expired.
   *
   * TODO: ensureValidSession: void and throws an error with details if invalid
   *
   * @param session The Hammerfest session to test.
   * @return Boolean indicating if the session is valid
   */
  testSession(session: HammerfestSession): Promise<boolean>;
}
