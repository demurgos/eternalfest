import { Ucs2StringType } from "kryo/ucs2-string";

export type HfestItemId = string;

export const $HfestItemId: Ucs2StringType = new Ucs2StringType({
  minLength: 1,
  maxLength: 4,
  trimmed: true,
  pattern: /^\d+$/,
});
