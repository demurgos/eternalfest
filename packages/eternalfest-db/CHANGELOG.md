# 0.8.3 (2021-03-29)

- **[Fix]** Add standalone creation script for schema version 3 to 8.

# 0.8.2 (2019-12-31)

- **[Fix]** Update dependencies.

# 0.8.1 (2019-10-22)

- **[Fix]** Change `user_items.item_count` type from `BIGINT` to `INT`.

# 0.8.0 (2019-10-21)

- **[Breaking change]** Update latest schema version to `V008`.
- **[Breaking change]** Add `main_locale` to `games`, `game_modes` and `game_options`.
- **[Breaking change]** Add `display_name` to `game_modes` and `game_options`.
- **[Feature]** Add `LOCALE_ID` enum type.
- **[Feature]** Add `game_locales`, `game_mode_locales` and `game_option_locales`.
- **[Feature]** Add `user_items` view.

# 0.7.0 (2019-05-17)

- **[Breaking change]** Update latest schema version to `V007`.
- **[Breaking change]** Rename `mode_tag` to `mode_key` and `option_tag` to `option_key`.
- **[Feature]** Add `stats` field to `run_results`.
- **[Feature]** Ensure all schemas have a `version` comment.
- **[Internal]** Update `CONTRIBUTING.md`.

# 0.6.0 (2019-05-14)

- **[Breaking change]** Restrict dropping files and blobs in use.
- **[Breaking change]** Update latest schema version to `V006`.
- **[Feature]** Support passing DB version to `restore` command.
- **[Feature]** Use `Sint32` for `run_result.scores[i]`.
- **[Feature]** Create `blob_deletion_queue` table.
- **[Fix]** Fix creation of void DB for V003 and V004.
- **[Fix]** Add foreign key constraint for game icons.

# 0.4.0 (2019-01-03)

- **[Breaking change]** Release schema `004` with updated `run_results`. Column `raw` is dropped,
  `scores`, `max_level` and `items` are added.

# 0.3.1 (2018-12-31)

- **[Fix]** Set `DbVersion.V003` as latest version.

# 0.3.0 (2018-12-31)

- **[Breaking change]** Add version `003`.
- **[Breaking change]** Rename `runs#details` to `runs#detail`.
- **[Feature]** Add `runs#volume`.
- **[Feature]** Add `hfest_items#is_hidden`.
- **[Feature]** Add `run_results`.
- **[Feature]** Add upgrade script, from `001` to `002`.
- **[Feature]** Add upgrade script, from `002` to `003`.

# 0.2.5 (2018-12-16)

- **[Feature]** Add `runs` table.

# 0.2.4 (2018-12-15)

- **[Fix]** Fix typo in exported db enums.

# 0.2.3 (2018-12-14)

- **[Feature]** Add CLI command to create an empty DB.
- **[Feature]** Add `public_access_from` to `games`.
- **[Fix]** Expose all enum values as constants.

# 0.2.2 (2018-12-11)

- **[Breaking change]** Move foreign key constraint from `hfest_identities` to `identities`
- **[Fix]** Expose `Session` schema.
- **[Fix]** Allow `user_id` to be `null` schema.

# 0.2.1 (2018-12-06)

**[Fix]** Add `buffer_id` to upload sessions.

# 0.2.0 (2018-12-06)

- **[Breaking change]** Rename `DbState` to `DbVersion`, change `setState` signature.
- **[Breaking change]** The hammerfest ids are now `VARCHAR(4)` instead of `INT`.
- **[Feature]** Add tables for `FileService`.
- **[Feature]** Add support for backups.
- **[Fix]** `DbState.Initial` correctly populates the DB with Hammerfest items.

# 0.1.0

- **[Internal]** First commit.
