# Contributing

## Development

### Required environment

- [_Node.js_][node-home]: `12.x.x`
- [_npm_][npm-cli]: `6.9.x`, bundled with Node (check the version)
- [_git_][git-home]: should be in the `PATH`
- [_postgres_][postgres-home]: `10.7` or higher

### Set-up the project

```shell
git clone https://gitlab.com/eternalfest/eternalfest-db.git
cd eternalfest-db
npm install
```

### Create the database

Open a postgres session with the proper rights (for example with `sudo -u postgres -i psql`) then
create a fresh user and database with the following SQL commands:
```sql
CREATE USER eternalfest WITH ENCRYPTED PASSWORD '<password>';
CREATE DATABASE eternalfest_db WITH owner=eternalfest encoding='UTF-8';
\c eternalfest_db
ALTER SCHEMA public OWNER TO eternalfest;
\q
```

Copy the `.env.example` file to `.env` and fill in the user and database you just created.

Finally, run `npm test` to check that everything is working.

[node-home]: https://nodejs.org/en/
[npm-cli]: https://docs.npmjs.com/cli/npm
[git-home]: https://git-scm.com/
[postgres-home]: https://www.postgresql.org/