# Eternalfest DB

[![build status](https://gitlab.com/eternalfest/eternalfest-db/badges/master/build.svg)](https://gitlab.com/eternalfest/eternalfest-db/commits/master)

Main DB for Eternalfest.

## Additional constraints that must be handled manually

- A `user` cannot be linked to two `identity` of type `hfest` with the same `server` value.
- If a `user` exists, then there is at least one `is_administrator`.

## Changelog

See [CHANGELOG.md](./CHANGELOG.md).

## License

[MIT License © 2018 Eternalfest](./LICENSE.md)
