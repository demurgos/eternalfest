import { Url } from "@eternalfest/api-core/types/url";

export interface ServerAppConfig {
  externalBaseUri?: Url;
  isIndexNextToServerMain: boolean;
  isProduction: boolean;
  efApi?: any;
}
