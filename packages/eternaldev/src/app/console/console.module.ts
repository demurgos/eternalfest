import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { HelpersModule } from "../../modules/helpers/helpers.module";
import { ConsoleComponent } from "./console.component";
import { ConsoleRowComponent } from "./console-row.component";
import { PrintConsoleComponent } from "./print-console.component";
import { PropertyDescriptorComponent } from "./property-descriptor.component";
import { RemoteObjectComponent } from "./remote-object.component";
import { TreeNodeComponent } from "./tree-node.component";
import { TreeViewComponent } from "./tree-view.component";

@NgModule({
  declarations: [
    ConsoleComponent,
    ConsoleRowComponent,
    PrintConsoleComponent,
    PropertyDescriptorComponent,
    RemoteObjectComponent,
    TreeNodeComponent,
    TreeViewComponent,
  ],
  exports: [ConsoleComponent, PrintConsoleComponent, TreeViewComponent],
  imports: [
    HelpersModule,
    CommonModule,
    FormsModule,
  ],
})
export class ConsoleModule {
}
