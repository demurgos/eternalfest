export interface TreeNode<Root, Child = Root> {
  data: Root;
  children?: TreeNode<Child>[];
  isLeaf: boolean;

  getChildren?(): Promise<TreeNode<Child>[]>;
}
