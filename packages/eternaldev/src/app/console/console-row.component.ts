import { Component, Input, OnInit } from "@angular/core";

import {ConsoleRow, TextRow} from "../../modules/console/console-row";
import { RemoteObjectNode } from "./remote-object-node";

@Component({
  selector: "efd-console-row",
  templateUrl: "./console-row.component.html",
  styleUrls: [],
})
export class ConsoleRowComponent implements OnInit {
  @Input("row")
    row!: ConsoleRow;

  remoteObjectNode?: RemoteObjectNode;

  constructor() {
  }

  ngOnInit(): void {
    if (this.row.type === "flash-object") {
      this.remoteObjectNode = new RemoteObjectNode(this.row.runtime, this.row.object);
    }
  }

  asTextRow(row: ConsoleRow): TextRow {
    return row as TextRow;
  }

  asssertRemoteObjectNode(node: undefined | RemoteObjectNode): RemoteObjectNode {
    return node!;
  }
}
