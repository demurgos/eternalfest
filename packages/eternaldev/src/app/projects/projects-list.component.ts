import { ChangeDetectorRef, Component, OnInit } from "@angular/core";

import { ShortProject } from "../../main/api/project/short-project";
import { ProjectService } from "../../modules/api/project.service";

@Component({
  selector: "efd-projects-list",
  templateUrl: "./projects-list.component.html",
  styleUrls: ["./projects-list.component.scss"],
})
export class ProjectsListComponent implements OnInit {
  projects: readonly ShortProject[] | undefined;

  private projectService: ProjectService;
  private changeDetectorRef: ChangeDetectorRef;

  constructor(changeDetectorRef: ChangeDetectorRef, projectService: ProjectService) {
    this.changeDetectorRef = changeDetectorRef;
    this.projectService = projectService;
    this.projects = undefined;
  }

  async ngOnInit(): Promise<void> {
    this.projects = await this.projectService.getProjects(true);
    this.changeDetectorRef.detectChanges();
  }
}
