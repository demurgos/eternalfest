import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";

import {HelpersModule} from "../../modules/helpers/helpers.module";
import {GameComponent} from "./game.component";

@NgModule({
  declarations: [GameComponent],
  exports: [GameComponent],
  imports: [
    HelpersModule,
    CommonModule,
    FormsModule,
  ],
})
export class GameModule {
}
