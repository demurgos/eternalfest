import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {path: "", redirectTo: "projects", pathMatch: "full"},
  {path: "projects", loadChildren: () => import("./projects/projects.module").then(({ProjectsModule}) => ProjectsModule)},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {initialNavigation: "enabledBlocking", onSameUrlNavigation: "reload"}),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
