import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { ConsoleModule } from "../modules/console/console.module";
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";

@NgModule({
  imports: [
    AppRoutingModule,
    BrowserModule.withServerTransition({appId: "eternaldev"}),
    CommonModule,
    ConsoleModule,
    HttpClientModule,
  ],
  declarations: [AppComponent],
  exports: [AppComponent],
})
export class AppModule {
}
