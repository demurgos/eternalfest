import { NgModule } from "@angular/core";

import { HfestUri } from "./hfest-uri.pipe";

@NgModule({
  declarations: [
    HfestUri,
  ],
  exports: [
    HfestUri,
  ],
})
export class HelpersModule {
}
