import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "hfestUri",
  pure: true,
})
export class HfestUri implements PipeTransform {
  transform(type: "login"): string {
    return "/login";
  }
}
