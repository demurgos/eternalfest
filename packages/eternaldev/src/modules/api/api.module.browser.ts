import { NgModule } from "@angular/core";

import { GameService } from "./game.service";
import { BrowserGameService } from "./game.service.browser";
import { GameResolverService } from "./game-resolver.service";
import { GamesResolverService } from "./games-resolver.service";
import { ProjectService } from "./project.service";
import { BrowserProjectService } from "./project.service.browser";
import { RunService } from "./run.service";
import { BrowserRunService } from "./run.service.browser";
import { RunResolverService } from "./run-resolver.service";
import { SelfUserResolverService } from "./self-user-resolver.service";
import { UserService } from "./user.service";
import { BrowserUserService } from "./user.service.browser";
import { UserResolverService } from "./user-resolver.service";

@NgModule({
  providers: [
    GameResolverService,
    GamesResolverService,
    SelfUserResolverService,
    RunResolverService,
    UserResolverService,
    {provide: GameService, useClass: BrowserGameService},
    {provide: ProjectService, useClass: BrowserProjectService},
    {provide: RunService, useClass: BrowserRunService},
    {provide: UserService, useClass: BrowserUserService},
  ],
})
export class BrowserApiModule {
}
