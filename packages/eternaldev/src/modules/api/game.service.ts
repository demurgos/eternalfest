import { Injectable } from "@angular/core";
import { ShortGame } from "@eternalfest/api-core/game/short-game";
import { Game } from "@eternalfest/api-core/game2/game";
import { UuidHex } from "kryo/uuid-hex";

@Injectable()
export abstract class GameService {
  abstract getGames(): Promise<readonly ShortGame[]>;

  abstract getGameById(gameId: UuidHex): Promise<Game | undefined>;
}
