import { Injectable } from "@angular/core";
import { Project } from "@eternalfest/api-core/eternaldev/project";
import { Game } from "@eternalfest/api-core/game2/game";
import { UuidHex } from "kryo/uuid-hex";

import { ShortProject } from "../../main/api/project/short-project";
import { ProjectService } from "./project.service";

@Injectable()
export class ServerProjectService extends ProjectService {
  constructor() {
    super();
  }

  getGameByProjectId(projectId: UuidHex): Promise<Game> {
    throw new Error("NotImplemented");
  }

  getProjectById(projectId: UuidHex): Promise<Project> {
    throw new Error("NotImplemented");
  }

  getProjects(acceptCached: boolean): Promise<ShortProject[]> {
    throw new Error("NotImplemented");
  }
}
