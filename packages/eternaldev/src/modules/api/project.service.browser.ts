import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { $Project, Project } from "@eternalfest/api-core/eternaldev/project";
import { Game } from "@eternalfest/api-core/game2/game";
import { Incident } from "incident";
import { ArrayIoType, ArrayType } from "kryo/array";
import { UuidHex } from "kryo/uuid-hex";
import { JSON_VALUE_READER } from "kryo-json/json-value-reader";

import { $ShortProject, ShortProject } from "../../main/api/project/short-project";
import { ProjectService } from "./project.service";
import {apiUri} from "./utils/api-uri";

const $ShortProjectArray: ArrayIoType<ShortProject> = new ArrayType({itemType: $ShortProject, maxLength: Infinity});

@Injectable()
export class BrowserProjectService extends ProjectService {
  private http: HttpClient;

  constructor(http: HttpClient) {
    super();
    this.http = http;
  }

  async getProjects(): Promise<ShortProject[]> {
    const raw: any = await this.http.get(apiUri("projects")).toPromise();
    let result: ShortProject[];
    try {
      result = $ShortProjectArray.read(JSON_VALUE_READER, raw);
    } catch (err) {
      console.error(err);
      result = [];
    }
    return result;
  }

  async getProjectById(projectId: UuidHex): Promise<Project> {
    const raw: any = await this.http.get(apiUri("projects", projectId)).toPromise();
    let result: Project;
    try {
      result = $Project.read(JSON_VALUE_READER, raw);
    } catch (err) {
      console.error(err);
      throw err;
    }
    return result;
  }

  async getGameByProjectId(projectId: UuidHex): Promise<Game> {
    throw new Incident("NotImplemented: BrowserProjectService#getGameByProjectId");
  }
}

// import {Injectable} from "@angular/core";
// import {Http, Response} from "@angular/http";
// import {BehaviorSubject} from "rxjs/BehaviorSubject";
// import {Observable} from "rxjs/Observable";
// import {Subject} from "rxjs/Subject";
// import urlJoin from "url-join";
// import {Game} from "../../api-types/game/objects/game";
// import {Project} from "../../api-types/project/objects/project";
// import {Uuid} from "../../api-types/scalars/uuid";
//
// const externalUri: string = "http://localhost:50317";
//
// @Injectable()
// export class ProjectService {
//   private http: Http;
//   private projectsCache: Project[] | undefined;
//   private projects: BehaviorSubject<Project[]>;
//
//   constructor(http: Http) {
//     this.http = http;
//     this.projectsCache = undefined;
//   }
//
//   getProjects(acceptCached: boolean = false): Observable<Project[]> {
//     let result: Subject<Project[]>;
//     if (acceptCached && this.projectsCache !== undefined) {
//       result = new BehaviorSubject<Project[]>(this.projectsCache);
//     } else {
//       result = new Subject<Project[]>();
//     }
//
//     this.http.get(urlJoin(externalUri, "/api/v1/projects"))
//       .subscribe((rawRes: Response): void => {
//         const projects: Project[] = rawRes.json();
//         this.projectsCache = projects;
//         result.next(projects);
//       });
//     return result;
//   }
//
//   getProjectById(projectId: Uuid): Observable<Project> {
//     return this.http.get(urlJoin(externalUri, "/api/v1/projects", projectId))
//       .map((rawRes: Response): Project => {
//         return rawRes.json();
//       });
//   }
//
//   getGameByProjectId(projectId: Uuid): Observable<Game> {
//     return this.http.get(urlJoin(externalUri, "/api/v1/projects", projectId, "game"))
//       .map((rawRes: Response): Game => {
//         return rawRes.json();
//       });
//   }
// }
