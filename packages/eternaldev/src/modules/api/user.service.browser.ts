import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { $User, User } from "@eternalfest/api-core/user/user";
import { UuidHex } from "kryo/uuid-hex";
import { JSON_VALUE_READER } from "kryo-json/json-value-reader";

import { UserService } from "./user.service";
import {apiUri} from "./utils/api-uri";

@Injectable()
export class BrowserUserService extends UserService {
  private http: HttpClient;

  constructor(http: HttpClient) {
    super();
    this.http = http;
  }

  async getUserById(userId: UuidHex): Promise<User | undefined> {
    const raw: any = await this.http.get(apiUri("users", userId)).toPromise();
    return $User.read(JSON_VALUE_READER, raw);
  }
}
