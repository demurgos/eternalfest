import { NgModule } from "@angular/core";

import { GameService } from "./game.service";
import { ServerGameService } from "./game.service.server";
import { GameResolverService } from "./game-resolver.service";
import { GamesResolverService } from "./games-resolver.service";
import { ProjectService } from "./project.service";
import { ServerProjectService } from "./project.service.server";
import { RunService } from "./run.service";
import { ServerRunService } from "./run.service.server";
import { RunResolverService } from "./run-resolver.service";
import { SelfUserResolverService } from "./self-user-resolver.service";
import { UserService } from "./user.service";
import { ServerUserService } from "./user.service.server";
import { UserResolverService } from "./user-resolver.service";

@NgModule({
  providers: [
    GameResolverService,
    GamesResolverService,
    SelfUserResolverService,
    RunResolverService,
    UserResolverService,
    {provide: GameService, useClass: ServerGameService},
    {provide: ProjectService, useClass: ServerProjectService},
    {provide: RunService, useClass: ServerRunService},
    {provide: UserService, useClass: ServerUserService},
  ],
})
export class ServerApiModule {
}
