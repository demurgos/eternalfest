import { Injectable } from "@angular/core";
import { User } from "@eternalfest/api-core/user/user";
import { UuidHex } from "kryo/uuid-hex";

@Injectable()
export abstract class UserService {
  abstract getUserById(userId: UuidHex): Promise<User | undefined>;
}
