import { Inject, Injectable, NgZone } from "@angular/core";
import { AuthContext } from "@eternalfest/api-core/auth/auth-context";
import { ShortGame } from "@eternalfest/api-core/game/short-game";
import { Game } from "@eternalfest/api-core/game2/game";
import { Api as EfApi } from "@eternalfest/rest-server/create-api-router";
import { UuidHex } from "kryo/uuid-hex";

import { AUTH_CONTEXT, EF_API } from "../../main/ng/tokens";
import { GameService } from "./game.service";
import { runUnzoned } from "./utils/run-unzoned.server";

@Injectable()
export class ServerGameService extends GameService {
  private readonly auth: AuthContext;
  private readonly efApi: EfApi;
  private readonly ngZone: NgZone;

  constructor(@Inject(AUTH_CONTEXT) auth: AuthContext, @Inject(EF_API) efApi: EfApi, ngZone: NgZone) {
    super();
    this.auth = auth;
    this.efApi = efApi;
    this.ngZone = ngZone;
  }

  async getGames(): Promise<readonly ShortGame[]> {
    return runUnzoned(this.ngZone, "ServerGameService#getGames", () => this._getGames());
  }

  async getGameById(gameId: UuidHex): Promise<Game | undefined> {
    return runUnzoned(this.ngZone, "ServerGameService#getGameById", () => this._getGameById(gameId));
  }

  private async _getGames(): Promise<readonly ShortGame[]> {
    throw new Error("NotImplemented: _getGames");
  }

  private async _getGameById(gameId: UuidHex): Promise<Game | undefined> {
    throw new Error("NotImplemented: _getGameById");
  }
}
