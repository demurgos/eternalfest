import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {$ShortGame, ShortGame} from "@eternalfest/api-core/game/short-game";
import {$Game, Game} from "@eternalfest/api-core/game2/game";
import {ArrayIoType, ArrayType} from "kryo/array";
import {UuidHex} from "kryo/uuid-hex";
import {JSON_VALUE_READER} from "kryo-json/json-value-reader";

import {GameService} from "./game.service";
import {apiUri} from "./utils/api-uri";

const $GameArray: ArrayIoType<ShortGame> = new ArrayType({itemType: $ShortGame, maxLength: Infinity});

@Injectable()
export class BrowserGameService extends GameService {
  private http: HttpClient;

  constructor(http: HttpClient) {
    super();
    this.http = http;
  }

  async getGames(): Promise<readonly ShortGame[]> {
    return [];
    const raw: any = await this.http.get(apiUri("games")).toPromise();
    let result: readonly ShortGame[];
    try {
      result = $GameArray.read(JSON_VALUE_READER, raw);
    } catch (err) {
      console.error(err);
      result = [];
    }
    return result;
  }

  async getGameById(gameId: UuidHex): Promise<Game | undefined> {
    const raw: any = await this.http.get(apiUri("games", gameId)).toPromise();
    let result: Game;
    try {
      result = $Game.read(JSON_VALUE_READER, raw);
    } catch (err) {
      console.error(err);
      throw err;
    }
    return result;
  }
}
