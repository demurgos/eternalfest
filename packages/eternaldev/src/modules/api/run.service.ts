import { Injectable } from "@angular/core";
import { CreateDebugRunOptions } from "@eternalfest/api-core/eternaldev/create-debug-run-options";
import { CreateRunOptions } from "@eternalfest/api-core/run/create-run-options";
import { Run } from "@eternalfest/api-core/run/run";
import { UuidHex } from "kryo/uuid-hex";

@Injectable()
export abstract class RunService {
  abstract createRun(options: CreateRunOptions): Promise<Run>;

  abstract createDebugRun(options: CreateDebugRunOptions): Promise<Run>;

  abstract getRunById(gameId: UuidHex): Promise<Run | undefined>;
}
