import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import {
  $CreateDebugRunOptions,
  CreateDebugRunOptions,
} from "@eternalfest/api-core/eternaldev/create-debug-run-options";
import { CreateRunOptions } from "@eternalfest/api-core/run/create-run-options";
import { $Run, Run } from "@eternalfest/api-core/run/run";
import { UuidHex } from "kryo/uuid-hex";
import { JSON_VALUE_READER } from "kryo-json/json-value-reader";
import { JSON_VALUE_WRITER } from "kryo-json/json-value-writer";

import { RunService } from "./run.service";
import {apiUri} from "./utils/api-uri";

@Injectable()
export class BrowserRunService extends RunService {
  private http: HttpClient;

  constructor(http: HttpClient) {
    super();
    this.http = http;
  }

  async createRun(options: CreateRunOptions): Promise<Run> {
    return this.createDebugRun({...options, gameId: options.game.id, userId: options.user.id});
  }

  async createDebugRun(options: CreateDebugRunOptions): Promise<Run> {
    let result: Run;
    try {
      const reqBody: any = $CreateDebugRunOptions.write(JSON_VALUE_WRITER, options);
      const raw: any = await this.http.post(apiUri("runs"), reqBody).toPromise();
      result = $Run.read(JSON_VALUE_READER, raw);
    } catch (err) {
      console.error(err);
      throw err;
    }
    return result;
  }

  async getRunById(runId: UuidHex): Promise<Run | undefined> {
    const raw: any = await this.http.get(apiUri("runs", runId)).toPromise();
    return $Run.read(JSON_VALUE_READER, raw);
  }
}
