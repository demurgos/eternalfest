import { Injectable } from "@angular/core";
import { Project } from "@eternalfest/api-core/eternaldev/project";
import { Game } from "@eternalfest/api-core/game2/game";
import { UuidHex } from "kryo/uuid-hex";

import { ShortProject } from "../../main/api/project/short-project";

@Injectable()
export abstract class ProjectService {
  abstract getProjects(acceptCached: boolean): Promise<ShortProject[]>;

  abstract getProjectById(projectId: UuidHex): Promise<Project>;

  abstract getGameByProjectId(projectId: UuidHex): Promise<Game>;
}
