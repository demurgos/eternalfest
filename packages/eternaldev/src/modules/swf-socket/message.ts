export interface SwfSocketMessage<N extends string = string, D = any> {
  name: N;
  data: D;
}
