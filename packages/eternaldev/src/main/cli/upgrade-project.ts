import { ProfileMap } from "@eternalfest/api-core/eternaldev/profile-map";
import { ProjectManifest as LegacyManifest } from "@eternalfest/api-core/eternaldev/project-manifest";
import { UrlFragment } from "@eternalfest/api-core/eternaldev/url-fragment";
import { FamiliesString } from "@eternalfest/api-core/game/families-string";
import { GameDescription } from "@eternalfest/api-core/game/game-description";
import { GameDisplayName } from "@eternalfest/api-core/game/game-display-name";
import { GameModeKey } from "@eternalfest/api-core/game/game-mode-key";
import { GameOptionKey } from "@eternalfest/api-core/game/game-option-key";
import { LocaleId } from "@eternalfest/api-core/types/locale-id";
import { Url } from "@eternalfest/api-core/types/url";
import * as efcCr from "@eternalfest/efc/compiler-result";
import * as efcConfig from "@eternalfest/efc/java/config";
import * as efcUtils from "@eternalfest/efc/utils";
import fs from "fs";
import furi from "furi";
import { UuidHex } from "kryo/uuid-hex";
import sysPath from "path";
import { firstValueFrom } from "rxjs";

import { Manifest } from "../api/manifest/manifest.js";
import { ManifestContent } from "../api/manifest/manifest-content.js";
import { ManifestLevelMap } from "../api/manifest/manifest-level-map.js";
import { ManifestLocaleContent } from "../api/manifest/manifest-locale-content.js";
import { ManifestLocaleMap } from "../api/manifest/manifest-locale-map.js";
import { ManifestMode } from "../api/manifest/manifest-mode.js";
import { ManifestModeMap } from "../api/manifest/manifest-mode-map.js";
import { ManifestOptionMap } from "../api/manifest/manifest-option-map.js";
import { ManifestPatchman } from "../api/manifest/manifest-patchman.js";
import { gameModeKeyToDisplayName, gameOptionKeyToDisplayName, readManifest } from "../api/project/legacy.js";
import { emitTomlManifest } from "../api/project/local-service.js";
import { outputFileAsync } from "../fs-utils.js";

export async function upgradeProject(): Promise<void> {
  const cwdUrl: Url = furi.fromSysPath(process.cwd());
  const javaConfigPath: Url = furi.join(cwdUrl.toString(), "config.xml");
  const outPath: Url = furi.join(cwdUrl.toString(), "eternalfest.toml");

  const javaConfig: efcCr.CompilerResult<efcConfig.ResolvedConfig> = await firstValueFrom(efcConfig.fromPath(javaConfigPath as import("url").URL));
  const legacyManifest: LegacyManifest = await readManifest(cwdUrl);
  const buildInfo: BuildInfo = await getBuildInfo(cwdUrl);

  const manifest: Manifest = await upgradeManifest(cwdUrl, legacyManifest, javaConfig.unwrap(), buildInfo);
  const tomlStr: string = emitTomlManifest(manifest);
  await outputFileAsync(outPath, Buffer.from(tomlStr));

  console.log("OK");
}

export async function upgradeManifest(
  dir: Url,
  legacyManifest: LegacyManifest,
  javaConfig: efcConfig.ResolvedConfig,
  buildInfo: BuildInfo,
): Promise<Manifest> {
  const dirName: string = sysPath.basename(furi.toSysPath(dir.toString()));

  const displayName: GameDisplayName | undefined = legacyManifest.displayName;
  const description: GameDescription | undefined = legacyManifest.description;
  const icon: UrlFragment | undefined = legacyManifest.icon;
  const remoteId: UuidHex | undefined = legacyManifest.remoteId;
  const repository: string = `https://gitlab.com/eternalfest/games/${dirName}`;
  const mainLocale: LocaleId = "fr-FR";
  const families: FamiliesString | undefined = legacyManifest.families;

  let hf: UrlFragment | undefined;
  let assets: UrlFragment | undefined;
  const musics: UrlFragment[] = [];

  if (legacyManifest.resources !== undefined) {
    for (const res of legacyManifest.resources) {
      if (res.tag === "game-swf") {
        hf = res.path;
      } else if (res.tag === "music") {
        musics.push(res.path);
      }
    }
  }
  if (!javaConfig.assets.useBaseAssets) {
    throw new Error("Cannot upgrade project that does not use base assets");
  }
  if (javaConfig.assets.roots.length > 1) {
    throw new Error("Cannot upgrade project with multiple asset roots");
  }
  if (javaConfig.assets.roots.length === 1) {
    const javaAssets: Url = furi.fromSysPath(javaConfig.assets.roots[0]);
    assets = furi.relative(dir.toString(), javaAssets.toString());
  }
  const content: ManifestContent = await upgradeContent(dir, legacyManifest, javaConfig, buildInfo.patcherFramework === "patchman");

  let patchman: ManifestPatchman | undefined;
  let external: ManifestPatchman | undefined;
  if (buildInfo.patcherFramework === "patchman") {
    patchman = {
      build: "./build/patchman.swf",
    };
  } else if (buildInfo.patcherFramework === "external") {
    external = {
      build: "./external.swf",
    };
  }

  const modes: ManifestModeMap = await upgradeModes(dir, legacyManifest);
  const profiles: ProfileMap | undefined = legacyManifest.profiles;

  const locales: ManifestLocaleMap = new Map();
  for (const [localeId, value] of javaConfig.locales) {
    let realLocale: LocaleId;
    switch (localeId) {
      case "en":
        realLocale = "en-US";
        break;
      case "es":
        realLocale = "es-SP";
        break;
      case "fr":
        realLocale = "fr-FR";
        break;
      default:
        realLocale = localeId;
    }
    const content: ManifestLocaleContent = {
      lands: furi.relative(dir.toString(), furi.fromSysPath(value.lands)),
      keys: furi.relative(dir.toString(), furi.fromSysPath(value.keys)),
      items: furi.relative(dir.toString(), furi.fromSysPath(value.items)),
      statics: furi.relative(dir.toString(), furi.fromSysPath(value.statics)),
    };
    locales.set(realLocale, {build: `./build/locales.${realLocale}.json`, content});
  }

  return {
    displayName,
    description,
    icon,
    remoteId,
    repository,
    mainLocale,
    families,
    hf,
    musics,
    assets,
    content,
    patchman,
    external,
    modes,
    profiles,
    locales,
  };
}

export async function upgradeContent(
  dir: Url,
  _legacyManifest: LegacyManifest,
  javaConfig: efcConfig.ResolvedConfig,
  mtGame: boolean,
): Promise<ManifestContent> {
  const build: UrlFragment = furi.relative(dir.toString(), furi.fromSysPath(javaConfig.outFile));
  const scoreItems: UrlFragment = furi.relative(dir.toString(), furi.fromSysPath(javaConfig.scoreItemsFile));
  const specialItems: UrlFragment = furi.relative(dir.toString(), furi.fromSysPath(javaConfig.specialItemsFile));
  const dimensions: UrlFragment = furi.relative(dir.toString(), furi.fromSysPath(javaConfig.dimensionsFile));
  const data: UrlFragment = furi.relative(dir.toString(), furi.fromSysPath(javaConfig.patcherDataDir));
  const levelsDir: Url = furi.fromSysPath(javaConfig.levelsDir);

  const levels: ManifestLevelMap = new Map();
  for (const ent of fs.readdirSync(levelsDir as import("url").URL, {withFileTypes: true})) {
    if (!ent.isDirectory()) {
      continue;
    }
    const levelSetUri: Url = furi.join(levelsDir.toString(), ent.name);
    if (!isNonEmptyDir(levelSetUri)) {
      continue;
    }
    switch (ent.name) {
      case "adv_puits":
        levels.set("0", {
          levels: furi.relative(dir.toString(), levelSetUri.toString()),
          var: mtGame ? "xml_adventure" : "LVLS_MAIN",
          did: "0",
        });
        break;
      case "adv_dim1":
        levels.set("1", {
          levels: furi.relative(dir.toString(), levelSetUri.toString()),
          var: mtGame ? "xml_deepnight" : "LVLS_DIM1",
          did: "1",
          padding: true,
        });
        break;
      case "adv_dim2":
        levels.set("2", {
          levels: furi.relative(dir.toString(), levelSetUri.toString()),
          var: mtGame ? "xml_hiko" : "LVLS_DIM2",
          did: "2",
          padding: true,
        });
        break;
      case "adv_dim3":
        levels.set("3", {
          levels: furi.relative(dir.toString(), levelSetUri.toString()),
          var: mtGame ? "xml_ayame" : "LVLS_DIM3",
          did: "3",
          padding: true,
        });
        break;
      case "adv_dim4":
        levels.set("4", {
          levels: furi.relative(dir.toString(), levelSetUri.toString()),
          var: mtGame ? "xml_hk" : "LVLS_DIM4",
          did: "4",
          padding: true,
        });
        break;
      case "apprentissage":
        levels.set("shareware", {
          levels: furi.relative(dir.toString(), levelSetUri.toString()),
          var: mtGame ? "xml_shareware" : "LVLS_SHAREWARE",
        });
        break;
      case "soccerfest":
        levels.set("soccer", {levels: furi.relative(dir.toString(), levelSetUri.toString()), var: mtGame ? "xml_soccer" : "LVLS_SOCCER"});
        break;
      case "timeattack":
        levels.set("time", {
          levels: furi.relative(dir.toString(), levelSetUri.toString()),
          var: mtGame ? "xml_time" : "LVLS_TIME",
          sameSize: true,
        });
        break;
      case "timeattack_multi":
        levels.set("multitime", {
          levels: furi.relative(dir.toString(), levelSetUri.toString()),
          var: mtGame ? "xml_multitime" : "LVLS_MULTI_TIME",
          sameSize: true,
        });
        break;
      case "tutorial":
        levels.set("tutorial", {levels: furi.relative(dir.toString(), levelSetUri.toString()), var: mtGame ? "xml_tutorial" : "LVLS_TUTO"});
        break;
      case "fjv":
        levels.set("fjv", {levels: furi.relative(dir.toString(), levelSetUri.toString()), var: mtGame ? "xml_fjv" : "LVLS_FJV"});
        break;
      case "hof":
        levels.set("hof", {levels: furi.relative(dir.toString(), levelSetUri.toString()), var: mtGame ? "xml_hof" : "LVLS_HOF"});
        break;
      case "multi":
        levels.set("multi", {levels: furi.relative(dir.toString(), levelSetUri.toString()), var: mtGame ? "xml_multi" : "LVLS_UNK"});
        break;
      case "test":
        levels.set("test", {levels: furi.relative(dir.toString(), levelSetUri.toString()), var: mtGame ? "xml_test" : "LVLS_TEST"});
        break;
      case "dev":
        levels.set("dev", {levels: furi.relative(dir.toString(), levelSetUri.toString()), var: mtGame ? "xml_dev" : "LVLS_DEV"});
        break;
    }
  }

  return {
    build,
    scoreItems,
    specialItems,
    dimensions,
    data,
    levels,
  };
}

async function upgradeModes(_dir: Url, legacyManifest: LegacyManifest): Promise<ManifestModeMap> {
  const modes: ManifestModeMap = new Map();
  if (legacyManifest.modes === undefined) {
    return modes;
  }
  for (const legacyMode of legacyManifest.modes) {
    let key: GameModeKey | undefined = legacyMode.key;
    if (key === undefined) {
      key = legacyMode.id;
    }
    if (key === undefined) {
      continue;
    }
    const options: ManifestOptionMap = new Map();
    for (const legacyOption of legacyMode.options) {
      let key: GameOptionKey | undefined = legacyOption.key;
      if (key === undefined) {
        key = legacyOption.id;
      }
      if (key === undefined) {
        continue;
      }
      options.set(
        key,
        {state: legacyOption.state, displayName: gameOptionKeyToDisplayName(key)},
      );
    }
    const mode: ManifestMode = {
      state: legacyMode.state,
      displayName: gameModeKeyToDisplayName(key),
      options,
    };
    modes.set(key, mode);
  }
  return modes;
}

function isNonEmptyDir(dir: Url): boolean {
  return fs.readdirSync(dir as import("url").URL).filter((x) => x !== ".gitkepp").length > 0;
}

interface BuildInfo {
  hasAssets: boolean;
  patcherFramework?: "external" | "patchman";
}

async function getBuildInfo(dir: Url): Promise<BuildInfo> {
  let hasAssets: boolean = false;
  let patcherFramework: "external" | "patchman" | undefined = undefined;
  if (fs.existsSync(new Url("./assets.xml", dir) as import("url").URL)) {
    hasAssets = true;
  }
  try {
    const buildBat: string = (await efcUtils.withTextFile(furi.join(dir, "./build.bat"), efcCr.of)).unwrap();
    if (/external/.test(buildBat)) {
      patcherFramework = "external";
    } else if (/patchman/.test(buildBat)) {
      patcherFramework = "patchman";
    }
  } catch (e) {
    // Ignore error
  }
  if (patcherFramework === undefined) {
    try {
      const buildSh: string = (await efcUtils.withTextFile(furi.join(dir, "./build.sh"), efcCr.of)).unwrap();
      if (/external/.test(buildSh)) {
        patcherFramework = "external";
      } else if (/patchman/.test(buildSh)) {
        patcherFramework = "patchman";
      }
    } catch (e) {
      // Ignore error
    }
  }
  if (patcherFramework === undefined) {
    try {
      const packageJson: string = (await efcUtils.withTextFile(furi.join(dir, "./package.json"), efcCr.of)).unwrap();
      if (/patchman/.test(packageJson)) {
        patcherFramework = "patchman";
      } else if (/external/.test(packageJson)) {
        patcherFramework = "external";
      }
    } catch (e) {
      // Ignore error
    }
  }
  return {hasAssets, patcherFramework};
}
