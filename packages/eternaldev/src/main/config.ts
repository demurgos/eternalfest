import { fromSysPath } from "furi";
import process from "process";

export interface ServerConfig {
  eternalfest: EternalfestConfig;
  data: DataConfig;
}

export interface EternalfestConfig {
  /**
   * Internal HTTP port
   */
  httpPort: number;

  /**
   * Public URI of the server
   */
  externalUri: URL;
}

export interface DataConfig {
  root: URL;
}

const DEFAULT_HTTP_PORT: number = 50317;

export function getConfigFromEnv(cwd: URL, env: NodeJS.ProcessEnv): ServerConfig {
  let httpPort: number;
  if (typeof env.EF_HTTP_PORT === "string") {
    httpPort = parseInt(env.EF_HTTP_PORT, 10);
  } else {
    httpPort = DEFAULT_HTTP_PORT;
  }
  let externalUri: URL;
  if (typeof env.EF_EXTERNAL_URI === "string") {
    externalUri = new URL(env.EF_EXTERNAL_URI);
  } else {
    externalUri = new URL("http://localhost");
    externalUri.port = httpPort.toString(10);
  }

  return {
    eternalfest: {
      httpPort,
      externalUri,
    },
    data: {root: cwd}
  };
}

export async function getLocalConfig(): Promise<ServerConfig> {
  const cwd = fromSysPath(process.cwd());
  return getConfigFromEnv(cwd, process.env);
}
