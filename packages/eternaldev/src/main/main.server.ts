import { Url } from "@eternalfest/api-core/types/url";
import efGame from "@eternalfest/game";
import loader from "@eternalfest/loader";
import koaCors from "@koa/cors";
import Router from "@koa/router";
import fs from "fs";
import furi from "furi";
import Koa from "koa";
import koaLogger from "koa-logger";
import koaMount from "koa-mount";
import koaSend from "koa-send";
import koaStaticCache from "koa-static-cache";
import { createRequire } from "module";
import sysPath from "path";

import { ServerAppConfig } from "../server/config.js";
import { createLocalApiRouter as createApiRouter } from "./api-router.js";
import { getLocalConfig, ServerConfig as Config } from "./config.js";
import { createKoaLocaleNegotiator, LocaleNegotiator } from "./koa-locale-negotiation.js";
import { Locale } from "./locales.js";
import { LocalEfApi as Api, LocalEfApiOptions, withEfApi } from "./with-ef-api.js";

const PROJECT_ROOT: Url = furi.join(import .meta.url, "../..");
const IS_PRODUCTION: boolean = process.env.NODE_ENV === "production";
const APP_DIR: Url = furi.join(PROJECT_ROOT.toString(), "app");
const BROWSER_APP_DIR: Url = furi.join(APP_DIR.toString(), "browser");

async function main(efApi: Api): Promise<void> {
  const config: Config = await getLocalConfig();
  console.log("Server configuration:");
  console.log(`HTTP port: ${config.eternalfest.httpPort}`);
  console.log(`External URI: ${config.eternalfest.externalUri}`);
  console.log(`Data directory: ${config.data.root}`);
  console.log(`Is production: ${IS_PRODUCTION}`);

  const apps: Apps = await findApps();

  if (IS_PRODUCTION) {
    if (apps.dev !== undefined) {
      throw new Error("Aborting: dev app build exists. Remove it before starting the server in production mode");
    }
  }

  const appConfig: ServerAppConfig = {
    externalBaseUri: config.eternalfest.externalUri,
    isIndexNextToServerMain: true,
    isProduction: IS_PRODUCTION,
    efApi: efApi,
  };

  const prodAppRouters: Map<string, Koa> = new Map();
  for (const [locale, prodApp] of apps.prod) {
    const appRouter: Koa = await loadAppRouter(prodApp.serverMain, appConfig);
    prodAppRouters.set(locale, appRouter);
  }
  let defaultRouter: Koa | undefined = prodAppRouters.get("en-US");
  if (defaultRouter === undefined) {
    if (IS_PRODUCTION) {
      throw new Error("Aborting: Missing `en-US` app");
    }
    if (apps.dev !== undefined) {
      defaultRouter = await loadAppRouter(apps.dev.serverMain, appConfig);
    } else {
      throw new Error("Aborting: Missing default app (`en-US` or `dev`)");
    }
  }

  const router: Koa = new Koa();

  router.use(koaLogger());
  if (!IS_PRODUCTION) {
    router.use(koaCors({origin: "http://localhost:4200", credentials: true}));
  }

  const ONE_DAY: number = 24 * 3600;
  router.use(koaStaticCache(furi.toSysPath(BROWSER_APP_DIR.toString()), {maxAge: ONE_DAY}));

  const apiRouter: Router = await createApiRouter(efApi);
  router.use(koaMount("/api/v1", apiRouter.routes()));
  router.use(koaMount("/api/v1", apiRouter.allowedMethods()));

  router.use(koaMount("/assets/loader.swf", sendLoader));
  async function sendLoader(cx: Koa.Context): Promise<void> {
    const loaderFuri: Url = loader.getLoaderUri(loader.Version.Flash8);
    const loaderSysPath: string = furi.toSysPath(loaderFuri.toString());
    const loaderDir: string = sysPath.dirname(loaderSysPath);
    const loaderBasename: string = sysPath.basename(loaderSysPath);
    await koaSend(cx, loaderBasename, {root: loaderDir});
  }

  router.use(koaMount("/assets/game.swf", sendGame));
  async function sendGame(cx: Koa.Context): Promise<void> {
    const gameFuri: Url = efGame.getGameUri();
    const gameSysPath: string = furi.toSysPath(gameFuri.toString());
    const gameDir: string = sysPath.dirname(gameSysPath);
    const gameBasename: string = sysPath.basename(gameSysPath);
    await koaSend(cx, gameBasename, {root: gameDir});
  }

  const i18nRouter: Koa = createI18nRouter(defaultRouter, prodAppRouters);
  router.use(koaMount("/", i18nRouter));

  router.listen(config.eternalfest.httpPort, () => {
    console.log(`Listening on internal port ${config.eternalfest.httpPort}, externally available at ${config.eternalfest.externalUri}`);
  });
}

function createI18nRouter(defaultRouter: Koa, localizedRouters: Map<Locale, Koa>): Koa {
  const router: Koa = new Koa();

  const localeNegotiator: LocaleNegotiator<Koa.Context> = createKoaLocaleNegotiator({
    cookieName: "locale",
    queryName: "l",
    supportedLocales: localizedRouters.keys(),
  });

  const defaultMiddleware: Koa.Middleware = koaMount(defaultRouter);
  const localizedMiddlewares: Map<Locale, Koa.Middleware> = new Map();
  for (const [locale, app] of localizedRouters) {
    localizedMiddlewares.set(locale, koaMount(app));
  }

  router.use(async (cx, next) => {
    const locale: Locale | undefined = localeNegotiator(cx);
    if (locale !== undefined) {
      const middleware: Koa.Middleware | undefined = localizedMiddlewares.get(locale);
      if (middleware !== undefined) {
        return middleware(cx, next);
      }
      // We matched a locale but don't have a corresponding router
      // TODO: Log warning? We should never reach this point since available
      //       locales are generated from available routers.
    }
    return defaultMiddleware(cx, next);
  });

  return router;
}

async function loadAppRouter(serverMain: Url, serverAppConfig: ServerAppConfig): Promise<Koa> {
  let serverMod: unknown;
  try {
    // TODO: Use dynamic import instead of require (as of 2022-10-05, this causes an issue with zone and yarn PnP/ESM)
    const serverPath: string = furi.toSysPath(serverMain);
    const req = createRequire(import.meta.url);
    serverMod = req(serverPath);
  } catch (e: unknown) {
    console.error((e as any).stack);
    throw new Error(`FailedToLoadServer ${serverMain}: ${e}`);
  }
  const appRouterFn: Function = getAppRouterFn(serverMod);
  let appRouter: Koa;
  try {
    appRouter = await appRouterFn(serverAppConfig);
  } catch (err) {
    throw new Error(`App router creation failed: ${serverMain}\nCaused by: ${(err as any).stack}`);
  }
  return appRouter;

  function getAppRouterFn(mod: unknown): Function {
    if (typeof mod !== "object" || mod === null) {
      throw new Error(`Failed to load app server: ${serverMain}`);
    }
    const modDefault: unknown = Reflect.get(mod, "default");
    if (typeof modDefault !== "object" || modDefault === null) {
      const appFn: unknown = Reflect.get(mod, "app");
      if (typeof appFn === "function") {
        return appFn;
      }
      throw new Error(`Invalid app server: expected CommonJS module with \`default\` export: ${serverMain}`);
    }
    const appFn: unknown = Reflect.get(modDefault, "app");
    if (typeof appFn !== "function") {
      throw new Error(`Invalid app server: expected exported \`app\` function: ${serverMain}`);
    }
    return appFn;
  }
}

interface App {
  name: string;
  browserDir: Url;
  serverDir: Url;
  serverMain: Url;
}

interface Apps {
  dev?: App;
  prod: Map<string, App>;
}

async function findApps(): Promise<Apps> {
  const browserAppEnts: readonly fs.Dirent[] = await fs.promises.readdir(BROWSER_APP_DIR as URL, {withFileTypes: true});
  const serverAppEnts: readonly fs.Dirent[] = await fs.promises.readdir(furi.join(APP_DIR as URL, "server"), {withFileTypes: true});

  const browserApps: ReadonlySet<string> = pickDirectoryNames(browserAppEnts);
  const serverApps: ReadonlySet<string> = pickDirectoryNames(serverAppEnts);
  const diff: SetDiff<string> | null = diffSets(browserApps, serverApps);

  if (diff !== null) {
    const messages: string[] = [];
    if (diff.leftExtra.size > 0) {
      messages.push(`browser apps without server: ${JSON.stringify([...diff.leftExtra])}`);
    }
    if (diff.rightExtra.size > 0) {
      messages.push(`server apps without browser: ${JSON.stringify([...diff.rightExtra])}`);
    }
    throw new Error(`Mismatch between compiled app types: ${messages.join(", ")}`);
  }

  let dev: App | undefined;
  const prod: Map<string, App> = new Map();
  for (const appName of browserApps) {
    const app: App = resolveApp(APP_DIR, appName);
    if (appName === "dev") {
      dev = app;
    } else {
      prod.set(appName, app);
    }
  }

  return {dev, prod};

  function resolveApp(appDir: Url, name: string): App {
    const serverDir: Url = furi.join(appDir.toString(), "server", name);
    return {
      name,
      browserDir: furi.join(BROWSER_APP_DIR.toString(), name),
      serverDir,
      serverMain: furi.join(serverDir.toString(), "main.js"),
    };
  }

  function pickDirectoryNames(dirEnts: Iterable<fs.Dirent>): Set<string> {
    const names: Set<string> = new Set();
    for (const dirEnt of dirEnts) {
      if (dirEnt.name === "assets") {
        continue;
      }
      if (dirEnt.isDirectory()) {
        names.add(dirEnt.name);
      }
    }
    return names;
  }

  interface SetDiff<T> {
    leftExtra: Set<T>;
    rightExtra: Set<T>;
  }

  function diffSets(left: ReadonlySet<string>, right: ReadonlySet<string>): SetDiff<string> | null {
    const leftExtra: Set<string> = new Set();
    for (const l of left) {
      if (!right.has(l)) {
        leftExtra.add(l);
      }
    }
    if (leftExtra.size === 0 && left.size === right.size) {
      return null;
    }
    const rightExtra: Set<string> = new Set();
    for (const r of right) {
      if (!left.has(r)) {
        rightExtra.add(r);
      }
    }
    return {leftExtra, rightExtra};
  }
}

export async function startServer(options: LocalEfApiOptions): Promise<void> {
  return withEfApi(options, (efApi): Promise<never> => {
    // Create a never-resolving promise so the API is never closed
    return new Promise<never>(() => {
      main(efApi);
    });
  });
}
