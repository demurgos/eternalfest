import {Url} from "@eternalfest/api-core/types/url";
import {Api as EfApi} from "@eternalfest/rest-server/create-api-router";
import {KoaAuth} from "@eternalfest/rest-server/koa-auth";
import fs from "fs";
import furi from "furi";

import {InMemoryAuthService} from "./api/auth/in-memory-service.js";
import {InMemoryBlobService} from "./api/blob/in-memory-service.js";
import {LocalFileService} from "./api/file/local-service.js";
import {LocalGame2Service} from "./api/game2/local-service.js";
import {LocalProjectService} from "./api/project/local-service.js";
import {InMemoryRunService} from "./api/run/in-memory-service.js";
import {InMemoryUserService} from "./api/user/in-memory-service.js";
import {getRecentProjects, touchRecentProject} from "./lib/recent-projects.js";

export interface LocalEfApi extends EfApi {
  project: LocalProjectService;
  run: InMemoryRunService;
}

export interface LocalEfApiOptions {
  filesThrottle?: number;
  noGitIntegration?: boolean;
}

export async function withEfApi<R>(options: LocalEfApiOptions, handler: (efApi: LocalEfApi) => Promise<R>): Promise<R> {
  const localProjects: readonly Url[] = await getLocalProjects();
  const file: LocalFileService = new LocalFileService({
    throttle: options.filesThrottle,
  });
  const project: LocalProjectService = new LocalProjectService(file, {
    projectDirs: localProjects,
    noGitIntegration: options.noGitIntegration,
  });
  const blob: InMemoryBlobService = new InMemoryBlobService(file);
  const user: InMemoryUserService = new InMemoryUserService();
  const game2: LocalGame2Service = new LocalGame2Service(project);
  const run: InMemoryRunService = new InMemoryRunService(game2, user);
  const auth: InMemoryAuthService = new InMemoryAuthService();
  const koaAuth: KoaAuth = new KoaAuth(auth);
  const efApi: LocalEfApi = {auth, blob, file, game2, koaAuth, run, user, project};
  return handler(efApi);
}

async function getLocalProjects(): Promise<Url[]> {
  const cwdProject: Url | undefined = await tryGetCwdProject();
  if (cwdProject !== undefined) {
    await touchRecentProject(cwdProject);
  }
  return getRecentProjects();
}

export async function tryGetCwdProject(): Promise<Url | undefined> {
  const cwdUrl: Url = furi.fromSysPath(process.cwd());
  if (fs.existsSync(furi.join(cwdUrl.toString(), "eternalfest.toml"))) {
    return cwdUrl;
  }
  return undefined;
}
