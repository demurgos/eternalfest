import { AuthContext } from "@eternalfest/api-core/auth/auth-context";
import {
  $CreateDebugRunOptions,
  CreateDebugRunOptions,
} from "@eternalfest/api-core/eternaldev/create-debug-run-options";
import { $Project, Project } from "@eternalfest/api-core/eternaldev/project";
import { $Run, Run } from "@eternalfest/api-core/run/run";
import efDevkit from "@eternalfest/devkit";
import { createApiRouter } from "@eternalfest/rest-server/create-api-router";
import Router from "@koa/router";
import Koa from "koa";
import koaBodyParser from "koa-bodyparser";
import koaCompose from "koa-compose";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";
import { JSON_VALUE_READER } from "kryo-json/json-value-reader";
import { JSON_VALUE_WRITER } from "kryo-json/json-value-writer";
import { QS_VALUE_READER } from "kryo-qs/qs-value-reader";

import { $ShortProject, ShortProject } from "./api/project/short-project.js";
import { LocalEfApi } from "./with-ef-api.js";

async function createProjectsRouter(api: LocalEfApi): Promise<Router> {
  const router: Router = new Router();

  router.get("/", getProjects);

  async function getProjects(cx: Koa.Context): Promise<void> {
    const projects: ShortProject[] = await api.project.getProjects();
    cx.response.body = projects.map((project) => $ShortProject.write(JSON_VALUE_WRITER, project));
  }

  router.get("/:project_id", getProjectById);

  async function getProjectById(cx: Koa.Context): Promise<void> {
    const rawProjectId: string = cx.params["project_id"];
    // const auth: AuthContext = await api.koaAuth.auth(ctx);
    const projectId: UuidHex = $UuidHex.read(QS_VALUE_READER, rawProjectId);
    const project: Project | undefined = await api.project.getProjectById(projectId);
    if (project === undefined) {
      cx.status = 404;
      return;
    }
    cx.response.body = $Project.write(JSON_VALUE_WRITER, project);
  }

  return router;
}

async function createDevkitRouter(): Promise<Router> {
  const router: Router = new Router();

  router.post("/editor", openEditor);

  async function openEditor(cx: Koa.Context): Promise<void> {
    cx.status = 202;
    efDevkit.levelEditor.exec({
      args: [],
      cwd: process.cwd(),
      stdio: "inherit",
    });
  }

  return router;
}

export async function createLocalApiRouter(efApi: LocalEfApi): Promise<Router> {
  const router: Router = new Router();

  const devkit = await createDevkitRouter();
  router.use("/devkit", devkit.routes(), devkit.allowedMethods());
  const projects = await createProjectsRouter(efApi);
  router.use("/projects", projects.routes(), projects.allowedMethods());

  router.post("/runs", koaCompose([koaBodyParser(), createRun]));

  async function createRun(cx: Koa.Context): Promise<void> {
    const auth: AuthContext = await efApi.koaAuth.auth(cx);
    let options: CreateDebugRunOptions;
    try {
      options = $CreateDebugRunOptions.read(JSON_VALUE_READER, cx.request.body);
    } catch (err) {
      console.error(err);
      cx.response.status = 422;
      cx.response.body = {error: "InvalidRequest"};
      return;
    }
    const run: Run = await efApi.run.createDebugRun(auth, options);
    cx.response.body = $Run.write(JSON_VALUE_WRITER, run);
  }

  const baseApi = createApiRouter(efApi);
  router.use(baseApi.routes() as any, baseApi.allowedMethods() as any);

  return router;
}
