type Diagnostic = any;

export interface CompilerResultLike<T> {
  readonly diagnostics: ReadonlyArray<Diagnostic>;
  readonly value: T | typeof NO_VALUE;
}

export type CompilerResultRecord<T> = { [P in keyof T]: CompilerResult<T[P]> };

export const NO_VALUE: unique symbol = Symbol("NO_VALUE");

export class CompilerResult<T> {
  public readonly diagnostics: ReadonlyArray<Diagnostic>;
  public readonly value: T | typeof NO_VALUE;

  public constructor(value: T | typeof NO_VALUE, diagnostics: ReadonlyArray<Diagnostic>) {
    this.diagnostics = diagnostics;
    this.value = value;
  }

  public map<R>(project: (value: T) => R): CompilerResult<R> {
    return map(this, project);
  }

  public async mapAsync<R>(project: (value: T) => Promise<R>): Promise<CompilerResult<R>> {
    return mapAsync(this, project);
  }

  public flatMap<R>(project: (value: T) => CompilerResultLike<R>): CompilerResult<R> {
    return flatMap(this, project);
  }

  public async flatMapAsync<R>(project: (value: T) => Promise<CompilerResultLike<R>>): Promise<CompilerResult<R>> {
    return flatMapAsync(this, project);
  }

  public mapDiagnostics(project: (value: Diagnostic) => Diagnostic): CompilerResult<T> {
    return mapDiagnostics(this, project);
  }

  public flatTap(f: (cr: this) => void): this {
    f(this);
    return this;
  }

  public hasValue(): boolean {
    return hasValue(this);
  }

  public unwrap(): T {
    return unwrap(this);
  }
}

export function error(diagnostic: Diagnostic): CompilerResult<never> {
  return new CompilerResult<never>(NO_VALUE, [diagnostic]);
}

export function warn<T>(value: T, diagnostic: Diagnostic): CompilerResult<T> {
  return new CompilerResult(value, [diagnostic]);
}

export function errors(diagnostic: ReadonlyArray<Diagnostic>): CompilerResult<never> {
  return new CompilerResult<never>(NO_VALUE, diagnostic);
}

export function join<T>(crs: CompilerResultRecord<T>): CompilerResult<T> {
  const diagnostics: Diagnostic[] = [];
  const result: Partial<T> = {};
  let noValue: boolean = false;
  for (const [key, cr] of (Object.entries(crs) as [string, CompilerResult<any>][])) {
    for (const diag of cr.diagnostics) {
      diagnostics.push(diag);
    }
    if (cr.value === NO_VALUE) {
      noValue = true;
    } else if (!noValue) {
      Reflect.set(result, key, cr.value);
    }
  }
  return new CompilerResult(noValue ? NO_VALUE : (result as T), diagnostics);
}

export function joinArray<T0>(crs: [CompilerResultLike<T0>]): CompilerResult<[T0]>;
export function joinArray<T0, T1>(crs: [CompilerResultLike<T0>, CompilerResultLike<T1>]): CompilerResult<[T0, T1]>;
export function joinArray<T>(crs: Iterable<CompilerResultLike<T>>): CompilerResult<T[]>;
export function joinArray<T>(crs: Iterable<CompilerResultLike<T>>): CompilerResult<T[]> {
  const diagnostics: Diagnostic[] = [];
  const result: T[] = [];
  let noValue: boolean = false;
  for (const cr of crs) {
    for (const diag of cr.diagnostics) {
      diagnostics.push(diag);
    }
    if (cr.value === NO_VALUE) {
      noValue = true;
    } else if (!noValue) {
      result.push(cr.value);
    }
  }
  return new CompilerResult(noValue ? NO_VALUE : result, diagnostics);
}

// export function joinArray<T0>(crs: [CompilerResult<T0>]): CompilerResult<[T0]>;
// export function joinArray<T0, T1>(crs: [CompilerResult<T0>, CompilerResult<T1>]): CompilerResult<[T0, T1]>;
export async function joinArrayAsync<T>(crs: AsyncIterable<CompilerResultLike<T>>): Promise<CompilerResult<T[]>> {
  const diagnostics: Diagnostic[] = [];
  const result: T[] = [];
  let noValue: boolean = false;
  for await (const cr of crs) {
    for (const diag of cr.diagnostics) {
      diagnostics.push(diag);
    }
    if (cr.value === NO_VALUE) {
      noValue = true;
    } else if (!noValue) {
      result.push(cr.value);
    }
  }
  return new CompilerResult(noValue ? NO_VALUE : result, diagnostics);
}

export function joinMap<K, V>(crs: Iterable<[K, CompilerResultLike<V>]>): CompilerResult<Map<K, V>> {
  const diagnostics: Diagnostic[] = [];
  const result: Map<K, V> = new Map();
  let noValue: boolean = false;
  for (const [key, cr] of crs) {
    for (const diag of cr.diagnostics) {
      diagnostics.push(diag);
    }
    if (cr.value === NO_VALUE) {
      noValue = true;
    } else if (!noValue) {
      result.set(key, cr.value);
    }
  }
  return new CompilerResult(noValue ? NO_VALUE : result, diagnostics);
}

export function map<T, R>(cr: CompilerResultLike<T>, project: (value: T) => R): CompilerResult<R> {
  return cr.value === NO_VALUE
    ? from(cr) as CompilerResult<never>
    : new CompilerResult(project(cr.value), cr.diagnostics);
}

export function mapDiagnostics<T>(
  cr: CompilerResultLike<T>,
  project: (value: Diagnostic) => Diagnostic,
): CompilerResult<T> {
  const newDiagnostics: Diagnostic[] = [];
  for (const diagnostic of cr.diagnostics) {
    newDiagnostics.push(project(diagnostic));
  }
  return new CompilerResult(cr.value, newDiagnostics);
}

export async function mapAsync<T, R>(
  cr: CompilerResultLike<T>,
  project: (value: T) => Promise<R>,
): Promise<CompilerResult<R>> {
  return cr.value === NO_VALUE
    ? from(cr) as CompilerResult<never>
    : new CompilerResult(await project(cr.value), cr.diagnostics);
}

export function flatMap<T, R>(
  cr: CompilerResultLike<T>,
  project: (value: T) => CompilerResultLike<R>,
): CompilerResult<R> {
  if (cr.value === NO_VALUE) {
    return from(cr) as CompilerResult<never>;
  }
  const newCr: CompilerResultLike<R> = project(cr.value);
  const diagnostics: Diagnostic[] = [...cr.diagnostics];
  for (const diagnostic of newCr.diagnostics) {
    diagnostics.push(diagnostic);
  }
  return new CompilerResult(newCr.value, diagnostics);
}

export async function flatMapAsync<T, R>(
  cr: CompilerResultLike<T>,
  project: (value: T) => Promise<CompilerResultLike<R>>,
): Promise<CompilerResult<R>> {
  if (cr.value === NO_VALUE) {
    return from(cr) as CompilerResult<never>;
  }
  const newCr: CompilerResultLike<R> = await project(cr.value);
  const diagnostics: Diagnostic[] = [...cr.diagnostics];
  for (const diagnostic of newCr.diagnostics) {
    diagnostics.push(diagnostic);
  }
  return new CompilerResult(newCr.value, diagnostics);
}

export function of<T>(value: T): CompilerResult<T> {
  return new CompilerResult(value, []);
}

export function from<T>(cr: CompilerResultLike<T>): CompilerResult<T> {
  return cr instanceof CompilerResult ? cr : new CompilerResult(cr.value, cr.diagnostics);
}

export function hasValue(cr: CompilerResultLike<any>): boolean {
  return cr.value !== NO_VALUE;
}

export function unwrap<T>(cr: CompilerResultLike<T>): T {
  if (cr.value === NO_VALUE) {
    throw new Error("UnwrappingCompilerResultWithoutValue");
  }
  return cr.value;
}
