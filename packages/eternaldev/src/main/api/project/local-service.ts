import { Blob } from "@eternalfest/api-core/blob/blob";
import { BlobIdRef } from "@eternalfest/api-core/blob/blob-id-ref";
import { LocalFileMap } from "@eternalfest/api-core/eternaldev/local-file-map";
import { ProfileMap } from "@eternalfest/api-core/eternaldev/profile-map";
import { Project } from "@eternalfest/api-core/eternaldev/project";
import { FamiliesString } from "@eternalfest/api-core/game/families-string";
import { GameCategory } from "@eternalfest/api-core/game/game-category";
import { GameDisplayName } from "@eternalfest/api-core/game/game-display-name";
import { $GameModeState } from "@eternalfest/api-core/game/game-mode-state";
import { $GameOptionState, GameOptionState } from "@eternalfest/api-core/game/game-option-state";
import { GameBuild } from "@eternalfest/api-core/game2/game-build";
import { GameEngineType } from "@eternalfest/api-core/game2/game-engine-type";
import { GameOptionSpecMap } from "@eternalfest/api-core/game2/game-option-spec-map";
import { ObjectType } from "@eternalfest/api-core/types/object-type";
import { Url } from "@eternalfest/api-core/types/url";
import { toUuid } from "@eternalfest/api-core/utils/helpers";
import { VERSION as LOADER_VERSION } from "@eternalfest/loader";
import childProcess from "child_process";
import furi from "furi";
import incident, { Incident } from "incident";
import { UuidHex } from "kryo/uuid-hex";
import { JsonValueReader } from "kryo-json/json-value-reader";
import { JsonWriter } from "kryo-json/json-writer";
import resolve from "resolve";
import rx, { firstValueFrom } from "rxjs";
import rxOp from "rxjs/operators";
import toml from "toml";
import { fileURLToPath } from "url";

import { readTextAsync } from "../../fs-utils.js";
import { LocalFileService } from "../file/local-service.js";
import { $Manifest, Manifest } from "../manifest/manifest.js";
import { ManifestContent } from "../manifest/manifest-content.js";
import { ManifestExternal } from "../manifest/manifest-external.js";
import { ManifestLevel } from "../manifest/manifest-level.js";
import { ManifestLocaleMap } from "../manifest/manifest-locale-map.js";
import { ManifestModeMap } from "../manifest/manifest-mode-map.js";
import { ManifestPatchman } from "../manifest/manifest-patchman.js";
import { CompilerResult as Cr, error as crError, of as crOf } from "./compiler-result.js";
import { getObservableFileVersion } from "./observable-fs.js";
import { ShortProject } from "./short-project.js";

const JSON_VALUE_READER: JsonValueReader = new JsonValueReader();
const JSON_WRITER: JsonWriter = new JsonWriter();

const DEFAULT_FAMILIES: FamiliesString = "0,7,1000,1001,1002,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19";

export interface LocalProjectServiceOptions {
  projectDirs: Iterable<Url>,
  noGitIntegration?: boolean,
}

export class LocalProjectService {
  private readonly fileService: LocalFileService;
  private readonly furiToManifest: Map<string, rx.Observable<Cr<Manifest | undefined>>>;
  private readonly idToFuri: Map<UuidHex, Url>;
  private readonly furiToId: Map<string, UuidHex>;
  private withGitIntegration: boolean;

  public constructor(fileService: LocalFileService, opts: LocalProjectServiceOptions) {
    const furiToManifest: Map<string, rx.Observable<Cr<Manifest | undefined>>> = new Map();
    const idToFuri: Map<UuidHex, Url> = new Map();
    const furiToId: Map<string, UuidHex> = new Map();

    for (const projectDir of opts.projectDirs) {
      const projectDirStr: string = projectDir.toString();
      if (!furiToManifest.has(projectDirStr)) {
        furiToManifest.set(projectDirStr, observeManifestByDir(projectDir));
        const id: UuidHex = toUuid(projectDirStr);
        idToFuri.set(id, projectDir);
        furiToId.set(projectDirStr, id);
      }
    }

    this.fileService = fileService;
    this.furiToManifest = furiToManifest;
    this.idToFuri = idToFuri;
    this.furiToId = furiToId;
    this.withGitIntegration = !opts.noGitIntegration;
  }

  public disableGitIntegration() {
    this.withGitIntegration = false;
  }

  public async getProjects(): Promise<ShortProject[]> {
    const shortProjects: rx.Observable<(ShortProject | undefined)>[] = [];
    for (const [dir, manifest$] of this.furiToManifest) {
      const shortProject$: rx.Observable<ShortProject | undefined> = manifest$
        .pipe(rxOp.map((manifestCr: Cr<Manifest | undefined>): ShortProject | undefined => {
          if (!manifestCr.hasValue()) {
            // An unexpected error occurred
            return undefined;
          }
          const manifest: Manifest | undefined = manifestCr.unwrap();
          if (manifest === undefined) {
            // Manifest not found
            return undefined;
          }
          const localUrl: string = dir.toString();
          const projectId: UuidHex | undefined = this.furiToId.get(localUrl);
          if (projectId === undefined) {
            throw new Error("AssertionError: Expected known id for project URI");
          }
          const displayName: GameDisplayName = manifest.displayName !== undefined ? manifest.displayName : "Untitled";
          const project: ShortProject = {
            id: projectId,
            localUrl,
            displayName,
            description: manifest.description,
          };
          return project;
        }));
      shortProjects.push(shortProject$);
    }

    const projects: ShortProject[] = await firstValueFrom(rx.combineLatest(shortProjects)
      .pipe(rxOp.map((projects: (ShortProject | undefined)[]): ShortProject[] => {
        const result: ShortProject[] = [];
        for (const p of projects) {
          if (p !== undefined) {
            result.push(p);
          }
        }
        return result;
      })));
    return projects;
  }

  public async getProjectById(projectId: UuidHex): Promise<Project | undefined> {
    const dir: Url | undefined = this.idToFuri.get(projectId);
    if (dir === undefined) {
      return undefined;
    }
    const manifest$: rx.Observable<Cr<Manifest | undefined>> | undefined = this.furiToManifest.get(dir.toString());
    if (manifest$ === undefined) {
      throw new Error("AssertionError: Expected known manifest for project URI");
    }
    const manifestCr: Cr<Manifest | undefined> = await firstValueFrom(manifest$);
    const manifest: Manifest | undefined = manifestCr.unwrap();
    if (manifest === undefined) {
      return undefined;
    }
    return resolveProject(projectId, dir, this.fileService, manifest, this.withGitIntegration);
  }

  public async getProjectByRoot(root: Url): Promise<Project | undefined> {
    const id: UuidHex | undefined = this.furiToId.get(root.toString());
    if (id === undefined) {
      return undefined;
    }
    return this.getProjectById(id);
  }
}

const ALLOWED_RESOURCE_EXTS: Map<string, string>  = new Map([
  ["swf", "application/x-shockwave-flash"],
  ["xml", "application/xml"],
  ["mp3", "audio/mp3"],
  ["png", "image/png"],
]);

async function resolveProject(id: UuidHex, dir: Url, fileService: LocalFileService, manifest: Manifest, getGitCommit: boolean): Promise<Project> {
  const files: LocalFileMap = new Map();
  function resolve(url: string, name: string, mimeType: string): Blob & BlobIdRef {
    if (!url.startsWith("./") && !url.startsWith("../")) {
      throw new Error(`Invalid ${name}: must start with "./" or "../": ${JSON.stringify(url)}`);
    }

    const mediaType = ALLOWED_RESOURCE_EXTS.get(url.split(".").pop()!);
    if (mediaType === undefined) {
      throw new Error(`Invalid ${name}: unknown file type: ${JSON.stringify(url)}`);
    }
    if (mediaType !== mimeType) {
      throw new Error(`Invalid ${name}: wrong MIME type ${mimeType} for: ${JSON.stringify(url)}`);
    }

    const resolvedUrl = furi.join(dir, url);
    const id = fileService.createFileUuidFromUrl(resolvedUrl);
    files.set(id, { url: resolvedUrl.toString() });
    return {
      type: ObjectType.Blob,
      id,
      mediaType,
      byteSize: 0,
    };
  }

  const game: GameBuild = {
    version: manifest.version ?? await readVersionFromPackageJson(dir),
    gitCommitRef: getGitCommit ? await readGitCommitRef(dir) : null,
    createdAt: new Date(),
    mainLocale: manifest.mainLocale ?? "fr-FR",
    displayName: manifest.displayName ?? "Untitled",
    description: manifest.description ?? "",
    icon: manifest.icon !== undefined ? resolve(manifest.icon, "icon", "image/png") : null,
    loader: LOADER_VERSION,
    engine: {type: GameEngineType.V96}, // will be filled later.
    patcher: null, // will be filled later.
    debug: null, // TODO
    content: manifest.content !== undefined ? resolve(manifest.content.build, "content.build", "application/xml") : null,
    contentI18n: null,
    musics: manifest.musics === undefined ? [] : manifest.musics.map((music, i) => {
      const blob = resolve(music, `musics[${i}]`, "audio/mp3");
      return { displayName: music, blob };
    }),
    modes: new Map(), // will be filled later.
    families: manifest.families ?? DEFAULT_FAMILIES,
    category: manifest.category ?? GameCategory.Other,
    i18n: new Map(), // will be filled later.
  };

  if (manifest.hf !== undefined) {
    game.engine = {
      type: GameEngineType.Custom,
      blob: resolve(manifest.hf, "hf", "application/x-shockwave-flash"),
    };
  }

  if (manifest.patchman !== undefined) {
    game.patcher = {
      blob: resolve(manifest.patchman.build, "patchman.build", "application/x-shockwave-flash"),
      framework: {
        name: "patchman",
        version: await resolvePackageVersion(dir, "@patchman/patchman"),
      }
    };
  } else if (manifest.external) {
    game.patcher = {
      blob: resolve(manifest.external.build, "external.build", "application/x-shockwave-flash"),
      framework: {
        name: "external",
        version: await resolvePackageVersion(dir, "@eternalfest/external"),
      }
    };
  }

  if (manifest.locales !== undefined) {
    for (const [localeId, localeMeta] of manifest.locales) {
      game.i18n.set(localeId, {
        contentI18n: resolve(localeMeta.build, `locales.${localeId}.build`, "application/xml"),
        displayName: localeMeta.displayName,
        description: localeMeta.description,
        icon: undefined, // TODO
        modes: undefined, // TODO
      });
    }
  }

  // Move main locale contents from the locale map to the fields in GameBuild.
  const mainLocale = game.i18n.get(game.mainLocale);
  if (mainLocale !== undefined) {
    game.i18n.delete(game.mainLocale);
    // Safe, because of the return type of 'resolve`.
    game.contentI18n = mainLocale.contentI18n as unknown as Blob;

    if (mainLocale.displayName !== undefined) {
      if (game.displayName !== undefined) {
        throw new Error("Duplicate displayName for main locale");
      } else {
        game.displayName = mainLocale.displayName;
      }
    }

    if (mainLocale.description !== undefined) {
      if (game.description !== undefined) {
        throw new Error("Duplicate description for main locale");
      } else {
        game.description = mainLocale.description;
      }
    }
  }

  if (manifest.modes !== undefined) {
    for (const [modeId, mode] of manifest.modes) {
      const options: GameOptionSpecMap = new Map();
      if (mode.options !== undefined) {
        for (const [optionId, option] of mode.options) {
          options.set(optionId, {
            displayName: option.displayName ?? `<${optionId}>`,
            isVisible: option.state !== GameOptionState.Disabled,
            isEnabled: option.state === GameOptionState.Enabled,
            defaultValue: option.state === GameOptionState.Locked,
          });
        }
      }
      game.modes.set(modeId, {
        displayName: mode.displayName ?? `<${modeId}>`,
        isVisible: mode.state === "enabled",
        options,
      });
    }
  }

  return {
    id,
    remoteId: manifest.remoteId,
    localUrl: dir.toString(),
    game,
    files,
    profiles: manifest.profiles !== undefined ? manifest.profiles : new Map(),
  };
}

async function readVersionFromPackageJson(dir: Url): Promise<string> {
  try {
    const text = await readTextAsync(furi.join(dir, "package.json"));
    const raw: any = JSON.parse(text);
    if (typeof raw === "object" && typeof raw.version === "string") {
      return raw.version;
    } else {
      throw new Error("Invalid package.json file");
    }
  } catch(e) {
    throw new Error("Invalid package.json file", { cause: e });
  }
}

function resolvePackageVersion(dir: Url, packName: string): Promise<string> {
  return new Promise((resolveCb, rejectCb) => {
    const basedir: string = fileURLToPath(dir);
    resolve(packName + "/package.json", { basedir }, (err, _resolved, pkgMeta) => {
      if (pkgMeta === undefined) {
        rejectCb(err);
      } else {
        resolveCb(pkgMeta.version);
      }
    });
  });
}

function readGitCommitRef(dir: Url): Promise<string | null> {
  const child = childProcess.spawn("git", ["rev-parse", "--verify", "HEAD"], {
    // Note: node 14 & 15 don't support URLs here.
    cwd: fileURLToPath(dir),
    stdio: ["ignore", "pipe", "ignore"],
  });

  let gitOutput: string = "";
  child.stdout.setEncoding("utf-8");
  child.stdout.on("data", data => {
    gitOutput += data.toString();
  });

  return new Promise((resolve, reject) => {
    child.on("exit", (code, _signal) => {
      if (code === 0) {
        const commit = gitOutput.trim();
        if (commit === "") {
          reject(new Incident("UnexpectedGitOutput: empty commit ref"));
        } else {
          resolve(commit);
        }
      } else {
        // Not in a git repository.
        resolve(null);
      }
    });
  });
}

export function observeManifestByDir(path: Url): rx.Observable<Cr<Manifest | undefined>> {
  return rx.combineLatest([
    observeTomlManifest(furi.join(path.toString(), ["eternalfest.toml"])),
    observeJsonManifest(furi.join(path.toString(), ["eternalfest.json"])),
  ])
    .pipe(rxOp.map(([tomlCr, jsonCr]): Cr<Manifest | undefined> => {
      if (tomlCr.hasValue()) {
        const tomlManifest: Manifest | undefined = tomlCr.unwrap();
        if (tomlManifest !== undefined) {
          return tomlCr;
        }
      } else if (jsonCr.hasValue()) {
        const jsonManifest: Manifest | undefined = jsonCr.unwrap();
        if (jsonManifest !== undefined) {
          return jsonCr;
        }
      }
      if (fileURLToPath(path as import("url").URL) === process.cwd()) {
        console.warn(tomlCr);
        console.warn(jsonCr);
      }
      return crError(new incident.Incident("ManifestNotFound", {path: path.toString()}));
    }));
}

function observeJsonManifest(path: Url): rx.Observable<Cr<Manifest | undefined>> {
  return getObservableFileVersion(path).pipe(rxOp.share(), rxOp.switchMap(() => getJsonManifest(path)));
}

async function getJsonManifest(path: Url): Promise<Cr<Manifest | undefined>> {
  let text: string;
  try {
    text = await readTextAsync(path as import("url").URL);
  } catch (err) {
    if ((err as NodeJS.ErrnoException).code === "ENOENT") {
      return crOf(undefined);
    } else {
      throw err;
    }
  }
  let manifest: Manifest;
  try {
    const raw: any = JSON.parse(text);
    manifest = $Manifest.read(JSON_VALUE_READER, raw);
  } catch (err) {
    return crError(err);
  }
  return crOf(manifest);
}

function observeTomlManifest(path: Url): rx.Observable<Cr<Manifest | undefined>> {
  return getObservableFileVersion(path).pipe(rxOp.switchMap(() => getTomlManifest(path)));
}

async function getTomlManifest(path: Url): Promise<Cr<Manifest | undefined>> {
  let text: string;
  try {
    text = await readTextAsync(path as import("url").URL);
  } catch (err) {
    if ((err as NodeJS.ErrnoException).code === "ENOENT") {
      return crOf(undefined);
    } else {
      throw err;
    }
  }
  let manifest: Manifest;
  try {
    const raw: any = toml.parse(text);
    manifest = $Manifest.read(JSON_VALUE_READER, raw);
  } catch (err) {
    return crError(err);
  }
  return crOf(manifest);
}

export function emitTomlManifest(manifest: Manifest): string {
  const lines: string[] = [];
  if (manifest.displayName !== undefined) {
    lines.push(`display_name = ${JSON.stringify(manifest.displayName)}`);
  }
  if (manifest.description !== undefined) {
    lines.push(`description = ${JSON.stringify(manifest.description)}`);
  }
  if (manifest.icon !== undefined) {
    lines.push(`icon = ${JSON.stringify(manifest.icon)}`);
  }
  if (manifest.remoteId !== undefined) {
    lines.push(`remote_id = ${JSON.stringify(manifest.remoteId)}`);
  }
  if (manifest.repository !== undefined) {
    lines.push(`repository = ${JSON.stringify(manifest.repository)}`);
  }
  if (manifest.mainLocale !== undefined) {
    lines.push(`main_locale = ${JSON.stringify(manifest.mainLocale)}`);
  }
  if (manifest.families !== undefined) {
    lines.push(`families = ${JSON.stringify(manifest.families)}`);
  }
  if (manifest.hf !== undefined) {
    lines.push(`hf = ${JSON.stringify(manifest.hf)}`);
  }
  if (manifest.musics !== undefined) {
    lines.push(`musics = ${JSON.stringify(manifest.musics, null, 2)}`);
  }
  if (manifest.content !== undefined) {
    lines.push("");
    lines.push("[content]");
    lines.push(emitTomlManifestContent(manifest.content));
  }
  if (manifest.patchman !== undefined) {
    lines.push("");
    lines.push("[patchman]");
    lines.push(emitTomlManifestPatchman(manifest.patchman));
  }
  if (manifest.external !== undefined) {
    lines.push("");
    lines.push("[external]");
    lines.push(emitTomlManifestExternal(manifest.external));
  }
  if (manifest.modes !== undefined) {
    lines.push("");
    lines.push(emitTomlManifestModes(manifest.modes));
  }
  if (manifest.profiles !== undefined) {
    lines.push("");
    lines.push(emitTomlManifestProfiles(manifest.profiles));
  }
  if (manifest.locales !== undefined) {
    lines.push("");
    lines.push(emitTomlManifestLocales(manifest.locales));
  }
  lines.push("");
  return lines.join("\n");
}

function emitTomlManifestContent(content: ManifestContent): string {
  const lines: string[] = [];
  if (content.build !== undefined) {
    lines.push(`build = ${JSON.stringify(content.build)}`);
  }
  if (content.scoreItems !== undefined) {
    lines.push(`score_items = ${JSON.stringify(content.scoreItems)}`);
  }
  if (content.specialItems !== undefined) {
    lines.push(`special_items = ${JSON.stringify(content.specialItems)}`);
  }
  if (content.dimensions !== undefined) {
    lines.push(`dimensions = ${JSON.stringify(content.dimensions)}`);
  }
  if (content.data !== undefined) {
    lines.push(`data = ${JSON.stringify(content.data)}`);
  }
  if (content.levels !== undefined) {
    if (typeof content.levels === "string") {
      lines.push(`levels = ${JSON.stringify(content.levels)}`);
    } else {
      for (const [key, level] of content.levels) {
        lines.push(`[content.levels.${key}]`);
        lines.push(emitTomlManifestLevel(level));
      }
    }
  }
  return lines.join("\n");
}

function emitTomlManifestLevel(level: ManifestLevel): string {
  const lines: string[] = [];
  if (level.levels !== undefined) {
    lines.push(`levels = ${JSON.stringify(level.levels)}`);
  }
  if (level.var !== undefined) {
    lines.push(`var = ${JSON.stringify(level.var)}`);
  }
  if (level.padding !== undefined) {
    lines.push(`padding = ${JSON.stringify(level.padding)}`);
  }
  if (level.sameSize !== undefined) {
    lines.push(`same_size = ${JSON.stringify(level.sameSize)}`);
  }
  if (level.did !== undefined) {
    lines.push(`did = ${JSON.stringify(level.did)}`);
  }
  return lines.join("\n");
}

function emitTomlManifestPatchman(pm: ManifestPatchman): string {
  const lines: string[] = [];
  if (pm.build !== undefined) {
    lines.push(`build = ${JSON.stringify(pm.build)}`);
  }
  return lines.join("\n");
}

function emitTomlManifestExternal(external: ManifestExternal): string {
  const lines: string[] = [];
  if (external.build !== undefined) {
    lines.push(`build = ${JSON.stringify(external.build)}`);
  }
  return lines.join("\n");
}

function emitTomlManifestModes(modes: ManifestModeMap): string {
  const lines: string[] = [];
  for (const [key, mode] of modes) {
    lines.push(`[modes.${key}]`);
    lines.push(`display_name = ${JSON.stringify(mode.displayName)}`);
    lines.push(`state = ${$GameModeState.write(JSON_WRITER, mode.state)}`);
    if (mode.options !== undefined) {
      lines.push(`[modes.${key}.options]`);
      for (const [okey, opt] of mode.options) {
        lines.push(`${okey} = {display_name = ${JSON.stringify(opt.displayName)}, state = ${$GameOptionState.write(JSON_WRITER, opt.state)}}`);
      }
    }
  }
  return lines.join("\n");
}

function emitTomlManifestProfiles(profiles: ProfileMap): string {
  const lines: string[] = [];
  for (const [key, profile] of profiles) {
    lines.push(`[profiles.${key}]`);
    if (profile.user !== undefined) {
      lines.push(`user = {id = ${JSON.stringify(profile.user.id)}, display_name = ${JSON.stringify(profile.user.displayName)}}`);
    }
    if (profile.items !== undefined) {
      const itemDict: string[] = [];
      for (const [id, count] of profile.items) {
        itemDict.push(`${id} = ${count}`);
      }
      lines.push(`items = {${itemDict.join(", ")}}`);
    }
  }
  return lines.join("\n");
}

function emitTomlManifestLocales(locales: ManifestLocaleMap): string {
  const lines: string[] = [];
  for (const [key, locale] of locales) {
    lines.push(`[locales.${key}]`);
    if (locale.build !== undefined) {
      lines.push(`build = ${JSON.stringify(locale.build)}`);
    }
    if (typeof locale.content === "string") {
      lines.push(`content = ${JSON.stringify(locale.content)}`);
    } else {
      lines.push(`[locales.${key}.content]`);
      lines.push(`keys = ${JSON.stringify(locale.content.keys)}`);
      lines.push(`items = ${JSON.stringify(locale.content.items)}`);
      lines.push(`lands = ${JSON.stringify(locale.content.lands)}`);
      lines.push(`statics = ${JSON.stringify(locale.content.statics)}`);
    }
  }
  return lines.join("\n");
}
