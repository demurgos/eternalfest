
import { $ProjectManifest as $LegacyManifest, ProjectManifest as LegacyManifest } from "@eternalfest/api-core/eternaldev/project-manifest";
import { GameModeDisplayName } from "@eternalfest/api-core/game/game-mode-display-name";
import { GameModeKey } from "@eternalfest/api-core/game/game-mode-key";
import { GameOptionDisplayName } from "@eternalfest/api-core/game/game-option-display-name";
import { GameOptionKey } from "@eternalfest/api-core/game/game-option-key";
import { Url } from "@eternalfest/api-core/types/url";
import fs from "fs";
import furi from "furi";
import incident from "incident";
import { JSON_VALUE_READER } from "kryo-json/json-value-reader";
import toml from "toml";


export async function readManifest(root: Url): Promise<LegacyManifest> {
  {
    const resolved: Url = furi.join(root.toString(), ["eternalfest.toml"]);
    const text: string | undefined = await tryReadTextAsync(resolved);
    if (text !== undefined) {
      return $LegacyManifest.read(JSON_VALUE_READER, toml.parse(text));
    }
  }
  {
    const resolved: Url = furi.join(root.toString(), ["eternalfest.json"]);
    const text: string | undefined = await tryReadTextAsync(resolved);
    if (text !== undefined) {
      return $LegacyManifest.read(JSON_VALUE_READER, JSON.parse(text));
    }
  }
  throw new incident.Incident("ManifestNotFound", {root: root.toString()});
}

async function tryReadTextAsync(filePath: Url): Promise<string | undefined> {
  return new Promise<string | undefined>((resolve, _reject) => {
    fs.readFile(filePath as import("url").URL, {encoding: "utf-8"}, (err: NodeJS.ErrnoException | null, data: string) => {
      if (err !== null) {
        if (err.code !== "ENOENT") {
          console.warn(err);
        }
        resolve(undefined);
      } else {
        resolve(data);
      }
    });
  });
}

export function gameModeKeyToDisplayName(key: GameModeKey): GameModeDisplayName {
  switch (key) {
    case "bossrush":
      return "Boss Rush";
    case "multicoop":
      return "Multi Coopératif";
    case "multitime":
      return "TA Multi";
    case "soccer":
      return "Soccerfest";
    case "soccer_0":
      return "Gazon maudit";
    case "soccer_1":
      return "Temple du Ballon";
    case "soccer_2":
      return "VolleyFest";
    case "soccer_3":
      return "Maitrise aérienne";
    case "soccer_4":
      return "Piste temporelle";
    case "soccer_5":
      return "Stade de Basalte";
    case "soccer_6":
      return "Coupe de glace";
    case "soccer_7":
      return "Carton noir";
    case "soccer_8":
      return "Terrain oublié";
    case "soccer_9":
      return "Orange gardien";
    case "soccer_10":
      return "Chaudron cracheur";
    case "soccer_11":
      return "Salle à pics";
    case "soccer_12":
      return "Kiwi Arena";
    case "soccer_13":
      return "Cages inviolées";
    case "soccer_14":
      return "Dojo Fulguro";
    case "soccer_alea":
      return "Terrain aléatoire";
    case "solo":
      return "Aventure";
    case "timeattack":
      return "Time Attack";
    case "tutorial":
      return "Apprentissage";
    default:
      return key;
  }
}

export function gameOptionKeyToDisplayName(key: GameOptionKey): GameOptionDisplayName {
  switch (key) {
    case "bombcontrol": return "Bombotopia";
    case "bombexpert": return "Explosifs Instables";
    case "boost": return "Tornade";
    case "ice": return "Âge de Glace";
    case "insight": return "Intuition";
    case "kickcontrol": return "Contrôle avancé du ballon";
    case "lava": return "Lave";
    case "lifesharing": return "Partage de vies";
    case "mirror": return "Miroir";
    case "mirrormulti": return "Miroir";
    case "nightmare": return "Cauchemar";
    case "nightmaremulti": return "Cauchemar";
    case "ninja": return "Ninjutsu";
    case "nojutsu": return "Nojutsu";
    case "randomfest": return "Randomfest";
    case "soccerbomb": return "Bombes";
    case "soccer_short": return "Durée de match courte";
    case "soccer_normal": return "Durée de match normale";
    case "soccer_long": return "Durée de match longue";
    default: return key;
  }
}
