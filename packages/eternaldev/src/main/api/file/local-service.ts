import { AuthContext } from "@eternalfest/api-core/auth/auth-context";
import { MediaType } from "@eternalfest/api-core/blob/media-type";
import { CreateDirectoryOptions } from "@eternalfest/api-core/file/create-directory-options";
import { CreateFileOptions } from "@eternalfest/api-core/file/create-file-options";
import { DeleteItemOptions } from "@eternalfest/api-core/file/delete-item-options";
import { Directory } from "@eternalfest/api-core/file/directory";
import { Drive } from "@eternalfest/api-core/file/drive";
import { DriveItem } from "@eternalfest/api-core/file/drive-item";
import { DriveItemType } from "@eternalfest/api-core/file/drive-item-type";
import { File } from "@eternalfest/api-core/file/file";
import { GetItemByPathOptions } from "@eternalfest/api-core/file/get-item-by-path-options";
import { FileService } from "@eternalfest/api-core/file/service";
import { Url } from "@eternalfest/api-core/types/url";
import { toUuid } from "@eternalfest/api-core/utils/helpers";
import fs from "fs";
import incident from "incident";
import { UuidHex } from "kryo/uuid-hex";
import stream from "stream";

function sleep(timeoutMs: number): Promise<void> {
  return new Promise((resolve) => {
    setTimeout(resolve, timeoutMs);
  });
}

class ThrottleGroup {
  #bytesPerSecond: number;
  #maxChunkSize: number;

  constructor(bytesPerSecond: number) {
    this.#bytesPerSecond = bytesPerSecond;
    this.#maxChunkSize = Math.max(Math.floor(bytesPerSecond / 10));
  }

  public createStream(): stream.Duplex {
    // TODO: Throttling should be applied between all elements of the group
    const self = this;
    return stream.Duplex.from(async function*(readable: AsyncIterable<Uint8Array>): AsyncGenerator<Uint8Array, void, undefined> {
      for await (const chunk of await readable) {
        for (let i = 0; i < chunk.length; i += self.#maxChunkSize) {
          const subChunk = chunk.slice(i, i + self.#maxChunkSize);
          const milliseconds = Math.floor(1000 * subChunk.length / self.#bytesPerSecond);
          await sleep(milliseconds);
          yield subChunk;
        }
      }
    });
  }
}

interface LocalFile {
  url: Url;
}

interface LocalFileServiceOptions {
  throttle?: number;
}

async function fileFromLocalFile(localFile: LocalFile): Promise<File> {
  const id: UuidHex = toUuid(localFile.url.toString());
  let mediaType: MediaType;
  if (localFile.url.toString().match(/\.swf$/)) {
    mediaType = "application/x-shockwave-flash";
  } else if (localFile.url.toString().match(/\.xml$/)) {
    mediaType = "application/xml";
  }  else if (localFile.url.toString().match(/\.mp3$/)) {
    mediaType = "audio/mp3";
  }  else if (localFile.url.toString().match(/\.png$/)) {
    mediaType = "image/png";
  } else {
    mediaType = "application/octet-stream";
  }

  const stats: fs.Stats = fs.statSync(localFile.url as import("url").URL);

  return {
    id,
    displayName: "File",
    createdAt: stats.ctime,
    updatedAt: stats.mtime,
    type: DriveItemType.File,
    byteSize: stats.size,
    mediaType: mediaType,
  };
}

export class LocalFileService implements FileService {
  readonly #idToLocalFile: Map<UuidHex, LocalFile>;
  readonly #throttle: ThrottleGroup | null;

  constructor(options: LocalFileServiceOptions = {}) {
    this.#idToLocalFile = new Map();
    if (options.throttle === undefined || options.throttle <= 0) {
      this.#throttle = null;
    } else {
      this.#throttle = new ThrottleGroup(options.throttle);
    }
  }

  createFileUuidFromUrl(fileUrl: Url): UuidHex {
    const id: UuidHex = toUuid(fileUrl.toString());
    this.#idToLocalFile.set(id, {url: fileUrl});
    return id;
  }

  async createFileFromUrl(fileUrl: Url): Promise<File> {
    const id: UuidHex = toUuid(fileUrl.toString());
    const localFile: LocalFile = {url: fileUrl};
    this.#idToLocalFile.set(id, localFile);
    return fileFromLocalFile(localFile);
  }

  createDirectory(_auth: AuthContext, _options: CreateDirectoryOptions): Promise<Directory> {
    throw new incident.Incident("NotImplemented: LocalFileService#createDirectory");
  }

  createFile(_auth: AuthContext, _options: CreateFileOptions): Promise<File> {
    throw new incident.Incident("NotImplemented: LocalFileService#createFile");
  }

  deleteItem(_auth: AuthContext, _options: Readonly<DeleteItemOptions>): Promise<void> {
    throw new incident.Incident("NotImplemented: LocalFileService#deleteItem");
  }

  getDirectoryById(_auth: AuthContext, _directoryId: UuidHex): Promise<Directory> {
    throw new incident.Incident("NotImplemented: LocalFileService#getDirectoryById");
  }

  async getDirectoryChildrenById(_auth: AuthContext, _directoryId: UuidHex): Promise<readonly DriveItem[]> {
    throw new incident.Incident("NotImplemented: LocalFileService#getDirectoryChildrenById");
  }

  getDriveById(_auth: AuthContext, _driveId: UuidHex): Promise<Drive> {
    throw new incident.Incident("NotImplemented: LocalFileService#getDriveById");
  }

  getDriveByOwnerId(_auth: AuthContext, _ownerId: UuidHex): Promise<Drive> {
    throw new incident.Incident("NotImplemented: LocalFileService#getDriveByOwnerId");
  }

  getFileById(_auth: AuthContext, fileId: UuidHex): Promise<File> {
    const localFile: LocalFile | undefined = this.#idToLocalFile.get(fileId);
    if (localFile === undefined) {
      throw new Error("FileNotFound");
    }
    return fileFromLocalFile(localFile);
  }

  getItemByPath(
    _auth: AuthContext,
    _options: Readonly<GetItemByPathOptions>,
  ): Promise<DriveItem> {
    throw new incident.Incident("NotImplemented: LocalFileService#getItemByPath");
  }

  async readFileContent(_auth: AuthContext, fileId: UuidHex): Promise<stream.Readable> {
    const localFile: LocalFile | undefined = this.#idToLocalFile.get(fileId);
    if (localFile === undefined) {
      throw new Error("FileNotFound");
    }
    const stream = fs.createReadStream(localFile.url as import("url").URL);
    if (this.#throttle === null) {
      return stream;
    } else {
      return stream.pipe(this.#throttle.createStream());
    }
  }
}
