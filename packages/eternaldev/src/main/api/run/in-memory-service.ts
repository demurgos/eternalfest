import {AuthContext} from "@eternalfest/api-core/auth/auth-context";
import {CreateDebugRunOptions} from "@eternalfest/api-core/eternaldev/create-debug-run-options";
import {PartialProfile} from "@eternalfest/api-core/eternaldev/partial-profile";
import {Profile} from "@eternalfest/api-core/eternaldev/profile";
import {ProfileKey} from "@eternalfest/api-core/eternaldev/profile-key";
import {NullableGame} from "@eternalfest/api-core/game2/game";
import {Leaderboard} from "@eternalfest/api-core/leaderboard/leaderboard";
import {CreateRunOptions} from "@eternalfest/api-core/run/create-run-options";
import {Run} from "@eternalfest/api-core/run/run";
import {RunItems} from "@eternalfest/api-core/run/run-items";
import {RunStart} from "@eternalfest/api-core/run/run-start";
import {RunService} from "@eternalfest/api-core/run/service";
import {SetRunResultOptions} from "@eternalfest/api-core/run/set-run-result-options";
import {ObjectType} from "@eternalfest/api-core/types/object-type";
import {UserService} from "@eternalfest/api-core/user/service";
import {User} from "@eternalfest/api-core/user/user";
import {UserRef} from "@eternalfest/api-core/user/user-ref";
import {UuidHex} from "kryo/uuid-hex";
import uuidjs from "uuidjs";

import {LocalGame2Service} from "../game2/local-service.js";
import {DEFAULT_USER_DISPLAY_NAME, DEFAULT_USER_ID} from "../user/in-memory-service.js";

export class InMemoryRunService implements RunService {
  private readonly game: LocalGame2Service;
  private readonly user: UserService;
  private readonly idToRun: Map<UuidHex, Run>;
  private readonly runIdToItems: Map<UuidHex, RunItems>;

  constructor(game: LocalGame2Service, user: UserService) {
    this.game = game;
    this.user = user;
    this.idToRun = new Map();
    this.runIdToItems = new Map();
  }

  getGameUserItemsById(_auth: AuthContext, _userId: string, _gameId: string): Promise<RunItems | undefined> {
    throw new Error("Method not implemented: getGameUserItemsById.");
  }

  getLeaderboard(_auth: AuthContext, _gameId: string, _gameMode: string): Promise<Leaderboard> {
    throw new Error("Method not implemented: getLeaderboard.");
  }

  async createDebugRun(auth: AuthContext, options: CreateDebugRunOptions): Promise<Run> {
    const id: UuidHex = uuidjs.genV4().toString();
    const game: NullableGame = await this.game.getGame(auth, {
      game: {id: options.gameId},
      channelOffset: 0,
      channelLimit: 1
    });
    if (game === null) {
      throw new Error("GameNotFound");
    }
    const profile: Profile = await this.getDebugProfile(options.gameId, options.profile);

    const user: User | undefined = await this.user.getUserById(auth, options.userId);
    if (user === undefined) {
      throw new Error("UserNotFound");
    }
    const run: Run = {
      id,
      game: {id: game.id},
      user: profile.user,
      gameOptions: options.gameOptions,
      gameMode: options.gameMode,
      createdAt: new Date(),
      startedAt: null,
      result: null,
      settings: options.settings,
    };
    this.idToRun.set(id, run);
    this.runIdToItems.set(id, profile.items);
    return run;
  }

  async createRun(auth: AuthContext, options: CreateRunOptions): Promise<Run> {
    return this.createDebugRun(auth, {...options, userId: options.user.id, gameId: options.game.id});
  }

  async getRunById(_auth: AuthContext, runId: UuidHex): Promise<Run> {
    return this.idToRun.get(runId)!;
  }

  async startRun(auth: AuthContext, runId: UuidHex): Promise<RunStart> {
    const run: Run | undefined = this.idToRun.get(runId);
    if (run === undefined) {
      throw new Error("RunNotFound");
    }
    const game: NullableGame = await this.game.getGame(auth, {
      game: {id: run.game.id},
      channelOffset: 0,
      channelLimit: 1
    });
    if (game === null) {
      throw new Error("GameNotFound");
    }
    const key: UuidHex = uuidjs.genV4().toString();

    const items: RunItems = this.runIdToItems.get(runId)!;

    const runStart: RunStart = {
      families: game.channels.active.build.families,
      key,
      run: {id: runId},
      items,
    };

    const newRun: Run = {...run, startedAt: new Date()};
    this.idToRun.set(runId, newRun);
    return runStart;
  }

  async setRunResult(
    _auth: AuthContext,
    runId: UuidHex,
    options: SetRunResultOptions,
  ): Promise<Run> {
    console.log("Run result:");
    console.log(options);
    return this.idToRun.get(runId)!;
  }

  private async getDebugProfile(gameId: UuidHex, profileKey?: ProfileKey): Promise<Profile> {
    const partial: PartialProfile = await this.game.getDebugProfile(gameId, profileKey);
    const user: UserRef = {
      id: partial.user !== undefined && partial.user.id !== undefined ? partial.user.id : DEFAULT_USER_ID,
      type: ObjectType.User,
      displayName: partial.user !== undefined && partial.user.displayName !== undefined
        ? partial.user.displayName
        : DEFAULT_USER_DISPLAY_NAME,
    };
    const items: RunItems = partial.items !== undefined ? partial.items : new Map();
    return {user, items};
  }
}
