import { AuthContext } from "@eternalfest/api-core/auth/auth-context";
import { Blob } from "@eternalfest/api-core/blob/blob";
import { CreateBlobOptions } from "@eternalfest/api-core/blob/create-blob-options";
import { CreateUploadSessionOptions } from "@eternalfest/api-core/blob/create-upload-session-options";
import { GetBlobContentOptions } from "@eternalfest/api-core/blob/get-blob-content-options";
import { GetBlobOptions } from "@eternalfest/api-core/blob/get-blob-options";
import { BlobService } from "@eternalfest/api-core/blob/service";
import { UploadBytesOptions } from "@eternalfest/api-core/blob/upload-bytes-options";
import { UploadSession } from "@eternalfest/api-core/blob/upload-session";
import incident from "incident";
import stream from "stream";

import {LocalFileService} from "../file/local-service.js";

export class InMemoryBlobService implements BlobService {
  #file: LocalFileService;

  public constructor(file: LocalFileService) {
    this.#file = file;
  }

  public readonly hasImmutableBlobs: boolean = false;

  async createBlob(_auth: AuthContext, _options: CreateBlobOptions): Promise<Blob> {
    throw new incident.Incident("NotImplemented: InMemoryBlobService#createBlob");
  }

  async createUploadSession(_auth: AuthContext, _options: CreateUploadSessionOptions): Promise<UploadSession> {
    throw new incident.Incident("NotImplemented: InMemoryBlobService#createUploadSession");
  }

  async getBlobById(auth: AuthContext, options: GetBlobOptions): Promise<Blob> {
    const file = await this.#file.getFileById(auth, options.id);
    return {
      id: file.id,
      mediaType: file.mediaType,
      byteSize: file.byteSize,
    };
  }

  async readBlobData(auth: AuthContext, options: GetBlobContentOptions): Promise<stream.Readable> {
    return await this.#file.readFileContent(auth, options.id);
  }

  uploadBytes(_auth: AuthContext, _options: UploadBytesOptions): Promise<UploadSession> {
    throw new incident.Incident("NotImplemented: InMemoryBlobService#uploadBytes");
  }
}
