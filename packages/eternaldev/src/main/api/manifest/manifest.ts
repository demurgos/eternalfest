import { $ProfileMap, ProfileMap } from "@eternalfest/api-core/eternaldev/profile-map";
import { $UrlFragment, UrlFragment } from "@eternalfest/api-core/eternaldev/url-fragment";
import { $FamiliesString, FamiliesString } from "@eternalfest/api-core/game/families-string";
import { $GameCategory, GameCategory } from "@eternalfest/api-core/game/game-category";
import { $GameDescription, GameDescription } from "@eternalfest/api-core/game/game-description";
import { $GameDisplayName, GameDisplayName } from "@eternalfest/api-core/game/game-display-name";
import { $LocaleId, LocaleId } from "@eternalfest/api-core/types/locale-id";
import { $VersionString, VersionString } from "@eternalfest/api-core/types/version-string";
import { CaseStyle } from "kryo";
import { ArrayType } from "kryo/array";
import { RecordIoType, RecordType } from "kryo/record";
import { Ucs2StringType } from "kryo/ucs2-string";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

import { $ManifestContent, ManifestContent } from "./manifest-content.js";
import { $ManifestExternal, ManifestExternal } from "./manifest-external.js";
import { $ManifestLocaleMap, ManifestLocaleMap } from "./manifest-locale-map.js";
import { $ManifestModeMap, ManifestModeMap } from "./manifest-mode-map.js";
import { $ManifestObf, ManifestObf } from "./manifest-obf.js";
import { $ManifestPatchman, ManifestPatchman } from "./manifest-patchman.js";
import { $ManifestVersion, ManifestVersion } from "./manifest-version.js";

export interface Manifest {
  /**
   * The version of the manifest format (default: 1).
   */
  manifest?: ManifestVersion;
  /**
   * Version of the game. Uses version from package.json if not specified.
   */
  version?: VersionString;
  /**
   * Title of the game in the main locale.
   */
  displayName?: GameDisplayName;
  /**
   * Description of the game in the main locale.
   */
  description?: GameDescription;
  /**
   * Category of the game (default: Other).
   */
  category?: GameCategory;

  /**
   * URL to the icon, relative to the project root.
   */
  icon?: UrlFragment;

  /**
   * ID the game on the server.
   */
  remoteId?: UuidHex;

  /**
   * Repository URL.
   */
  repository?: string;

  /**
   * Locale ID for the main locale.
   *
   * @default `"fr-FR"`
   */
  mainLocale?: LocaleId;

  /**
   * Base families as a string of coma separated integers.
   */
  families?: FamiliesString;

  /**
   * File URL to the game base file (`game.swf`), relative to the project root.
   *
   * @default Latest version of the official game.
   */
  hf?: UrlFragment;

  /**
   * Ordered list of music file URLs, relative to the project root.
   *
   * @default `[]`
   */
  musics?: UrlFragment[];

  /**
   * File URL to the directory containing the project's assets, relative to the project root.
   *
   * // TODO: Accept array
   */
  assets?: UrlFragment;

  content?: ManifestContent;
  patchman?: ManifestPatchman;
  external?: ManifestExternal;
  locales?: ManifestLocaleMap;
  modes?: ManifestModeMap;
  profiles?: ProfileMap;

  obf?: ManifestObf;
}

export const $Manifest: RecordIoType<Manifest> = new RecordType<Manifest>({
  properties: {
    manifest: {type: $ManifestVersion, optional: true},
    version: {type: $VersionString, optional: true},
    displayName: {type: $GameDisplayName, optional: true},
    description: {type: $GameDescription, optional: true},
    category: {type: $GameCategory, optional: true},
    icon: {type: $UrlFragment, optional: true},
    remoteId: {type: $UuidHex, optional: true},
    repository: {type: new Ucs2StringType({maxLength: 300}), optional: true},
    mainLocale: {type: $LocaleId, optional: true},
    families: {type: $FamiliesString, optional: true},
    hf: {type: $UrlFragment, optional: true},
    musics: {
      type: new ArrayType({itemType: $UrlFragment, maxLength: 50}),
      optional: true,
    },
    assets: {type: $UrlFragment, optional: true},
    content: {type: $ManifestContent, optional: true},
    patchman: {type: $ManifestPatchman, optional: true},
    external: {type: $ManifestExternal, optional: true},
    locales: {type: $ManifestLocaleMap, optional: true},
    modes: {type: $ManifestModeMap, optional: true},
    profiles: {type: $ProfileMap, optional: true},
    obf: {type: $ManifestObf, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
