import { $GameModeKey, GameModeKey } from "@eternalfest/api-core/game/game-mode-key";
import { MapType } from "kryo/map";

import { $ManifestMode, ManifestMode } from "./manifest-mode.js";

export type ManifestModeMap = Map<GameModeKey, ManifestMode>;

export const $ManifestModeMap: MapType<GameModeKey, ManifestMode> = new MapType({
  keyType: $GameModeKey,
  valueType: $ManifestMode,
  maxSize: Infinity,
  assumeStringKey: true,
});
