import { $GameOptionDisplayName, GameOptionDisplayName } from "@eternalfest/api-core/game/game-option-display-name";
import { $GameOptionState, GameOptionState } from "@eternalfest/api-core/game/game-option-state";
import { DeepReadonly } from "@eternalfest/api-core/types/deep-readonly";
import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

import { GameOptionDescription } from "./game-option-description.js";

export interface ManifestOption {
  state: GameOptionState;
  displayName: GameOptionDisplayName;
  description?: GameOptionDescription;
}

export type ReadonlyManifestOption = DeepReadonly<ManifestOption>;

export const $ManifestOption: RecordIoType<ManifestOption> = new RecordType<ManifestOption>({
  properties: {
    displayName: {type: $GameOptionDisplayName},
    description: {type: $GameOptionDisplayName, optional: true},
    state: {type: $GameOptionState},
  },
  changeCase: CaseStyle.SnakeCase,
});
