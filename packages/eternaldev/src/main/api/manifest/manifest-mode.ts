import { GameDescription } from "@eternalfest/api-core/game/game-description";
import { GameDisplayName } from "@eternalfest/api-core/game/game-display-name";
import { $GameModeDisplayName } from "@eternalfest/api-core/game/game-mode-display-name";
import { $GameModeState, GameModeState } from "@eternalfest/api-core/game/game-mode-state";
import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

import { $GameModeDescription } from "./game-mode-description.js";
import { $ManifestOptionMap, ManifestOptionMap } from "./manifest-option-map.js";

export interface ManifestMode {
  state: GameModeState;
  displayName: GameDisplayName;
  description?: GameDescription;
  options?: ManifestOptionMap;
}

export const $ManifestMode: RecordIoType<ManifestMode> = new RecordType<ManifestMode>({
  properties: {
    state: {type: $GameModeState},
    displayName: {type: $GameModeDisplayName},
    description: {type: $GameModeDescription, optional: true},
    options: {type: $ManifestOptionMap, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
