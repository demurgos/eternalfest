import { Ucs2StringType } from "kryo/ucs2-string";

export type GameOptionDescription = string;

export const $GameOptionDescription: Ucs2StringType = new Ucs2StringType({
  trimmed: true,
  minLength: 1,
  maxLength: 1000,
});
