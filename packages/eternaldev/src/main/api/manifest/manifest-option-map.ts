import { $GameOptionKey, GameOptionKey } from "@eternalfest/api-core/game/game-option-key";
import { MapType } from "kryo/map";

import { $ManifestOption, ManifestOption } from "./manifest-option.js";

export type ManifestOptionMap = Map<GameOptionKey, ManifestOption>;

export const $ManifestOptionMap: MapType<GameOptionKey, ManifestOption> = new MapType({
  keyType: $GameOptionKey,
  valueType: $ManifestOption,
  maxSize: Infinity,
  assumeStringKey: true,
});
