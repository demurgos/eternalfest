import { Ucs2StringType } from "kryo/ucs2-string";

export type ManifestLevelKey = string;

export const $ManifestLevelKey: Ucs2StringType = new Ucs2StringType({maxLength: Infinity});
