import { Ucs2StringType } from "kryo/ucs2-string";

export type GameModeDescription = string;

export const $GameModeDescription: Ucs2StringType = new Ucs2StringType({
  trimmed: true,
  minLength: 1,
  maxLength: 1000,
});
