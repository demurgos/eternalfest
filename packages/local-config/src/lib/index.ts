import { getLocalConfig as nativeGetLocalConfig } from "@eternalfest/native/config";

export interface Config {
  eternalfest: EternalfestConfig;
  db: DbConfig;
  data: DataConfig;
  etwin: EtwinConfig;
}

export interface EternalfestConfig {
  /**
   * Internal HTTP port
   */
  httpPort: number;

  /**
   * Public URI of the server
   */
  externalUri: URL;

  /**
   * Secret key used to encrypt sensitive DB columns (password hashes, emails) or sign JWTs.
   */
  secret: string;

  /**
   * Secret key used for session cookies.
   */
  cookieKey: string;
}

export interface DbConfig {
  host: string;
  port: number;
  name: string;
  user: string;
  password: string;
  adminUser: string;
  adminPassword: string;
}

export interface DataConfig {
  root: URL;
}

export interface EtwinConfig {
  uri: URL;
  oauthClientId: string;
  oauthClientSecret: string;
}

export async function getLocalConfig(): Promise<Config> {
  return await nativeGetLocalConfig();
}
