import chai from "chai";

import { getLocalConfig } from "../lib/index.js";

describe("LocalConfig", function () {
  it("getLocalConfig", async function () {
    const config = await getLocalConfig();
    chai.assert.isObject(config);
  });
});
