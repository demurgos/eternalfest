import chai from "chai";

import { getLocalConfig } from "../lib/config.js";

describe("Config", function () {
  describe("getLocalConfig", function () {
    it("loads the local config", async function (this: Mocha.Context) {
      this.timeout(30000);
      const config = await getLocalConfig();
      chai.assert.isObject(config);
    });
  });
});
