import {$AuthContext, AuthContext} from "@eternalfest/api-core/auth/auth-context";
import {
  $AuthenticateHttpOptions,
  AuthenticateHttpOptions
} from "@eternalfest/api-core/auth/authenticate-http-options";
import {AuthService} from "@eternalfest/api-core/auth/service";
import {$Session, Session} from "@eternalfest/api-core/types/session";
import {UserDisplayName} from "@eternalfest/api-core/user/user-display-name";
import {UserId} from "@eternalfest/api-core/user/user-id";
import {JSON_READER} from "kryo-json/json-reader";
import {JSON_WRITER} from "kryo-json/json-writer";
import {promisify} from "util";

import native from "../../native/index.js";
import {NativeAuthStore} from "../auth-store.js";
import {NativeClock} from "../clock.js";
import {NativeEtwinClient} from "../etwin-client.js";
import {NativeUserStore} from "../user-store.js";

declare const NativeFileServiceBox: unique symbol;

export interface NativeAuthServiceOptions {
  authStore: NativeAuthStore;
  clock: NativeClock;
  etwinClient: NativeEtwinClient;
  userStore: NativeUserStore;
}

export class NativeAuthService implements AuthService {
  private static NEW = promisify(native.services.auth.new);
  private static AUTHENTICATE_HTTP = promisify(native.services.auth.authenticateHttp);
  private static ETWIN_OAUTH = promisify(native.services.auth.etwinOauth);
  private static SESSION = promisify(native.services.auth.session);
  private static CREATE_SESSION = promisify(native.services.auth.createSession);

  public readonly box: typeof NativeFileServiceBox;

  private constructor(box: typeof NativeFileServiceBox) {
    this.box = box;
  }

  public static async create(options: Readonly<NativeAuthServiceOptions>): Promise<NativeAuthService> {
    const box = await NativeAuthService.NEW(options.authStore.box, options.clock.box, options.etwinClient.box, options.userStore.box);
    return new NativeAuthService(box);
  }

  async authenticateHttp(options: AuthenticateHttpOptions): Promise<AuthContext> {
    const rawOptions: string = $AuthenticateHttpOptions.write(JSON_WRITER, options);
    const rawOut = await NativeAuthService.AUTHENTICATE_HTTP(this.box, rawOptions);
    return $AuthContext.read(JSON_READER, rawOut);
  }

  async etwinOauth(userId: UserId, userDisplayName: UserDisplayName): Promise<AuthContext> {
    const rawOut = await NativeAuthService.ETWIN_OAUTH(this.box, userId, userDisplayName);
    return $AuthContext.read(JSON_READER, rawOut);
  }

  async session(sessionId: string): Promise<AuthContext> {
    const rawOut = await NativeAuthService.SESSION(this.box, sessionId);
    return $AuthContext.read(JSON_READER, rawOut);
  }

  async createSession(_acx: AuthContext, userId: UserId): Promise<Session> {
    const rawOut = await NativeAuthService.CREATE_SESSION(this.box, userId);
    return $Session.read(JSON_READER, rawOut);
  }
}
