import {$AuthContext, AuthContext} from "@eternalfest/api-core/auth/auth-context";
import {$GameModeKey, GameModeKey} from "@eternalfest/api-core/game/game-mode-key";
import {$Leaderboard, Leaderboard} from "@eternalfest/api-core/leaderboard/leaderboard";
import {$CreateRunOptions, CreateRunOptions} from "@eternalfest/api-core/run/create-run-options";
import {$Run, Run} from "@eternalfest/api-core/run/run";
import {$RunItems, RunItems} from "@eternalfest/api-core/run/run-items";
import {$RunStart, RunStart} from "@eternalfest/api-core/run/run-start";
import {RunService} from "@eternalfest/api-core/run/service";
import {$SetRunResultOptions, SetRunResultOptions} from "@eternalfest/api-core/run/set-run-result-options";
import {$UuidHex, UuidHex} from "kryo/uuid-hex";
import {JSON_READER} from "kryo-json/json-reader";
import {JSON_WRITER} from "kryo-json/json-writer";
import {promisify} from "util";

import native from "../../native/index.js";
import {NativeBlobStore} from "../blob-store.js";
import {NativeClock} from "../clock.js";
import {NativeGameStore} from "../game-store.js";
import {NativeRunStore} from "../run-store.js";
import {NativeUserStore} from "../user-store.js";

declare const NativeRunServiceBox: unique symbol;

export interface NativeRunServiceOptions {
  blobStore: NativeBlobStore;
  clock: NativeClock;
  gameStore: NativeGameStore;
  runStore: NativeRunStore;
  userStore: NativeUserStore;
}

export class NativeRunService implements RunService {
  private static NEW = promisify(native.services.run.new);
  private static CREATE_RUN = promisify(native.services.run.createRun);
  private static GET_RUN = promisify(native.services.run.getRun);
  private static START_RUN = promisify(native.services.run.startRun);
  private static SET_RUN_RESULT = promisify(native.services.run.setRunResult);
  private static GET_GAME_USER_ITEMS_BY_ID = promisify(native.services.run.getGameUserItemsById);
  private static GET_LEADERBOARD = promisify(native.services.run.getLeaderboard);

  public readonly box: typeof NativeRunServiceBox;

  private constructor(box: typeof NativeRunServiceBox) {
    this.box = box;
  }

  public static async create(options: Readonly<NativeRunServiceOptions>): Promise<NativeRunService> {
    const box = await NativeRunService.NEW(options.blobStore.box, options.clock.box, options.gameStore.box, options.runStore.box, options.userStore.box);
    return new NativeRunService(box);
  }

  async createRun(acx: AuthContext, options: Readonly<CreateRunOptions>): Promise<Run> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawOptions: string = $CreateRunOptions.write(JSON_WRITER, options);
    const rawOut = await NativeRunService.CREATE_RUN(this.box, rawAcx, rawOptions);
    return $Run.read(JSON_READER, rawOut);
  }

  async getRunById(acx: AuthContext, runId: UuidHex): Promise<Run> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawRunId: string = $UuidHex.write(JSON_WRITER, runId);
    const rawOut = await NativeRunService.GET_RUN(this.box, rawAcx, rawRunId);
    return $Run.read(JSON_READER, rawOut);
  }

  async startRun(acx: AuthContext, runId: UuidHex): Promise<RunStart> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawRunId: string = $UuidHex.write(JSON_WRITER, runId);
    const rawOut = await NativeRunService.START_RUN(this.box, rawAcx, rawRunId);
    return $RunStart.read(JSON_READER, rawOut);
  }

  async setRunResult(acx: AuthContext, runId: UuidHex, result: SetRunResultOptions): Promise<Run> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawRunId: string = $UuidHex.write(JSON_WRITER, runId);
    const rawResult: string = $SetRunResultOptions.write(JSON_WRITER, result);
    const rawOut = await NativeRunService.SET_RUN_RESULT(this.box, rawAcx, rawRunId, rawResult);
    return $Run.read(JSON_READER, rawOut);
  }

  async getGameUserItemsById(acx: AuthContext, userId: UuidHex, gameId: UuidHex): Promise<RunItems> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawUserId: string = $UuidHex.write(JSON_WRITER, userId);
    const rawGameId: string = $UuidHex.write(JSON_WRITER, gameId);
    const rawOut = await NativeRunService.GET_GAME_USER_ITEMS_BY_ID(this.box, rawAcx, rawUserId, rawGameId);
    return $RunItems.read(JSON_READER, rawOut);
  }

  async getLeaderboard(acx: AuthContext, gameId: UuidHex, gameMode: GameModeKey): Promise<Leaderboard> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawGameId: string = $UuidHex.write(JSON_WRITER, gameId);
    const rawGameMode: string = $GameModeKey.write(JSON_WRITER, gameMode);
    const rawOut = await NativeRunService.GET_LEADERBOARD(this.box, rawAcx, rawGameId, rawGameMode);
    return $Leaderboard.read(JSON_READER, rawOut);
  }
}
