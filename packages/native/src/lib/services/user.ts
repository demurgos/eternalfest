import { $AuthContext, AuthContext } from "@eternalfest/api-core/auth/auth-context";
import { HfestIdentity } from "@eternalfest/api-core/hfest-identity/hfest-identity";
import {ObjectType} from "@eternalfest/api-core/types/object-type";
import { CreateUserWithCredentialsOptions } from "@eternalfest/api-core/user/create-user-with-credentials-options";
import { $GetUserOptions, GetUserOptions } from "@eternalfest/api-core/user/get-user-options";
import { $GetUsersOptions, GetUsersOptions } from "@eternalfest/api-core/user/get-users-options";
import { UserService } from "@eternalfest/api-core/user/service";
import { $ShortUser, ShortUser } from "@eternalfest/api-core/user/short-user";
import { $UpdateUserOptions, UpdateUserOptions } from "@eternalfest/api-core/user/update-user-options";
import { $User, User } from "@eternalfest/api-core/user/user";
import { UserDisplayName } from "@eternalfest/api-core/user/user-display-name";
import { $UserId, UserId } from "@eternalfest/api-core/user/user-id";
import { $UserListing, UserListing } from "@eternalfest/api-core/user/user-listing";
import { UserRef } from "@eternalfest/api-core/user/user-ref";
import { MapType } from "kryo/map";
import { JSON_READER } from "kryo-json/json-reader";
import { JSON_WRITER } from "kryo-json/json-writer";
import { promisify } from "util";

import native from "../../native/index.js";
import { NativeUserStore } from "../user-store.js";

declare const NativeUserServiceBox: unique symbol;

export interface NativeUserServiceOptions {
  userStore: NativeUserStore;
}

const $ShortUserMap = new MapType({keyType: $UserId, valueType: $ShortUser, assumeStringKey: true, maxSize: 10000});

export class NativeUserService implements UserService {
  private static NEW = promisify(native.services.user.new);
  private static UPSERT_FROM_ETWIN = promisify(native.services.user.upsertFromEtwin);
  private static GET_USER = promisify(native.services.user.getUser);
  private static GET_SHORT_USER = promisify(native.services.user.getShortUser);
  private static GET_SHORT_USERS = promisify(native.services.user.getShortUsers);
  private static GET_USERS = promisify(native.services.user.getUsers);
  private static UPDATE_USER = promisify(native.services.user.updateUser);

  public readonly box: typeof NativeUserServiceBox;

  private constructor(box: typeof NativeUserServiceBox) {
    this.box = box;
  }

  public static async create(options: Readonly<NativeUserServiceOptions>): Promise<NativeUserService> {
    const box = await NativeUserService.NEW(options.userStore.box);
    return new NativeUserService(box);
  }

  async upsertFromEtwin(acx: AuthContext, options: Readonly<ShortUser>): Promise<User> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawOptions: string = $ShortUser.write(JSON_WRITER, options);
    const rawOut = await NativeUserService.UPSERT_FROM_ETWIN(this.box, rawAcx, rawOptions);
    return $User.read(JSON_READER, rawOut);
  }

  async getUser(acx: AuthContext, options: Readonly<GetUserOptions>): Promise<User> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawOptions: string = $GetUserOptions.write(JSON_WRITER, options);
    const rawOut = await NativeUserService.GET_USER(this.box, rawAcx, rawOptions);
    return $User.read(JSON_READER, rawOut);
  }

  async getShortUser(acx: AuthContext, options: Readonly<GetUserOptions>): Promise<ShortUser> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawOptions: string = $GetUserOptions.write(JSON_WRITER, options);
    const rawOut = await NativeUserService.GET_SHORT_USER(this.box, rawAcx, rawOptions);
    return $ShortUser.read(JSON_READER, rawOut);
  }

  async getShortUsers(acx: AuthContext, options: Readonly<GetUsersOptions>): Promise<Map<UserId, ShortUser>> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawOptions: string = $GetUsersOptions.write(JSON_WRITER, options);
    const rawOut = await NativeUserService.GET_SHORT_USERS(this.box, rawAcx, rawOptions);
    return $ShortUserMap.read(JSON_READER, rawOut);
  }

  async getUsers(acx: AuthContext): Promise<UserListing> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawOut = await NativeUserService.GET_USERS(this.box, rawAcx);
    return $UserListing.read(JSON_READER, rawOut);
  }

  async updateUser(acx: AuthContext, options: Readonly<UpdateUserOptions>): Promise<User> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawOptions: string = $UpdateUserOptions.write(JSON_WRITER, options);
    const rawOut = await NativeUserService.UPDATE_USER(this.box, rawAcx, rawOptions);
    return $User.read(JSON_READER, rawOut);
  }

  async createUserWithCredentials(_acx: AuthContext, _options: CreateUserWithCredentialsOptions): Promise<User> {
    throw new Error("Method not implemented.");
  }

  async createUserWithHfestIdentity(_acx: AuthContext, _hfestIdentity: HfestIdentity): Promise<User> {
    throw new Error("Method not implemented.");
  }

  async getOrCreateUserWithEtwin(auth: AuthContext, userId: UserId, userDisplay: UserDisplayName): Promise<User> {
    return this.upsertFromEtwin(auth, {type: ObjectType.User, id: userId, displayName: userDisplay});
  }

  async getUserById(auth: AuthContext, userId: UserId): Promise<User | undefined> {
    // TODO: Remove global clock usage
    return this.getUser(auth, {id: userId, now: new Date()});
  }

  async getUserRefById(auth: AuthContext, userId: UserId): Promise<UserRef | undefined> {
    // TODO: Remove global clock usage
    return this.getShortUser(auth, {id: userId, now: new Date()});
  }

  async getUserRefsById(auth: AuthContext, userIds: ReadonlySet<UserId>): Promise<Map<string, UserRef | undefined>> {
    return this.getShortUsers(auth, {id: new Set([...userIds])});
  }
}
