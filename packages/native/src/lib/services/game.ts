import {$AuthContext, AuthContext} from "@eternalfest/api-core/auth/auth-context";
import {$ActiveGameChannel, ActiveGameChannel} from "@eternalfest/api-core/game2/active-game-channel";
import {
  $CreateGameBuildOptions,
  CreateGameBuildOptions
} from "@eternalfest/api-core/game2/create-game-build-options";
import {$CreateGameOptions, CreateGameOptions} from "@eternalfest/api-core/game2/create-game-options";
import {$Game, $NullableGame, Game, NullableGame} from "@eternalfest/api-core/game2/game";
import {$GameBuild, GameBuild} from "@eternalfest/api-core/game2/game-build";
import {$GetGameOptions, GetGameOptions} from "@eternalfest/api-core/game2/get-game-options";
import {$GetGamesOptions, GetGamesOptions} from "@eternalfest/api-core/game2/get-games-options";
import {Game2Service} from "@eternalfest/api-core/game2/service";
import {$SetGameFavoriteOptions, SetGameFavoriteOptions} from "@eternalfest/api-core/game2/set-game-favorite-options";
import {$ShortGameListing, ShortGameListing} from "@eternalfest/api-core/game2/short-game-listing";
import {
  $UpdateGameChannelOptions,
  UpdateGameChannelOptions
} from "@eternalfest/api-core/game2/update-game-channel-options";
import {JSON_READER} from "kryo-json/json-reader";
import {JSON_WRITER} from "kryo-json/json-writer";
import {promisify} from "util";

import native from "../../native/index.js";
import {NativeBlobStore} from "../blob-store.js";
import {NativeClock} from "../clock.js";
import {NativeGameStore} from "../game-store.js";
import {NativeUserStore} from "../user-store.js";

declare const NativeGameServiceBox: unique symbol;

export interface NativeGameServiceOptions {
  blobStore: NativeBlobStore;
  clock: NativeClock;
  gameStore: NativeGameStore;
  userStore: NativeUserStore;
}

export class NativeGameService implements Game2Service {
  private static NEW = promisify(native.services.game.new);
  private static CREATE_GAME = promisify(native.services.game.createGame);
  private static GET_GAME = promisify(native.services.game.getGame);
  private static GET_GAMES = promisify(native.services.game.getGames);
  private static CREATE_BUILD = promisify(native.services.game.createBuild);
  private static SET_GAME_FAVORITE = promisify(native.services.game.setGameFavorite);
  private static UPDATE_CHANNEL = promisify(native.services.game.updateChannel);

  public readonly box: typeof NativeGameServiceBox;

  private constructor(box: typeof NativeGameServiceBox) {
    this.box = box;
  }

  public static async create(options: Readonly<NativeGameServiceOptions>): Promise<NativeGameService> {
    const box = await NativeGameService.NEW(options.blobStore.box, options.clock.box, options.gameStore.box, options.userStore.box);
    return new NativeGameService(box);
  }

  async createGame(acx: AuthContext, options: Readonly<CreateGameOptions>): Promise<Game> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawOptions: string = $CreateGameOptions.write(JSON_WRITER, options);
    const rawOut = await NativeGameService.CREATE_GAME(this.box, rawAcx, rawOptions);
    return $Game.read(JSON_READER, rawOut);
  }

  async getGame(acx: AuthContext, options: Readonly<GetGameOptions>): Promise<NullableGame> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawOptions: string = $GetGameOptions.write(JSON_WRITER, options);
    const rawOut = await NativeGameService.GET_GAME(this.box, rawAcx, rawOptions);
    return $NullableGame.read(JSON_READER, rawOut);
  }

  async getGames(acx: AuthContext, options: Readonly<GetGamesOptions>): Promise<ShortGameListing> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawOptions: string = $GetGamesOptions.write(JSON_WRITER, options);
    const rawOut = await NativeGameService.GET_GAMES(this.box, rawAcx, rawOptions);
    return $ShortGameListing.read(JSON_READER, rawOut);
  }

  async createBuild(acx: AuthContext, options: Readonly<CreateGameBuildOptions>): Promise<GameBuild> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawOptions: string = $CreateGameBuildOptions.write(JSON_WRITER, options);
    const rawOut = await NativeGameService.CREATE_BUILD(this.box, rawAcx, rawOptions);
    return $GameBuild.read(JSON_READER, rawOut);
  }

  async setGameFavorite(acx: AuthContext, options: Readonly<SetGameFavoriteOptions>): Promise<unknown> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawOptions: string = $SetGameFavoriteOptions.write(JSON_WRITER, options);
    await NativeGameService.SET_GAME_FAVORITE(this.box, rawAcx, rawOptions);
    return {favorite: true};
  }

  async updateChannel(acx: AuthContext, options: Readonly<UpdateGameChannelOptions>): Promise<ActiveGameChannel> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawOptions: string = $UpdateGameChannelOptions.write(JSON_WRITER, options);
    const rawOut = await NativeGameService.UPDATE_CHANNEL(this.box, rawAcx, rawOptions);
    return $ActiveGameChannel.read(JSON_READER, rawOut);
  }
}
