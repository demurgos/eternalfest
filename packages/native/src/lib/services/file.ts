import { $AuthContext, AuthContext } from "@eternalfest/api-core/auth/auth-context";
import {
  $CreateDirectoryOptions,
  CreateDirectoryOptions
} from "@eternalfest/api-core/file/create-directory-options";
import { $CreateFileOptions, CreateFileOptions } from "@eternalfest/api-core/file/create-file-options";
import { $DeleteItemOptions, DeleteItemOptions } from "@eternalfest/api-core/file/delete-item-options";
import { $Directory, Directory } from "@eternalfest/api-core/file/directory";
import { $Drive, Drive } from "@eternalfest/api-core/file/drive";
import { $DriveItem, DriveItem } from "@eternalfest/api-core/file/drive-item";
import { $File, File } from "@eternalfest/api-core/file/file";
import { $GetDirectoryOptions, GetDirectoryOptions } from "@eternalfest/api-core/file/get-directory-options";
import {
  $GetDriveByOwnerOptions,
  GetDriveByOwnerOptions
} from "@eternalfest/api-core/file/get-drive-by-owner-options";
import { $GetDriveOptions, GetDriveOptions } from "@eternalfest/api-core/file/get-drive-options";
import { $GetFileOptions, GetFileOptions } from "@eternalfest/api-core/file/get-file-options";
import {
  $GetItemByPathOptions,
  GetItemByPathOptions
} from "@eternalfest/api-core/file/get-item-by-path-options";
import { FileService } from "@eternalfest/api-core/file/service";
import intoStream from "into-stream";
import { ArrayType } from "kryo/array";
import { UuidHex } from "kryo/uuid-hex";
import { JSON_READER } from "kryo-json/json-reader";
import { JSON_WRITER } from "kryo-json/json-writer";
import stream from "stream";
import { promisify } from "util";

import native from "../../native/index.js";
import { NativeBlobStore } from "../blob-store.js";
import { NativeFileStore } from "../file-store.js";
import { NativeUserStore } from "../user-store.js";

declare const NativeFileServiceBox: unique symbol;

export interface NativeFileServiceOptions {
  blobStore: NativeBlobStore;
  fileStore: NativeFileStore;
  userStore: NativeUserStore;
}

const $DriveItemList = new ArrayType({itemType: $DriveItem, maxLength: 100});

export class NativeFileService implements FileService {
  private static NEW = promisify(native.services.file.new);
  private static GET_DRIVE = promisify(native.services.file.getDrive);
  private static GET_DRIVE_BY_OWNER = promisify(native.services.file.getDriveByOwner);
  private static GET_ITEM_BY_PATH = promisify(native.services.file.getItemByPath);
  private static CREATE_DIRECTORY = promisify(native.services.file.createDirectory);
  private static GET_DIRECTORY = promisify(native.services.file.getDirectory);
  private static GET_DIRECTORY_CHILDREN = promisify(native.services.file.getDirectoryChildren);
  private static CREATE_FILE = promisify(native.services.file.createFile);
  private static GET_FILE = promisify(native.services.file.getFile);
  private static GET_FILE_DATA = promisify(native.services.file.getFileData);
  private static DELETE_ITEM = promisify(native.services.file.deleteItem);

  public readonly box: typeof NativeFileServiceBox;

  private constructor(box: typeof NativeFileServiceBox) {
    this.box = box;
  }

  public static async create(options: Readonly<NativeFileServiceOptions>): Promise<NativeFileService> {
    const box = await NativeFileService.NEW(options.blobStore.box, options.fileStore.box, options.userStore.box);
    return new NativeFileService(box);
  }

  async getDrive(acx: AuthContext, options: Readonly<GetDriveOptions>): Promise<Drive> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawOptions: string = $GetDriveOptions.write(JSON_WRITER, options);
    const rawOut = await NativeFileService.GET_DRIVE(this.box, rawAcx, rawOptions);
    return $Drive.read(JSON_READER, rawOut);
  }

  async getDriveByOwner(acx: AuthContext, options: Readonly<GetDriveByOwnerOptions>): Promise<Drive> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawOptions: string = $GetDriveByOwnerOptions.write(JSON_WRITER, options);
    let rawOut;
    try {
      rawOut = await NativeFileService.GET_DRIVE_BY_OWNER(this.box, rawAcx, rawOptions);
    } catch (e) {
      if (e instanceof Error && e.message === "Unauthorized") {
        e.name = "Unauthorized";
      }
      throw e;
    }
    return $Drive.read(JSON_READER, rawOut);
  }

  async getItemByPath(acx: AuthContext, options: Readonly<GetItemByPathOptions>): Promise<DriveItem> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawOptions: string = $GetItemByPathOptions.write(JSON_WRITER, options);
    const rawOut = await NativeFileService.GET_ITEM_BY_PATH(this.box, rawAcx, rawOptions);
    return $DriveItem.read(JSON_READER, rawOut);
  }

  async createDirectory(acx: AuthContext, options: Readonly<CreateDirectoryOptions>): Promise<Directory> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawOptions: string = $CreateDirectoryOptions.write(JSON_WRITER, options);
    let rawOut;
    try {
      rawOut = await NativeFileService.CREATE_DIRECTORY(this.box, rawAcx, rawOptions);
    } catch (e) {
      if (e instanceof Error && e.message === "Unauthorized") {
        e.name = "Unauthorized";
      }
      throw e;
    }
    return $Directory.read(JSON_READER, rawOut);
  }

  async getDirectory(acx: AuthContext, options: Readonly<GetDirectoryOptions>): Promise<Directory> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawOptions: string = $GetDirectoryOptions.write(JSON_WRITER, options);
    const rawOut = await NativeFileService.GET_DIRECTORY(this.box, rawAcx, rawOptions);
    return $Directory.read(JSON_READER, rawOut);
  }

  async getDirectoryChildren(acx: AuthContext, options: Readonly<GetDirectoryOptions>): Promise<DriveItem[]> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawOptions: string = $GetDriveOptions.write(JSON_WRITER, options);
    const rawOut = await NativeFileService.GET_DIRECTORY_CHILDREN(this.box, rawAcx, rawOptions);
    return $DriveItemList.read(JSON_READER, rawOut);
  }

  async createFile(acx: AuthContext, options: Readonly<CreateFileOptions>): Promise<File> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawOptions: string = $CreateFileOptions.write(JSON_WRITER, options);
    let rawOut;
    try {
      rawOut = await NativeFileService.CREATE_FILE(this.box, rawAcx, rawOptions);
    } catch (e) {
      if (e instanceof  Error && e.message === "Unauthorized") {
        e.name = "Unauthorized";
      }
      throw e;
    }
    return $File.read(JSON_READER, rawOut);
  }

  async getFile(acx: AuthContext, options: Readonly<GetFileOptions>): Promise<File> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawOptions: string = $GetFileOptions.write(JSON_WRITER, options);
    const rawOut = await NativeFileService.GET_FILE(this.box, rawAcx, rawOptions);
    return $File.read(JSON_READER, rawOut);
  }

  async getFileData(acx: AuthContext, options: Readonly<GetFileOptions>): Promise<stream.Readable> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawOptions: string = $GetFileOptions.write(JSON_WRITER, options);
    const rawOut: Uint8Array = await NativeFileService.GET_FILE_DATA(this.box, rawAcx, rawOptions);
    return intoStream(rawOut);
  }

  async deleteItem(acx: AuthContext, options: Readonly<DeleteItemOptions>): Promise<void> {
    const rawAcx: string = $AuthContext.write(JSON_WRITER, acx);
    const rawOptions: string = $DeleteItemOptions.write(JSON_WRITER, options);
    await NativeFileService.DELETE_ITEM(this.box, rawAcx, rawOptions);
  }

  getDirectoryById(auth: AuthContext, directoryId: UuidHex): Promise<Directory> {
    return this.getDirectory(auth, {id: directoryId});
  }

  getDirectoryChildrenById(auth: AuthContext, directoryId: UuidHex): Promise<ReadonlyArray<DriveItem>> {
    return this.getDirectoryChildren(auth, {id: directoryId});
  }

  getDriveById(auth: AuthContext, driveId: UuidHex): Promise<Drive> {
    return this.getDrive(auth, {id: driveId});
  }

  getDriveByOwnerId(auth: AuthContext, ownerId: UuidHex): Promise<Drive> {
    return this.getDriveByOwner(auth, {id: ownerId});
  }

  getFileById(auth: AuthContext, fileId: UuidHex): Promise<File> {
    return this.getFile(auth, {id: fileId});
  }

  readFileContent(auth: AuthContext, fileId: UuidHex): Promise<stream.Readable> {
    return this.getFileData(auth, {id: fileId});
  }
}
