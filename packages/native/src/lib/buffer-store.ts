import { Url } from "@eternalfest/api-core/types/url";
import { fileURLToPath } from "url";
import { promisify } from "util";

import native from "../native/index.js";
import { NativeUuidGenerator } from "./uuid.js";

declare const FsBufferStoreBox: unique symbol;
export type NativeBufferStoreBox = typeof FsBufferStoreBox;

export abstract class NativeBufferStore {
  public readonly box: NativeBufferStoreBox;

  constructor(box: NativeBufferStoreBox) {
    this.box = box;
  }
}

export interface FsBufferStoreOptions {
  uuidGenerator: NativeUuidGenerator;
  root: Url;
}

export class FsBufferStore extends NativeBufferStore {
  private static NEW = promisify(native.bufferStore.fs.new);

  private constructor(box: typeof FsBufferStoreBox) {
    super(box);
  }

  public static async create(options: Readonly<FsBufferStoreOptions>): Promise<FsBufferStore> {
    const root = fileURLToPath(options.root.toString());
    const box = await FsBufferStore.NEW(options.uuidGenerator.box, JSON.stringify(root));
    return new FsBufferStore(box);
  }
}
