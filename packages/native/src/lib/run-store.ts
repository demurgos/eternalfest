import {promisify} from "util";

import native from "../native/index.js";
import {NativeClock} from "./clock.js";
import {Database} from "./database.js";
import {NativeUuidGenerator} from "./uuid.js";

declare const PgRunStoreBox: unique symbol;
export type NativeRunStoreBox = typeof PgRunStoreBox;

export abstract class NativeRunStore {
  public readonly box: NativeRunStoreBox;

  constructor(box: NativeRunStoreBox) {
    this.box = box;
  }
}

export interface PgRunStoreOptions {
  clock: NativeClock;
  database: Database;
  uuidGenerator: NativeUuidGenerator;
}

export class PgRunStore extends NativeRunStore {
  private static NEW = promisify(native.runStore.pg.new);

  private constructor(box: typeof PgRunStoreBox) {
    super(box);
  }

  public static async create(options: Readonly<PgRunStoreOptions>): Promise<PgRunStore> {
    const box = await PgRunStore.NEW(options.clock.box, options.database.box, options.uuidGenerator.box);
    return new PgRunStore(box);
  }
}
