import { AuthContext } from "@eternalfest/api-core/auth/auth-context";
import { $Blob, Blob } from "@eternalfest/api-core/blob/blob";
import { $CreateBlobOptions, CreateBlobOptions } from "@eternalfest/api-core/blob/create-blob-options";
import {
  $CreateUploadSessionOptions,
  CreateUploadSessionOptions
} from "@eternalfest/api-core/blob/create-upload-session-options";
import { $GetBlobOptions, GetBlobOptions } from "@eternalfest/api-core/blob/get-blob-options";
import { BlobService } from "@eternalfest/api-core/blob/service";
import { $UploadOptions, UploadOptions } from "@eternalfest/api-core/blob/upload-options";
import { $UploadSession, UploadSession } from "@eternalfest/api-core/blob/upload-session";
import intoStream from "into-stream";
import { JSON_READER } from "kryo-json/json-reader";
import { JSON_WRITER } from "kryo-json/json-writer";
import stream from "stream";
import { promisify } from "util";

import native from "../native/index.js";
import { NativeBufferStore } from "./buffer-store.js";
import { NativeClock } from "./clock.js";
import { Database } from "./database.js";
import { NativeUuidGenerator } from "./uuid.js";

declare const PgBlobStoreBox: unique symbol;
export type NativeBlobStoreBox = typeof PgBlobStoreBox;

export abstract class NativeBlobStore implements BlobService {
  public readonly box: NativeBlobStoreBox;
  private static HAS_IMMUTABLE_BLOBS = native.blobStore.hasImmutableBlobs;
  private static CREATE_BLOB = promisify(native.blobStore.createBlob);
  private static GET_BLOB = promisify(native.blobStore.getBlob);
  private static GET_BLOB_DATA = promisify(native.blobStore.getBlobData);
  private static CREATE_UPLOAD_SESSION = promisify(native.blobStore.createUploadSession);
  private static UPLOAD = promisify(native.blobStore.upload);

  public readonly hasImmutableBlobs: boolean;

  constructor(box: NativeBlobStoreBox) {
    this.box = box;

    const rawImmutableBlobs = NativeBlobStore.HAS_IMMUTABLE_BLOBS(this.box);
    if (typeof rawImmutableBlobs !== "boolean") {
      throw new Error("InvalidReturnVal: expected boolean, got " + rawImmutableBlobs);
    }
    this.hasImmutableBlobs = rawImmutableBlobs;
  }

  async createBlob(_acx: AuthContext, options: Readonly<CreateBlobOptions>): Promise<Blob> {
    const rawOptions: string = $CreateBlobOptions.write(JSON_WRITER, options);
    const rawOut = await NativeBlobStore.CREATE_BLOB(this.box, rawOptions);
    return $Blob.read(JSON_READER, rawOut);
  }

  async getBlobById(_acx: AuthContext, options: Readonly<GetBlobOptions>): Promise<Blob> {
    const rawOptions: string = $GetBlobOptions.write(JSON_WRITER, options);
    const rawOut = await NativeBlobStore.GET_BLOB(this.box, rawOptions);
    return $Blob.read(JSON_READER, rawOut);
  }

  async readBlobData(_acx: AuthContext, options: Readonly<GetBlobOptions>): Promise<stream.Readable> {
    const rawOptions: string = $GetBlobOptions.write(JSON_WRITER, options);
    const rawOut: Uint8Array = await NativeBlobStore.GET_BLOB_DATA(this.box, rawOptions);
    return intoStream(rawOut);
  }

  async createUploadSession(_acx: AuthContext, options: Readonly<CreateUploadSessionOptions>): Promise<UploadSession> {
    const rawOptions: string = $CreateUploadSessionOptions.write(JSON_WRITER, options);
    const rawOut = await NativeBlobStore.CREATE_UPLOAD_SESSION(this.box, rawOptions);
    return $UploadSession.read(JSON_READER, rawOut);
  }

  async uploadBytes(_acx: AuthContext, options: Readonly<UploadOptions>): Promise<UploadSession> {
    const rawOptions: string = $UploadOptions.write(JSON_WRITER, options);
    const rawOut = await NativeBlobStore.UPLOAD(this.box, rawOptions);
    return $UploadSession.read(JSON_READER, rawOut);
  }
}

export interface PgBlobStoreOptions {
  bufferStore: NativeBufferStore;
  clock: NativeClock;
  database: Database;
  uuidGenerator: NativeUuidGenerator;
}

export class PgBlobStore extends NativeBlobStore {
  private static NEW = native.blobStore.pg.new;

  private constructor(box: typeof PgBlobStoreBox) {
    super(box);
  }

  public static async create(options: Readonly<PgBlobStoreOptions>): Promise<PgBlobStore> {
    const box = PgBlobStore.NEW(options.bufferStore.box, options.clock.box, options.database.box, options.uuidGenerator.box);
    return new PgBlobStore(box);
  }
}
