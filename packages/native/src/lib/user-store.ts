import native from "../native/index.js";
import { NativeClock } from "./clock.js";
import { Database } from "./database.js";

declare const PgUserStoreBox: unique symbol;
export type NativeUserStoreBox = typeof PgUserStoreBox;

export abstract class NativeUserStore {
  public readonly box: NativeUserStoreBox;

  constructor(box: NativeUserStoreBox) {
    this.box = box;
  }
}

export interface PgUserStoreOptions {
  clock: NativeClock;
  database: Database;
}

export class PgUserStore extends NativeUserStore {
  private static NEW = native.userStore.pg.new;

  private constructor(box: typeof PgUserStoreBox) {
    super(box);
  }

  public static async create(options: Readonly<PgUserStoreOptions>): Promise<PgUserStore> {
    const box = PgUserStore.NEW(options.clock.box, options.database.box);
    return new PgUserStore(box);
  }
}
