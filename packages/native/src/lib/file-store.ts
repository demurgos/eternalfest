import native from "../native/index.js";
import { NativeBlobStore } from "./blob-store.js";
import { NativeClock } from "./clock.js";
import { Database } from "./database.js";
import { NativeUuidGenerator } from "./uuid.js";

declare const PgFileStoreBox: unique symbol;
export type NativeFileStoreBox = typeof PgFileStoreBox;

export abstract class NativeFileStore {
  public readonly box: NativeFileStoreBox;

  constructor(box: NativeFileStoreBox) {
    this.box = box;
  }
}

export interface PgFileStoreOptions {
  blobStore: NativeBlobStore;
  clock: NativeClock;
  database: Database;
  uuidGenerator: NativeUuidGenerator;
}

export class PgFileStore extends NativeFileStore {
  private static NEW = native.fileStore.pg.new;

  private constructor(box: typeof PgFileStoreBox) {
    super(box);
  }

  public static async create(options: Readonly<PgFileStoreOptions>): Promise<PgFileStore> {
    const box = PgFileStore.NEW(options.blobStore.box, options.clock.box, options.database.box, options.uuidGenerator.box);
    return new PgFileStore(box);
  }
}
