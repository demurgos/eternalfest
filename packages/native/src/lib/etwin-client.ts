import { $AuthContext as $EtwinAuthContext,AuthContext as EtwinAuthContext } from "@eternal-twin/core/auth/auth-context";
import { EtwinClientService } from "@eternal-twin/core/etwin-client/service";
import { $ShortUser,ShortUser } from "@eternal-twin/core/user/short-user";
import { JSON_READER } from "kryo-json/json-reader";
import { promisify } from "util";

import native from "../native/index.js";
import { NativeClock } from "./clock.js";

declare const HttpEtwinClientBox: unique symbol;
export type NativeEtwinClientBox = typeof HttpEtwinClientBox;

export abstract class NativeEtwinClient implements EtwinClientService {
  public readonly box: NativeEtwinClientBox;
  private static GET_AUTH_SELF = promisify(native.etwinClient.getAuthSelf);
  private static GET_USER_BY_ID = promisify(native.etwinClient.getUserById);

  constructor(box: NativeEtwinClientBox) {
    this.box = box;
  }

  async getAuthSelf(accessToken: string): Promise<EtwinAuthContext> {
    const rawOut = await NativeEtwinClient.GET_AUTH_SELF(this.box, accessToken);
    return $EtwinAuthContext.read(JSON_READER, rawOut);
  }

  async getUserById(accessToken: string | null, userId: string): Promise<ShortUser> {
    const rawAcx: string = JSON.stringify(accessToken);
    const rawOut = await NativeEtwinClient.GET_USER_BY_ID(this.box, rawAcx, JSON.stringify(userId));
    return $ShortUser.read(JSON_READER, rawOut);
  }
}

export interface PgFileStoreOptions {
  clock: NativeClock;
  root: string;
}

export class HttpEtwinClient extends NativeEtwinClient {
  private static NEW = promisify(native.etwinClient.http.new);

  private constructor(box: typeof HttpEtwinClientBox) {
    super(box);
  }

  public static async create(options: Readonly<PgFileStoreOptions>): Promise<HttpEtwinClient> {
    const box = await HttpEtwinClient.NEW(options.clock.box, options.root);
    return new HttpEtwinClient(box);
  }
}
