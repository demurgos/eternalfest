import {promisify} from "util";

import native from "../native/index.js";
import {NativeClock} from "./clock.js";
import {Database} from "./database.js";
import {NativeUuidGenerator} from "./uuid.js";

declare const PgGameStoreBox: unique symbol;
export type NativeGameStoreBox = typeof PgGameStoreBox;

export abstract class NativeGameStore {
  public readonly box: NativeGameStoreBox;

  constructor(box: NativeGameStoreBox) {
    this.box = box;
  }
}

export interface PgGameStoreOptions {
  clock: NativeClock;
  database: Database;
  uuidGenerator: NativeUuidGenerator;
}

export class PgGameStore extends NativeGameStore {
  private static NEW = promisify(native.gameStore.pg.new);

  private constructor(box: typeof PgGameStoreBox) {
    super(box);
  }

  public static async create(options: Readonly<PgGameStoreOptions>): Promise<PgGameStore> {
    const box = await PgGameStore.NEW(options.clock.box, options.database.box, options.uuidGenerator.box);
    return new PgGameStore(box);
  }
}
