import { Url } from "@eternalfest/api-core/types/url";
import { promisify } from "util";

import native from "../native/index.js";

export interface Config {
  eternalfest: EternalfestConfig;
  db: DbConfig;
  data: DataConfig;
  etwin: EtwinConfig;
}

export interface EternalfestConfig {
  /**
   * Internal HTTP port
   */
  httpPort: number;

  /**
   * Public URI of the server
   */
  externalUri: Url;

  /**
   * Secret key used to encrypt sensitive DB columns (password hashes, emails) or sign JWTs.
   */
  secret: string;

  /**
   * Secret key used for session cookies.
   */
  cookieKey: string;
}

export interface DbConfig {
  host: string;
  port: number;
  name: string;
  adminUser: string;
  adminPassword: string;
  user: string;
  password: string;
}

export interface DataConfig {
  root: Url;
}

export interface EtwinConfig {
  uri: Url;
  oauthClientId: string;
  oauthClientSecret: string;
}

const GET_LOCAL_CONFIG = promisify(native.config.getLocalConfig);

export async function getLocalConfig(): Promise<Config> {
  const rawConfigJson: unknown = await GET_LOCAL_CONFIG();
  if (typeof  rawConfigJson !== "string") {
    throw new Error("AssertionError: expected rawConfigJson to be a string");
  }
  const rawConfig: any = JSON.parse(rawConfigJson);
  return {
    eternalfest: {
      httpPort: rawConfig.eternalfest.http_port,
      externalUri: rawConfig.eternalfest.external_uri,
      secret: rawConfig.eternalfest.secret,
      cookieKey: rawConfig.eternalfest.cookie_key,
    },
    db: {
      host: rawConfig.db.host,
      port: rawConfig.db.port,
      name: rawConfig.db.name,
      adminUser: rawConfig.db.admin_user,
      adminPassword: rawConfig.db.admin_password,
      user: rawConfig.db.user,
      password: rawConfig.db.password,
    },
    data: {
      root: new Url(rawConfig.data.root),
    },
    etwin: {
      uri: rawConfig.etwin.uri,
      oauthClientId: rawConfig.etwin.oauth_client_id,
      oauthClientSecret: rawConfig.etwin.oauth_client_secret,
    }
  };
}
