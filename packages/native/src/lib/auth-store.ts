import { promisify } from "util";

import native from "../native/index.js";
import { NativeClock } from "./clock.js";
import { Database } from "./database.js";
import { NativeUuidGenerator } from "./uuid.js";

declare const PgAuthStoreBox: unique symbol;
export type NativeAuthStoreBox = typeof PgAuthStoreBox;

export abstract class NativeAuthStore {
  public readonly box: NativeAuthStoreBox;

  constructor(box: NativeAuthStoreBox) {
    this.box = box;
  }
}

export interface PgAuthStoreOptions {
  clock: NativeClock;
  database: Database;
  uuidGenerator: NativeUuidGenerator;
}

export class PgAuthStore extends NativeAuthStore {
  private static NEW = promisify(native.authStore.pg.new);

  private constructor(box: typeof PgAuthStoreBox) {
    super(box);
  }

  public static async create(options: Readonly<PgAuthStoreOptions>): Promise<PgAuthStore> {
    const box = await PgAuthStore.NEW(options.clock.box, options.database.box, options.uuidGenerator.box);
    return new PgAuthStore(box);
  }
}
