use crate::neon_helpers::NeonNamespace;
use crate::run_store::pg::JsPgRunStore;
use eternalfest_core::run::RunStore;
use neon::prelude::*;
use std::sync::Arc;

pub fn create_namespace<'a, C: Context<'a>>(cx: &mut C) -> JsResult<'a, JsObject> {
  let ns = cx.empty_object();
  ns.set_with(cx, "pg", pg::create_namespace)?;
  Ok(ns)
}

pub fn get_native_run_store<'a, C: Context<'a>>(cx: &mut C, value: Handle<JsValue>) -> NeonResult<Arc<dyn RunStore>> {
  match value.downcast::<JsPgRunStore, _>(cx) {
    Ok(val) => {
      let val = Arc::clone(&**val);
      Ok(val)
    }
    Err(_) => cx.throw_type_error::<_, Arc<dyn RunStore>>("JsPgRunStore"),
  }
}

pub mod pg {
  use crate::clock::get_native_clock;
  use crate::database::JsPgPool;
  use crate::neon_helpers::{resolve_callback_with, NeonNamespace};
  use crate::uuid::get_native_uuid_generator;
  use eternalfest_core::clock::Clock;
  use eternalfest_core::uuid::UuidGenerator;
  use eternalfest_run_store::pg::PgRunStore;
  use neon::prelude::*;
  use sqlx::PgPool;
  use std::sync::Arc;

  pub fn create_namespace<'a, C: Context<'a>>(cx: &mut C) -> JsResult<'a, JsObject> {
    let ns = cx.empty_object();
    ns.set_function(cx, "new", new)?;
    Ok(ns)
  }

  pub type DynPgRunStore = PgRunStore<Arc<dyn Clock>, Arc<PgPool>, Arc<dyn UuidGenerator>>;
  pub type JsPgRunStore = JsBox<Arc<DynPgRunStore>>;

  pub fn new(mut cx: FunctionContext) -> JsResult<JsUndefined> {
    let clock = cx.argument::<JsValue>(0)?;
    let database = cx.argument::<JsPgPool>(1)?;
    let uuid_generator = cx.argument::<JsValue>(2)?;
    let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

    let clock: Arc<dyn Clock> = get_native_clock(&mut cx, clock)?;
    let database = Arc::new(PgPool::clone(&database));
    let uuid_generator: Arc<dyn UuidGenerator> = get_native_uuid_generator(&mut cx, uuid_generator)?;

    let res = async move {
      #[allow(clippy::type_complexity)]
      let inner: Arc<PgRunStore<Arc<dyn Clock>, Arc<PgPool>, Arc<dyn UuidGenerator>>> =
        Arc::new(PgRunStore::new(clock, database, uuid_generator).await);
      inner
    };

    resolve_callback_with(&mut cx, res, cb, |c: &mut TaskContext, res| Ok(c.boxed(res).upcast()))
  }
}
