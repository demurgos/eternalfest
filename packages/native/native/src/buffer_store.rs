use crate::buffer_store::fs::JsFsBufferStore;
use crate::neon_helpers::NeonNamespace;
use eternalfest_core::buffer::BufferStore;
use neon::prelude::*;
use std::sync::Arc;

pub fn create_namespace<'a, C: Context<'a>>(cx: &mut C) -> JsResult<'a, JsObject> {
  let ns = cx.empty_object();
  ns.set_with(cx, "fs", fs::create_namespace)?;
  Ok(ns)
}

pub fn get_native_buffer_store<'a, C: Context<'a>>(
  cx: &mut C,
  value: Handle<JsValue>,
) -> NeonResult<Arc<dyn BufferStore>> {
  match value.downcast::<JsFsBufferStore, _>(cx) {
    Ok(val) => {
      let val = Arc::clone(&**val);
      Ok(val)
    }
    Err(_) => cx.throw_type_error::<_, Arc<dyn BufferStore>>("JsFsBufferStore"),
  }
}

pub mod fs {
  use crate::neon_helpers::{resolve_callback_with, NeonNamespace};
  use crate::uuid::get_native_uuid_generator;
  use eternalfest_buffer_store::fs::FsBufferStore;
  use eternalfest_core::uuid::UuidGenerator;
  use neon::prelude::*;
  use std::path::PathBuf;
  use std::sync::Arc;

  pub fn create_namespace<'a, C: Context<'a>>(cx: &mut C) -> JsResult<'a, JsObject> {
    let ns = cx.empty_object();
    ns.set_function(cx, "new", new)?;
    Ok(ns)
  }

  pub type DynFsBufferStore = FsBufferStore<Arc<dyn UuidGenerator>>;
  pub type JsFsBufferStore = JsBox<Arc<DynFsBufferStore>>;

  pub fn new(mut cx: FunctionContext) -> JsResult<JsUndefined> {
    let uuid_generator = cx.argument::<JsValue>(0)?;
    let root = cx.argument::<JsString>(1)?;
    let cb = cx.argument::<JsFunction>(2)?.root(&mut cx);

    let root: PathBuf = serde_json::from_str(&root.value(&mut cx)).unwrap();

    let uuid_generator: Arc<dyn UuidGenerator> = get_native_uuid_generator(&mut cx, uuid_generator)?;
    let res = async move {
      let buffer_store = FsBufferStore::new(uuid_generator, root).await;
      let inner: Arc<DynFsBufferStore> = Arc::new(buffer_store);
      inner
    };

    resolve_callback_with(&mut cx, res, cb, |c: &mut TaskContext, res| Ok(c.boxed(res).upcast()))
  }
}
