use self::pg::JsPgBlobStore;
use crate::neon_helpers::{get_full_error_message, resolve_callback_serde, resolve_callback_with, NeonNamespace};
use eternalfest_core::blob::{
  BlobStore, CreateBlobOptions, CreateUploadSessionOptions, GetBlobDataOptions, GetBlobOptions, UploadOptions,
};
use eternalfest_core::types::{to_any_error, AnyError};
use neon::prelude::*;
use neon::types::buffer::TypedArray;
use std::sync::Arc;

pub fn create_namespace<'a, C: Context<'a>>(cx: &mut C) -> JsResult<'a, JsObject> {
  let ns = cx.empty_object();
  ns.set_with(cx, "pg", pg::create_namespace)?;
  ns.set_function(cx, "hasImmutableBlobs", has_immutable_blobs)?;
  ns.set_function(cx, "createBlob", create_blob)?;
  ns.set_function(cx, "getBlob", get_blob)?;
  ns.set_function(cx, "getBlobData", get_blob_data)?;
  ns.set_function(cx, "createUploadSession", create_upload_session)?;
  ns.set_function(cx, "upload", upload)?;
  Ok(ns)
}

pub fn get_native_blob_store<'a, C: Context<'a>>(cx: &mut C, value: Handle<JsValue>) -> NeonResult<Arc<dyn BlobStore>> {
  match value.downcast::<JsPgBlobStore, _>(cx) {
    Ok(val) => {
      let val = Arc::clone(&**val);
      Ok(val)
    }
    Err(_) => cx.throw_type_error::<_, Arc<dyn BlobStore>>("JsPgBlobStore"),
  }
}

pub fn has_immutable_blobs(mut cx: FunctionContext) -> JsResult<JsBoolean> {
  let inner = cx.argument::<JsValue>(0)?;
  let inner = get_native_blob_store(&mut cx, inner)?;
  let result = inner.has_immutable_blobs();
  Ok(cx.boolean(result))
}

pub fn create_blob(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let inner = cx.argument::<JsValue>(0)?;
  let inner = get_native_blob_store(&mut cx, inner)?;
  let options_json = cx.argument::<JsString>(1)?;
  let cb = cx.argument::<JsFunction>(2)?.root(&mut cx);

  let options: CreateBlobOptions = serde_json::from_str(&options_json.value(&mut cx)).unwrap();

  let res = async move { inner.create_blob(&options).await.map_err(to_any_error) };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn get_blob(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let inner = cx.argument::<JsValue>(0)?;
  let inner = get_native_blob_store(&mut cx, inner)?;
  let options_json = cx.argument::<JsString>(1)?;
  let cb = cx.argument::<JsFunction>(2)?.root(&mut cx);

  let options: GetBlobOptions = serde_json::from_str(&options_json.value(&mut cx)).unwrap();

  let res = async move { inner.get_blob(&options).await.map_err(to_any_error) };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn get_blob_data(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let inner = cx.argument::<JsValue>(0)?;
  let inner = get_native_blob_store(&mut cx, inner)?;
  let options_json = cx.argument::<JsString>(1)?;
  let cb = cx.argument::<JsFunction>(2)?.root(&mut cx);

  let options: GetBlobDataOptions = serde_json::from_str(&options_json.value(&mut cx)).unwrap();

  let res = async move { inner.get_blob_data(&options).await.map_err(to_any_error) };

  resolve_callback_with(
    &mut cx,
    res,
    cb,
    |cx: &mut TaskContext, res: Result<Vec<u8>, AnyError>| {
      let bytes: Vec<u8> = match res {
        Ok(bytes) => bytes,
        Err(e) => {
          let e = get_full_error_message(e.as_ref());
          let e = JsError::error(cx, &e).expect("FailedToAllocateJsError");
          return Err(e.upcast());
        }
      };
      let buffer_len: usize = bytes.len();
      let mut js_buffer = cx.buffer(buffer_len).expect("Failed to allocate");
      {
        let js_buffer: &mut [u8] = js_buffer.as_mut_slice(&mut *cx);
        js_buffer.copy_from_slice(bytes.as_slice());
      }
      Ok(js_buffer.upcast())
    },
  )
}

pub fn create_upload_session(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let inner = cx.argument::<JsValue>(0)?;
  let inner = get_native_blob_store(&mut cx, inner)?;
  let options_json = cx.argument::<JsString>(1)?;
  let cb = cx.argument::<JsFunction>(2)?.root(&mut cx);

  let options: CreateUploadSessionOptions = serde_json::from_str(&options_json.value(&mut cx)).unwrap();

  let res = async move { inner.create_upload_session(&options).await.map_err(to_any_error) };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn upload(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let inner = cx.argument::<JsValue>(0)?;
  let inner = get_native_blob_store(&mut cx, inner)?;
  let options_json = cx.argument::<JsString>(1)?;
  let cb = cx.argument::<JsFunction>(2)?.root(&mut cx);

  let options: UploadOptions = serde_json::from_str(&options_json.value(&mut cx)).unwrap();

  let res = async move { inner.upload(&options).await.map_err(to_any_error) };
  resolve_callback_serde(&mut cx, res, cb)
}

pub mod pg {
  use crate::buffer_store::get_native_buffer_store;
  use crate::clock::get_native_clock;
  use crate::database::JsPgPool;
  use crate::neon_helpers::NeonNamespace;
  use crate::uuid::get_native_uuid_generator;
  use eternalfest_blob_store::pg::PgBlobStore;
  use eternalfest_core::buffer::BufferStore;
  use eternalfest_core::clock::Clock;
  use eternalfest_core::uuid::UuidGenerator;
  use neon::prelude::*;
  use sqlx::PgPool;
  use std::sync::Arc;

  pub fn create_namespace<'a, C: Context<'a>>(cx: &mut C) -> JsResult<'a, JsObject> {
    let ns = cx.empty_object();
    ns.set_function(cx, "new", new)?;
    Ok(ns)
  }

  pub type DynPgBlobStore = PgBlobStore<Arc<dyn BufferStore>, Arc<dyn Clock>, Arc<PgPool>, Arc<dyn UuidGenerator>>;
  pub type JsPgBlobStore = JsBox<Arc<DynPgBlobStore>>;

  pub fn new(mut cx: FunctionContext) -> JsResult<JsPgBlobStore> {
    let buffer_store = cx.argument::<JsValue>(0)?;
    let clock = cx.argument::<JsValue>(1)?;
    let database = cx.argument::<JsPgPool>(2)?;
    let uuid_generator = cx.argument::<JsValue>(3)?;

    let buffer_store: Arc<dyn BufferStore> = get_native_buffer_store(&mut cx, buffer_store)?;
    let clock: Arc<dyn Clock> = get_native_clock(&mut cx, clock)?;
    let database = Arc::new(PgPool::clone(&database));
    let uuid_generator: Arc<dyn UuidGenerator> = get_native_uuid_generator(&mut cx, uuid_generator)?;
    let blob_service = PgBlobStore::new(buffer_store, clock, database, uuid_generator);
    let inner: Arc<DynPgBlobStore> = Arc::new(blob_service);
    Ok(cx.boxed(inner))
  }
}
