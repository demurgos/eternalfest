use crate::neon_helpers::NeonNamespace;
use crate::user_store::pg::JsPgUserStore;
use eternalfest_core::user::UserStore;
use neon::prelude::*;
use std::sync::Arc;

pub fn create_namespace<'a, C: Context<'a>>(cx: &mut C) -> JsResult<'a, JsObject> {
  let ns = cx.empty_object();
  ns.set_with(cx, "pg", pg::create_namespace)?;
  Ok(ns)
}

pub fn get_native_user_store<'a, C: Context<'a>>(cx: &mut C, value: Handle<JsValue>) -> NeonResult<Arc<dyn UserStore>> {
  match value.downcast::<JsPgUserStore, _>(cx) {
    Ok(val) => {
      let val = Arc::clone(&**val);
      Ok(val)
    }
    Err(_) => cx.throw_type_error::<_, Arc<dyn UserStore>>("JsPgUserStore"),
  }
}

pub mod pg {
  use crate::clock::get_native_clock;
  use crate::database::JsPgPool;
  use crate::neon_helpers::NeonNamespace;
  use eternalfest_core::clock::Clock;
  use eternalfest_user_store::pg::PgUserStore;
  use neon::prelude::*;
  use sqlx::PgPool;
  use std::sync::Arc;

  pub fn create_namespace<'a, C: Context<'a>>(cx: &mut C) -> JsResult<'a, JsObject> {
    let ns = cx.empty_object();
    ns.set_function(cx, "new", new)?;
    Ok(ns)
  }

  pub type DynPgUserStore = PgUserStore<Arc<dyn Clock>, Arc<PgPool>>;
  pub type JsPgUserStore = JsBox<Arc<DynPgUserStore>>;

  pub fn new(mut cx: FunctionContext) -> JsResult<JsPgUserStore> {
    let clock = cx.argument::<JsValue>(0)?;
    let database = cx.argument::<JsPgPool>(1)?;
    let clock: Arc<dyn Clock> = get_native_clock(&mut cx, clock)?;
    let database = Arc::new(PgPool::clone(&database));
    #[allow(clippy::type_complexity)]
    let inner: Arc<PgUserStore<Arc<dyn Clock>, Arc<PgPool>>> = Arc::new(PgUserStore::new(clock, database));
    Ok(cx.boxed(inner))
  }
}
