use crate::etwin_client::http::JsHttpEternaltwinClient;
use crate::neon_helpers::{resolve_callback_serde, NeonNamespace};
use eternalfest_core::user::UserId;
use etwin_client::{EtwinAuth, EtwinClient};
use neon::prelude::*;
use std::sync::Arc;

pub fn create_namespace<'a, C: Context<'a>>(cx: &mut C) -> JsResult<'a, JsObject> {
  let ns = cx.empty_object();
  ns.set_with(cx, "http", http::create_namespace)?;
  ns.set_function(cx, "getAuthSelf", get_auth_self)?;
  ns.set_function(cx, "getUserById", get_user_by_id)?;
  Ok(ns)
}

pub fn get_native_etwin_client<'a, C: Context<'a>>(
  cx: &mut C,
  value: Handle<JsValue>,
) -> NeonResult<Arc<dyn EtwinClient>> {
  match value.downcast::<JsHttpEternaltwinClient, _>(cx) {
    Ok(val) => {
      let val = Arc::clone(&**val);
      Ok(val)
    }
    Err(_) => cx.throw_type_error::<_, Arc<dyn EtwinClient>>("JsHttpEternaltwinClient"),
  }
}

pub fn get_auth_self(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let token = cx.argument::<JsString>(1)?;
  let cb = cx.argument::<JsFunction>(2)?.root(&mut cx);

  let this = get_native_etwin_client(&mut cx, this)?;
  let token: String = token.value(&mut cx);

  let res = async move { this.get_self(&EtwinAuth::Token(token)).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn get_user_by_id(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let token = cx.argument::<JsString>(1)?;
  let user_id = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_etwin_client(&mut cx, this)?;
  let token: Option<String> = serde_json::from_str(&token.value(&mut cx)).unwrap();
  let user_id: UserId = serde_json::from_str(&user_id.value(&mut cx)).unwrap();
  let auth = match token {
    None => EtwinAuth::Guest,
    Some(token) => EtwinAuth::Token(token),
  };

  let res = async move { this.get_user(&auth, user_id).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub mod http {
  use crate::clock::get_native_clock;
  use crate::neon_helpers::{resolve_callback_with, NeonNamespace};
  use eternalfest_core::clock::Clock;
  use etwin_client::http::HttpEternaltwinClient;
  use neon::prelude::*;
  use std::sync::Arc;
  use url::Url;

  pub fn create_namespace<'a, C: Context<'a>>(cx: &mut C) -> JsResult<'a, JsObject> {
    let ns = cx.empty_object();
    ns.set_function(cx, "new", new)?;
    Ok(ns)
  }

  pub type DynHttpEternaltwinClient = HttpEternaltwinClient<Arc<dyn Clock>>;
  pub type JsHttpEternaltwinClient = JsBox<Arc<DynHttpEternaltwinClient>>;

  pub fn new(mut cx: FunctionContext) -> JsResult<JsUndefined> {
    let clock = cx.argument::<JsValue>(0)?;
    let root = cx.argument::<JsString>(1)?;
    let cb = cx.argument::<JsFunction>(2)?.root(&mut cx);

    let clock: Arc<dyn Clock> = get_native_clock(&mut cx, clock)?;
    let root: Url = root.value(&mut cx).parse().unwrap();

    #[allow(clippy::type_complexity)]
    let res: Arc<DynHttpEternaltwinClient> = Arc::new(HttpEternaltwinClient::new(clock, root).unwrap());

    resolve_callback_with(&mut cx, async move { res }, cb, |c: &mut TaskContext, res| {
      Ok(c.boxed(res).upcast())
    })
  }
}
