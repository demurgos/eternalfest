use crate::neon_helpers::ModuleContextExt;
use neon::prelude::*;

mod auth_store;
mod blob_store;
mod buffer_store;
mod clock;
mod config;
mod database;
mod etwin_client;
mod file_store;
mod game_store;
mod neon_helpers;
mod run_store;
mod services;
mod tokio_runtime;
mod user_store;
mod uuid;

fn export(mut cx: ModuleContext) -> NeonResult<()> {
  let cx = &mut cx;
  cx.export_with("authStore", crate::auth_store::create_namespace)?;
  cx.export_with("blobStore", crate::blob_store::create_namespace)?;
  cx.export_with("bufferStore", crate::buffer_store::create_namespace)?;
  cx.export_with("clock", crate::clock::create_namespace)?;
  cx.export_with("config", crate::config::create_namespace)?;
  cx.export_with("database", crate::database::create_namespace)?;
  cx.export_with("etwinClient", crate::etwin_client::create_namespace)?;
  cx.export_with("fileStore", crate::file_store::create_namespace)?;
  cx.export_with("gameStore", crate::game_store::create_namespace)?;
  cx.export_with("runStore", crate::run_store::create_namespace)?;
  cx.export_with("services", crate::services::create_namespace)?;
  cx.export_with("uuid", crate::uuid::create_namespace)?;
  cx.export_with("userStore", crate::user_store::create_namespace)?;
  Ok(())
}

#[neon::main]
fn main(cx: ModuleContext) -> NeonResult<()> {
  export(cx)
}
