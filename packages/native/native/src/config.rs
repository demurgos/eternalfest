use crate::neon_helpers::{resolve_callback_serde, NeonNamespace};
use eternalfest_core::types::AnyError;
use neon::prelude::*;

pub fn create_namespace<'a, C: Context<'a>>(cx: &mut C) -> JsResult<'a, JsObject> {
  let ns = cx.empty_object();
  ns.set_function(cx, "getLocalConfig", get_local_config)?;
  Ok(ns)
}

pub fn get_local_config(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let cb = cx.argument::<JsFunction>(0)?.root(&mut cx);

  let res = async move { eternalfest_config::find_config(std::env::current_dir().unwrap()).map_err(AnyError::from) };

  resolve_callback_serde(&mut cx, res, cb)
}
