use crate::file_store::pg::JsPgFileStore;
use crate::neon_helpers::NeonNamespace;
use eternalfest_core::file::FileStore;
use neon::prelude::*;
use std::sync::Arc;

pub fn create_namespace<'a, C: Context<'a>>(cx: &mut C) -> JsResult<'a, JsObject> {
  let ns = cx.empty_object();
  ns.set_with(cx, "pg", pg::create_namespace)?;
  Ok(ns)
}

pub fn get_native_file_store<'a, C: Context<'a>>(cx: &mut C, value: Handle<JsValue>) -> NeonResult<Arc<dyn FileStore>> {
  match value.downcast::<JsPgFileStore, _>(cx) {
    Ok(val) => {
      let val = Arc::clone(&**val);
      Ok(val)
    }
    Err(_) => cx.throw_type_error::<_, Arc<dyn FileStore>>("JsPgFileStore"),
  }
}

pub mod pg {
  use crate::blob_store::get_native_blob_store;
  use crate::clock::get_native_clock;
  use crate::database::JsPgPool;
  use crate::neon_helpers::NeonNamespace;
  use crate::uuid::get_native_uuid_generator;
  use eternalfest_core::blob::BlobStore;
  use eternalfest_core::clock::Clock;
  use eternalfest_core::uuid::UuidGenerator;
  use eternalfest_file_store::pg::PgFileStore;
  use neon::prelude::*;
  use sqlx::PgPool;
  use std::sync::Arc;

  pub fn create_namespace<'a, C: Context<'a>>(cx: &mut C) -> JsResult<'a, JsObject> {
    let ns = cx.empty_object();
    ns.set_function(cx, "new", new)?;
    Ok(ns)
  }

  pub type DynPgFileStore = PgFileStore<Arc<dyn BlobStore>, Arc<dyn Clock>, Arc<PgPool>, Arc<dyn UuidGenerator>>;
  pub type JsPgFileStore = JsBox<Arc<DynPgFileStore>>;

  pub fn new(mut cx: FunctionContext) -> JsResult<JsPgFileStore> {
    let blob_store = cx.argument::<JsValue>(0)?;
    let clock = cx.argument::<JsValue>(1)?;
    let database = cx.argument::<JsPgPool>(2)?;
    let uuid_generator = cx.argument::<JsValue>(3)?;

    let blob_store: Arc<dyn BlobStore> = get_native_blob_store(&mut cx, blob_store)?;
    let clock: Arc<dyn Clock> = get_native_clock(&mut cx, clock)?;
    let database = Arc::new(PgPool::clone(&database));
    let uuid_generator: Arc<dyn UuidGenerator> = get_native_uuid_generator(&mut cx, uuid_generator)?;

    #[allow(clippy::type_complexity)]
    let inner: Arc<DynPgFileStore> = Arc::new(PgFileStore::new(blob_store, clock, database, uuid_generator));
    Ok(cx.boxed(inner))
  }
}
