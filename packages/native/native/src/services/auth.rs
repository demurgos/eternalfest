use crate::auth_store::get_native_auth_store;
use crate::clock::get_native_clock;
use crate::etwin_client::get_native_etwin_client;
use crate::neon_helpers::{resolve_callback_serde, resolve_callback_with, NeonNamespace};
use crate::user_store::get_native_user_store;
use eternalfest_core::auth::{AuthStore, SessionId};
use eternalfest_core::clock::Clock;
use eternalfest_core::user::{UserDisplayName, UserId, UserStore};
use eternalfest_services::auth::{AuthService, AuthenticateHttpOptions, DynAuthService};
use etwin_client::EtwinClient;
use neon::prelude::*;
use std::sync::Arc;

pub fn create_namespace<'a, C: Context<'a>>(cx: &mut C) -> JsResult<'a, JsObject> {
  let ns = cx.empty_object();
  ns.set_function(cx, "new", new)?;
  ns.set_function(cx, "authenticateHttp", authenticate_http)?;
  ns.set_function(cx, "etwinOauth", etwin_oauth)?;
  ns.set_function(cx, "session", session)?;
  ns.set_function(cx, "createSession", create_session)?;
  Ok(ns)
}

pub fn get_native_auth_service<'a, C: Context<'a>>(
  cx: &mut C,
  value: Handle<JsValue>,
) -> NeonResult<Arc<DynAuthService>> {
  match value.downcast::<JsAuthService, _>(cx) {
    Ok(val) => {
      let val = Arc::clone(&**val);
      Ok(val)
    }
    Err(_) => cx.throw_type_error::<_, Arc<DynAuthService>>("JsFileService"),
  }
}

pub type JsAuthService = JsBox<Arc<DynAuthService>>;

pub fn new(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let auth_store = cx.argument::<JsValue>(0)?;
  let clock = cx.argument::<JsValue>(1)?;
  let etwin_client = cx.argument::<JsValue>(2)?;
  let user_store = cx.argument::<JsValue>(3)?;
  let cb = cx.argument::<JsFunction>(4)?.root(&mut cx);

  let auth_store: Arc<dyn AuthStore> = get_native_auth_store(&mut cx, auth_store)?;
  let clock: Arc<dyn Clock> = get_native_clock(&mut cx, clock)?;
  let etwin_client: Arc<dyn EtwinClient> = get_native_etwin_client(&mut cx, etwin_client)?;
  let user_store: Arc<dyn UserStore> = get_native_user_store(&mut cx, user_store)?;

  let res = async move { Arc::new(AuthService::new(auth_store, clock, etwin_client, user_store)) };

  resolve_callback_with(&mut cx, res, cb, |c: &mut TaskContext, res| Ok(c.boxed(res).upcast()))
}

pub fn authenticate_http(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let options = cx.argument::<JsString>(1)?;
  let cb = cx.argument::<JsFunction>(2)?.root(&mut cx);

  let this = get_native_auth_service(&mut cx, this)?;
  let options: AuthenticateHttpOptions = serde_json::from_str(&options.value(&mut cx)).unwrap();

  let res = async move { this.authenticate_http(&options).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn etwin_oauth(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let user_id = cx.argument::<JsString>(1)?;
  let user_display_name = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_auth_service(&mut cx, this)?;
  let user_id: UserId = user_id.value(&mut cx).parse().unwrap();
  let user_display_name: UserDisplayName = user_display_name.value(&mut cx).parse().unwrap();

  let res = async move { this.etwin_oauth(user_id, &user_display_name).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn session(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let session_id = cx.argument::<JsString>(1)?;
  let cb = cx.argument::<JsFunction>(2)?.root(&mut cx);

  let this = get_native_auth_service(&mut cx, this)?;
  let session_id: SessionId = session_id.value(&mut cx).parse().unwrap();

  let res = async move { this.session(session_id).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn create_session(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let user_id = cx.argument::<JsString>(1)?;
  let cb = cx.argument::<JsFunction>(2)?.root(&mut cx);

  let this = get_native_auth_service(&mut cx, this)?;
  let user_id: UserId = user_id.value(&mut cx).parse().unwrap();

  let res = async move { this.create_session(user_id).await };
  resolve_callback_serde(&mut cx, res, cb)
}
