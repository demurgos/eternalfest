use crate::neon_helpers::{resolve_callback_serde, resolve_callback_with, NeonNamespace};
use crate::user_store::get_native_user_store;
use eternalfest_core::auth::AuthContext;
use eternalfest_core::user::{GetUserOptions, GetUsersOptions, ShortUser, UpdateUserOptions, UserStore};
use eternalfest_services::user::{DynUserService, UserService};
use neon::prelude::*;
use std::sync::Arc;

pub fn create_namespace<'a, C: Context<'a>>(cx: &mut C) -> JsResult<'a, JsObject> {
  let ns = cx.empty_object();
  ns.set_function(cx, "new", new)?;
  ns.set_function(cx, "upsertFromEtwin", upsert_from_etwin)?;
  ns.set_function(cx, "getUser", get_user)?;
  ns.set_function(cx, "getShortUser", get_short_user)?;
  ns.set_function(cx, "getShortUsers", get_short_users)?;
  ns.set_function(cx, "getUsers", get_users)?;
  ns.set_function(cx, "updateUser", update_user)?;
  Ok(ns)
}

pub fn get_native_user_service<'a, C: Context<'a>>(
  cx: &mut C,
  value: Handle<JsValue>,
) -> NeonResult<Arc<DynUserService>> {
  match value.downcast::<JsUserService, _>(cx) {
    Ok(val) => {
      let val = Arc::clone(&**val);
      Ok(val)
    }
    Err(_) => cx.throw_type_error::<_, Arc<DynUserService>>("JsUserService"),
  }
}

pub type JsUserService = JsBox<Arc<DynUserService>>;

pub fn new(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let user_store = cx.argument::<JsValue>(0)?;
  let cb = cx.argument::<JsFunction>(1)?.root(&mut cx);

  let user_store: Arc<dyn UserStore> = get_native_user_store(&mut cx, user_store)?;

  let res = async move { Arc::new(UserService::new(user_store)) };

  resolve_callback_with(&mut cx, res, cb, |c: &mut TaskContext, res| Ok(c.boxed(res).upcast()))
}

pub fn upsert_from_etwin(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let options = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_user_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let options: ShortUser = serde_json::from_str(&options.value(&mut cx)).unwrap();

  let res = async move { this.upsert_from_etwin(&acx, &options).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn get_user(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let options = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_user_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let options: GetUserOptions = serde_json::from_str(&options.value(&mut cx)).unwrap();

  let res = async move { this.get_user(&acx, &options).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn get_short_user(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let options = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_user_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let options: GetUserOptions = serde_json::from_str(&options.value(&mut cx)).unwrap();

  let res = async move { this.get_user_ref(&acx, &options).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn get_short_users(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let options = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_user_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let options: GetUsersOptions = serde_json::from_str(&options.value(&mut cx)).unwrap();

  let res = async move { this.get_user_refs(&acx, &options).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn get_users(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let cb = cx.argument::<JsFunction>(2)?.root(&mut cx);

  let this = get_native_user_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();

  let res = async move { this.get_users(&acx).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn update_user(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let options = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_user_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let options: UpdateUserOptions = serde_json::from_str(&options.value(&mut cx)).unwrap();

  let res = async move { this.update_user(&acx, &options).await };
  resolve_callback_serde(&mut cx, res, cb)
}
