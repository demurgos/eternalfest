use crate::neon_helpers::NeonNamespace;
use neon::prelude::*;

pub mod auth;
pub mod file;
pub mod game;
pub mod run;
pub mod user;

pub fn create_namespace<'a, C: Context<'a>>(cx: &mut C) -> JsResult<'a, JsObject> {
  let ns = cx.empty_object();
  ns.set_with(cx, "auth", auth::create_namespace)?;
  ns.set_with(cx, "file", file::create_namespace)?;
  ns.set_with(cx, "game", game::create_namespace)?;
  ns.set_with(cx, "run", run::create_namespace)?;
  ns.set_with(cx, "user", user::create_namespace)?;
  Ok(ns)
}
