use crate::blob_store::get_native_blob_store;
use crate::clock::get_native_clock;
use crate::game_store::get_native_game_store;
use crate::neon_helpers::{resolve_callback_serde, resolve_callback_with, NeonNamespace};
use crate::user_store::get_native_user_store;
use eternalfest_core::auth::AuthContext;
use eternalfest_core::blob::BlobStore;
use eternalfest_core::clock::Clock;
use eternalfest_core::game::requests::{
  CreateGame, CreateGameBuild, GetGame, GetGames, SetGameFavorite, UpdateGameChannel,
};
use eternalfest_core::game::GameStore;
use eternalfest_core::user::UserStore;
use eternalfest_services::game::{DynGameService, GameService};
use neon::prelude::*;
use std::sync::Arc;

pub fn create_namespace<'a, C: Context<'a>>(cx: &mut C) -> JsResult<'a, JsObject> {
  let ns = cx.empty_object();
  ns.set_function(cx, "new", new)?;
  ns.set_function(cx, "createGame", create_game)?;
  ns.set_function(cx, "getGame", get_game)?;
  ns.set_function(cx, "getGames", get_games)?;
  ns.set_function(cx, "createBuild", create_build)?;
  ns.set_function(cx, "updateChannel", update_channel)?;
  ns.set_function(cx, "setGameFavorite", set_game_favorite)?;
  Ok(ns)
}

pub fn get_native_game_service<'a, C: Context<'a>>(
  cx: &mut C,
  value: Handle<JsValue>,
) -> NeonResult<Arc<DynGameService>> {
  match value.downcast::<JsGameService, _>(cx) {
    Ok(val) => {
      let val = Arc::clone(&**val);
      Ok(val)
    }
    Err(_) => cx.throw_type_error::<_, Arc<DynGameService>>("JsGameService"),
  }
}

pub type JsGameService = JsBox<Arc<DynGameService>>;

pub fn new(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let blob_store = cx.argument::<JsValue>(0)?;
  let clock = cx.argument::<JsValue>(1)?;
  let game_store = cx.argument::<JsValue>(2)?;
  let user_store = cx.argument::<JsValue>(3)?;
  let cb = cx.argument::<JsFunction>(4)?.root(&mut cx);

  let blob_store: Arc<dyn BlobStore> = get_native_blob_store(&mut cx, blob_store)?;
  let clock: Arc<dyn Clock> = get_native_clock(&mut cx, clock)?;
  let game_store: Arc<dyn GameStore> = get_native_game_store(&mut cx, game_store)?;
  let user_store: Arc<dyn UserStore> = get_native_user_store(&mut cx, user_store)?;

  let res = async move { Arc::new(GameService::new(blob_store, clock, game_store, user_store)) };

  resolve_callback_with(&mut cx, res, cb, |c: &mut TaskContext, res| Ok(c.boxed(res).upcast()))
}

pub fn create_game(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let options = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_game_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let options: CreateGame = serde_json::from_str(&options.value(&mut cx)).unwrap();

  let res = async move { this.create_game(&acx, &options).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn get_game(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let options = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_game_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let options: GetGame = serde_json::from_str(&options.value(&mut cx)).unwrap();

  let res = async move { this.get_game(&acx, &options).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn get_games(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let options = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_game_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let options: GetGames = serde_json::from_str(&options.value(&mut cx)).unwrap();

  let res = async move { this.get_games(&acx, &options).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn create_build(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let options = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_game_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let options: CreateGameBuild = serde_json::from_str(&options.value(&mut cx)).unwrap();

  let res = async move { this.create_build(&acx, &options).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn set_game_favorite(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let options = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_game_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let options: SetGameFavorite = serde_json::from_str(&options.value(&mut cx)).unwrap();

  let res = async move { this.set_game_favorite(&acx, &options).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn update_channel(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let options = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_game_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let options: UpdateGameChannel = serde_json::from_str(&options.value(&mut cx)).unwrap();

  let res = async move { this.update_channel(&acx, &options).await };
  resolve_callback_serde(&mut cx, res, cb)
}
