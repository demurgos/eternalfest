use crate::blob_store::get_native_blob_store;
use crate::clock::get_native_clock;
use crate::game_store::get_native_game_store;
use crate::neon_helpers::{resolve_callback_serde, resolve_callback_with, NeonNamespace};
use crate::run_store::get_native_run_store;
use crate::user_store::get_native_user_store;
use eternalfest_core::auth::AuthContext;
use eternalfest_core::blob::BlobStore;
use eternalfest_core::clock::Clock;
use eternalfest_core::game::{GameId, GameModeKey, GameStore};
use eternalfest_core::run::{CreateRunOptions, RunId, RunStore, SetRunResultOptions};
use eternalfest_core::user::{UserId, UserStore};
use eternalfest_services::run::{DynRunService, RunService};
use neon::prelude::*;
use std::sync::Arc;

pub fn create_namespace<'a, C: Context<'a>>(cx: &mut C) -> JsResult<'a, JsObject> {
  let ns = cx.empty_object();
  ns.set_function(cx, "new", new)?;
  ns.set_function(cx, "createRun", create_run)?;
  ns.set_function(cx, "getRun", get_run)?;
  ns.set_function(cx, "startRun", start_run)?;
  ns.set_function(cx, "setRunResult", set_run_result)?;
  ns.set_function(cx, "getGameUserItemsById", get_game_user_items_by_id)?;
  ns.set_function(cx, "getLeaderboard", get_leaderboard)?;
  Ok(ns)
}

pub fn get_native_run_service<'a, C: Context<'a>>(
  cx: &mut C,
  value: Handle<JsValue>,
) -> NeonResult<Arc<DynRunService>> {
  match value.downcast::<JsRunService, _>(cx) {
    Ok(val) => {
      let val = Arc::clone(&**val);
      Ok(val)
    }
    Err(_) => cx.throw_type_error::<_, Arc<DynRunService>>("JsRunService"),
  }
}

pub type JsRunService = JsBox<Arc<DynRunService>>;

pub fn new(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let blob_store = cx.argument::<JsValue>(0)?;
  let clock = cx.argument::<JsValue>(1)?;
  let game_store = cx.argument::<JsValue>(2)?;
  let run_store = cx.argument::<JsValue>(3)?;
  let user_store = cx.argument::<JsValue>(4)?;
  let cb = cx.argument::<JsFunction>(5)?.root(&mut cx);

  let blob_store: Arc<dyn BlobStore> = get_native_blob_store(&mut cx, blob_store)?;
  let clock: Arc<dyn Clock> = get_native_clock(&mut cx, clock)?;
  let game_store: Arc<dyn GameStore> = get_native_game_store(&mut cx, game_store)?;
  let run_store: Arc<dyn RunStore> = get_native_run_store(&mut cx, run_store)?;
  let user_store: Arc<dyn UserStore> = get_native_user_store(&mut cx, user_store)?;

  let res = async move { Arc::new(RunService::new(blob_store, clock, game_store, run_store, user_store)) };

  resolve_callback_with(&mut cx, res, cb, |c: &mut TaskContext, res| Ok(c.boxed(res).upcast()))
}

pub fn create_run(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let options = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_run_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let options: CreateRunOptions = serde_json::from_str(&options.value(&mut cx)).unwrap();

  let res = async move { this.create_run(&acx, &options).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn get_run(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let run_id = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_run_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let run_id: RunId = serde_json::from_str(&run_id.value(&mut cx)).unwrap();

  let res = async move { this.get_run(&acx, run_id).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn start_run(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let run_id = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_run_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let run_id: RunId = serde_json::from_str(&run_id.value(&mut cx)).unwrap();

  let res = async move { this.start_run(&acx, run_id).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn set_run_result(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let run_id = cx.argument::<JsString>(2)?;
  let result = cx.argument::<JsString>(3)?;
  let cb = cx.argument::<JsFunction>(4)?.root(&mut cx);

  let this = get_native_run_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let run_id: RunId = serde_json::from_str(&run_id.value(&mut cx)).unwrap();
  let result: SetRunResultOptions = serde_json::from_str(&result.value(&mut cx)).unwrap();

  let res = async move { this.set_run_result(&acx, run_id, result).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn get_game_user_items_by_id(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let user_id = cx.argument::<JsString>(2)?;
  let game_id = cx.argument::<JsString>(3)?;
  let cb = cx.argument::<JsFunction>(4)?.root(&mut cx);

  let this = get_native_run_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let user_id: UserId = serde_json::from_str(&user_id.value(&mut cx)).unwrap();
  let game_id: GameId = serde_json::from_str(&game_id.value(&mut cx)).unwrap();

  let res = async move { this.get_user_items(&acx, user_id, game_id).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn get_leaderboard(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let game_id = cx.argument::<JsString>(2)?;
  let game_mode = cx.argument::<JsString>(3)?;
  let cb = cx.argument::<JsFunction>(4)?.root(&mut cx);

  let this = get_native_run_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let game_id: GameId = serde_json::from_str(&game_id.value(&mut cx)).unwrap();
  let game_mode: GameModeKey = serde_json::from_str(&game_mode.value(&mut cx)).unwrap();

  let res = async move { this.get_leaderboard(&acx, game_id, game_mode).await };
  resolve_callback_serde(&mut cx, res, cb)
}
