use crate::blob_store::get_native_blob_store;
use crate::file_store::get_native_file_store;
use crate::neon_helpers::{get_full_error_message, resolve_callback_serde, resolve_callback_with, NeonNamespace};
use crate::user_store::get_native_user_store;
use eternalfest_core::auth::AuthContext;
use eternalfest_core::blob::BlobStore;
use eternalfest_core::file::{
  CreateDirectoryOptions, CreateFileOptions, DeleteItemOptions, FileStore, GetDirectoryOptions, GetDriveByOwnerOptions,
  GetDriveOptions, GetFileOptions, GetItemByPathOptions,
};
use eternalfest_core::types::AnyError;
use eternalfest_core::user::UserStore;
use eternalfest_services::file::{DynFileService, FileService};
use neon::prelude::*;
use neon::types::buffer::TypedArray;
use std::sync::Arc;

pub fn create_namespace<'a, C: Context<'a>>(cx: &mut C) -> JsResult<'a, JsObject> {
  let ns = cx.empty_object();
  ns.set_function(cx, "new", new)?;
  ns.set_function(cx, "getDrive", get_drive)?;
  ns.set_function(cx, "getDriveByOwner", get_drive_by_owner)?;
  ns.set_function(cx, "getItemByPath", get_item_by_path)?;
  ns.set_function(cx, "createDirectory", create_directory)?;
  ns.set_function(cx, "getDirectory", get_directory)?;
  ns.set_function(cx, "getDirectoryChildren", get_directory_children)?;
  ns.set_function(cx, "createFile", create_file)?;
  ns.set_function(cx, "getFile", get_file)?;
  ns.set_function(cx, "getFileData", get_file_data)?;
  ns.set_function(cx, "deleteItem", delete_item)?;
  Ok(ns)
}

pub fn get_native_file_service<'a, C: Context<'a>>(
  cx: &mut C,
  value: Handle<JsValue>,
) -> NeonResult<Arc<DynFileService>> {
  match value.downcast::<JsFileService, _>(cx) {
    Ok(val) => {
      let val = Arc::clone(&**val);
      Ok(val)
    }
    Err(_) => cx.throw_type_error::<_, Arc<DynFileService>>("JsFileService"),
  }
}

pub type JsFileService = JsBox<Arc<DynFileService>>;

pub fn new(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let blob_store = cx.argument::<JsValue>(0)?;
  let file_store = cx.argument::<JsValue>(1)?;
  let user_store = cx.argument::<JsValue>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let blob_store: Arc<dyn BlobStore> = get_native_blob_store(&mut cx, blob_store)?;
  let file_store: Arc<dyn FileStore> = get_native_file_store(&mut cx, file_store)?;
  let user_store: Arc<dyn UserStore> = get_native_user_store(&mut cx, user_store)?;

  let res = async move { Arc::new(FileService::new(blob_store, file_store, user_store)) };

  resolve_callback_with(&mut cx, res, cb, |c: &mut TaskContext, res| Ok(c.boxed(res).upcast()))
}

pub fn get_drive(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let options = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_file_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let options: GetDriveOptions = serde_json::from_str(&options.value(&mut cx)).unwrap();

  let res = async move { this.get_drive(&acx, &options).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn get_drive_by_owner(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let options = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_file_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let options: GetDriveByOwnerOptions = serde_json::from_str(&options.value(&mut cx)).unwrap();

  let res = async move { this.get_drive_by_owner(&acx, &options).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn get_item_by_path(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let options = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_file_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let options: GetItemByPathOptions = serde_json::from_str(&options.value(&mut cx)).unwrap();

  let res = async move { this.get_item_by_path(&acx, &options).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn create_directory(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let options = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_file_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let options: CreateDirectoryOptions = serde_json::from_str(&options.value(&mut cx)).unwrap();

  let res = async move { this.create_directory(&acx, &options).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn get_directory(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let options = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_file_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let options: GetDirectoryOptions = serde_json::from_str(&options.value(&mut cx)).unwrap();

  let res = async move { this.get_directory(&acx, &options).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn get_directory_children(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let options = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_file_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let options: GetDirectoryOptions = serde_json::from_str(&options.value(&mut cx)).unwrap();

  let res = async move { this.get_directory_children(&acx, &options).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn create_file(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let options = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_file_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let options: CreateFileOptions = serde_json::from_str(&options.value(&mut cx)).unwrap();

  let res = async move { this.create_file(&acx, &options).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn get_file(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let options = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_file_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let options: GetFileOptions = serde_json::from_str(&options.value(&mut cx)).unwrap();

  let res = async move { this.get_file(&acx, &options).await };
  resolve_callback_serde(&mut cx, res, cb)
}

pub fn get_file_data(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let options = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_file_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let options: GetFileOptions = serde_json::from_str(&options.value(&mut cx)).unwrap();

  let res = async move { this.get_file_data(&acx, &options).await };

  resolve_callback_with(
    &mut cx,
    res,
    cb,
    |cx: &mut TaskContext, res: Result<Vec<u8>, AnyError>| {
      let bytes: Vec<u8> = match res {
        Ok(bytes) => bytes,
        Err(e) => {
          let e = get_full_error_message(e.as_ref());
          let e = JsError::error(cx, &e).expect("FailedToAllocateJsError");
          return Err(e.upcast());
        }
      };
      let buffer_len: usize = bytes.len();
      let mut js_buffer = cx.buffer(buffer_len).expect("Failed to allocate");
      {
        let js_buffer: &mut [u8] = js_buffer.as_mut_slice(&mut *cx);
        js_buffer.copy_from_slice(bytes.as_slice());
      }
      Ok(js_buffer.upcast())
    },
  )
}

pub fn delete_item(mut cx: FunctionContext) -> JsResult<JsUndefined> {
  let this = cx.argument::<JsValue>(0)?;
  let acx = cx.argument::<JsString>(1)?;
  let options = cx.argument::<JsString>(2)?;
  let cb = cx.argument::<JsFunction>(3)?.root(&mut cx);

  let this = get_native_file_service(&mut cx, this)?;
  let acx: AuthContext = serde_json::from_str(&acx.value(&mut cx)).unwrap();
  let options: DeleteItemOptions = serde_json::from_str(&options.value(&mut cx)).unwrap();

  let res = async move { this.delete_item(&acx, &options).await };
  resolve_callback_serde(&mut cx, res, cb)
}
